﻿using System.Web;
using System.Web.Optimization;

namespace GoNyLaw
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquerymigrate").Include(
                        "~/Scripts/jquery-migrate-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryeasing").Include(
                        "~/Scripts/jquery.easing.{version}.js"));

            bundles.Add(new StyleBundle("~/Content/fontawesome").Include(
                     "~/Content/font-awesome.min.css"));

            bundles.Add(new StyleBundle("~/bundles/bootstrapdatetimepickercss").Include(
                       "~/Content/bootstrap-datetimepicker.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/bundles/cssajaxloader").Include(
                        "~/Content/ajax-loader.css"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapjs").Include(
                        "~/Scripts/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/momentjs").Include(
                       "~/Scripts/moment.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/niftyjs").Include(
                      "~/Scripts/nifty.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/sparklinejs").Include(
                     "~/Scripts/jquery.sparkline.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/skyconsjs").Include(
                     "~/Scripts/skycons.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/switcheryjs").Include(
                     "~/Scripts/switchery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapselectjs").Include(
                     "~/Scripts/bootstrap-select.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/pacejs").Include(
                     "~/Scripts/pace.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapdatetimepickerjs").Include(
                       "~/Scripts/bootstrap-datetimepicker.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/respondjs").Include(
                       "~/Scripts/respond.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/fastclickjs").Include(
                      "~/Scripts/fastclick.js"));

            bundles.Add(new ScriptBundle("~/bundles/gonylawjs").Include(
                     "~/Scripts/gonylaw-nifty.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/dashboardjs").Include(
                     "~/Scripts/dashboard.min.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrapcss").Include(
                        "~/Content/bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/bundles/bootstrapthemecss").Include(
                        "~/Content/bootstrap-theme.min.css"));

            bundles.Add(new StyleBundle("~/bundles/niftycss").Include(
                        "~/Content/nifty.min.css"));

            bundles.Add(new StyleBundle("~/bundles/animatecss").Include(
                        "~/Content/animate.min.css"));

            bundles.Add(new StyleBundle("~/bundles/switcherycss").Include(
                        "~/Content/switchery.min.css"));

            bundles.Add(new StyleBundle("~/bundles/bootstrapselectcss").Include(
                        "~/Content/bootstrap-select.min.css"));

            bundles.Add(new StyleBundle("~/bundles/pacecss").Include(
                        "~/Content/pace.min.css"));

            bundles.Add(new StyleBundle("~/bundles/niftyiconscss").Include(
                        "~/Content/nifty-icons.min.css"));

            bundles.Add(new StyleBundle("~/bundles/gonylawcss").Include(
                        "~/Content/gonylaw-nifty.min.css"));

            bundles.Add(new StyleBundle("~/bundles/gonylawthemecss").Include(
                        "~/Content/gonylaw-theme.min.css"));
        }
    }
}
