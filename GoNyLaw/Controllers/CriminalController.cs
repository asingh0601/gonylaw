﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoNyLaw.Models;
using GoNyLaw.Helpers;
using System.Globalization;
using System.IO;
using System.Data.Entity.Migrations;
using System.Data.Entity.Infrastructure;

namespace GoNyLaw.Controllers
{
    public class CriminalController : Controller
    {
        [HttpPost]
        public ActionResult CriminalCaseQuestionnaire()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            UserContext db = new UserContext();
            string cid = Request.Form["id"];
            int id = 0;
            Int32.TryParse(cid, out id);
            Client c = db.Clients.Find(id);
            ViewBag.name = c.fullname.Decrypt();
            ViewBag.cellphone = c.cellphone.Decrypt();
            if (!string.IsNullOrEmpty(c.emailid))
                ViewBag.emailid = c.emailid.Decrypt();
            else
                ViewBag.emailid = "";
            if (!string.IsNullOrEmpty(c.street))
                ViewBag.street = c.street.Decrypt();
            else
                ViewBag.street = "";
            ViewBag.city = c.city;
            ViewBag.zip = c.zip;
            ViewBag.cid = cid;
            ViewBag.aid = LoginPersistence.getadminid();
            ViewBag.refno = "CRIM" + Maths.GetRandomNumber();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CriminalCaseQuestionnaireSubmission(CriminalCase cc)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            cc.dateregister = DateTime.Today;
            cc.adminsassigned = LoginPersistence.getadminid().ToString();
            cc = cc.Encrypt().ApplyDateCorrection();
            ModelState.Clear();
            if (ModelState.IsValid)
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                db.CriminalCases.Add(cc);
                db.SaveChanges();
                ActionLogs.CriminalCase(cc, "C", "NA", "Case Created", "Created By ", null);
                return RedirectToAction("AdminCriminalCasesList", "Criminal");
            }
            return View();
        }
        public ActionResult AdminCriminalCasesList()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            int aid = LoginPersistence.getadminid();
            List<CriminalCase> c = db.CriminalCases.Where(x => x.archived == 0).ToList();
            List<CriminalCase> clist = new List<CriminalCase>();
            foreach (var item in c)
            {
                if (!string.IsNullOrEmpty(item.adminsassigned))
                    if (item.adminsassigned.Split(',').Contains(aid.ToString()))
                        clist.Add(item);
            }
            IEnumerable<CriminalCase> cc = clist.AsEnumerable().OrderBy(x => x.fullname.Decrypt().Substring(x.fullname.Decrypt().LastIndexOf(' ') + 1)).ApplyDateCorrection();
            return View(cc);
        }
        public ActionResult SuperAdminCriminalCasesList()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            int aid = LoginPersistence.getsuperadminid();
            IEnumerable<CriminalCase> c = db.CriminalCases.Where(x => x.archived == 0);
            IEnumerable<CriminalCase> cc = c.OrderBy(x => x.fullname.Decrypt().Substring(x.fullname.Decrypt().LastIndexOf(' ') + 1)).ApplyDateCorrection();
            return View(cc);
        }
        public ActionResult AdminArchivedCriminalCasesList()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            int aid = LoginPersistence.getadminid();
            List<CriminalCase> c = db.CriminalCases.Where(x => x.archived == 1).ToList();
            List<CriminalCase> clist = new List<CriminalCase>();
            foreach (var item in c)
            {
                if (!string.IsNullOrEmpty(item.adminsassigned))
                    if (item.adminsassigned.Split(',').Contains(aid.ToString()))
                        clist.Add(item);
            }
            IEnumerable<CriminalCase> cc = clist.AsEnumerable().OrderBy(x => x.fullname.Decrypt()).ApplyDateCorrection();
            return View(cc);
        }
        public ActionResult SuperAdminArchivedCriminalCasesList()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            int aid = LoginPersistence.getsuperadminid();
            IEnumerable<CriminalCase> c = db.CriminalCases.Where(x => x.archived == 1);
            IEnumerable<CriminalCase> cc = c.OrderBy(x => x.fullname.Decrypt()).ApplyDateCorrection();
            return View(cc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CriminalCaseEdit()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            string refno = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            CriminalCase cc = db.CriminalCases.Where(x => x.refno == refno).FirstOrDefault().Decrypt().ApplyDateCorrection();
            return View(cc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CriminalCaseView()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            string refno = Request.Form["refno"];
            CriminalCase cc = db.CriminalCases.Where(x => x.refno == refno).FirstOrDefault().Decrypt().ApplyDateCorrection();
            return View(cc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CriminalCaseUpdate(CriminalCase cc)
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            bool defIsModified = false;
            bool ptrIsModified = false;
            bool decIsModified = false;
            bool mappIsModified = false;
            bool wadeIsModified = false;
            bool huntleyIsModified = false;
            bool dunawayIsModified = false;
            bool otherIsModified = false;
            using (CaseContext db = new CaseContext())
            {
                CriminalCase ccold = new CaseContext().CriminalCases.Find(cc.refno);
                if (ccold.motion != cc.motion)
                    defIsModified = true;
                if (ccold.ptr != cc.ptr)
                    ptrIsModified = true;
                if (ccold.dec != cc.dec)
                    decIsModified = true;
                if (ccold.mapp != cc.mapp)
                    mappIsModified = true;
                if (ccold.wade != cc.wade)
                    wadeIsModified = true;
                if (ccold.huntley != cc.huntley)
                    huntleyIsModified = true;
                if (ccold.dunaway != cc.dunaway)
                    dunawayIsModified = true;
                if (ccold.other != cc.other)
                    otherIsModified = true;
                db.CriminalCases.AddOrUpdate(cc.Encrypt());
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
            }
            string motionmodifiedfields = "";
            string ordermodifiedfields = "";
            if (defIsModified)
            {
                if (!string.IsNullOrEmpty(motionmodifiedfields))
                    motionmodifiedfields = ", Def";
                else
                    motionmodifiedfields = "Motions - Def";
            }
            if (ptrIsModified)
            {
                if (!string.IsNullOrEmpty(motionmodifiedfields))
                    motionmodifiedfields = ", PTR";
                else
                    motionmodifiedfields = "Motions - PTR";
            }
            if (decIsModified)
            {
                if (!string.IsNullOrEmpty(motionmodifiedfields))
                    motionmodifiedfields = ", Dec";
                else
                    motionmodifiedfields = "Motions - Dec";
            }
            if (mappIsModified)
            {
                if (!string.IsNullOrEmpty(ordermodifiedfields))
                    ordermodifiedfields = ", Mapp";
                else
                    ordermodifiedfields = "Orders - Mapp";
            }
            if (wadeIsModified)
            {
                if (!string.IsNullOrEmpty(ordermodifiedfields))
                    ordermodifiedfields = ", Wade";
                else
                    ordermodifiedfields = "Orders - Wade";
            }
            if (huntleyIsModified)
            {
                if (!string.IsNullOrEmpty(ordermodifiedfields))
                    ordermodifiedfields = ", Huntley";
                else
                    ordermodifiedfields = "Orders - Huntley";
            }
            if (dunawayIsModified)
            {
                if (!string.IsNullOrEmpty(ordermodifiedfields))
                    ordermodifiedfields = ", Dunaway";
                else
                    ordermodifiedfields = "Orders - Dunaway";
            }
            if (otherIsModified)
            {
                if (!string.IsNullOrEmpty(ordermodifiedfields))
                    ordermodifiedfields = ", Other";
                else
                    ordermodifiedfields = "Orders - Other";
            }
            string modtext = "";
            if (!string.IsNullOrEmpty(motionmodifiedfields))
                modtext = motionmodifiedfields;
            if (!string.IsNullOrEmpty(ordermodifiedfields))
                modtext = modtext + " " + ordermodifiedfields;
            string logtext = "";
            if (LoginPersistence.CheckAdmin())
            {
                if (!string.IsNullOrEmpty(modtext))
                    logtext = "Modified " + modtext;
                else
                    logtext = "Modified General Fields Only";
                ActionLogs.CriminalCase(cc, "X", "NA", "Case Edited", logtext, null);
            }
            return RedirectToAction("CloseAndRefreshParent", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SuperAdminCriminalCreateTask()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            ViewBag.refno = Request.Form["refno"];
            ViewBag.adminsassigned = Request.Form["adminsassigned"];
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SuperAdminCriminalCreateTaskSubmission(CriminalTask ct)
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            TaskContext db = new TaskContext();
            ct.dateassigned = DateTime.Today;
            ModelState.Clear();
            if (ModelState.IsValid)
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                db.CriminalTasks.Add(ct);
                db.SaveChanges();
            }
            CriminalCase cc = new CaseContext().CriminalCases.Find(ct.refno);
            string[] ad = ct.adminsassigned.Split(',');
            string admins = "";
            int id = 0;
            for (int i = 0; i < ad.Length; i++)
            {
                Int32.TryParse(ad[i], out id);
                if (!string.IsNullOrEmpty(admins))
                    admins = admins + ", " + new UserContext().Admins.Find(id).fullname.Decrypt();
                else
                    admins += new UserContext().Admins.Find(id).fullname.Decrypt();
            }
            ActionLogs.CriminalCase(cc, "T", admins, "Task Created", ct.task, null);
            Notifications.CriminalTaskAssigned(ct);
            return RedirectToAction("Close", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CriminalCourtProceedings()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            ViewBag.refno = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            CaseContext db = new CaseContext();
            CriminalCase cc = db.CriminalCases.Find(Request.Form["refno"]).Decrypt();
            string judge = "", part = "", room = "";
            if (!string.IsNullOrEmpty(cc.judge4))
            {
                judge = cc.judge4;
                part = cc.part4;
                room = cc.room4;
            }
            else if (!string.IsNullOrEmpty(cc.judge3))
            {
                judge = cc.judge3;
                part = cc.part3;
                room = cc.room3;
            }
            else if (!string.IsNullOrEmpty(cc.judge2))
            {
                judge = cc.judge2;
                part = cc.part2;
                room = cc.room2;
            }
            else
            {
                judge = cc.judge1;
                part = cc.part1;
                room = cc.room1;
            }
            ViewBag.judge = judge;
            ViewBag.part = part;
            ViewBag.room = room;
            string dt = "";
            CriminalAction ca = new ActionContext().CriminalActions.Where(x => x.refno == cc.refno).OrderByDescending(x => x.dateaction).FirstOrDefault();
            if (!Object.ReferenceEquals(ca, null))
                dt = ca.nextcourtdate.ApplyDateCorrection();
            if (!string.IsNullOrEmpty(dt))
                ViewBag.dateaction = dt;
            else
                ViewBag.dateaction = DateTime.Today;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CriminalCourtProceedingsSubmission(CriminalAction ca)
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            ViewBag.refno = Request.Form["refno"];
            ActionContext db = new ActionContext();
            ModelState.Clear();
            if (ModelState.IsValid)
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                db.CriminalActions.Add(ca);
                db.SaveChanges();
                CriminalCase cc = new CaseContext().CriminalCases.Find(ca.refno);
                ActionLogs.CriminalCase(cc, "A", "NA", "Court Date", "J-" + ca.judge + ", R-" + ca.room + ", P-" + ca.part + ":" + ca.actiontaken + ", D-" + ca.nextcourtdate.ApplyDateCorrection(), null);
                Notifications.CriminalCourtDateEntered(cc);
            }
            return RedirectToAction("CloseAndRefreshParent", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminViewAssignedTask()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            TaskContext db = new TaskContext();
            string refno = Request.Form["refno"];
            IEnumerable<CriminalTask> ct = db.CriminalTasks.Where(x => x.refno == refno).OrderByDescending(x => DbFunctions.DiffDays(x.daterequired, x.dateassigned));
            return View(ct);
        }
        [HttpGet]
        public ActionResult MarkTaskComplete()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            string i = Request.QueryString["id"];
            string text = Request.QueryString["text"];
            int id = 0;
            Int32.TryParse(i, out id);
            CriminalTask ct;
            using (TaskContext db = new TaskContext())
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                ct = db.CriminalTasks.Find(id);
                ct.datecompleted = DateTime.Today;
                ct.notes = text;
                db.SaveChanges();
            }
            CriminalCase cc = new CaseContext().CriminalCases.Find(ct.refno);
            ActionLogs.CriminalCase(cc, "T", "NA", "Task Complete", "Task - " + ct.task + ". Comment - " + text, null);
            Notifications.CriminalTaskComplete(ct);
            return RedirectToAction("CloseAndRefreshParent", "Home");
        }
        public ActionResult MyTaskList()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            TaskContext db = new TaskContext();
            int aid = LoginPersistence.getadminid();
            DateTime dt = DateTime.Parse("01/01/2015");
            List<CriminalTask> c = db.CriminalTasks.OrderByDescending(x => DbFunctions.DiffDays(x.daterequired, x.dateassigned)).ToList();
            List<CriminalTask> clist = new List<CriminalTask>();
            foreach (var item in c)
            {
                if (!string.IsNullOrEmpty(item.adminsassigned))
                    if (item.adminsassigned.Split(',').Contains(aid.ToString()))
                        clist.Add(item);
            }
            IEnumerable<CriminalTask> ct = clist.AsEnumerable();
            return View(ct);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendCriminalCaseEmail()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string cid = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            ViewBag.defaultrecepient = Request.Form["defaultrecepient"];
            CaseContext db = new CaseContext();
            CriminalCase cc = db.CriminalCases.Find(cid);
            Client c = new UserContext().Clients.Find(cc.clientid);
            ViewBag.refno = cid;
            ViewBag.clientname = c.fullname.Decrypt();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessCriminalCaseEmail()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string refno = Request.Form["refno"];
            string body = Request.Form["body"];
            string subject = Request.Form["subject"];
            string recepients = Request.Form["recepients"];
            string[] path = new string[5];
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase file1 = Request.Files["file1"];
                HttpPostedFileBase file2 = Request.Files["file2"];
                HttpPostedFileBase file3 = Request.Files["file3"];
                HttpPostedFileBase file4 = Request.Files["file4"];
                HttpPostedFileBase file5 = Request.Files["file5"];
                if (!Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno))
                {
                    Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno);
                }
                if (!Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails"))
                {
                    Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails");
                }
                if (file1 != null && file1.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file1.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[0] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file1.SaveAs(path[0]);
                }
                if (file2 != null && file2.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file2.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[1] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file2.SaveAs(path[1]);
                }
                if (file3 != null && file3.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file3.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[2] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file3.SaveAs(path[2]);
                }
                if (file4 != null && file4.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file4.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[3] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file4.SaveAs(path[3]);
                }
                if (file5 != null && file5.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file5.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[4] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file5.SaveAs(path[4]);
                }
            }
            Notifications.CriminalCaseEmail(refno, recepients, subject, body, path);
            return RedirectToAction("Close", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendCriminalCaseSMS()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string cid = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            CaseContext db = new CaseContext();
            CriminalCase cc = db.CriminalCases.Find(cid);
            Client c = new UserContext().Clients.Find(cc.clientid);
            ViewBag.refno = cid;
            ViewBag.clientname = c.fullname.Decrypt();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessCriminalCaseSMS()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string refno = Request.Form["refno"];
            string body = Request.Form["body"];
            Notifications.CriminalCaseSMS(refno, body);
            return RedirectToAction("Close", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CriminalCaseLogs()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string cid = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            LogContext db = new LogContext();
            IEnumerable<CriminalLog> c = db.CriminalLogs.Where(x => x.refno == cid);
            IEnumerable<CriminalLog> cl = c.OrderByDescending(x => x.dateaction);
            return View(cl);
        }
        public ActionResult AllCriminalCaseLogs()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            LogContext db = new LogContext();
            IEnumerable<CriminalLog> cl = db.CriminalLogs.OrderByDescending(x => x.dateaction);
            return View(cl);
        }
        public ActionResult AllTaskList()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            TaskContext db = new TaskContext();
            DateTime dt = DateTime.Parse("01/01/2015");
            IEnumerable<CriminalTask> ct = db.CriminalTasks.OrderByDescending(x => DbFunctions.DiffDays(x.daterequired, x.dateassigned)).ToList();
            return View(ct);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CaseDelete(FormCollection form)
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string cid = form["cid"];
            CriminalCase delete;
            using (var db = new CaseContext())
            {
                delete = db.CriminalCases.Where(x => x.refno == cid).FirstOrDefault();
            }
            using (var newdb = new CaseContext())
            {
                newdb.Entry(delete).State = System.Data.Entity.EntityState.Deleted;
                newdb.SaveChanges();
            }
            return RedirectToAction("CloseAndRefreshParent", "Home");
        }
    }
}
