﻿using GoNyLaw.Helpers;
using GoNyLaw.Models;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace GoNyLaw.Controllers
{
    public class DivorceController : Controller
    {
        public int getid()
        {
            UserContext db = new UserContext();
            int id = LoginPersistence.getadminid();
            return db.Admins.Find(id).id;
        }
        public ActionResult DivorceNotice()
        {
            return View();
        }
        public ActionResult DivorceMain()
        {
            IEnumerable<DivorceCase> dc = null;
            CaseContext db = new CaseContext();
            int uid = LoginPersistence.getadminid();
            UserContext udb = new UserContext();
            Admin u = udb.Admins.Find(uid);
            dc = db.DivorceCases.Where(x => x.agency_id == u.id && x.archived == "N").OrderByDescending(x => x.case_date).ToList();
            return View(dc);
        }
        [HttpGet]
        public ActionResult DivorceWithoutChildren()
        {
            OriginSense.SetOrigin(ControllerContext.RouteData.Values["action"].ToString(), ControllerContext.RouteData.Values["controller"].ToString());


            ViewBag.case_id = "DIVN" + Maths.GetRandomNumber();
            UserContext db = new UserContext();
            int id = LoginPersistence.getadminid();
            var agency = db.Admins.Find(id);
            ViewBag.agencyid = agency.id;
            ViewBag.dd = DateTime.Now.ToString();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DivorceWithoutChildren(DivorceCase dc)
        {

            UserContext db1 = new UserContext();
            int id = LoginPersistence.getadminid();
            var agency = db1.Admins.Find(id);
            ViewBag.agencyid = agency.id;
            ViewBag.case_id = dc.case_id;
            ViewBag.dd = DateTime.Now.ToString();
            CaseContext db = new CaseContext();
            ModelState.Clear();
            if (ModelState.IsValid)
            {
                if (dc.r.grounds == "1")
                {
                    dc.a = null;
                    dc.c = null;
                }
                else if (dc.r.grounds == "2" || dc.r.grounds == "3")
                {
                    dc.c = null;
                }
                else
                {
                    dc.a = null;
                }
                dc = dc.Encrypt();
                db.DivorceCases.Add(dc);
                db.SaveChanges();
                db1.SaveChanges();
                return RedirectToAction("DivorceMain", "Divorce");
            }
            return View(dc);
        }
        [HttpGet]
        public ActionResult DivorceWithChildren()
        {
            OriginSense.SetOrigin(ControllerContext.RouteData.Values["action"].ToString(), ControllerContext.RouteData.Values["controller"].ToString());


            ViewBag.case_id = "DIVC" + Maths.GetRandomNumber();
            UserContext db = new UserContext();
            int id = LoginPersistence.getadminid();
            var agency = db.Admins.Find(id);
            ViewBag.agencyid = agency.id;
            ViewBag.dd = DateTime.Now.ToString();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DivorceWithChildren(DivorceCase dc)
        {

            UserContext db1 = new UserContext();
            int id = LoginPersistence.getadminid();
            var agency = db1.Admins.Find(id);
            ViewBag.agencyid = agency.id;
            ViewBag.case_id = dc.case_id;
            ViewBag.dd = DateTime.Now.ToString();
            CaseContext db = new CaseContext();
            ModelState.Clear();
            if (ModelState.IsValid)
            {
                if (dc.r.grounds == "1")
                {
                    dc.a = null;
                    dc.c = null;
                }
                else if (dc.r.grounds == "2" || dc.r.grounds == "3")
                {
                    dc.c = null;
                }
                else
                {
                    dc.a = null;
                }
                dc = dc.Encrypt();
                db.DivorceCases.Add(dc);
                db.SaveChanges();
                db1.SaveChanges();

                return RedirectToAction("DivorceMain", "Divorce");
            }
            return View(dc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DivorceEdit(FormCollection form)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            string cid = form["cid"].ToUpperCase();
            CaseContext db = new CaseContext();
            DivorceCase dc = db.DivorceCases.Find(cid).Decrypt();
            return View(dc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DivorceUpdate(DivorceCase dc)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("AdminLogin", "Home");
            CaseContext db = new CaseContext();
            ModelState.Clear();
            if (ModelState.IsValid)
            {
                DivorceCase dcold = db.DivorceCases.Find(dc.case_id);
                dcold = dcold.Modify(dc, 1);
                dcold.p = dcold.p.Modify(dc.p);
                dcold.d = dcold.d.Modify(dc.d);
                dcold.m = dcold.m.Modify(dc.m);
                dcold.r = dcold.r.Modify(dc.r);
                if (dc.r.grounds == "2" || dc.r.grounds == "3")
                {
                    if (dc.a == null)
                    {
                        dc.a = new Abandonment();
                    }
                    dc.c = null;
                    dcold.a = dcold.a.Modify(dc.a);
                }
                if (dc.r.grounds == "4")
                {
                    if (dc.c == null)
                    {
                        dc.c = new Cruelty();
                    }
                    dc.a = null;
                    dcold.c = dcold.c.Modify(dc.c);
                }
                if (dc.casetype == "2")
                {
                    dcold.ch = dcold.ch.Modify(dc.ch);
                    dcold.co = dcold.co.Modify(dc.co);
                    dcold.h = dcold.h.Modify(dc.h);
                }
                dcold = dcold.Encrypt();
                db.SaveChanges();
                return RedirectToAction("Close", "Home");
            }
            return View(dc);
        }
        [HttpGet]
        public ActionResult DivorceViewSecurityCheck()
        {
            ViewBag.cid = Request.QueryString["cid"].Base64Decode();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DivorceViewClient(FormCollection form)
        {
            string cid = form["case_id"];
            string smscode = form["smscode"];
            CaseContext db = new CaseContext();
            DivorceCase dc = db.DivorceCases.Find(cid).Decrypt();
            if (dc.smscode == smscode.SHA256_Hash())
                return View(dc);
            else
                return RedirectToAction("DivorceViewSecurityCheck", "Divorce", new { cid = cid.Base64Encode() });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DivorceView(FormCollection form)
        {

            string cid = form["cid"];
            CaseContext db = new CaseContext();
            DivorceCase dc = db.DivorceCases.Find(cid).Decrypt();
            return View(dc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DivorceDocuments()
        {

            string cid = Request.Form["cid"];
            CaseContext db = new CaseContext();
            DivorceCase dc = db.DivorceCases.Where(x => x.case_id == cid).FirstOrDefault().Decrypt();
            if (Request.Form["downidentity"] != null && Request.Form["downidentity"] != "")
            {
                var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
                if (dc.casetype == "1")
                {
                    if (Request.Form["downidentity"] == "downloadfacts")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow10 == "1" || dc.allow10 == "4")
                            {
                                Documents.Divorce1_FindingsOfFact(dc);
                                Downloads.Get(folder, "Findings_Of_Fact_Conclusions", dc.case_id);
                            }
                            else if (dc.allow10 == "2")
                            {
                                Downloads.GetBypass(folder, "Findings_Of_Fact_Conclusions", dc.case_id);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadregularity")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow6 == "1" || dc.allow6 == "4")
                            {
                                Documents.Divorce1_AffidavitOfRegularity(dc);
                                Downloads.Get(folder, "Affidavit_Of_Regularity", dc.case_id);
                            }
                            else if (dc.allow6 == "2")
                            {
                                Downloads.GetBypass(folder, "Affidavit_Of_Regularity", dc.case_id);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadverified")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow17 == "1" || dc.allow17 == "4")
                            {
                                Documents.Divorce1_VerifiedComplaint(dc);
                                Downloads.Get(folder, "Verified_Complaint", dc.case_id);
                            }
                            else if (dc.allow17 == "2")
                            {
                                Downloads.GetBypass(folder, "Verified_Complaint", dc.case_id);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadsummons")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow16 == "1" || dc.allow16 == "4")
                            {
                                Documents.Divorce1_Summons(dc);
                                Downloads.Get(folder, "Summons_With_Notice", dc.case_id);
                            }
                            else if (dc.allow16 == "2")
                            {
                                Downloads.GetBypass(folder, "Summons_With_Notice", dc.case_id);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadplaintiff")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow4 == "1" || dc.allow4 == "4")
                            {
                                Documents.Divorce1_AffidavitOfPlaintiff(dc);
                                Downloads.Get(folder, "Affidavit_of_Plaintiff", dc.case_id);
                            }
                            else if (dc.allow4 == "2")
                            {
                                Downloads.GetBypass(folder, "Affidavit_of_Plaintiff", dc.case_id);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloaddefendant")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow1 == "1" || dc.allow1 == "4")
                            {
                                Documents.Divorce1_AffidavitOfDefendant(dc);
                                Downloads.Get(folder, "Affidavit_of_Defendant", dc.case_id);
                            }
                            else if (dc.allow1 == "2")
                            {
                                Downloads.GetBypass(folder, "Affidavit_of_Defendant", dc.case_id);
                            }
                        }
                    }
                }
                else if (dc.casetype == "2")
                {
                    if (Request.Form["downidentity"] == "downloadfacts")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow10 == "1" || dc.allow10 == "4")
                            {
                                Documents.Divorce2_FindingsOfFact(dc);
                                Downloads.Get(folder, "Findings_Of_Fact_Conclusions", dc.case_id);
                            }
                            else if (dc.allow10 == "2")
                            {
                                Downloads.GetBypass(folder, "Findings_Of_Fact_Conclusions", dc.case_id);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadregularity")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow6 == "1" || dc.allow6 == "4")
                            {
                                Documents.Divorce2_AffidavitOfRegularity(dc);
                                Downloads.Get(folder, "Affidavit_Of_Regularity", dc.case_id);
                            }
                            else if (dc.allow6 == "2")
                            {
                                Downloads.GetBypass(folder, "Affidavit_Of_Regularity", dc.case_id);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadverified")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow17 == "1" || dc.allow17 == "4")
                            {
                                Documents.Divorce2_VerifiedComplaint(dc);
                                Downloads.Get(folder, "Verified_Complaint", dc.case_id);
                            }
                            else if (dc.allow17 == "2")
                            {
                                Downloads.GetBypass(folder, "Verified_Complaint", dc.case_id);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadplaintiff")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow4 == "1" || dc.allow4 == "4")
                            {
                                Documents.Divorce2_AffidavitOfPlaintiff(dc);
                                Downloads.Get(folder, "Affidavit_Of_Plaintiff", dc.case_id);
                            }
                            else if (dc.allow4 == "2")
                            {
                                Downloads.GetBypass(folder, "Affidavit_Of_Plaintiff", dc.case_id);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadsummons")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow16 == "1" || dc.allow16 == "4")
                            {
                                Documents.Divorce2_Summons(dc);
                                Downloads.Get(folder, "Summons_With_Notice", dc.case_id);
                            }
                            else if (dc.allow16 == "2")
                            {
                                Downloads.GetBypass(folder, "Summons_With_Notice", dc.case_id);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloaddefendant")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow1 == "1" || dc.allow1 == "4")
                            {
                                Documents.Divorce2_AffidavitOfDefendant(dc);
                                Downloads.Get(folder, "Affidavit_of_Defendant", dc.case_id);
                            }
                            else if (dc.allow1 == "2")
                            {
                                Downloads.GetBypass(folder, "Affidavit_of_Defendant", dc.case_id);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadsupportsheet")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (dc.allow9 == "1" || dc.allow9 == "4")
                            {
                                Documents.Divorce2_ChildCare(dc);
                                Downloads.Get(folder, "Child_Support_Worksheet", dc.case_id);
                            }
                            else if (dc.allow9 == "2")
                            {
                                Downloads.GetBypass(folder, "Child_Support_Worksheet", dc.case_id);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadsupportrefsheet")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            Downloads.GetBypass(@"D:\home\GoNyLaw\Documents\Divorce", "Child_Support_Reference_Sheet", "");
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadjudgement")
                {
                    if (dc.case_id.Length > 1)
                    {
                        Downloads.GetBypass(folder, "Judgement_Of_Divorce", dc.case_id);
                    }
                }
                if (Request.Form["downidentity"] == "downloadservice")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (dc.allow7 == "1" || dc.allow7 == "4")
                        {
                            Documents.Divorce_AffidavitOfService(dc);
                            Downloads.Get(folder, "Affidavit_of_Service", dc.case_id);
                        }
                        else if (dc.allow7 == "2")
                        {
                            Downloads.GetBypass(folder, "Affidavit_of_Service", dc.case_id);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadauthorization")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (dc.allow8 == "1" || dc.allow8 == "4")
                        {
                            Documents.Divorce_Authorization(dc);
                            Downloads.Get(folder, "Authorization", dc.case_id);
                        }
                        else if (dc.allow8 == "2")
                        {
                            Downloads.GetBypass(folder, "Authorization", dc.case_id);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadbarriers")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (dc.allow15 == "1" || dc.allow15 == "4")
                        {
                            Documents.Divorce_RemovalOfBarriers(dc);
                            Downloads.Get(folder, "Removal_Of_Barriers", dc.case_id);
                        }
                        else if (dc.allow15 == "2")
                        {
                            Downloads.GetBypass(folder, "Removal_Of_Barriers", dc.case_id);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadplaintiffnossn")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (dc.allow3 == "1" || dc.allow3 == "4")
                        {
                            Documents.Divorce_NoSocialPlaintiff(dc);
                            Downloads.Get(folder, "Affidavit_of_NoSSN_Plaintiff", dc.case_id);
                        }
                        else if (dc.allow3 == "2")
                        {
                            Downloads.GetBypass(folder, "Affidavit_of_NoSSN_Plaintiff", dc.case_id);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadchildrennossn")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (dc.allow10 == "1" || dc.allow10 == "4")
                        {
                            Documents.Divorce_NoSocialChildren(dc);
                            Downloads.Get(folder, "Affidavit_of_NoSSN_Children", dc.case_id);
                        }
                        else if (dc.allow10 == "2")
                        {
                            Downloads.GetBypass(folder, "Affidavit_of_NoSSN_Children", dc.case_id);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloaddefendantnossn")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (dc.allow2 == "1" || dc.allow2 == "4")
                        {
                            Documents.Divorce_NoSocialDefendant(dc);
                            Downloads.Get(folder, "Affidavit_of_NoSSN_Defendant", dc.case_id);
                        }
                        else if (dc.allow2 == "2")
                        {
                            Downloads.GetBypass(folder, "Affidavit_of_NoSSN_Defendant", dc.case_id);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloaddefendantrefusessn")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (dc.allow5 == "1" || dc.allow5 == "4")
                        {
                            Documents.Divorce_RefuseSocialDefendant(dc);
                            Downloads.Get(folder, "Affidavit_of_RefuseSSN_Defendant", dc.case_id);
                        }
                        else if (dc.allow5 == "2")
                        {
                            Downloads.GetBypass(folder, "Affidavit_of_RefuseSSN_Defendant", dc.case_id);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadchildrenrefusessn")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (dc.allow18 == "1" || dc.allow18 == "4")
                        {
                            Documents.Divorce_RefuseSocialChildren(dc);
                            Downloads.Get(folder, "Affidavit_of_RefuseSSN_Children", dc.case_id);
                        }
                        else if (dc.allow18 == "2")
                        {
                            Downloads.GetBypass(folder, "Affidavit_of_RefuseSSN_Children", dc.case_id);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadnote")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (dc.allow11 == "1" || dc.allow11 == "4")
                        {
                            Documents.Divorce_NoteOfIssue(dc);
                            Downloads.Get(folder, "Note_Of_Issue", dc.case_id);
                        }
                        else if (dc.allow11 == "2")
                        {
                            Downloads.GetBypass(folder, "Note_Of_Issue", dc.case_id);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadnotice")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (dc.allow12 == "1" || dc.allow12 == "4")
                        {
                            Documents.Divorce_NoticeOfEntry(dc);
                            Downloads.Get(folder, "Notice_Of_Entry", dc.case_id);
                        }
                        else if (dc.allow12 == "2")
                        {
                            Downloads.GetBypass(folder, "Notice_Of_Entry", dc.case_id);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadpart")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (dc.allow13 == "1" || dc.allow13 == "4")
                        {
                            Documents.Divorce_Part130Certification(dc);
                            Downloads.Get(folder, "Part_130_Certification", dc.case_id);
                        }
                        else if (dc.allow13 == "2")
                        {
                            Downloads.GetBypass(folder, "Part_130_Certification", dc.case_id);
                        }
                    }
                }
            }
            return View(dc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminDivorceDocuments()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("AdminLogin", "Home");
            string cid = Request.Form["cid"];
            CaseContext db = new CaseContext();
            DivorceCase dc = db.DivorceCases.Find(cid).Decrypt();
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (Request.Form["upidentity"] != null && Request.Form["upidentity"] != "")
            {
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadsummons")
                {
                    Request.Files["File"].Upload(folder, "Summons_With_Notice", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadjudgement")
                {
                    Request.Files["File"].Upload(folder, "Judgement_of_Divorce", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadregularity")
                {
                    Request.Files["File"].Upload(folder, "Affidavit_of_Regularity", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadverified")
                {
                    Request.Files["File"].Upload(folder, "Verified_Complaint", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadservice")
                {
                    Request.Files["File"].Upload(folder, "Affidavit_of_Service", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadplaintiff")
                {
                    Request.Files["File"].Upload(folder, "Affidavit_of_Plaintiff", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploaddefendant")
                {
                    Request.Files["File"].Upload(folder, "Affidavit_of_Defendant", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadauthorization")
                {
                    Request.Files["File"].Upload(folder, "Authorization", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadbarriers")
                {
                    Request.Files["File"].Upload(folder, "Removal_Of_Barriers", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadplaintiffnossn")
                {
                    Request.Files["File"].Upload(folder, "Affidavit_of_NoSSN_Plaintiff", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadchildrennossn")
                {
                    Request.Files["File"].Upload(folder, "Affidavit_of_NoSSN_Children", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploaddefendantnossn")
                {
                    Request.Files["File"].Upload(folder, "Affidavit_of_NoSSN_Defendant", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploaddefendantrefusessn")
                {
                    Request.Files["File"].Upload(folder, "Affidavit_of_RefuseSSN_Defendant", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadchildrenrefusessn")
                {
                    Request.Files["File"].Upload(folder, "Affidavit_of_RefuseSSN_Children", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadsupportsheet")
                {
                    Request.Files["File"].Upload(folder, "Child_Support_Worksheet", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadnote")
                {
                    Request.Files["File"].Upload(folder, "Note_of_Issue", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadnotice")
                {
                    Request.Files["File"].Upload(folder, "Notice_of_Entry", cid);
                }
                if (Request.Files.Count > 0 && Request.Form["upidentity"] == "uploadsupportrefsheet")
                {
                    Request.Files["File"].Upload(@"D:\home\GoNyLaw\Documents\Divorce", "Child_Support_Reference_Sheet", "");
                }
            }
            else if (Request.Form["downidentity"] != null && Request.Form["downidentity"] != "")
            {
                string getflag = "";
                if (!string.IsNullOrWhiteSpace(Request.Form["getuploaded"]))
                    getflag = Request.Form["getuploaded"];
                if (dc.casetype == "1")
                {
                    if (Request.Form["downidentity"] == "downloadverified")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Verified_Complaint", dc.case_id);
                            else
                            {
                                Documents.Divorce1_VerifiedComplaint(dc);
                                Downloads.Get(folder, "Verified_Complaint", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadsummons")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Summons_With_Notice", dc.case_id);
                            else
                            {
                                Documents.Divorce1_Summons(dc);
                                Downloads.Get(folder, "Summons_With_Notice", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadplaintiff")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Affidavit_of_Plaintiff", dc.case_id);
                            else
                            {
                                Documents.Divorce1_AffidavitOfPlaintiff(dc);
                                Downloads.Get(folder, "Affidavit_of_Plaintiff", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloaddefendant")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Affidavit_of_Defendant", dc.case_id);
                            else
                            {
                                Documents.Divorce1_AffidavitOfDefendant(dc);
                                Downloads.Get(folder, "Affidavit_of_Defendant", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadfacts")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Findings_Of_Fact_Conclusions", dc.case_id);
                            else
                            {
                                Documents.Divorce1_FindingsOfFact(dc);
                                Downloads.Get(folder, "Findings_Of_Fact_Conclusions", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadjudgement")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Judgement_Of_Divorce", dc.case_id);
                            else
                            {
                                Documents.Divorce1_Judgement(dc);
                                Downloads.Get(folder, "Judgement_Of_Divorce", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadregularity")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Affidavit_of_Regularity", dc.case_id);
                            else
                            {
                                Documents.Divorce1_AffidavitOfRegularity(dc);
                                Downloads.Get(folder, "Affidavit_of_Regularity", dc.case_id, 1);
                            }
                        }
                    }
                }
                else if (dc.casetype == "2")
                {
                    if (Request.Form["downidentity"] == "downloadchildrennossn")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Affidavit_of_NoSSN_Children", dc.case_id);
                            else
                            {
                                Documents.Divorce_NoSocialChildren(dc);
                                Downloads.Get(folder, "Affidavit_of_NoSSN_Children", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadchildrenrefusessn")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Affidavit_of_RefuseSSN_Children", dc.case_id);
                            else
                            {
                                Documents.Divorce_RefuseSocialChildren(dc);
                                Downloads.Get(folder, "Affidavit_of_RefuseSSN_Children", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadverified")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Verified_Complaint", dc.case_id);
                            else
                            {
                                Documents.Divorce2_VerifiedComplaint(dc);
                                Downloads.Get(folder, "Verified_Complaint", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadcasereg")
                    {
                        if (dc.case_id.Length > 1)
                        {
                                Documents.Divorce2_NYSCaseRegistry(dc);
                                Downloads.GetPDF(folder, "NYS_Case_Registry", dc.case_id);
                        }
                    }
                    if (Request.Form["downidentity"] == "downloaducs111")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            Documents.Divorce2_UCS111(dc);
                            Downloads.GetPDF(folder, "UCS-111", dc.case_id);
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadscuinfo")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            Documents.Divorce2_SCUInfoSheet(dc);
                            Downloads.Get(folder, "Support_Collection_Unit", dc.case_id, 1);
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadregularity")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Affidavit_of_Regularity", dc.case_id);
                            else
                            {
                                Documents.Divorce2_AffidavitOfRegularity(dc);
                                Downloads.Get(folder, "Affidavit_of_Regularity", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadsummons")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Summons_With_Notice", dc.case_id);
                            else
                            {
                                Documents.Divorce2_Summons(dc);
                                Downloads.Get(folder, "Summons_With_Notice", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadplaintiff")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Affidavit_of_Plaintiff", dc.case_id);
                            else
                            {
                                Documents.Divorce2_AffidavitOfPlaintiff(dc);
                                Downloads.Get(folder, "Affidavit_of_Plaintiff", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloaddefendant")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Affidavit_of_Defendant", dc.case_id);
                            else
                            {
                                Documents.Divorce2_AffidavitOfDefendant(dc);
                                Downloads.Get(folder, "Affidavit_of_Defendant", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadsupportsheet")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Child_Support_Worksheet", dc.case_id);
                            else
                            {
                                Documents.Divorce2_ChildCare(dc);
                                Downloads.Get(folder, "Child_Support_Worksheet", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadfacts")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Findings_Of_Fact_Conclusions", dc.case_id);
                            else
                            {
                                Documents.Divorce2_FindingsOfFact(dc);
                                Downloads.Get(folder, "Findings_Of_Fact_Conclusions", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadjudgement")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            if (getflag == "1")
                                Downloads.GetBypass(folder, "Judgement_Of_Divorce", dc.case_id);
                            else
                            {
                                Documents.Divorce2_Judgement(dc);
                                Downloads.Get(folder, "Judgement_Of_Divorce", dc.case_id, 1);
                            }
                        }
                    }
                    if (Request.Form["downidentity"] == "downloadsupportrefsheet")
                    {
                        if (dc.case_id.Length > 1)
                        {
                            Downloads.GetBypass(@"D:\home\GoNyLaw\Documents\Divorce", "Child_Support_Reference_Sheet", "");
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloaddissolution")
                {
                    if (dc.case_id.Length > 1)
                    {
                        Documents.Divorce_CertificateofDissolution(dc);
                        Downloads.GetPDF(folder, "Certificate_of_Dissolution", dc.case_id);
                    }
                }
                if (Request.Form["downidentity"] == "downloadrji")
                {
                    if (dc.case_id.Length > 1)
                    {
                        Documents.Divorce_RJI(dc);
                        Downloads.GetPDF(folder, "RJI", dc.case_id);
                    }
                }
                if (Request.Form["downidentity"] == "downloadauthorization")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (getflag == "1")
                            Downloads.GetBypass(folder, "Authorization", dc.case_id);
                        else
                        {
                            Documents.Divorce_Authorization(dc);
                            Downloads.Get(folder, "Authorization", dc.case_id, 1);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadbarriers")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (getflag == "1")
                            Downloads.GetBypass(folder, "Removal_Of_Barriers", dc.case_id);
                        else
                        {
                            Documents.Divorce_RemovalOfBarriers(dc);
                            Downloads.Get(folder, "Removal_Of_Barriers", dc.case_id, 1);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadplaintiffnossn")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (getflag == "1")
                            Downloads.GetBypass(folder, "Affidavit_of_NoSSN_Plaintiff", dc.case_id);
                        else
                        {
                            Documents.Divorce_NoSocialPlaintiff(dc);
                            Downloads.Get(folder, "Affidavit_of_NoSSN_Plaintiff", dc.case_id, 1);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloaddefendantnossn")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (getflag == "1")
                            Downloads.GetBypass(folder, "Affidavit_of_NoSSN_Defendant", dc.case_id);
                        else
                        {
                            Documents.Divorce_NoSocialDefendant(dc);
                            Downloads.Get(folder, "Affidavit_of_NoSSN_Defendant", dc.case_id, 1);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadpart")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (getflag == "1")
                            Downloads.GetBypass(folder, "Part_130_Certification", dc.case_id);
                        else
                        {
                            Documents.Divorce_Part130Certification(dc);
                            Downloads.Get(folder, "Part_130_Certification", dc.case_id, 1);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloaddefendantrefusessn")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (getflag == "1")
                            Downloads.GetBypass(folder, "Affidavit_of_RefuseSSN_Defendant", dc.case_id);
                        else
                        {
                            Documents.Divorce_RefuseSocialDefendant(dc);
                            Downloads.Get(folder, "Affidavit_of_RefuseSSN_Defendant", dc.case_id, 1);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadservice")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (getflag == "1")
                            Downloads.GetBypass(folder, "Affidavit_of_Service", dc.case_id);
                        else
                        {
                            Documents.Divorce_AffidavitOfService(dc);
                            Downloads.Get(folder, "Affidavit_of_Service", dc.case_id, 1);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadnote")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (getflag == "1")
                            Downloads.GetBypass(folder, "Note_of_Issue", dc.case_id);
                        else
                        {
                            Documents.Divorce_NoteOfIssue(dc);
                            Downloads.Get(folder, "Note_of_Issue", dc.case_id, 1);
                        }
                    }
                }
                if (Request.Form["downidentity"] == "downloadnotice")
                {
                    if (dc.case_id.Length > 1)
                    {
                        if (getflag == "1")
                            Downloads.GetBypass(folder, "Notice_of_Entry", dc.case_id);
                        else
                        {
                            Documents.Divorce_NoticeOfEntry(dc);
                            Downloads.Get(folder, "Notice_of_Entry", dc.case_id, 1);
                        }
                    }
                }
            }
            return View(dc);
        }
        public ActionResult AdminDivorceMain()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            return View();
        }
        [HttpGet]
        public JsonResult GetDivorce()
        {
            if (!LoginPersistence.CheckAdmin())
                return null;
            int skip = 0, take = 0;
            Int32.TryParse(Request.QueryString["s"], out skip);
            Int32.TryParse(Request.QueryString["t"], out take);
            CaseContext db = new CaseContext();
            IEnumerable<DivorceCase> dc = null;
            //dc = db.DivorceCases.Where(x => x.case_id== "DIVC582773").ToList();
            if (take == -1)
                dc = db.DivorceCases.OrderByDescending(x => x.case_date).Skip(skip).ToList();
            else
                dc = db.DivorceCases.OrderByDescending(x => x.case_date).Take(take).ToList();
            var jsonResult = Json(dc, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        [HttpGet]
        public JsonResult GetPaymentConfig()
        {
            if (!LoginPersistence.CheckAdmin())
                return null;
            var p = new SettingsContext().PaymentConfig.Where(x => x.id == 1)
                                                   .Select(x => new { x.divorce_no_kids_total, x.divorce_no_kids_platinum, x.divorce_no_kids_first, x.divorce_no_kids_second, x.divorce_with_kids_total, x.divorce_with_kids_platinum, x.divorce_with_kids_first, x.divorce_with_kids_second })
                                                   .FirstOrDefault();
            var jsonResult = Json(p, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CaseSettings()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("AdminLogin", "Home");
            string cid = Request.Form["cid"];
            CaseContext db = new CaseContext();
            DivorceCase dc = db.DivorceCases.Find(cid).Decrypt();
            ViewBag.dd = DateTime.Now.ToString();
            return View(dc);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult UpdateCaseSettings(DivorceCase dc)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("AdminLogin", "Home");
            CaseContext db = new CaseContext();
            DivorceCase dcold = db.DivorceCases.Find(dc.case_id);
            Admin a = new UserContext().Admins.Find(dcold.agency_id);
            bool docsstatuschanged = dc.HasReceivedDocsStatusChanged(ref dcold);
            string status = dcold.status;
            dcold.agency_id = dc.agency_id;
            dcold.notification_preference = dc.notification_preference;
            dcold.index_no = dc.index_no;
            dcold.index_date = dc.index_date;
            dcold.basis_venue = dc.basis_venue;
            dcold.county_venue = dc.county_venue;
            dcold.waiver_default = dc.waiver_default;
            dcold.date_summons = dc.date_summons;
            dcold.status = dc.status;
            dcold.date_status = dc.date_status;
            dcold.comments = dc.comments;
            dcold.archived = dc.archived;
            dcold.allow1 = dc.allow1;
            dcold.allow2 = dc.allow2;
            dcold.allow3 = dc.allow3;
            dcold.allow4 = dc.allow4;
            dcold.allow5 = dc.allow5;
            dcold.allow6 = dc.allow6;
            dcold.allow7 = dc.allow7;
            dcold.allow8 = dc.allow8;
            dcold.allow9 = dc.allow9;
            dcold.allow10 = dc.allow10;
            dcold.allow11 = dc.allow11;
            dcold.allow12 = dc.allow12;
            dcold.allow13 = dc.allow13;
            dcold.allow14 = dc.allow14;
            dcold.allow15 = dc.allow15;
            dcold.allow16 = dc.allow16;
            dcold.allow17 = dc.allow17;
            dcold.allow18 = dc.allow18;
            if (dcold.casetype == "2" && !object.ReferenceEquals(dcold.co, null) && dcold.co.any_order == "Y")
            {
                if (!string.IsNullOrWhiteSpace(dcold.co.c1_index))
                    dcold.received_co1 = dc.received_co1;
                if (!string.IsNullOrWhiteSpace(dcold.co.c2_index))
                    dcold.received_co2 = dc.received_co2;
                if (!string.IsNullOrWhiteSpace(dcold.co.c3_index))
                    dcold.received_co3 = dc.received_co3;
                if (!string.IsNullOrWhiteSpace(dcold.co.c4_index))
                    dcold.received_co4 = dc.received_co4;
            }
            if (Object.ReferenceEquals(null, dcold.t) && !Object.ReferenceEquals(null, dc.t))
                dcold.t = new Transaction();
            if (!Object.ReferenceEquals(null, dcold.t))
            {
                dcold.t.txn_id1 = dc.t.txn_id1;
                dcold.t.amount1 = dc.t.amount1;
                dcold.t.date1 = dc.t.date1;
                dcold.t.txn_id2 = dc.t.txn_id2;
                dcold.t.amount2 = dc.t.amount2;
                dcold.t.date2 = dc.t.date2;
            }
            db.SaveChanges();
            return RedirectToAction("Close", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RJI()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("AdminLogin", "Home");
            var x = new DivorceCase();
            x.case_id = Request.Form["cid"];
            return new ActionAsPdf("RJIPrinting", x) { FileName = "RJI_" + Request.Form["cid"] + ".pdf", PageOrientation = Rotativa.Options.Orientation.Portrait, PageSize = Rotativa.Options.Size.Letter, PageMargins = { Top = 20 }, PageHeight = 280, CustomSwitches = "--zoom 0.95" };
        }
        public ActionResult RJIPrinting(DivorceCase d)
        {
            string cid = d.case_id;
            CaseContext db = new CaseContext();
            DivorceCase dc = db.DivorceCases.Find(cid);
            return View(dc);
        }
        public ActionResult RJIAddendum()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("AdminLogin", "Home");
            var x = new RJIAddendumFix();
            x.case_id = Request.Form["cid"];
            x.pfirstname = Request.Form["pfirstname"];
            x.plastname = Request.Form["plastname"];
            x.dfirstname = Request.Form["dfirstname"];
            x.dlastname = Request.Form["dlastname"];
            x.c1firstname = Request.Form["c1firstname"];
            x.c1lastname = Request.Form["c1lastname"];
            x.c2firstname = Request.Form["c2firstname"];
            x.c2lastname = Request.Form["c2lastname"];
            x.c3firstname = Request.Form["c3firstname"];
            x.c3lastname = Request.Form["c3lastname"];
            x.c4firstname = Request.Form["c4firstname"];
            x.c4lastname = Request.Form["c4lastname"];
            x.c5firstname = Request.Form["c5firstname"];
            x.c5lastname = Request.Form["c5lastname"];
            return new ActionAsPdf("RJIAddendumPrinting", x) { FileName = "RJI_Addendum_" + Request.Form["cid"] + ".pdf", PageOrientation = Rotativa.Options.Orientation.Landscape, PageSize = Rotativa.Options.Size.Letter, PageMargins = { Top = 15 }, PageHeight = 280, CustomSwitches = "--zoom 0.95" };
        }
        public ActionResult RJIAddendumPrinting(RJIAddendumFix r)
        {
            string cid = r.case_id;
            ViewBag.pfname = r.pfirstname;
            ViewBag.plname = r.plastname;
            ViewBag.dfname = r.dfirstname;
            ViewBag.dlname = r.dlastname;
            ViewBag.c1fname = r.c1firstname;
            ViewBag.c1lname = r.c1lastname;
            ViewBag.c2fname = r.c2firstname;
            ViewBag.c2lname = r.c2lastname;
            ViewBag.c3fname = r.c3firstname;
            ViewBag.c3lname = r.c3lastname;
            ViewBag.c4fname = r.c4firstname;
            ViewBag.c4lname = r.c4lastname;
            ViewBag.c5fname = r.c5firstname;
            ViewBag.c5lname = r.c5lastname;
            CaseContext db = new CaseContext();
            DivorceCase dc = db.DivorceCases.Find(cid);
            return View(dc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DivorceDelete(FormCollection form)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            DivorceCase delete;
            using (var db = new CaseContext())
            {
                delete = db.DivorceCases.Find(form["cid"]);
            }
            using (var newdb = new CaseContext())
            {
                newdb.Entry(delete).State = System.Data.Entity.EntityState.Deleted;
                newdb.SaveChanges();
            }
            return RedirectToAction("Close", "Home");
        }
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        [HttpPost]
        public bool TransferPayment(FormCollection form)
        {
            bool checkbox1 = false, checkbox2 = false, response = false;
            string refno_from = form["case_id"].ToUpper();
            string refno_to = form["refno"].ToUpper();
            DivorceCase dc = new DivorceCase();
            if (!string.IsNullOrEmpty(form["p1"]))
            {
                string checkRespstr = form["p1"];
                checkbox1 = Convert.ToBoolean(checkRespstr);
            }
            if (!string.IsNullOrEmpty(form["p2"]))
            {
                string checkRespstr = form["p2"];
                checkbox2 = Convert.ToBoolean(checkRespstr);
            }
            if (checkbox1 && checkbox2)
            {
                using (CaseContext db = new CaseContext())
                {
                    DivorceCase dc_from = db.DivorceCases.Find(refno_from);
                    DivorceCase dc_to = db.DivorceCases.Find(refno_to);
                    dc = dc_from;
                    dc_to.t = dc_from.t;
                    dc_from.t = null;
                    dc.status = "8";
                    db.SaveChanges();
                    response = true;
                }
            }
            else if (!checkbox1 && checkbox2)
            {
                using (CaseContext db = new CaseContext())
                {
                    DivorceCase dc_from = db.DivorceCases.Find(refno_from);
                    DivorceCase dc_to = db.DivorceCases.Find(refno_to);
                    dc = dc_from;
                    if (object.ReferenceEquals(null, dc_to.t))
                    {
                        dc_to.t = new Transaction();
                        dc_to.t.txn_id1 = dc_from.t.txn_id2;
                        dc_from.t.amount1 = dc_from.t.amount2;
                        dc_to.t.date1 = dc_from.t.date2;
                        dc_from.t.txn_id2 = null;
                        dc_from.t.amount2 = 0;
                        dc_from.t.date2 = null;
                        dc.status = "8";
                        db.SaveChanges();
                        response = true;
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(dc_to.t.txn_id1))
                        {
                            dc_to.t.txn_id1 = dc_from.t.txn_id2;
                            dc_from.t.amount1 = dc_from.t.amount2;
                            dc_to.t.date1 = dc_from.t.date2;
                            dc_from.t.txn_id2 = null;
                            dc_from.t.amount2 = 0;
                            dc_from.t.date2 = null;
                            dc.status = "8";
                            db.SaveChanges();
                            response = true;
                        }
                        else
                        {
                            dc_to.t.txn_id2 = dc_from.t.txn_id2;
                            dc_from.t.amount2 += dc_from.t.amount2;
                            dc_to.t.date2 = dc_from.t.date2;
                            dc_from.t.txn_id2 = null;
                            dc_from.t.amount2 = 0;
                            dc_from.t.date2 = null;
                            dc.status = "8";
                            db.SaveChanges();
                            response = true;
                        }
                    }
                }
            }
            else if (checkbox1 && !checkbox2)
            {
                using (CaseContext db = new CaseContext())
                {
                    DivorceCase dc_from = db.DivorceCases.Find(refno_from);
                    DivorceCase dc_to = db.DivorceCases.Find(refno_to);
                    dc = dc_from;
                    if (object.ReferenceEquals(null, dc_to.t))
                    {
                        dc_to.t = new Transaction();
                        dc_to.t.txn_id1 = dc_from.t.txn_id1;
                        dc_from.t.amount1 = dc_from.t.amount1;
                        dc_to.t.date1 = dc_from.t.date1;
                        dc_from.t.txn_id2 = dc_from.t.txn_id2;
                        dc_from.t.amount2 = dc_from.t.amount2;
                        dc_from.t.date2 = dc_from.t.date2;
                        dc_from.t.txn_id2 = null;
                        dc_from.t.amount2 = 0;
                        dc_from.t.date2 = null;
                        dc.status = "8";
                        db.SaveChanges();
                        response = true;
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(dc_to.t.txn_id1))
                        {
                            dc_to.t.txn_id1 = dc_from.t.txn_id1;
                            dc_from.t.amount1 = dc_from.t.amount1;
                            dc_to.t.date1 = dc_from.t.date1;
                            dc_from.t.txn_id2 = dc_from.t.txn_id2;
                            dc_from.t.amount2 = dc_from.t.amount2;
                            dc_from.t.date2 = dc_from.t.date2;
                            dc_from.t.txn_id2 = null;
                            dc_from.t.amount2 = 0;
                            dc_from.t.date2 = null;
                            dc.status = "8";
                            db.SaveChanges();
                            response = true;
                        }
                        else
                        {
                            dc_to.t.txn_id2 = dc_from.t.txn_id1;
                            dc_from.t.amount2 += dc_from.t.amount1;
                            dc_to.t.date2 = dc_from.t.date1;
                            dc_from.t.txn_id2 = dc_from.t.txn_id2;
                            dc_from.t.amount2 = dc_from.t.amount2;
                            dc_from.t.date2 = dc_from.t.date2;
                            dc_from.t.txn_id2 = null;
                            dc_from.t.amount2 = 0;
                            dc_from.t.date2 = null;
                            dc.status = "8";
                            db.SaveChanges();
                            response = true;
                        }
                    }
                }
            }
            if (response)
            {
                Admin a = new UserContext().Admins.Find(dc.agency_id);
                //Notifications.DivorceRefundIssued_OtherDivorce(dc, a, refno_to);

            }
            return response;
        }
        //public ActionResult DivorceLogs()
        //{
        //    if (!LoginPersistence.CheckAdmin())
        //        return RedirectToAction("AdminLogin", "Home");
        //    string cid = Request.Form["cid"];
        //    //IEnumerable<DivorceLog> dl = new LogContext().DivorceLogs.Where(x => x.case_id == cid);
        //    return View(dl);
        //}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DivorceUpload()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            if (Request.Files.Count > 0)
            {
                Request.Files["File"].UploadMisc(@"D:\home\GoNyLaw\Documents\Divorce\", System.IO.Path.GetFileNameWithoutExtension(Request.Files["File"].FileName), Request.Form["cid"]);
            }
            return RedirectToAction("JustClose", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DivorceDownload()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            string cid = Request.Form["cid"];
            DivorceCase dc = new CaseContext().DivorceCases.Find(cid);
            return View(dc);
        }
    }
}