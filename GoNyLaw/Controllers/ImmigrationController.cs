﻿using GoNyLaw.Helpers;
using GoNyLaw.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoNyLaw.Controllers
{
    public class ImmigrationController : Controller
    {
        [HttpPost]
        public ActionResult ImmigrationCaseQuestionnaire()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            UserContext db = new UserContext();
            string cid = Request.Form["id"];
            int id = 0;
            Int32.TryParse(cid, out id);
            Client c = db.Clients.Find(id);
            ViewBag.name = c.fullname.Decrypt();
            ViewBag.cellphone = c.cellphone.Decrypt();
            if (!string.IsNullOrEmpty(c.emailid))
                ViewBag.emailid = c.emailid.Decrypt();
            else
                ViewBag.emailid = "";
            if (!string.IsNullOrEmpty(c.street))
                ViewBag.street = c.street.Decrypt();
            else
                ViewBag.street = "";
            ViewBag.city = c.city;
            ViewBag.zip = c.zip;
            ViewBag.cid = cid;
            ViewBag.aid = LoginPersistence.getadminid();
            ViewBag.refno = "IMMI" + Maths.GetRandomNumber();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImmigrationCaseQuestionnaireSubmission(ImmigrationCase bc)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            bc.dateregister = DateTime.Today;
            bc.adminsassigned = LoginPersistence.getadminid().ToString();
            bc = bc.Encrypt().ApplyDateCorrection();
            ModelState.Clear();
            string[] path = new string[5];
            if (ModelState.IsValid)
            {                
                db.Configuration.ValidateOnSaveEnabled = false;
                db.ImmigrationCases.Add(bc);
                db.SaveChanges();
                return RedirectToAction("AdminImmigrationCasesList", "Immigration");
            }
            ActionLogs.ImmigrationCase(bc, "C", "NA", "Case Created", "Created By ", null);
            return View();
        }
        public ActionResult AdminImmigrationCasesList()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            int aid = LoginPersistence.getadminid();
            List<ImmigrationCase> c = db.ImmigrationCases.Where(x => x.archived == 0).ToList();
            List<ImmigrationCase> clist = new List<ImmigrationCase>();
            foreach (var item in c)
            {
                if (!string.IsNullOrEmpty(item.adminsassigned))
                    if (item.adminsassigned.Split(',').Contains(aid.ToString()))
                        clist.Add(item);
            }
            IEnumerable<ImmigrationCase> cc = clist.AsEnumerable().OrderBy(x => x.fullname.Decrypt().Substring(x.fullname.Decrypt().LastIndexOf(' ') + 1)).ApplyDateCorrection();
            return View(cc);
        }
        public ActionResult SuperAdminImmigrationCasesList()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            int aid = LoginPersistence.getsuperadminid();
            IEnumerable<ImmigrationCase> c = db.ImmigrationCases.Where(x => x.archived == 0);
            IEnumerable<ImmigrationCase> cc = c.OrderBy(x => x.fullname.Decrypt().Substring(x.fullname.Decrypt().LastIndexOf(' ') + 1)).ApplyDateCorrection();
            return View(cc);
        }
        public ActionResult AdminArchivedImmigrationCasesList()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            int aid = LoginPersistence.getadminid();
            List<ImmigrationCase> c = db.ImmigrationCases.Where(x => x.archived == 1).ToList();
            List<ImmigrationCase> clist = new List<ImmigrationCase>();
            foreach (var item in c)
            {
                if (!string.IsNullOrEmpty(item.adminsassigned))
                    if (item.adminsassigned.Split(',').Contains(aid.ToString()))
                        clist.Add(item);
            }
            IEnumerable<ImmigrationCase> cc = clist.AsEnumerable().OrderBy(x => x.fullname.Decrypt());
            return View(cc);
        }
        public ActionResult SuperAdminArchivedImmigrationCasesList()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            int aid = LoginPersistence.getsuperadminid();
            IEnumerable<ImmigrationCase> c = db.ImmigrationCases.Where(x => x.archived == 1);
            IEnumerable<ImmigrationCase> cc = c.OrderBy(x => x.fullname.Decrypt());
            return View(cc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImmigrationCaseEdit()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            string refno = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            ImmigrationCase cc = db.ImmigrationCases.Where(x => x.refno == refno).FirstOrDefault().Decrypt().ApplyDateCorrection();
            return View(cc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImmigrationCaseView()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            string refno = Request.Form["refno"];
            ImmigrationCase cc = db.ImmigrationCases.Where(x => x.refno == refno).FirstOrDefault().Decrypt().ApplyDateCorrection();
            return View(cc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImmigrationCaseUpdate(ImmigrationCase cc)
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            using (CaseContext db = new CaseContext())
            {
                ImmigrationCase ccold = new CaseContext().ImmigrationCases.Find(cc.refno);
                string modtext = "", logtext = "";                
                if (!string.IsNullOrEmpty(modtext))
                    logtext = modtext + " modified.";
                else
                    logtext = "General Fields modified.";
                ActionLogs.ImmigrationCase(cc, "X", "NA", "Case Edited", logtext, null);
                db.ImmigrationCases.AddOrUpdate(cc.Encrypt());
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
            }
            return RedirectToAction("CloseAndRefreshParent", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SuperAdminImmigrationCreateTask()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            ViewBag.refno = Request.Form["refno"];
            ViewBag.adminsassigned = Request.Form["adminsassigned"];
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SuperAdminImmigrationCreateTaskSubmission(ImmigrationTask ct)
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            TaskContext db = new TaskContext();
            ct.dateassigned = DateTime.Today;
            ModelState.Clear();
            if (ModelState.IsValid)
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                db.ImmigrationTasks.Add(ct);
                db.SaveChanges();
            }
            string[] ad = ct.adminsassigned.Split(',');
            string admins = "";
            int id = 0;
            for (int i = 0; i < ad.Length; i++)
            {
                Int32.TryParse(ad[i], out id);
                if (!string.IsNullOrEmpty(admins))
                    admins = admins + ", " + new UserContext().Admins.Find(id).fullname.Decrypt();
                else
                    admins += new UserContext().Admins.Find(id).fullname.Decrypt();
            }
            ImmigrationCase bc = new CaseContext().ImmigrationCases.Find(ct.refno);
            ActionLogs.ImmigrationCase(bc, "T", admins, "Task Created", ct.task, null);
            Notifications.ImmigrationTaskAssigned(ct);
            return RedirectToAction("Close", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminViewAssignedTask()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            TaskContext db = new TaskContext();
            string refno = Request.Form["refno"];
            IEnumerable<ImmigrationTask> ct = db.ImmigrationTasks.Where(x => x.refno == refno).OrderByDescending(x => DbFunctions.DiffDays(x.daterequired, x.dateassigned));
            return View(ct);
        }
        [HttpGet]
        public ActionResult MarkTaskComplete()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            string i = Request.QueryString["id"];
            string text = Request.QueryString["text"];
            int id = 0;
            Int32.TryParse(i, out id);
            ImmigrationTask ct;
            using (TaskContext db = new TaskContext())
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                ct = db.ImmigrationTasks.Find(id);
                ct.datecompleted = DateTime.Today;
                ct.notes = text;
                db.SaveChanges();
            }
            ImmigrationCase bc = new CaseContext().ImmigrationCases.Find(ct.refno);
            ActionLogs.ImmigrationCase(bc, "T", "NA", "Task - '" + ct.task + "' is complete", text, null);
            Notifications.ImmigrationTaskComplete(ct);
            return RedirectToAction("CloseAndRefreshParent", "Home");
        }
        public ActionResult MyTaskList()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            TaskContext db = new TaskContext();
            int aid = LoginPersistence.getadminid();
            DateTime dt = DateTime.Parse("01/01/2015");
            List<ImmigrationTask> c = db.ImmigrationTasks.OrderByDescending(x => DbFunctions.DiffDays(x.daterequired, x.dateassigned)).ToList();
            List<ImmigrationTask> clist = new List<ImmigrationTask>();
            foreach (var item in c)
            {
                if (!string.IsNullOrEmpty(item.adminsassigned))
                    if (item.adminsassigned.Split(',').Contains(aid.ToString()))
                        clist.Add(item);
            }
            IEnumerable<ImmigrationTask> ct = clist.AsEnumerable();
            return View(ct);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendImmigrationCaseEmail()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string cid = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            ViewBag.defaultrecepient = Request.Form["defaultrecepient"];
            CaseContext db = new CaseContext();
            ImmigrationCase cc = db.ImmigrationCases.Find(cid);
            Client c = new UserContext().Clients.Find(cc.clientid);
            ViewBag.refno = cid;
            ViewBag.clientname = c.fullname.Decrypt();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessImmigrationCaseEmail()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string refno = Request.Form["refno"];
            string body = Request.Form["body"];
            string subject = Request.Form["subject"];
            string recepients = Request.Form["recepients"];
            string[] path = new string[5];
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase file1 = Request.Files["file1"];
                HttpPostedFileBase file2 = Request.Files["file2"];
                HttpPostedFileBase file3 = Request.Files["file3"];
                HttpPostedFileBase file4 = Request.Files["file4"];
                HttpPostedFileBase file5 = Request.Files["file5"];
                if (!Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno))
                {
                    Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno);
                }
                if (!Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails"))
                {
                    Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails");
                }
                if (file1 != null && file1.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file1.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[0] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file1.SaveAs(path[0]);
                }
                if (file2 != null && file2.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file2.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[1] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file2.SaveAs(path[1]);
                }
                if (file3 != null && file3.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file3.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[2] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file3.SaveAs(path[2]);
                }
                if (file4 != null && file4.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file4.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[3] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file4.SaveAs(path[3]);
                }
                if (file5 != null && file5.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file5.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[4] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file5.SaveAs(path[4]);
                }
            }
            Notifications.ImmigrationCaseEmail(refno, recepients, subject, body, path);
            return RedirectToAction("Close", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendImmigrationCaseSMS()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string cid = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            CaseContext db = new CaseContext();
            ImmigrationCase cc = db.ImmigrationCases.Find(cid);
            Client c = new UserContext().Clients.Find(cc.clientid);
            ViewBag.refno = cid;
            ViewBag.clientname = c.fullname.Decrypt();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessImmigrationCaseSMS()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string refno = Request.Form["refno"];
            string body = Request.Form["body"];
            Notifications.ImmigrationCaseSMS(refno, body);
            return RedirectToAction("Close", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImmigrationCaseLogs()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string cid = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            LogContext db = new LogContext();
            IEnumerable<ImmigrationLog> c = db.ImmigrationLogs.Where(x => x.refno == cid);
            IEnumerable<ImmigrationLog> cl = c.OrderByDescending(x => x.dateaction);
            return View(cl);
        }
        public ActionResult AllImmigrationCaseLogs()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            LogContext db = new LogContext();
            IEnumerable<ImmigrationLog> cl = db.ImmigrationLogs.OrderByDescending(x => x.dateaction);
            return View(cl);
        }
        public ActionResult AllTaskList()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            TaskContext db = new TaskContext();
            DateTime dt = DateTime.Parse("01/01/2015");
            IEnumerable<ImmigrationTask> ct = db.ImmigrationTasks.OrderByDescending(x => DbFunctions.DiffDays(x.daterequired, x.dateassigned)).ToList();
            return View(ct);
        }
        public ActionResult CaseDelete(FormCollection form)
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string cid = form["cid"];
            ImmigrationCase delete;
            using (var db = new CaseContext())
            {
                delete = db.ImmigrationCases.Where(x => x.refno == cid).FirstOrDefault();
            }
            using (var newdb = new CaseContext())
            {
                newdb.Entry(delete).State = System.Data.Entity.EntityState.Deleted;
                newdb.SaveChanges();
            }
            return RedirectToAction("CloseAndRefreshParent", "Home");
        }
    }
}