﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoNyLaw.Models;
using GoNyLaw.Helpers;
using System.Text;
using System.IO;
using System.Data.Entity.Migrations;
using System.Data.Entity;
using System.Net;
using System.Globalization;

namespace GoNyLaw.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Thanks()
        {
            return View();
        }
        [HttpGet]
        public ActionResult SignUp()
        {
            Client c = new Client();
            c.recaptcha = "T";
            return View(c);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignUp(Client c)
        {
            UserContext db = new UserContext();
            c.dateregister = DateTime.Today;
            ModelState.Clear();
            c = c.Encrypt();
            c.otp = Maths.SMSCode().Encrypt();
            if (ModelState.IsValid)
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                db.Clients.Add(c);
                db.SaveChanges();
                LoginPersistence.SetUser(c.emailid);
                Notifications.UserRegistration(c);
                if (LoginPersistence.CheckAdmin())
                    return RedirectToAction("CloseAndRefreshParent", "Home");
                else
                    return RedirectToAction("SignIn", "Home");
            }
            return View();
        }
        [HttpGet]
        public ActionResult SignIn()
        {
            Client c = new Client();
            c.recaptcha = "T";
            if (LoginPersistence.CheckUser())
                return RedirectToAction("LandingPage", "Home");
            return View(c);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignIn(Client c)
        {
            if (!Recaptcha.Validate(Request.Params["g-recaptcha-response"]))
            {
                c.recaptcha = "F";
                return View(c);
            }
            UserContext db = new UserContext();
            string otp = c.password.Encrypt();
            c.password = c.password.SHA256_Hash();
            c.emailid = c.emailid.Encrypt();
            Client clientx = db.Clients.Where(x => x.emailid == c.emailid).FirstOrDefault();
            if ((clientx.otpverified == 0 && clientx.otp == otp) || (clientx.otpverified == 1 && clientx.password == c.password))
            {
                LoginPersistence.SetUser(c.emailid);
                ViewBag.message1 = "Success!";
                ViewBag.message2 = "You are authenticated.";
                if (clientx.otpverified == 0)
                    return RedirectToAction("UserNewPassword");
                return View(c);
            }
            else
            {
                ViewBag.message1 = "Error!";
                ViewBag.message2 = "Wrong E-mail Id or Password.";
                return View(c);
            }
        }
        public ActionResult SignOut()
        {
            LoginPersistence.UnsetUser();
            return RedirectToAction("Index", "Home");
        }
        public ActionResult UserNewPassword()
        {
            if (!LoginPersistence.CheckUser())
                return RedirectToAction("Index", "Home");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserNewPassword(Client c)
        {
            if (!LoginPersistence.CheckUser())
                return RedirectToAction("Index", "Home");
            int id = LoginPersistence.getuserid();
            using (UserContext db = new UserContext())
            {
                Client cc = db.Clients.Find(id);
                cc.password = c.password.SHA256_Hash();
                cc.otpverified = 1;
                cc.otp = null;
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
                return RedirectToAction("LandingPage", "Home");
            }
        }
        public ActionResult LandingPage()
        {
            if (!LoginPersistence.CheckUser())
                return RedirectToAction("SignIn", "Home");
            return View();
        }
        [HttpGet]
        public ActionResult AdminSignUp()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminSignUp(Admin a)
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            UserContext db = new UserContext();
            a.dateregister = DateTime.Today;
            a.otp = Maths.SMSCode().Encrypt();
            ModelState.Clear();
            a = a.Encrypt();
            if (ModelState.IsValid)
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                db.Admins.Add(a);
                db.SaveChanges();
                Notifications.AdminOTP(a);
                return RedirectToAction("CloseAndRefreshParent", "Home");
            }
            return View(a);
        }
        [HttpGet]
        public ActionResult AdminSignIn()
        {
            Admin a = new Admin();
            a.recaptcha = "T";
            if (LoginPersistence.CheckAdmin())
            {
                return RedirectToAction("AdminLandingPage", "Home");
            }
            return View(a);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminSignIn(Admin a)
        {
            if (!Recaptcha.Validate(Request.Params["g-recaptcha-response"]))
            {
                a.recaptcha = "F";
                return View(a);
            }
            UserContext db = new UserContext();
            string otp = a.password.Encrypt();
            a.password = a.password.SHA256_Hash();
            a.emailid = a.emailid.Encrypt();
            Admin adminx = db.Admins.Where(x => x.emailid == a.emailid).FirstOrDefault();
            if ((adminx.otpverified == 0 && adminx.otp == otp) || (adminx.otpverified == 1 && adminx.password == a.password))
            {
                LoginPersistence.UnsetSuperAdmin();
                LoginPersistence.UnsetAdmin();
                if (adminx.IsSuperAdmin)
                    LoginPersistence.SetSuperAdmin(adminx.emailid, adminx.fullname);
                LoginPersistence.SetAdmin(adminx.emailid, adminx.fullname);
                ViewBag.message1 = "Success!";
                ViewBag.message2 = "You are authenticated.";
                if (adminx.otpverified == 0)
                    return RedirectToAction("AdminNewPassword");
                return View(a);
            }
            else
            {
                ViewBag.message1 = "Error!";
                ViewBag.message2 = "Wrong E-mail Id or Password.";
                return View(a);
            }
        }
        public ActionResult AdminSignOut()
        {
            LoginPersistence.UnsetAdmin();
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public ActionResult AdminNewPassword()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminNewPassword(Admin a)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            int id = LoginPersistence.getadminid();
            using (UserContext db = new UserContext())
            {
                Admin aa = db.Admins.Find(id);
                aa.password = a.password.SHA256_Hash();
                aa.otpverified = 1;
                aa.otp = null;
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
                return RedirectToAction("AdminLandingPage", "Home");
            }
        }
        public ActionResult AdminLandingPage()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            return View();
        }
        public ActionResult ManageAdmins()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            UserContext db = new UserContext();
            IEnumerable<Admin> a = db.Admins;
            IEnumerable<Admin> admin = a.OrderBy(x => x.fullname.Decrypt());
            return View(admin);
        }
        public ActionResult ManageClients()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            UserContext db = new UserContext();
            IEnumerable<Client> c = db.Clients;
            IEnumerable<Client> client = c.OrderBy(x => x.fullname.Decrypt().Substring(x.fullname.Decrypt().LastIndexOf(' ') + 1));
            return View(client);
        }
        public ActionResult ManagePotentialClients()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            UserContext db = new UserContext();
            IEnumerable<PotentialClient> c = db.PotentialClients;
            IEnumerable<PotentialClient> client = c.OrderBy(x => x.fullname.Decrypt().Substring(x.fullname.Decrypt().LastIndexOf(' ') + 1));
            return View(client);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientEdit(FormCollection form)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            int cid = 0;
            Int32.TryParse(form["cid"], out cid);
            UserContext db = new UserContext();
            Client c = db.Clients.Where(x => x.id == cid).FirstOrDefault().Decrypt().ApplyDateCorrection();
            return View(c);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientUpdate(Client p)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            ModelState.Clear();
            using (UserContext db = new UserContext())
            {
                Client pc = db.Clients.Find(p.id);
                db.Configuration.ValidateOnSaveEnabled = false;
                pc.fullname = p.fullname.Encrypt();
                pc.homephone = p.homephone.Encrypt();
                pc.cellphone = p.cellphone.Encrypt();
                pc.emailid = p.emailid.Encrypt();
                pc.street = p.street.Encrypt();
                pc.city = p.city;
                pc.state = p.state;
                pc.zip = p.zip;
                pc.langpref = p.langpref;
                pc.initialinterest = p.initialinterest;
                int result = db.SaveChanges();
                if (result >= 0)
                    return RedirectToAction("CloseAndRefreshParent", "Home");
            }
            return View(p);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientDelete(FormCollection form)
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            int cid = 0;
            Int32.TryParse(form["cid"], out cid);
            Client delete;
            using (var db = new UserContext())
            {
                delete = db.Clients.Where(x => x.id == cid).FirstOrDefault();
            }
            using (var newdb = new UserContext())
            {
                newdb.Entry(delete).State = System.Data.Entity.EntityState.Deleted;

                newdb.SaveChanges();
            }
            return RedirectToAction("CloseAndRefreshParent", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientPasswordReset(FormCollection form)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            int cid = 0;
            Int32.TryParse(form["cid"], out cid);
            Client reset;
            using (var db = new UserContext())
            {
                reset = db.Clients.Where(x => x.id == cid).FirstOrDefault();
                reset.password = Maths.SMSCode().Encrypt();
                reset.confirmpassword = reset.password;
                db.SaveChanges();
                SMS.SendSMS("Your Unique Code is " + reset.password.Decrypt(), reset.cellphone.Decrypt());
            }
            return RedirectToAction("Close", "Home");
        }
        public ActionResult CreateNewCase()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            UserContext db = new UserContext();
            string cid = Request.Form["cid"];
            int id = 0;
            Int32.TryParse(cid, out id);
            Client c = db.Clients.Find(id);
            return View(c);
        }
        public ActionResult Close()
        {
            return View();
        }
        public ActionResult CloseAndRefreshParent()
        {
            return View();
        }
        [AllowAnonymous]
        public bool ScheduledTasks()
        {
            if (Request.QueryString["apikey"] != "RXyV62u" || Request.QueryString["transactionkey"] != "Tr56gYYhoKLhFd9989")
                return false;
            IEnumerable<CriminalCase> cclist = new CaseContext().CriminalCases.Where(x => x.archived == 0).ToList();
            foreach (var cc in cclist)
            {
                Notifications.CriminalTaskStatusChanged(cc);
                Notifications.CriminalMotions(cc);
                Notifications.CriminalCourtDateEntered(cc, 1);
            }
            return true;
        }
        [AllowAnonymous]
        public bool CreateCalendar()
        {
            iCalendar.CreateEvent();
            return true;
        }
        public ActionResult CalendarWebDisplay()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            return View();
        }
        [HttpGet]
        public ActionResult RegisterForConsultation()
        {
            PotentialClient p = new PotentialClient();
            p.recaptcha = "T";
            return View(p);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterForConsultation(PotentialClient p)
        {
            UserContext db = new UserContext();
            p.dateregister = DateTime.Today;
            p.appointment = MinDateCorrection.MinDate();
            ModelState.Clear();
            p = p.Encrypt();
            if (ModelState.IsValid)
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                db.PotentialClients.Add(p);
                db.SaveChanges();
                if (LoginPersistence.CheckAdmin())
                    return RedirectToAction("CloseAndRefreshParent", "Home");
                else
                    return RedirectToAction("Thanks", "Home");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PotentialClientEdit(FormCollection form)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            int cid = 0;
            Int32.TryParse(form["cid"], out cid);
            UserContext db = new UserContext();
            PotentialClient p = db.PotentialClients.Find(cid).Decrypt();
            return View(p);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PotentialClientUpdate(PotentialClient p)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            ModelState.Clear();
            using (UserContext db = new UserContext())
            {
                PotentialClient pc = db.PotentialClients.Find(p.id);
                db.Configuration.ValidateOnSaveEnabled = false;
                pc.fullname = p.fullname.Encrypt();
                pc.homephone = p.homephone.Encrypt();
                pc.cellphone = p.cellphone.Encrypt();
                pc.emailid = p.emailid.Encrypt();
                pc.street = p.street.Encrypt();
                pc.city = p.city;
                pc.state = p.state;
                pc.zip = p.zip;
                pc.langpref = p.langpref;
                pc.initialinterest = p.initialinterest;
                pc.besttimetocall = p.besttimetocall;
                pc.casedetails = p.casedetails;
                pc.appointment = p.appointment;
                pc.appointmentdetails = p.appointmentdetails;
                int result = db.SaveChanges();
                if (result >= 0)
                    return RedirectToAction("CloseAndRefreshParent", "Home");
            }
            return View(p);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PotentialClientDelete(FormCollection form)
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            int cid = 0;
            Int32.TryParse(form["cid"], out cid);
            PotentialClient delete;
            using (var db = new UserContext())
            {
                delete = db.PotentialClients.Find(cid);
            }
            using (var newdb = new UserContext())
            {
                newdb.Entry(delete).State = System.Data.Entity.EntityState.Deleted;

                newdb.SaveChanges();
            }
            return RedirectToAction("CloseAndRefreshParent", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConvertToClient(FormCollection form)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            int cid = 0;
            Int32.TryParse(form["cid"], out cid);
            UserContext db = new UserContext();
            PotentialClient p = db.PotentialClients.Find(cid);
            Client c = new Client
            {
                dateregister = DateTime.Today,
                fullname = p.fullname,
                homephone = p.homephone,
                cellphone = p.cellphone,
                emailid = p.emailid,
                street = p.street,
                city = p.city,
                state = p.state,
                zip = p.zip,
                langpref = p.langpref,
                initialinterest = p.initialinterest,
                otp = Maths.SMSCode().Encrypt(),
                otpverified = 0
            };
            db.Clients.Add(c);
            db.SaveChanges();
            PotentialClient delete = p;
            using (var newdb = new UserContext())
            {
                newdb.Entry(delete).State = System.Data.Entity.EntityState.Deleted;
                newdb.SaveChanges();
            }
            Notifications.UserRegistration(c);
            return RedirectToAction("ManageClients", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ViewAllCasesClient(FormCollection form)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            int id = 0;
            Int32.TryParse(form["cid"], out id);
            ViewBag.id = id;
            return View();
        }
        public ActionResult MasterAccount()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            AccountingContext db = new AccountingContext();
            IEnumerable<MasterAccount> ma = db.MasterAccountData.Decrypt();
            return View(ma);
        }
        [HttpPost]
        public ActionResult MasterAccount(FormCollection form)
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            AccountingContext db = new AccountingContext();
            string start = form["startdate"];
            string end = form["enddate"];
            DateTime startdate, enddate;
            IEnumerable<MasterAccount> ma = null;
            startdate = DateTime.ParseExact(start, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            enddate = DateTime.ParseExact(end, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            ViewBag.s = start;
            ViewBag.e = end;
            ma = db.MasterAccountData.Where(x => x.date_of_operation >= startdate && x.date_of_operation <= enddate).Decrypt();
            return View(ma);
        }
        public ActionResult SubAccounts()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            AccountingContext db = new AccountingContext();
            IEnumerable<SubAccount> sa = db.SubAccountData.Decrypt();
            return View(sa);
        }
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        [HttpPost]
        public bool CreateSubAccount(FormCollection form)
        {
            double running_balance = 0.0;
            AccountingContext db = new AccountingContext();
            SubAccount sa = new SubAccount();
            MasterAccount ma = new MasterAccount();
            double bal = 0.0;
            int check=0;
            Double.TryParse(form["balance"], out bal);
            Int32.TryParse(form["checkno"], out check);
            DateTime openingdate= DateTime.ParseExact(form["openingdate"], "MM/dd/yyyy", CultureInfo.CurrentCulture);
            sa.date_of_creation = openingdate;
            sa.account_holder_name = form["name"];
            sa.gonylaw_ref_no = form["refno"];
            sa.current_balance = bal;
            sa.Encrypt();
            db.SubAccountData.Add(sa);
            db.SaveChanges();
            MasterAccount malast = db.MasterAccountData.OrderByDescending(x => x.id).FirstOrDefault();
            if (!object.ReferenceEquals(malast, null))
                running_balance = malast.running_balance;
            ma.date_of_operation = openingdate;
            ma.sub_account_id = sa.id;
            ma.transaction_type = 1;
            ma.transaction_amount = bal;
            ma.running_balance = running_balance + bal;
            ma.payable_to = form["depositor"];
            ma.check_no = check;
            ma.remarks = form["remarks"];
            ma.Encrypt();
            db.MasterAccountData.Add(ma);
            db.SaveChanges();
            return true;
        }
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        [HttpPost]
        public bool AddFunds(FormCollection form)
        {
            double running_balance = 0;
            AccountingContext db = new AccountingContext();
            MasterAccount ma = new MasterAccount();
            int id = 0, checkno = 0;
            double amount = 0.0;
            Int32.TryParse(form["id"], out id);
            Double.TryParse(form["amount"], out amount);
            Int32.TryParse(form["checkno"], out checkno);
            DateTime depositdate = DateTime.ParseExact(form["depositdate"], "MM/dd/yyyy", CultureInfo.CurrentCulture);
            SubAccount sa = db.SubAccountData.Find(id);
            sa.last_financial_updation = depositdate;
            sa.current_balance = sa.current_balance + amount;
            db.SaveChanges();
            MasterAccount malast = db.MasterAccountData.OrderByDescending(x => x.id).FirstOrDefault();
            if (!object.ReferenceEquals(malast, null))
                running_balance = malast.running_balance;
            ma.date_of_operation = depositdate;
            ma.sub_account_id = sa.id;
            ma.transaction_type = 2;
            ma.transaction_amount = amount;
            ma.running_balance = running_balance + amount;
            ma.check_no = checkno;
            ma.payable_to = form["depositor"];
            ma.remarks = form["remarks"];
            ma.Encrypt();
            db.MasterAccountData.Add(ma);
            db.SaveChanges();
            return true;
        }
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        [HttpPost]
        public bool WithdrawFunds(FormCollection form)
        {
            double running_balance = 0;
            AccountingContext db = new AccountingContext();
            MasterAccount ma = new MasterAccount();
            int id = 0, checkno = 0;
            Double amount = 0.0;
            Int32.TryParse(form["id"], out id);
            Double.TryParse(form["amount"], out amount);
            Int32.TryParse(form["checkno"], out checkno);
            DateTime withdrawaldate = DateTime.ParseExact(form["withdrawaldate"], "MM/dd/yyyy", CultureInfo.CurrentCulture);
            SubAccount sa = db.SubAccountData.Find(id);
            MasterAccount malast = db.MasterAccountData.OrderByDescending(x => x.id).FirstOrDefault();
            if (!object.ReferenceEquals(malast, null))
                running_balance = malast.running_balance;
            if (sa.current_balance < amount || running_balance < amount)
                return false;
            sa.last_financial_updation = withdrawaldate;
            sa.current_balance = sa.current_balance - amount;
            db.SaveChanges();
            ma.date_of_operation = withdrawaldate;
            ma.sub_account_id = sa.id;
            ma.transaction_type = 3;
            ma.transaction_amount = amount;
            ma.running_balance = running_balance - amount;
            ma.check_no = checkno;
            ma.payable_to = form["payee"];
            ma.remarks = form["remarks"];
            ma.Encrypt();
            db.MasterAccountData.Add(ma);
            db.SaveChanges();
            return true;
        }
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        [HttpPost]
        public string CloseAccount(FormCollection form)
        {
            double running_balance = 0;
            AccountingContext db = new AccountingContext();
            MasterAccount ma = new MasterAccount();
            int id = 0, checkno = 0;
            Double amount = 0.0;
            Int32.TryParse(form["id"], out id);
            Int32.TryParse(form["checkno"], out checkno);
            DateTime closuredate = DateTime.ParseExact(form["closuredate"], "MM/dd/yyyy", CultureInfo.CurrentCulture);
            bool check = db.MasterAccountData.Any(x => x.sub_account_id == id && x.transaction_type == 4);
            if (check)
                return "cl";
            SubAccount sa = db.SubAccountData.Find(id);
            MasterAccount malast = db.MasterAccountData.OrderByDescending(x => x.id).FirstOrDefault();
            if (!object.ReferenceEquals(malast, null))
                running_balance = malast.running_balance;
            amount = sa.current_balance;
            sa.date_of_closure = closuredate;
            sa.current_balance = 0;
            db.SaveChanges();
            ma.date_of_operation = closuredate;
            ma.sub_account_id = sa.id;
            ma.transaction_type = 4;
            ma.transaction_amount = amount;
            ma.running_balance = running_balance - amount;
            ma.check_no = checkno;
            ma.payable_to = form["payee"];
            ma.remarks = form["remarks"];
            ma.Encrypt();
            db.MasterAccountData.Add(ma);
            db.SaveChanges();
            return "true";
        }
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        [HttpPost]
        public string TransactionDetail(FormCollection form)
        {
            int id = 0;
            Int32.TryParse(form["id"], out id);
            AccountingContext db = new AccountingContext();
            SubAccount sa = db.SubAccountData.Find(id);
            return "$" + sa.current_balance.ToString("n2");
        }
        public ActionResult AccountStatement()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            int id = 0;
            Int32.TryParse(Request.QueryString["id"], out id);
            AccountingContext db = new AccountingContext();
            IEnumerable<MasterAccount> ma = db.MasterAccountData.Where(x => x.sub_account_id == id).Decrypt();
            return View(ma);
        }

    }
}