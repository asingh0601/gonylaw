﻿using GoNyLaw.Helpers;
using GoNyLaw.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoNyLaw.Controllers
{
    public class BankruptcyController : Controller
    {
        [HttpPost]
        public ActionResult BankruptcyPetitionQuestionnaire()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            UserContext db = new UserContext();
            string cid = Request.Form["id"];
            int id = 0;
            Int32.TryParse(cid, out id);
            Client c = db.Clients.Find(id);
            ViewBag.name = c.fullname.Decrypt();
            ViewBag.cellphone = c.cellphone.Decrypt();
            if (!string.IsNullOrEmpty(c.emailid))
                ViewBag.emailid = c.emailid.Decrypt();
            else
                ViewBag.emailid = "";
            if (!string.IsNullOrEmpty(c.street))
                ViewBag.street = c.street.Decrypt();
            else
                ViewBag.street = "";
            ViewBag.city = c.city;
            ViewBag.zip = c.zip;
            ViewBag.cid = cid;
            ViewBag.aid = LoginPersistence.getadminid();
            ViewBag.refno = "BANK" + Maths.GetRandomNumber();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BankruptcyPetitionQuestionnaireSubmission(BankruptcyCase bc)
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            bc.dateregister = DateTime.Today;
            bc.adminsassigned = LoginPersistence.getadminid().ToString();
            bc = bc.Encrypt().ApplyDateCorrection();
            ModelState.Clear();
            string[] path = new string[5];
            if (ModelState.IsValid)
            {
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file1 = Request.Files["file1"];
                    HttpPostedFileBase file2 = Request.Files["file2"];
                    HttpPostedFileBase file3 = Request.Files["file3"];
                    HttpPostedFileBase file4 = Request.Files["file4"];
                    HttpPostedFileBase file5 = Request.Files["file5"];
                    HttpPostedFileBase file6 = Request.Files["file6"];
                    HttpPostedFileBase file7 = Request.Files["file7"];
                    HttpPostedFileBase file8 = Request.Files["file8"];
                    HttpPostedFileBase file9 = Request.Files["file9"];
                    if (!Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno))
                    {
                        Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno);
                    }
                    if (!Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails"))
                    {
                        Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails");
                    }
                    if (file1 != null && file1.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file1.FileName);
                        if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName))
                        {
                            fileName = Maths.GetRandomNumber() + "_" + fileName;
                        }
                        path[0] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName;
                        bc.fileclass1 = fileName;
                        file1.SaveAs(path[0]);
                    }
                    if (file2 != null && file2.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file2.FileName);
                        if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName))
                        {
                            fileName = Maths.GetRandomNumber() + "_" + fileName;
                        }
                        path[1] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName;
                        file2.SaveAs(path[1]);
                        bc.fileclass2 = fileName;
                    }
                    if (file3 != null && file3.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file3.FileName);
                        if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName))
                        {
                            fileName = Maths.GetRandomNumber() + "_" + fileName;
                        }
                        path[2] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName;
                        file3.SaveAs(path[2]);
                        bc.filedischargeletter = fileName;
                    }
                    if (file4 != null && file4.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file4.FileName);
                        if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName))
                        {
                            fileName = Maths.GetRandomNumber() + "_" + fileName;
                        }
                        path[3] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName;
                        file4.SaveAs(path[3]);
                        bc.miscfile1 = fileName;
                    }
                    if (file5 != null && file5.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file5.FileName);
                        if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName))
                        {
                            fileName = Maths.GetRandomNumber() + "_" + fileName;
                        }
                        path[4] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName;
                        file5.SaveAs(path[4]);
                        bc.miscfile2 = fileName;
                    }
                    if (file6 != null && file6.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file6.FileName);
                        if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName))
                        {
                            fileName = Maths.GetRandomNumber() + "_" + fileName;
                        }
                        path[5] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName;
                        file6.SaveAs(path[5]);
                        bc.miscfile3 = fileName;
                    }
                    if (file7 != null && file7.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file7.FileName);
                        if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName))
                        {
                            fileName = Maths.GetRandomNumber() + "_" + fileName;
                        }
                        path[6] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName;
                        file7.SaveAs(path[6]);
                        bc.miscfile4 = fileName;
                    }
                    if (file8 != null && file8.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file8.FileName);
                        if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName))
                        {
                            fileName = Maths.GetRandomNumber() + "_" + fileName;
                        }
                        path[7] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName;
                        file8.SaveAs(path[7]);
                        bc.miscfile5 = fileName;
                    }
                    if (file9 != null && file9.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file9.FileName);
                        if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName))
                        {
                            fileName = Maths.GetRandomNumber() + "_" + fileName;
                        }
                        path[8] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + bc.refno + @"\ReceivedEmails\" + fileName;
                        file9.SaveAs(path[8]);
                        bc.fileapplication = fileName;
                    }
                }
                db.Configuration.ValidateOnSaveEnabled = false;
                db.BankruptcyCases.Add(bc);
                db.SaveChanges();
                return RedirectToAction("AdminBankruptcyCasesList", "Bankruptcy");
            }
            ActionLogs.BankruptcyCase(bc, "C", "NA", "Case Created", "Created By ", null);
            return View();
        }
        public ActionResult AdminBankruptcyCasesList()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            int aid = LoginPersistence.getadminid();
            List<BankruptcyCase> c = db.BankruptcyCases.Where(x => x.archived == 0).ToList();
            List<BankruptcyCase> clist = new List<BankruptcyCase>();
            foreach (var item in c)
            {
                if (!string.IsNullOrEmpty(item.adminsassigned))
                    if (item.adminsassigned.Split(',').Contains(aid.ToString()))
                        clist.Add(item);
            }
            IEnumerable<BankruptcyCase> cc = clist.AsEnumerable().OrderBy(x => x.fullname.Decrypt().Substring(x.fullname.Decrypt().LastIndexOf(' ') + 1)).ApplyDateCorrection();
            return View(cc);
        }
        public ActionResult SuperAdminBankruptcyCasesList()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            int aid = LoginPersistence.getsuperadminid();
            IEnumerable<BankruptcyCase> c = db.BankruptcyCases.Where(x => x.archived == 0);
            IEnumerable<BankruptcyCase> cc = c.OrderBy(x => x.fullname.Decrypt().Substring(x.fullname.Decrypt().LastIndexOf(' ') + 1)).ApplyDateCorrection();
            return View(cc);
        }
        public ActionResult AdminArchivedBankruptcyCasesList()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            int aid = LoginPersistence.getadminid();
            List<BankruptcyCase> c = db.BankruptcyCases.Where(x => x.archived == 1).ToList();
            List<BankruptcyCase> clist = new List<BankruptcyCase>();
            foreach (var item in c)
            {
                if (!string.IsNullOrEmpty(item.adminsassigned))
                    if (item.adminsassigned.Split(',').Contains(aid.ToString()))
                        clist.Add(item);
            }
            IEnumerable<BankruptcyCase> cc = clist.AsEnumerable().OrderBy(x => x.fullname.Decrypt());
            return View(cc);
        }
        public ActionResult SuperAdminArchivedBankruptcyCasesList()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            int aid = LoginPersistence.getsuperadminid();
            IEnumerable<BankruptcyCase> c = db.BankruptcyCases.Where(x => x.archived == 1);
            IEnumerable<BankruptcyCase> cc = c.OrderBy(x => x.fullname.Decrypt());
            return View(cc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BankruptcyCaseEdit()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            string refno = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            BankruptcyCase cc = db.BankruptcyCases.Where(x => x.refno == refno).FirstOrDefault().Decrypt().ApplyDateCorrection();
            return View(cc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BankruptcyCaseView()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            CaseContext db = new CaseContext();
            string refno = Request.Form["refno"];
            BankruptcyCase cc = db.BankruptcyCases.Where(x => x.refno == refno).FirstOrDefault().Decrypt().ApplyDateCorrection();
            return View(cc);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BankruptcyCaseUpdate(BankruptcyCase cc)
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            using (CaseContext db = new CaseContext())
            {
                BankruptcyCase ccold = new CaseContext().BankruptcyCases.Find(cc.refno);
                string modtext = "", logtext = "";
                if (ccold.dateclass1 != cc.dateclass1)
                    modtext = "First ";
                if (ccold.dateclass1 != cc.dateclass1)
                {
                    if (!string.IsNullOrEmpty(modtext))
                        modtext = ", Second";
                    else
                        modtext = "Second";
                }
                if (!string.IsNullOrEmpty(modtext))
                    modtext = " Credit Counseling Date";
                if (ccold.courtdatetime != cc.courtdatetime)
                {
                    if (!string.IsNullOrEmpty(modtext))
                        modtext = ", Court Date";
                    else
                        modtext = "Court Date";
                }
                if (ccold.datedischargeletter != cc.datedischargeletter)
                {
                    if (!string.IsNullOrEmpty(modtext))
                        modtext = ", Discharge Letter";
                    else
                        modtext = "Discharge Letter";
                }
                if (!string.IsNullOrEmpty(modtext))
                    logtext = modtext + " modified.";
                else
                    logtext = "General Fields modified.";
                ActionLogs.BankruptcyCase(cc, "X", "NA", "Case Edited", logtext, null);
                db.BankruptcyCases.AddOrUpdate(cc.Encrypt());
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
            }
            return RedirectToAction("CloseAndRefreshParent", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SuperAdminBankruptcyCreateTask()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            ViewBag.refno = Request.Form["refno"];
            ViewBag.adminsassigned = Request.Form["adminsassigned"];
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SuperAdminBankruptcyCreateTaskSubmission(BankruptcyTask ct)
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            TaskContext db = new TaskContext();
            ct.dateassigned = DateTime.Today;
            ModelState.Clear();
            if (ModelState.IsValid)
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                db.BankruptcyTasks.Add(ct);
                db.SaveChanges();
            }
            string[] ad = ct.adminsassigned.Split(',');
            string admins = "";
            int id = 0;
            for (int i = 0; i < ad.Length; i++)
            {
                Int32.TryParse(ad[i], out id);
                if (!string.IsNullOrEmpty(admins))
                    admins = admins + ", " + new UserContext().Admins.Find(id).fullname.Decrypt();
                else
                    admins += new UserContext().Admins.Find(id).fullname.Decrypt();
            }
            BankruptcyCase bc = new CaseContext().BankruptcyCases.Find(ct.refno);
            ActionLogs.BankruptcyCase(bc, "T", admins, "Task Created", ct.task, null);
            Notifications.BankruptcyTaskAssigned(ct);
            return RedirectToAction("Close", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminViewAssignedTask()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            TaskContext db = new TaskContext();
            string refno = Request.Form["refno"];
            IEnumerable<BankruptcyTask> ct = db.BankruptcyTasks.Where(x => x.refno == refno).OrderByDescending(x => DbFunctions.DiffDays(x.daterequired, x.dateassigned));
            return View(ct);
        }
        [HttpGet]
        public ActionResult MarkTaskComplete()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            string i = Request.QueryString["id"];
            string text = Request.QueryString["text"];
            int id = 0;
            Int32.TryParse(i, out id);
            BankruptcyTask ct;
            using (TaskContext db = new TaskContext())
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                ct = db.BankruptcyTasks.Find(id);
                ct.datecompleted = DateTime.Today;
                ct.notes = text;
                db.SaveChanges();
            }
            BankruptcyCase bc = new CaseContext().BankruptcyCases.Find(ct.refno);
            ActionLogs.BankruptcyCase(bc, "T", "NA", "Task - '" + ct.task + "' is complete", text, null);
            Notifications.BankruptcyTaskComplete(ct);
            return RedirectToAction("CloseAndRefreshParent", "Home");
        }
        public ActionResult MyTaskList()
        {
            if (!LoginPersistence.CheckAdmin())
                return RedirectToAction("Index", "Home");
            TaskContext db = new TaskContext();
            int aid = LoginPersistence.getadminid();
            DateTime dt = DateTime.Parse("01/01/2015");
            List<BankruptcyTask> c = db.BankruptcyTasks.OrderByDescending(x => DbFunctions.DiffDays(x.daterequired, x.dateassigned)).ToList();
            List<BankruptcyTask> clist = new List<BankruptcyTask>();
            foreach (var item in c)
            {
                if (!string.IsNullOrEmpty(item.adminsassigned))
                    if (item.adminsassigned.Split(',').Contains(aid.ToString()))
                        clist.Add(item);
            }
            IEnumerable<BankruptcyTask> ct = clist.AsEnumerable();
            return View(ct);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendBankruptcyCaseEmail()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string cid = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            ViewBag.defaultrecepient = Request.Form["defaultrecepient"];
            CaseContext db = new CaseContext();
            BankruptcyCase cc = db.BankruptcyCases.Find(cid);
            Client c = new UserContext().Clients.Find(cc.clientid);
            ViewBag.refno = cid;
            ViewBag.clientname = c.fullname.Decrypt();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessBankruptcyCaseEmail()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string refno = Request.Form["refno"];
            string body = Request.Form["body"];
            string subject = Request.Form["subject"];
            string recepients = Request.Form["recepients"];
            string[] path = new string[5];
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase file1 = Request.Files["file1"];
                HttpPostedFileBase file2 = Request.Files["file2"];
                HttpPostedFileBase file3 = Request.Files["file3"];
                HttpPostedFileBase file4 = Request.Files["file4"];
                HttpPostedFileBase file5 = Request.Files["file5"];
                if (!Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno))
                {
                    Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno);
                }
                if (!Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails"))
                {
                    Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails");
                }
                if (file1 != null && file1.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file1.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[0] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file1.SaveAs(path[0]);
                }
                if (file2 != null && file2.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file2.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[1] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file2.SaveAs(path[1]);
                }
                if (file3 != null && file3.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file3.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[2] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file3.SaveAs(path[2]);
                }
                if (file4 != null && file4.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file4.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[3] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file4.SaveAs(path[3]);
                }
                if (file5 != null && file5.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file5.FileName);
                    if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName))
                    {
                        fileName = Maths.GetRandomNumber() + "_" + fileName;
                    }
                    path[4] = System.AppDomain.CurrentDomain.BaseDirectory + @"\Uploads\" + refno + @"\ReceivedEmails\" + fileName;
                    file5.SaveAs(path[4]);
                }
            }
            Notifications.BankruptcyCaseEmail(refno, recepients, subject, body, path);
            return RedirectToAction("Close", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendBankruptcyCaseSMS()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string cid = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            CaseContext db = new CaseContext();
            BankruptcyCase cc = db.BankruptcyCases.Find(cid);
            Client c = new UserContext().Clients.Find(cc.clientid);
            ViewBag.refno = cid;
            ViewBag.clientname = c.fullname.Decrypt();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessBankruptcyCaseSMS()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string refno = Request.Form["refno"];
            string body = Request.Form["body"];
            Notifications.BankruptcyCaseSMS(refno, body);
            return RedirectToAction("Close", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BankruptcyCaseLogs()
        {
            if (!LoginPersistence.CheckAdmin() && !LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string cid = Request.Form["refno"];
            ViewBag.type = Request.Form["type"];
            LogContext db = new LogContext();
            IEnumerable<BankruptcyLog> c = db.BankruptcyLogs.Where(x => x.refno == cid);
            IEnumerable<BankruptcyLog> cl = c.OrderByDescending(x => x.dateaction);
            return View(cl);
        }
        public ActionResult AllBankruptcyCaseLogs()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            LogContext db = new LogContext();
            IEnumerable<BankruptcyLog> cl = db.BankruptcyLogs.OrderByDescending(x => x.dateaction);
            return View(cl);
        }
        public ActionResult AllTaskList()
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            TaskContext db = new TaskContext();
            DateTime dt = DateTime.Parse("01/01/2015");
            IEnumerable<BankruptcyTask> ct = db.BankruptcyTasks.OrderByDescending(x => DbFunctions.DiffDays(x.daterequired, x.dateassigned)).ToList();
            return View(ct);
        }
        public ActionResult CaseDelete(FormCollection form)
        {
            if (!LoginPersistence.CheckSuperAdmin())
                return RedirectToAction("Index", "Home");
            string cid = form["cid"];
            BankruptcyCase delete;
            using (var db = new CaseContext())
            {
                delete = db.BankruptcyCases.Where(x => x.refno == cid).FirstOrDefault();
            }
            using (var newdb = new CaseContext())
            {
                newdb.Entry(delete).State = System.Data.Entity.EntityState.Deleted;
                newdb.SaveChanges();
            }
            return RedirectToAction("CloseAndRefreshParent", "Home");
        }
    }
}