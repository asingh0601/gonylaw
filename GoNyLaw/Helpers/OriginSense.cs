﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoNyLaw.Helpers
{
    public class OriginSense
    {
        public static void SetOrigin(string action, string controller)
        {
            Reset();
            HttpCookie actionCookie = new HttpCookie("action", action);
            actionCookie.Expires = DateTime.MinValue;
            HttpContext.Current.Response.Cookies.Add(actionCookie);
            HttpCookie controllerCookie = new HttpCookie("controller", controller);
            controllerCookie.Expires = DateTime.MinValue;
            HttpContext.Current.Response.Cookies.Add(controllerCookie);
        }
        public static void Act()
        {
            if (HttpContext.Current.Request.Cookies["action"] == null || HttpContext.Current.Request.Cookies["action"] == null)
                return;
            string action = HttpContext.Current.Request.Cookies["action"].Value;
            string controller = HttpContext.Current.Request.Cookies["controller"].Value;
            if (action != null && action != "" && controller != null && controller != "")
            {
                Reset();
                HttpContext.Current.Response.Redirect(HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/" + controller + "/" + action);
            }
        }
        public static void Reset()
        {
            HttpCookie actionCookie = new HttpCookie("action");
            actionCookie.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(actionCookie);
            HttpCookie controllerCookie = new HttpCookie("controller");
            controllerCookie.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(controllerCookie);
        }
    }
}