﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoNyLaw.Models;

namespace GoNyLaw.Helpers
{
    public class DropDownData
    {
        public static SelectList dssn = new SelectList(
    new List<SelectListItem>
    {
                new SelectListItem {Text = "Provided SSN", Value="Y"},
                new SelectListItem { Text = "Doesn't Have SSN", Value="N"},
                new SelectListItem { Text = "Refused SSN", Value="R"},
    }, "Value", "Text");

        public static SelectList pssn = new SelectList(
          new List<SelectListItem>
          {
                new SelectListItem {Text = "Have SSN", Value="Y"},
                new SelectListItem { Text = "Don't Have SSN", Value="N"},
          }, "Value", "Text");
        public static SelectList permission = new SelectList(
           new List<SelectListItem>
           {
                new SelectListItem {Text = "Don't Allow", Value="0"},
                new SelectListItem { Text = "Allow", Value="1"},
                new SelectListItem { Text = "ByPass", Value = "2" },
                new SelectListItem { Text = "Received", Value="3"},
                new SelectListItem { Text = "Received and Defective", Value="4"},
           }, "Value", "Text");

        public static SelectList permissionb = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Don't Allow", Value="0"},
                new SelectListItem { Text = "Upload & Allow", Value="2"},
            }, "Value", "Text");

        public static SelectList waiver_default = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Waiver", Value="1"},
                new SelectListItem { Text = "Default", Value="2"}
            }, "Value", "Text");
        public static SelectList basisvenue = new SelectList(
    new List<SelectListItem>
    {
                new SelectListItem {Text = "Plaintiff's County", Value="P"},
                new SelectListItem { Text = "Defendant's County", Value="D"},
                new SelectListItem { Text = "CPLR Sec. 509", Value = "C" }
    }, "Value", "Text");
        public static SelectList notification_preference = new SelectList(
    new List<SelectListItem>
    {
                new SelectListItem {Text = "Client And Agency", Value="1"},
                new SelectListItem { Text = "Agency Only", Value="0"},
    }, "Value", "Text");
        public static SelectList nomarriage = new SelectList(
     new List<SelectListItem>
     {
                new SelectListItem {Text = "First", Value="1"},
                new SelectListItem { Text = "Second", Value="2"},
                new SelectListItem { Text = "Third", Value="3"},
                new SelectListItem { Text = "Fourth", Value="4"},
                new SelectListItem { Text = "Fifth", Value="5"},
                new SelectListItem { Text = "Sixth", Value="6"}
     }, "Value", "Text");
        public static SelectList race = new SelectList(
    new List<SelectListItem>
    {
                new SelectListItem {Text = "Hispanic", Value="H"},
                new SelectListItem { Text = "White", Value="W"},
                new SelectListItem { Text = "Black", Value="B"},
                new SelectListItem { Text = "Other", Value="O"},
    }, "Value", "Text");
        public static SelectList states = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem() {Text="Alabama", Value="AL"},
                new SelectListItem() { Text="Alaska", Value="AK"},
                new SelectListItem() { Text="Arizona", Value="AZ"},
                new SelectListItem() { Text="Arkansas", Value="AR"},
                new SelectListItem() { Text="California", Value="CA"},
                new SelectListItem() { Text="Colorado", Value="CO"},
                new SelectListItem() { Text="Connecticut", Value="CT"},
                new SelectListItem() { Text="District of Columbia", Value="DC"},
                new SelectListItem() { Text="Delaware", Value="DE"},
                new SelectListItem() { Text="Florida", Value="FL"},
                new SelectListItem() { Text="Georgia", Value="GA"},
                new SelectListItem() { Text="Hawaii", Value="HI"},
                new SelectListItem() { Text="Idaho", Value="ID"},
                new SelectListItem() { Text="Illinois", Value="IL"},
                new SelectListItem() { Text="Indiana", Value="IN"},
                new SelectListItem() { Text="Iowa", Value="IA"},
                new SelectListItem() { Text="Kansas", Value="KS"},
                new SelectListItem() { Text="Kentucky", Value="KY"},
                new SelectListItem() { Text="Louisiana", Value="LA"},
                new SelectListItem() { Text="Maine", Value="ME"},
                new SelectListItem() { Text="Maryland", Value="MD"},
                new SelectListItem() { Text="Massachusetts", Value="MA"},
                new SelectListItem() { Text="Michigan", Value="MI"},
                new SelectListItem() { Text="Minnesota", Value="MN"},
                new SelectListItem() { Text="Mississippi", Value="MS"},
                new SelectListItem() { Text="Missouri", Value="MO"},
                new SelectListItem() { Text="Montana", Value="MT"},
                new SelectListItem() { Text="Nebraska", Value="NE"},
                new SelectListItem() { Text="Nevada", Value="NV"},
                new SelectListItem() { Text="New Hampshire", Value="NH"},
                new SelectListItem() { Text="New Jersey", Value="NJ"},
                new SelectListItem() { Text="New Mexico", Value="NM"},
                new SelectListItem() { Text="New York", Value="NY"},
                new SelectListItem() { Text="North Carolina", Value="NC"},
                new SelectListItem() { Text="North Dakota", Value="ND"},
                new SelectListItem() { Text="Ohio", Value="OH"},
                new SelectListItem() { Text="Oklahoma", Value="OK"},
                new SelectListItem() { Text="Oregon", Value="OR"},
                new SelectListItem() { Text="Pennsylvania", Value="PA"},
                new SelectListItem() { Text="Rhode Island", Value="RI"},
                new SelectListItem() { Text="South Carolina", Value="SC"},
                new SelectListItem() { Text="South Dakota", Value="SD"},
                new SelectListItem() { Text="Tennessee", Value="TN"},
                new SelectListItem() { Text="Texas", Value="TX"},
                new SelectListItem() { Text="Utah", Value="UT"},
                new SelectListItem() { Text="Vermont", Value="VT"},
                new SelectListItem() { Text="Virginia", Value="VA"},
                new SelectListItem() { Text="Washington", Value="WA"},
                new SelectListItem() { Text="West Virginia", Value="WV"},
                new SelectListItem() { Text="Wisconsin", Value="WI"},
                new SelectListItem() { Text="Wyoming", Value="WY"}
            }, "Value", "Text", "NY");

        public static SelectList clientis = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Plaintiff", Value="P"},
                new SelectListItem { Text = "Defendant", Value="D"},
            }, "Value", "Text");

        public static SelectList scanarea = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Lumbar", Value="L"},
                new SelectListItem { Text = "Cervical", Value="C"},
                new SelectListItem { Text = "Right Shoulder", Value="RS"},
                new SelectListItem { Text = "Left Shoulder", Value="LS"},
                new SelectListItem { Text = "Right Knee", Value="RK"},
                new SelectListItem { Text = "Left Knee", Value="LK"},
                new SelectListItem { Text = "Other", Value="O"},
            }, "Value", "Text");

        public static SelectList scantype = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem { Text = "Bulge", Value="B"},
                new SelectListItem { Text = "Herniation", Value="H"},
                new SelectListItem { Text = "Other", Value="O"},
            }, "Value", "Text");

        public static SelectList civilstatus = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Single", Value="S"},
                new SelectListItem { Text = "Married", Value="M"},
                new SelectListItem {Text = "Divorced", Value="D"},
                new SelectListItem { Text = "Widowed", Value="W"},
            }, "Value", "Text");

        public static SelectList ambulanceservice = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Treated at Scene & Released", Value="S"},
                new SelectListItem { Text = "Transported to Emergency room", Value="E"},
            }, "Value", "Text");

        public static SelectList emergencyroom = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Treated & Released", Value="R"},
                new SelectListItem { Text = "Admitted to Hospital", Value="A"},
            }, "Value", "Text");

        public static SelectList clientroll = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Driver", Value="D"},
                new SelectListItem { Text = "Passenger", Value="P"},
                new SelectListItem { Text = "Pedestrian", Value="F"},
            }, "Value", "Text");

        public static SelectList salaryper = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Week", Value="W"},
                new SelectListItem { Text = "Month", Value="M"},
                new SelectListItem { Text = "Year", Value="Y"},
            }, "Value", "Text");

        public static SelectList one_to_ten = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem { Text = "1", Value="1"},
                new SelectListItem { Text = "2", Value="2"},
                new SelectListItem { Text = "3", Value="3"},
                new SelectListItem { Text = "4", Value="4"},
                new SelectListItem { Text = "5", Value="5"},
                new SelectListItem { Text = "6", Value="6"},
                new SelectListItem { Text = "7", Value="7"},
                new SelectListItem { Text = "8", Value="8"},
                new SelectListItem { Text = "9", Value="9"},
                new SelectListItem { Text = "10", Value="10"},
            }, "Value", "Text");

        public static SelectList one_to_five = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem { Text = "1", Value="1"},
                new SelectListItem { Text = "2", Value="2"},
                new SelectListItem { Text = "3", Value="3"},
                new SelectListItem { Text = "4", Value="4"},
                new SelectListItem { Text = "5", Value="5"},
            }, "Value", "Text");

        public static SelectList one_to_three = new SelectList(
           new List<SelectListItem>
           {
                new SelectListItem { Text = "1", Value="1"},
                new SelectListItem { Text = "2", Value="2"},
                new SelectListItem { Text = "3", Value="3"},
           }, "Value", "Text");

        public static SelectList typeaccident = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Automobile/Motor Cycle", Value="A"},
                new SelectListItem { Text = "Premises", Value="P"},
                new SelectListItem { Text = "Wrongful Death", Value="W"},
                new SelectListItem { Text = "Medical Malpractice", Value="M"},
                new SelectListItem { Text = "Other", Value="O"},
            }, "Value", "Text");

        public static SelectList emailrecepients = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Client", Value="1"},
                new SelectListItem { Text = "ADA", Value="2"},
            }, "Value", "Text");

        public static SelectList interest = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Criminal", Value="Criminal"},
                new SelectListItem { Text = "Bankruptcy", Value="Bankruptcy"},
                new SelectListItem { Text = "Immigration", Value="Immigration"},
                new SelectListItem { Text = "PersonalInjury", Value="Personal Injury"},
            }, "Value", "Text");

        public static SelectList casetypelist = new SelectList(
           new List<SelectListItem>
           {
                new SelectListItem {Text = "Criminal", Value="Criminal"},
                new SelectListItem { Text = "Bankruptcy", Value="Bankruptcy"},
           }, "Value", "Text");

        public static SelectList immigrationtypes = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Type 1", Value="1"},
                new SelectListItem { Text = "Type 2", Value="2"},
                new SelectListItem { Text = "Type 3", Value="3"},
                new SelectListItem { Text = "Type 4", Value="4"},
                new SelectListItem { Text = "Type 5", Value="5"},
                new SelectListItem { Text = "Type 6", Value="6"},
                new SelectListItem { Text = "Type 7", Value="7"},
                new SelectListItem { Text = "Type 8", Value="8"},
                new SelectListItem { Text = "Type 9", Value="9"},
                new SelectListItem { Text = "Type 10", Value="10"},
            }, "Value", "Text");

        public static SelectList besttime = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "08:00 AM - 11:59 AM", Value="1"},
                new SelectListItem { Text = "12:00 PM - 03:59 PM", Value="2"},
                new SelectListItem { Text = "04:00 PM - 07:59 PM", Value="3"},
                new SelectListItem { Text = "After 08:00 PM", Value="24"}
            }, "Value", "Text");

        public static SelectList language = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "English", Value="E"},
                new SelectListItem { Text = "Spanish", Value="S"},
            }, "Value", "Text");

        public static SelectList archived = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Active", Value="0"},
                new SelectListItem { Text = "Archived", Value="1"},
            }, "Value", "Text");

        public static SelectList casetype = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Civil", Value="Civ"},
                new SelectListItem { Text = "Criminal", Value="Cri"},
            }, "Value", "Text");

        public static SelectList court = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Criminal Court", Value="5"},
                new SelectListItem {Text = "Civil Court", Value="1"},
                new SelectListItem { Text = "Supreme Court", Value="2"},
                new SelectListItem { Text = "Federal Eastern District", Value="3"},
                new SelectListItem { Text = "Federal Southern District", Value="4"},
            }, "Value", "Text");

        public static SelectList picourt = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem { Text = "Supreme Court", Value="S"},
                new SelectListItem {Text = "Civil Court", Value="C"},
                new SelectListItem {Text = "Federal Court", Value="F"},
            }, "Value", "Text");

        public static SelectList picourtcounty = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem { Text = "Bronx", Value="1"},
                new SelectListItem {Text = "New York", Value="2"},
                new SelectListItem {Text = "Kings", Value="3"},
                new SelectListItem {Text = "Queens", Value="4"},
                new SelectListItem {Text = "Richmond", Value="5"},
                new SelectListItem {Text = "Nassau", Value="6"},
                new SelectListItem {Text = "Suffolk", Value="7"},
                new SelectListItem {Text = "Westchester", Value="8"},
                 new SelectListItem {Text = "Eastern District", Value="9"},
                new SelectListItem {Text = "Southern District", Value="10"},
            }, "Value", "Text");

        public static SelectList bankruptcycourt = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Eastern District", Value="1"},
                new SelectListItem { Text = "Southern District", Value="2"},
            }, "Value", "Text");

        public static SelectList easterndistrictcourt = new SelectList(
           new List<SelectListItem>
           {
                new SelectListItem {Text = "271-C Cadman Plaza East, Brooklyn, New York 11201-1800", Value="1"},
                new SelectListItem { Text = "560 Federal Plaza, Central Islip, New York 11722-4437", Value="2"},
           }, "Value", "Text");

        public static SelectList southerndistrictcourt = new SelectList(
           new List<SelectListItem>
           {
                new SelectListItem {Text = "One Bowling Green, New York, New York 10004-1408", Value="3"},
                new SelectListItem { Text = "300 Quarropas Street, White Plains, New York 10601", Value="4"},
           }, "Value", "Text");

        public static SelectList county = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "New York", Value="Y"},
                new SelectListItem { Text = "Kings", Value="K"},
                new SelectListItem { Text = "Queens", Value="Q"},
                new SelectListItem { Text = "Bronx", Value="B"},
                new SelectListItem { Text = "Richmond", Value="R"},
                new SelectListItem { Text = "Westchester", Value="W"},
                new SelectListItem { Text = "Nassau", Value="N"},
                new SelectListItem { Text = "Suffolk", Value="S"},
            }, "Value", "Text");

        public static SelectList courtaddress = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "100 Centre Street, New York, New York 10007", Value="1"},
                new SelectListItem { Text = "111 Centre Street, New York, New York 10007  ", Value="2"},
                new SelectListItem { Text = "320 Jay Street, Brooklyn, New York 11201 ", Value="3"},
                new SelectListItem { Text = "120 Schermerhorn Street, Brooklyn, New York 11201 ", Value="4"},
                new SelectListItem { Text = "125-01 Queens Blvd, Kew Gardens, New York 11415 ", Value="5"},
                new SelectListItem { Text = "215 East 161 Street, Bronx, New York 10451 ", Value="6"},
                new SelectListItem { Text = "851 Grand Concourse, Bronx, New York 10451 ", Value="7"},
                new SelectListItem { Text = "26 Central Avenue, Staten Island, New York 10301 ", Value="8"},
                new SelectListItem { Text = "Other", Value="9"},
            }, "Value", "Text");

        public static SelectList docketorindex = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Ind. No.", Value="I"},
                new SelectListItem { Text = "Docket No.", Value="D"},
            }, "Value", "Text");

        public static SelectList yesno = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Yes", Value="Y"},
                new SelectListItem { Text = "No", Value="N"},
            }, "Value", "Text");
        static List<Admin> a = new List<Admin>();
        public static IEnumerable<SelectListItem> admins = a.Select(c => new SelectListItem
        {
            Value = c.id.ToString(),
            Text = c.fullname.Decrypt().ToTitleCase()
        }).OrderBy(x => x.Text);
        private static List<Admin> Decrypt(List<Admin> a)
        {
            foreach (var item in a)
                item.fullname = item.fullname.Decrypt();
            return a;
        }
        public static SelectList status = new SelectList(
           new List<SelectListItem>
           {
                new SelectListItem { Text = "New", Value="1"},
                new SelectListItem { Text = "No Contact", Value="2"},
                new SelectListItem { Text = "Info Reviewed with Agency", Value = "3" },
                new SelectListItem { Text = "Info Reviewed with Agency more Info/Docs", Value = "15" },
                new SelectListItem { Text = "Index Number Purchased", Value = "16" },
                new SelectListItem { Text = "Contested", Value = "4" },
                new SelectListItem { Text = "Documents Received/Ready for Filing", Value = "5" },
                new SelectListItem { Text = "Missing Documents/Read Email", Value = "6" },
                new SelectListItem { Text = "All Documents Received/Payment Hold", Value = "17" },
                new SelectListItem { Text = "On Hold (Client or Agency)", Value = "9" },
                new SelectListItem { Text = "With Service", Value = "7" },
                new SelectListItem { Text = "In Court", Value = "10" },
                new SelectListItem { Text = "Court Requested More Info", Value = "11" },
                new SelectListItem { Text = "Judgement Signed", Value = "12" },
                new SelectListItem { Text = "Judgement Sent", Value = "13" },
                new SelectListItem { Text = "Refund Issued via Check", Value = "14" },
                new SelectListItem { Text = "Refund Issued via Credit Card", Value = "18" },
                new SelectListItem { Text = "Refund Credited to other Divorce", Value = "8" }
           }, "Value", "Text");
        public static IEnumerable<Country> GetCountries()
        {
            return CountryList.LIST_OF_COUNTRIES
                 .Select(x => new Country
                 {
                     ID = x.Value,
                     Name = x.Key.ToTitleCase()
                 })
                                 .GroupBy(c => c.ID)
                                 .Select(c => c.First())
                                 .OrderBy(x => x.Name);
        }

        static UserContext db = new UserContext();
        static List<SalesPerson> s = new List<SalesPerson>();
        public static IEnumerable<SelectListItem> salesperson = s.Select(s => new SelectListItem
        {
            Value = s.id.ToString(),
            Text = s.id + " - " + s.fullname.Decrypt().ToTitleCase()
        });

        public static SelectList sex = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Male", Value="M"},
                new SelectListItem { Text = "Female", Value="F"}
            }, "Value", "Text");

        public static SelectList email_recepients = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Default", Value="0"},
                new SelectListItem { Text = "Agency Only", Value="1"},
                new SelectListItem { Text = "Client Only", Value="2"},
                new SelectListItem { Text = "Agency and Client", Value="3"},
            }, "Value", "Text");

        public static SelectList showagency = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Agency With Divorces", Value="No"},
                new SelectListItem { Text = "All Agencies", Value="Yes"}
            }, "Value", "Text");

        public static SelectList bposition = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "President", Value="P"},
                new SelectListItem { Text = "Vice President", Value="V"},
                new SelectListItem {Text = "Treasurer", Value="T"},
                new SelectListItem {Text = "Stock Holder", Value="S"},
                new SelectListItem {Text = "Other", Value="O"}
            }, "Value", "Text");

        public static SelectList corplegalstructure = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Sole Proprietorship", Value="S"},
                new SelectListItem { Text = "Corporation", Value="C"},
                new SelectListItem {Text = "Partnership", Value="P"},
                new SelectListItem {Text = "Other Business Structure", Value="O"},
                new SelectListItem {Text = "Limited Liability Company - LLC", Value="L"}
            }, "Value", "Text");

        public static SelectList lastpaymentsuccessful = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "No Payment", Value="0"},
                new SelectListItem { Text = "Successful", Value="1"},
                new SelectListItem {Text = "Failed", Value="-1"},
            }, "Value", "Text");

        public static SelectList memtype = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Platinum", Value="P"},
                new SelectListItem { Text = "Gold", Value="G"},
                new SelectListItem { Text = "Bronze", Value="B"}
            }, "Value", "Text");

        public static SelectList corpsuffix = new SelectList(
           new List<SelectListItem>
           {
                new SelectListItem {Text = "Inc.", Value="IS"},
                new SelectListItem { Text = "Corp.", Value="CS"},
                new SelectListItem {Text = "Incorporated", Value="IL"},
                new SelectListItem {Text = "Corporation", Value="CL"}
           }, "Value", "Text");

        public static SelectList llcsuffix = new SelectList(
          new List<SelectListItem>
          {
                new SelectListItem {Text = "LLC", Value="S"},
                new SelectListItem { Text = "Limited Liability Company", Value="L"},
          }, "Value", "Text");

        public static SelectList sorttime = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Since Yesterday", Value="Y"},
                new SelectListItem {Text = "Last 3 days", Value="T"},
                new SelectListItem {Text = "Last 7 days", Value="W"},
                new SelectListItem {Text = "Last 30 days", Value="M"},
                new SelectListItem { Text = "Last 3 Months", Value="Q"},
                new SelectListItem { Text = "Last 6 Months", Value="H"},
                new SelectListItem { Text = "Since an Year", Value="S"},
            }, "Value", "Text");

        public static SelectList bcorp = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Corporation", Value="C"},
                new SelectListItem { Text = "LLC", Value="L"},
                new SelectListItem { Text = "None", Value="N"}
            }, "Value", "Text");

        public static SelectList frequency = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Monthly", Value="M"},
                new SelectListItem { Text = "Weekly", Value="W"},
                new SelectListItem { Text = "Biweekly", Value="B"}
            }, "Value", "Text");

        public static SelectList echecks = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "E-Check Enabled", Value="0"},
                new SelectListItem { Text = "E-Check Disabled", Value="1"},
            }, "Value", "Text");

        public static SelectList childliveswith = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "With Plaintiff", Value="P"},
                new SelectListItem { Text = "With Defendant", Value="D"},
                new SelectListItem { Text = "With a Third Party", Value="T"},
            }, "Value", "Text");

        public static SelectList noofchildren = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "One", Value="1"},
                new SelectListItem { Text = "Two", Value="2"},
                new SelectListItem { Text = "Three", Value="3"},
                new SelectListItem { Text = "Four", Value="4"},
                new SelectListItem { Text = "Five", Value="5"},
                new SelectListItem { Text = "Six", Value="6"}
            }, "Value", "Text");

        public static SelectList noofcourtorders = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "One", Value="1"},
                new SelectListItem { Text = "Two", Value="2"},
                new SelectListItem { Text = "Three", Value="3"},
                new SelectListItem { Text = "Four", Value="4"},
            }, "Value", "Text");

        public static SelectList applicable = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Plaintiff’s residence in N.Y. for 2 years", Value="1"},
                new SelectListItem { Text = "Defendant’s residence in N.Y. for 2 years", Value="2"},
                new SelectListItem { Text = "Marriage in N.Y. plus 1 year residence by Plaintiff", Value="3"},
                new SelectListItem { Text = "Marriage in N.Y. plus 1 year residence by Defendant", Value="4"},
                new SelectListItem { Text = "Having lived in N.Y. as husband and wife plus 1 year residence by Plaintiff", Value="5"},
                new SelectListItem { Text = "Having lived in N.Y. as husband and wife plus 1 year residence by Defendant", Value="6"},
                new SelectListItem { Text = "The act upon which the divorce is founded occurred in N.Y. and a party has resided in N.Y. for 1 year", Value="7"},
                new SelectListItem { Text = "The act upon which the divorce is founded occurred in N.Y. and both parties live in N.Y.", Value="8"},
            }, "Value", "Text");

        public static SelectList grounds = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Irretrievable Breakdown Of Marriage Since Six Months) ", Value="1"},
                new SelectListItem { Text = "Abandonment Since One Year", Value="2"},
                new SelectListItem { Text = "Constructive Abandonment (Cessation of Sexual Relations since One Year)", Value="3"},
                new SelectListItem { Text = "Cruel And Inhuman Treatment", Value="4"},
            }, "Value", "Text");

        public static SelectList abandonment = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "My Spouse Abandoned Marital Home (above 1 yrs.) - No Intention to Return", Value="1"},
                new SelectListItem { Text = "I was locked out of marital home (above 1 yrs.)", Value="2"}
            }, "Value", "Text");

        public static SelectList education = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Elementary 0", Value="E0"},
                new SelectListItem { Text = "Elementary 1", Value="E1"},
                new SelectListItem { Text = "Elementary 2", Value="E2"},
                new SelectListItem { Text = "Elementary 3", Value="E3"},
                new SelectListItem { Text = "Elementary 4", Value="E4"},
                new SelectListItem { Text = "Elementary 5", Value="E5"},
                new SelectListItem { Text = "Elementary 6", Value="E6"},
                new SelectListItem { Text = "Elementary 7", Value="E7"},
                new SelectListItem { Text = "Elementary 8", Value="E8"},
                new SelectListItem { Text = "Highschool 1", Value="H1"},
                new SelectListItem { Text = "Highschool 2", Value="H2"},
                new SelectListItem { Text = "Highschool 3", Value="H3"},
                new SelectListItem { Text = "Highschool 4", Value="H4"},
                new SelectListItem { Text = "College 1", Value="C1"},
                new SelectListItem { Text = "College 2", Value="C2"},
                new SelectListItem { Text = "College 3", Value="C3"},
                new SelectListItem { Text = "College 4", Value="C4"},
                new SelectListItem { Text = "College 5+", Value="C5"}
            }, "Value", "Text");

        public static SelectList clergy = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Civil Ceremony", Value="C"},
                new SelectListItem { Text = "Religious Ceremony", Value="R"}
            }, "Value", "Text");

        public static SelectList typeofcourtorder = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Child Support", Value="S"},
                new SelectListItem { Text = "Custody", Value="C"},
                new SelectListItem { Text = "Visitation", Value="V"},
                new SelectListItem { Text = "Order of Protection", Value="O"},
            }, "Value", "Text");

        public static SelectList typeofcourt = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Family Court", Value="F"},
                new SelectListItem { Text = "Criminal Court", Value="C"},
                new SelectListItem { Text = "Supreme Court", Value="S"},
            }, "Value", "Text");
      
        public static SelectList agency_waiver_default = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "Yes", Value="1"},
                new SelectListItem { Text = "No", Value="2"}
            }, "Value", "Text");


        public static SelectList archived1 = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem {Text = "All Cases", Value="A"},
                new SelectListItem {Text = "Active", Value="N"},
                new SelectListItem { Text = "Archived", Value="Y"}
            }, "Value", "Text", "N");

        public static SelectList stoppayment = new SelectList(
           new List<SelectListItem>
           {
                new SelectListItem {Text = "Active", Value="0"},
                new SelectListItem {Text = "Stop", Value="1"}
           }, "Value", "Text");
        public static SelectList cssn = new SelectList(
           new List<SelectListItem>
           {
                new SelectListItem {Text = "Has SSN", Value="Y"},
                new SelectListItem { Text = "Doesn't Have SSN", Value="N"},
                new SelectListItem { Text = "Parent Refused SSN", Value="R"},
           }, "Value", "Text");
        public static SelectList planholder = new SelectList(
         new List<SelectListItem>
         {
                new SelectListItem {Text = "Plaintiff", Value="P"},
                new SelectListItem { Text = "Defendant", Value="D"},
                new SelectListItem { Text = "None", Value = "N" }
         }, "Value", "Text");
        public static SelectList healthresponsible = new SelectList(
            new List<SelectListItem>
            {
                new SelectListItem { Text = "Plaintiff's", Value="P"},
                new SelectListItem { Text = "Defendant's", Value="D"}
            }, "Value", "Text");
    }
}
