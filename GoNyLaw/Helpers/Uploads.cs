﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GoNyLaw.Helpers
{
    public static class Uploads
    {
        public static string Upload(this HttpPostedFileBase file, string folder, string filename, string refno)
        {
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (!Directory.Exists(folder + @"\AdminUploads\"))
            {
                Directory.CreateDirectory(folder + @"\AdminUploads\");
            }
            if (file != null && file.ContentLength > 0)
            {
                var ext = Path.GetExtension(file.FileName);
                if (File.Exists(folder + @"\AdminUploads\" + filename + ext))
                {
                    File.Delete(folder + @"\AdminUploads\" + filename + ext);
                }
                var path = folder + @"\AdminUploads\" + filename + ext;
                file.SaveAs(path);
                return "Success";
            }
            return "Failed";
        }
        public static string UploadMisc(this HttpPostedFileBase file, string folder, string filename, string refno)
        {
            if (!string.IsNullOrWhiteSpace(refno))
                folder += refno;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (!Directory.Exists(folder + @"\AdminMiscUploads\"))
            {
                Directory.CreateDirectory(folder + @"\AdminMiscUploads\");
            }
            if (file != null && file.ContentLength > 0)
            {
                var ext = Path.GetExtension(file.FileName);
                if (File.Exists(folder + @"\AdminMiscUploads\" + filename + ext))
                {
                    File.Delete(folder + @"\AdminMiscUploads\" + filename + ext);
                }
                var path = folder + @"\AdminMiscUploads\" + filename + ext;
                file.SaveAs(path);
                return "Success";
            }
            return "Failed";
        }
    }
}