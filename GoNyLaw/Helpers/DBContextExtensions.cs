﻿using GoNyLaw.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace GoNyLaw.Helpers
{
    public static class DBContextExtensions
    {
        public static T Modify<T>(this T t, T tnew, int dtflag = 0)
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (dtflag == 0)
                {
                    if (property.PropertyType == typeof(string) || property.PropertyType == typeof(DateTime) || (property.PropertyType == typeof(int) && property.Name != "id" && property.Name != "case_id"))
                    {
                        if (!object.ReferenceEquals(property.GetValue(tnew), null))
                            property.SetValue(t, property.GetValue(tnew));
                    }
                }
                else
                {
                    if (property.PropertyType == typeof(string) || (property.PropertyType == typeof(int) && property.Name != "id" && property.Name != "case_id"))
                    {
                        if (!object.ReferenceEquals(property.GetValue(tnew), null))
                            property.SetValue(t, property.GetValue(tnew));
                    }
                }
            }
            return t;
        }
        public static Client Encrypt(this Client c)
        {
            c.fullname = c.fullname.ToTitleCase().Encrypt();
            c.cellphone = c.cellphone.Encrypt();
            c.homephone = c.homephone.Encrypt();
            if (!string.IsNullOrEmpty(c.emailid))
                c.emailid = c.emailid.ToLowerCase().Encrypt();
            if (!string.IsNullOrEmpty(c.street))
                c.street = c.street.ToTitleCase().Encrypt();
            return c;
        }
        public static Client Decrypt(this Client c)
        {
            c.fullname = c.fullname.Decrypt();
            c.cellphone = c.cellphone.Decrypt();
            c.homephone = c.homephone.Decrypt();
            if (!string.IsNullOrEmpty(c.emailid))
                c.emailid = c.emailid.Decrypt();
            if (!string.IsNullOrEmpty(c.street))
                c.street = c.street.Decrypt();
            return c;
        }
        public static PotentialClient Encrypt(this PotentialClient c)
        {
            c.fullname = c.fullname.ToTitleCase().Encrypt();
            c.cellphone = c.cellphone.Encrypt();
            c.homephone = c.homephone.Encrypt();
            if (!string.IsNullOrEmpty(c.emailid))
                c.emailid = c.emailid.ToLowerCase().Encrypt();
            if (!string.IsNullOrEmpty(c.street))
                c.street = c.street.ToTitleCase().Encrypt();
            return c;
        }
        public static PotentialClient Decrypt(this PotentialClient c)
        {
            c.fullname = c.fullname.Decrypt();
            c.cellphone = c.cellphone.Decrypt();
            c.homephone = c.homephone.Decrypt();
            if (!string.IsNullOrEmpty(c.emailid))
                c.emailid = c.emailid.Decrypt();
            if (!string.IsNullOrEmpty(c.street))
                c.street = c.street.Decrypt();
            return c;
        }
        public static Admin Encrypt(this Admin a)
        {
            a.fullname = a.fullname.ToTitleCase().Encrypt();
            a.cellphone = a.cellphone.Encrypt();
            a.emailid = a.emailid.ToLowerCase().Encrypt();
            return a;
        }
        public static Admin Decrypt(this Admin a)
        {
            a.fullname = a.fullname.Decrypt();
            a.cellphone = a.cellphone.Decrypt();
            a.emailid = a.emailid.Decrypt();
            return a;
        }
        public static CriminalCase Encrypt(this CriminalCase cc)
        {
            cc.fullname = cc.fullname.ToTitleCase().Encrypt();
            cc.cellphone = cc.cellphone.Encrypt();
            cc.emailid = cc.emailid.ToLowerCase().Encrypt();
            cc.contactfullname = cc.contactfullname.Encrypt();
            cc.contacttelephone = cc.contacttelephone.Encrypt();
            cc.street = cc.street.ToTitleCase().Encrypt();
            cc.docketorindexnumber = cc.docketorindexnumber.Encrypt();
            cc.ada1 = cc.ada1.ToTitleCase().Encrypt();
            cc.adaphone1 = cc.adaphone1.Encrypt();
            cc.adaemail1 = cc.adaemail1.ToLowerCase().Encrypt();
            cc.ada2 = cc.ada2.ToTitleCase().Encrypt();
            cc.adaphone2 = cc.adaphone2.Encrypt();
            cc.adaemail2 = cc.adaemail2.ToLowerCase().Encrypt();
            cc.ada3 = cc.ada3.ToTitleCase().Encrypt();
            cc.adaphone3 = cc.adaphone3.Encrypt();
            cc.adaemail3 = cc.adaemail3.ToLowerCase().Encrypt();
            cc.ada4 = cc.ada4.ToTitleCase().Encrypt();
            cc.adaphone4 = cc.adaphone4.Encrypt();
            cc.adaemail4 = cc.adaemail4.ToLowerCase().Encrypt();
            cc.judge1 = cc.judge1.ToTitleCase().Encrypt();
            cc.room1 = cc.room1.Encrypt();
            cc.part1 = cc.part1.Encrypt();
            cc.judge2 = cc.judge2.ToTitleCase().Encrypt();
            cc.room2 = cc.room2.Encrypt();
            cc.part2 = cc.part2.Encrypt();
            cc.judge3 = cc.judge3.ToTitleCase().Encrypt();
            cc.room3 = cc.room3.Encrypt();
            cc.part3 = cc.part3.Encrypt();
            cc.judge4 = cc.judge4.ToTitleCase().Encrypt();
            cc.room4 = cc.room4.Encrypt();
            cc.part4 = cc.part4.Encrypt();
            return cc;
        }
        public static CriminalCase Decrypt(this CriminalCase cc)
        {
            cc.fullname = cc.fullname.Decrypt();
            cc.cellphone = cc.cellphone.Decrypt();
            cc.emailid = cc.emailid.Decrypt();
            cc.contactfullname = cc.contactfullname.Decrypt();
            cc.contacttelephone = cc.contacttelephone.Decrypt();
            cc.street = cc.street.Decrypt();
            cc.docketorindexnumber = cc.docketorindexnumber.Decrypt();
            cc.ada1 = cc.ada1.Decrypt();
            cc.adaphone1 = cc.adaphone1.Decrypt();
            cc.adaemail1 = cc.adaemail1.Decrypt();
            cc.ada2 = cc.ada2.Decrypt();
            cc.adaphone2 = cc.adaphone2.Decrypt();
            cc.adaemail2 = cc.adaemail2.Decrypt();
            cc.ada3 = cc.ada3.Decrypt();
            cc.adaphone3 = cc.adaphone3.Decrypt();
            cc.adaemail3 = cc.adaemail3.Decrypt();
            cc.ada4 = cc.ada4.Decrypt();
            cc.adaphone4 = cc.adaphone4.Decrypt();
            cc.adaemail4 = cc.adaemail4.Decrypt();
            cc.judge1 = cc.judge1.Decrypt();
            cc.room1 = cc.room1.Decrypt();
            cc.part1 = cc.part1.Decrypt();
            cc.judge2 = cc.judge2.Decrypt();
            cc.room2 = cc.room2.Decrypt();
            cc.part2 = cc.part2.Decrypt();
            cc.judge3 = cc.judge3.Decrypt();
            cc.room3 = cc.room3.Decrypt();
            cc.part3 = cc.part3.Decrypt();
            cc.judge4 = cc.judge4.Decrypt();
            cc.room4 = cc.room4.Decrypt();
            cc.part4 = cc.part4.Decrypt();
            return cc;
        }
        public static BankruptcyCase Encrypt(this BankruptcyCase bc)
        {
            bc.fullname = bc.fullname.ToTitleCase().Encrypt();
            bc.cellphone = bc.cellphone.Encrypt();
            bc.emailid = bc.emailid.ToLowerCase().Encrypt();
            bc.contactfullname = bc.contactfullname.Encrypt();
            bc.contacttelephone = bc.contacttelephone.Encrypt();
            bc.street = bc.street.ToTitleCase().Encrypt();
            return bc;
        }
        public static BankruptcyCase Decrypt(this BankruptcyCase bc)
        {
            bc.fullname = bc.fullname.Decrypt();
            bc.cellphone = bc.cellphone.Decrypt();
            bc.emailid = bc.emailid.Decrypt();
            bc.contactfullname = bc.contactfullname.Decrypt();
            bc.contacttelephone = bc.contacttelephone.Decrypt();
            bc.street = bc.street.Decrypt();
            return bc;
        }
        public static ImmigrationCase Encrypt(this ImmigrationCase bc)
        {
            bc.fullname = bc.fullname.ToTitleCase().Encrypt();
            bc.cellphone = bc.cellphone.Encrypt();
            bc.emailid = bc.emailid.ToLowerCase().Encrypt();
            bc.contactfullname = bc.contactfullname.Encrypt();
            bc.contacttelephone = bc.contacttelephone.Encrypt();
            bc.street = bc.street.ToTitleCase().Encrypt();
            return bc;
        }
        public static ImmigrationCase Decrypt(this ImmigrationCase bc)
        {
            bc.fullname = bc.fullname.Decrypt();
            bc.cellphone = bc.cellphone.Decrypt();
            bc.emailid = bc.emailid.Decrypt();
            bc.contactfullname = bc.contactfullname.Decrypt();
            bc.contacttelephone = bc.contacttelephone.Decrypt();
            bc.street = bc.street.Decrypt();
            return bc;
        }
        public static PersonalInjury Encrypt(this PersonalInjury bc)
        {
            bc.fullname = bc.fullname.ToTitleCase().Encrypt();
            bc.phone = bc.phone.Encrypt();
            bc.ssn = bc.ssn.Encrypt();
            bc.mobile = bc.mobile.Encrypt();
            bc.emailid = bc.emailid.ToLowerCase().Encrypt();
            bc.contactfullname = bc.contactfullname.Encrypt();
            bc.contacttelephone = bc.contacttelephone.Encrypt();
            bc.address = bc.address.ToTitleCase().Encrypt();
            return bc;
        }
        public static PersonalInjury Decrypt(this PersonalInjury bc)
        {
            bc.fullname = bc.fullname.Decrypt();
            bc.phone = bc.phone.Decrypt();
            bc.ssn = bc.ssn.Decrypt();
            bc.mobile = bc.mobile.Decrypt();
            bc.emailid = bc.emailid.Decrypt();
            bc.contactfullname = bc.contactfullname.Decrypt();
            bc.contacttelephone = bc.contacttelephone.Decrypt();
            bc.address = bc.address.Decrypt();
            return bc;
        }

        public static MasterAccount Encrypt(this MasterAccount ma)
        {
            ma.payable_to = ma.payable_to.ToTitleCase().Encrypt();
            return ma;
        }
        public static MasterAccount Decrypt(this MasterAccount ma)
        {
            ma.payable_to = ma.payable_to.Decrypt();
            return ma;
        }
        public static IEnumerable<MasterAccount> Decrypt(this IEnumerable<MasterAccount> malist)
        {
            foreach (var ma in malist)
                ma.payable_to = ma.payable_to.Decrypt();
            return malist;
        }
        public static SubAccount Encrypt(this SubAccount sa)
        {
            sa.account_holder_name = sa.account_holder_name.ToTitleCase().Encrypt();
            sa.gonylaw_ref_no = sa.gonylaw_ref_no.ToUpperCase().Encrypt();
            return sa;
        }
        public static SubAccount Decrypt(this SubAccount sa)
        {
            sa.account_holder_name = sa.account_holder_name.Decrypt();
            sa.gonylaw_ref_no = sa.gonylaw_ref_no.Decrypt();
            return sa;
        }
        public static IEnumerable<SubAccount> Decrypt(this IEnumerable<SubAccount> salist)
        {
            foreach(var sa in salist)
            {
                sa.account_holder_name = sa.account_holder_name.Decrypt();
                sa.gonylaw_ref_no = sa.gonylaw_ref_no.Decrypt();
            }
            return salist;
        }
        public static SalesPerson Encrypt(this SalesPerson a)
        {
            if (object.ReferenceEquals(a, null))
                return a;
            a.fullname = a.fullname.ToTitleCase().Encrypt();
            a.phone = a.phone.Encrypt();
            a.mobile = a.mobile.Encrypt();
            a.emailid = a.emailid.ToLowerCase().Encrypt();
            a.street = a.street.ToTitleCase().Encrypt();
            a.city = a.city.ToTitleCase().Encrypt();
            a.state = a.state.Encrypt();
            a.county = a.county.ToTitleCase().Encrypt();
            a.zip = a.zip.Encrypt();
            a.password = a.password.SHA256_Hash();
            return a;
        }
        public static SalesPerson Decrypt(this SalesPerson a)
        {
            if (object.ReferenceEquals(a, null))
                return a;
            a.fullname = a.fullname.Decrypt();
            a.phone = a.phone.Decrypt();
            a.mobile = a.mobile.Decrypt();
            a.emailid = a.emailid.Decrypt();
            a.street = a.street.Decrypt();
            a.city = a.city.Decrypt();
            a.state = a.state.Decrypt();
            a.county = a.county.Decrypt();
            a.zip = a.zip.Decrypt();
            return a;
        }
        public static PaymentConfig Encrypt(this PaymentConfig p)
        {
            if (object.ReferenceEquals(p, null))
                return p;
            p.ApiLoginID = p.ApiLoginID.Encrypt();
            p.ApiTransactionKey = p.ApiTransactionKey.Encrypt();
            return p;
        }
        public static PaymentConfig Decrypt(this PaymentConfig p)
        {
            if (object.ReferenceEquals(p, null))
                return p;
            p.ApiLoginID = p.ApiLoginID.Decrypt();
            p.ApiTransactionKey = p.ApiTransactionKey.Decrypt();
            return p;
        }
        public static DivorceCase Encrypt(this DivorceCase dc)
        {
            if (object.ReferenceEquals(dc, null))
                return dc;
            dc.p.phone = dc.p.phone.Encrypt();
            dc.p.mob = dc.p.mob.Encrypt();
            dc.p.email = dc.p.email.ToLowerCase().Encrypt();
            dc.p.ssn = dc.p.ssn.Encrypt();
            dc.d.ssn = dc.d.ssn.Encrypt();
            if (dc.casetype == "2")
            {
                dc.ch.c1_ssn = dc.ch.c1_ssn.Encrypt();
                dc.ch.c2_ssn = dc.ch.c2_ssn.Encrypt();
                dc.ch.c3_ssn = dc.ch.c3_ssn.Encrypt();
                dc.ch.c4_ssn = dc.ch.c4_ssn.Encrypt();
                dc.ch.c5_ssn = dc.ch.c5_ssn.Encrypt();
                dc.ch.c6_ssn = dc.ch.c6_ssn.Encrypt();
            }
            return dc;
        }
        public static DivorceCase Decrypt(this DivorceCase dc)
        {
            if (object.ReferenceEquals(dc, null))
                return dc;
            dc.p.phone = dc.p.phone.Decrypt();
            dc.p.mob = dc.p.mob.Decrypt();
            dc.p.email = dc.p.email.Decrypt();
            dc.p.ssn = dc.p.ssn.Decrypt();
            dc.d.ssn = dc.d.ssn.Decrypt();
            if (dc.casetype == "2")
            {
                dc.ch.c1_ssn = dc.ch.c1_ssn.Decrypt();
                dc.ch.c2_ssn = dc.ch.c2_ssn.Decrypt();
                dc.ch.c3_ssn = dc.ch.c3_ssn.Decrypt();
                dc.ch.c4_ssn = dc.ch.c4_ssn.Decrypt();
                dc.ch.c5_ssn = dc.ch.c5_ssn.Decrypt();
                dc.ch.c6_ssn = dc.ch.c6_ssn.Decrypt();
            }
            return dc;
        }
    }
}