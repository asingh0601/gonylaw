﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace GoNyLaw.Helpers
{
    public static class MinDateCorrection
    {
        static DateTime mindate = DateTime.Parse("01/01/2016");
        public static T ApplyDateCorrection<T>(this T t)
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (property.PropertyType == typeof(DateTime?) || property.PropertyType == typeof(DateTime))
                {                   
                    DateTime dt = DateTime.Parse(property.GetValue(t).ToString());
                    if (dt < mindate)
                        property.SetValue(t, mindate);
                }
            }
            return t;
        }
        public static string ApplyDateCorrection(this DateTime dt)
        {
            if (dt == null || dt <= mindate) return "____________";
            else return dt.ToString("MM/dd/yyyy");
        }
        public static string ApplyDateCorrectionBlank(this DateTime dt)
        {
            if (dt == null || dt <= mindate) return "";
            else return dt.ToString("MM/dd/yyyy");
        }
        public static string ApplyDateTimeCorrection(this DateTime dt)
        {
            DateTime testdate = DateTime.Parse("01/01/2016");
            if (dt == null || dt <= mindate)
                return "____________";
            else return dt.ToString("MM/dd/yyyy hh:mm tt");
        }
        public static bool IsMinDate(this DateTime dt)
        {
            if (dt == null || dt.Date == mindate.Date)
                return true;
            return false;
        }
        public static DateTime MinDate()
        {
            return mindate;
        }
    }
}