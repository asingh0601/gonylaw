﻿using GoNyLaw.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GoNyLaw.Helpers
{
    public class ActionLogs
    {
        public static void CriminalCase(CriminalCase cc, string type, string recepient, string action, string details, string[] path)
        {
            using (LogContext db = new LogContext())
            {
                string name = "";
                if (LoginPersistence.CheckAdmin())
                {
                    int id = LoginPersistence.getadminid();
                    name = " - " + new UserContext().Admins.Find(id).fullname.Decrypt();
                }
                else if (LoginPersistence.CheckSuperAdmin())
                {
                    int id = LoginPersistence.getsuperadminid();
                    name = " - " + new UserContext().Admins.Find(id).fullname.Decrypt();
                }
                else
                {
                    name = "";
                }
                CriminalLog cl = new CriminalLog();
                cl.refno = cc.refno;
                cl.adminsassigned = cc.adminsassigned;
                cl.dateaction = DateTime.Now;
                cl.type = type;
                cl.details = details.RemoveHTMLTags() + name;
                cl.action = action;
                cl.recepient = recepient;
                if (path != null)
                {
                    if (!string.IsNullOrEmpty(path[0]))
                    {
                        cl.filename1 = Path.GetFileName(path[0]);
                    }
                    if (!string.IsNullOrEmpty(path[1]))
                    {
                        cl.filename2 = Path.GetFileName(path[1]);
                    }
                    if (!string.IsNullOrEmpty(path[2]))
                    {
                        cl.filename3 = Path.GetFileName(path[2]);
                    }
                    if (!string.IsNullOrEmpty(path[3]))
                    {
                        cl.filename4 = Path.GetFileName(path[3]);
                    }
                    if (!string.IsNullOrEmpty(path[4]))
                    {
                        cl.filename5 = Path.GetFileName(path[4]);
                    }
                }
                db.CriminalLogs.Add(cl);
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
            }
        }
        public static void BankruptcyCase(BankruptcyCase cc, string type, string recepient, string action, string details, string[] path)
        {
            using (LogContext db = new LogContext())
            {
                string name = "";
                if (LoginPersistence.CheckAdmin())
                {
                    int id = LoginPersistence.getadminid();
                    name = new UserContext().Admins.Find(id).fullname.Decrypt();
                }
                else if (LoginPersistence.CheckSuperAdmin())
                {
                    int id = LoginPersistence.getsuperadminid();
                    name = new UserContext().Admins.Find(id).fullname.Decrypt();
                }
                BankruptcyLog cl = new BankruptcyLog();
                cl.refno = cc.refno;
                cl.adminsassigned = cc.adminsassigned;
                cl.dateaction = DateTime.Now;
                cl.type = type;
                cl.details = details.RemoveHTMLTags() + " - " + name;
                cl.action = action;
                cl.recepient = recepient;
                if (path != null)
                {
                    if (!string.IsNullOrEmpty(path[0]))
                    {
                        cl.filename1 = Path.GetFileName(path[0]);
                    }
                    if (!string.IsNullOrEmpty(path[1]))
                    {
                        cl.filename2 = Path.GetFileName(path[1]);
                    }
                    if (!string.IsNullOrEmpty(path[2]))
                    {
                        cl.filename3 = Path.GetFileName(path[2]);
                    }
                    if (!string.IsNullOrEmpty(path[3]))
                    {
                        cl.filename4 = Path.GetFileName(path[3]);
                    }
                    if (!string.IsNullOrEmpty(path[4]))
                    {
                        cl.filename5 = Path.GetFileName(path[4]);
                    }
                }
                db.BankruptcyLogs.Add(cl);
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
            }
        }
        public static void ImmigrationCase(ImmigrationCase cc, string type, string recepient, string action, string details, string[] path)
        {
            using (LogContext db = new LogContext())
            {
                string name = "";
                if (LoginPersistence.CheckAdmin())
                {
                    int id = LoginPersistence.getadminid();
                    name = new UserContext().Admins.Find(id).fullname.Decrypt();
                }
                else if (LoginPersistence.CheckSuperAdmin())
                {
                    int id = LoginPersistence.getsuperadminid();
                    name = new UserContext().Admins.Find(id).fullname.Decrypt();
                }
                ImmigrationLog cl = new ImmigrationLog();
                cl.refno = cc.refno;
                cl.adminsassigned = cc.adminsassigned;
                cl.dateaction = DateTime.Now;
                cl.type = type;
                cl.details = details.RemoveHTMLTags() + " - " + name;
                cl.action = action;
                cl.recepient = recepient;
                if (path != null)
                {
                    if (!string.IsNullOrEmpty(path[0]))
                    {
                        cl.filename1 = Path.GetFileName(path[0]);
                    }
                    if (!string.IsNullOrEmpty(path[1]))
                    {
                        cl.filename2 = Path.GetFileName(path[1]);
                    }
                    if (!string.IsNullOrEmpty(path[2]))
                    {
                        cl.filename3 = Path.GetFileName(path[2]);
                    }
                    if (!string.IsNullOrEmpty(path[3]))
                    {
                        cl.filename4 = Path.GetFileName(path[3]);
                    }
                    if (!string.IsNullOrEmpty(path[4]))
                    {
                        cl.filename5 = Path.GetFileName(path[4]);
                    }
                }
                db.ImmigrationLogs.Add(cl);
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
            }
        }
        public static void PersonalInjury(PersonalInjury cc, string type, string recepient, string action, string details, string[] path)
        {
            using (LogContext db = new LogContext())
            {
                string name = "";
                if (LoginPersistence.CheckAdmin())
                {
                    int id = LoginPersistence.getadminid();
                    name = " - " + new UserContext().Admins.Find(id).fullname.Decrypt();
                }
                else if (LoginPersistence.CheckSuperAdmin())
                {
                    int id = LoginPersistence.getsuperadminid();
                    name = " - " + new UserContext().Admins.Find(id).fullname.Decrypt();
                }
                else
                {
                    name = "";
                }
                CriminalLog cl = new CriminalLog();
                cl.refno = cc.refno;
                cl.adminsassigned = cc.adminsassigned;
                cl.dateaction = DateTime.Now;
                cl.type = type;
                cl.details = details.RemoveHTMLTags() + name;
                cl.action = action;
                cl.recepient = recepient;
                if (path != null)
                {
                    if (!string.IsNullOrEmpty(path[0]))
                    {
                        cl.filename1 = Path.GetFileName(path[0]);
                    }
                    if (!string.IsNullOrEmpty(path[1]))
                    {
                        cl.filename2 = Path.GetFileName(path[1]);
                    }
                    if (!string.IsNullOrEmpty(path[2]))
                    {
                        cl.filename3 = Path.GetFileName(path[2]);
                    }
                    if (!string.IsNullOrEmpty(path[3]))
                    {
                        cl.filename4 = Path.GetFileName(path[3]);
                    }
                    if (!string.IsNullOrEmpty(path[4]))
                    {
                        cl.filename5 = Path.GetFileName(path[4]);
                    }
                }
                db.CriminalLogs.Add(cl);
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
            }
        }
    }
}