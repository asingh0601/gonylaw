﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoNyLaw.Models;
using System.Diagnostics;

namespace GoNyLaw.Helpers
{
    public static class Notifications
    {
        public static void AdminOTP(Admin a)
        {
            string smstext = "";
            if (a.langpref == "S")
            {
                //Correction Required
                smstext = "GoNyLaw - Estimado Administrador: Su Pin de Acceso Único es " + a.otp.Decrypt();
            }
            else
            {
                smstext = "GoNyLaw - Dear Admin, Your One Time Login Pin is " + a.otp.Decrypt();
            }
            SMS.SendSMS(smstext, a.cellphone.Decrypt());
        }
        public static void UserRegistration(Client c)
        {
            string smstext = "", emailsubject = "", emailtext = "";
            if (c.initialinterest == "Bankruptcy")
            {
                if (c.langpref == "S")
                {
                    smstext = "Gracias por eligir la Oficina Del Abogado Heriberto Cabrera para ayudar con su bancarrota. Si tienes prguntas favor de llamar 718-439-3600. Su pin es " + c.otp.Decrypt();
                    emailsubject = "Registro exitoso";
                    emailtext = "Gracias por selecionar la Oficina Del Abogado Heriberto Cabrera para asistirle con su peticion de bancarrota. Si usded tiene algunas preguntas favor de llamanos al teléfono 718-439-3600. Su numero de pin es " + c.otp.Decrypt() + ". Para seguir su caso por internet favor de ir a <a href='https://www.gonylaw.com'>gonylaw.com</a>.</b>" + Footer();
                }
                else
                {
                    smstext = "Thank you for selecting Law Office of Heribero Cabrera to assist with your bankruptcy. Call us @ 718-439-3600 with questions. Unique pin " + c.otp.Decrypt();
                    emailsubject = "Registration Successful";
                    emailtext = "Thank you for selecting the Law Office of Heribero Cabrera to assist you with your bankruptcy petition. If you have any questions pertaining to your case please feel free to call us at 718-439-3600. Your unique pin is " + c.otp.Decrypt() + ". Please sign on to <a href='https://www.gonylaw.com'>gonylaw.com</a> to follow the progress of your case." + Footer();
                }
            }
            if (c.initialinterest == "Criminal")
            {
                if (c.langpref == "S")
                {
                    smstext = "Gracias por eligir la Oficina Del Abogado Heriberto Cabrera para ayudar con su Criminal caso. Si tienes prguntas favor de llamar 718-439-3600. Su pin es " + c.otp.Decrypt();
                    emailsubject = "Registro exitoso";
                    emailtext = "Gracias por selecionar la Oficina Del Abogado Heriberto Cabrera para asistirle con su caso de criminal. Si usded tiene algunas preguntas favor de llamanos al teléfono 718-439-3600. Su numero de pin es " + c.otp.Decrypt() + ". Para seguir su caso por internet favor de ir a <a href='https://www.gonylaw.com'>gonylaw.com</a>.</b>" + Footer();
                }
                else
                {
                    smstext = "Thank you for selecting Law Office of Heribero Cabrera to assist with your Criminal Case. Call us @ 718-439-3600 with questions. Unique pin " + c.otp.Decrypt();
                    emailsubject = "Registration Successful";
                    emailtext = "Thank you for selecting the Law Office of Heribero Cabrera to assist you with your Criminal Case. If you have any questions pertaining to your case please feel free to call us at 718-439-3600. Your unique pin is " + c.otp.Decrypt() + ". Please sign on to <a href='https://www.gonylaw.com'>gonylaw.com</a> to follow the progress of your case." + Footer();
                }
            }
            if (c.initialinterest == "Immigration")
            {
                if (c.langpref == "S")
                {
                    smstext = "Gracias por eligir la Oficina Del Abogado Heriberto Cabrera para ayudar con su Immigracion. Si tienes prguntas favor de llamar 718-439-3600. Su pin es " + c.otp.Decrypt();
                    emailsubject = "Registro exitoso";
                    emailtext = "Gracias por selecionar la Oficina Del Abogado Heriberto Cabrera para asistirle con su caso de immigracion. Si usded tiene algunas preguntas favor de llamanos al teléfono 718-439-3600. Su numero de pin es " + c.otp.Decrypt() + ". Para seguir su caso por internet favor de ir a <a href='https://www.gonylaw.com'>gonylaw.com</a>.</b>" + Footer();
                }
                else
                {
                    smstext = "Thank you for selecting Law Office of Heribero Cabrera to assist with your Immigration. Call us @ 718-439-3600 with questions. Unique pin " + c.otp.Decrypt();
                    emailsubject = "Registration Successful";
                    emailtext = "Thank you for selecting the Law Office of Heribero Cabrera to assist you with your Immigration. If you have any questions pertaining to your case please feel free to call us at 718-439-3600. Your unique pin is " + c.otp.Decrypt() + ". Please sign on to <a href='https://www.gonylaw.com'>gonylaw.com</a> to follow the progress of your case." + Footer();
                }
            }
            SMS.SendSMS(smstext, c.cellphone.Decrypt());
            if (!string.IsNullOrEmpty(c.emailid))
                Email.SendEmail(c.emailid.Decrypt(), emailsubject, emailtext, null);
        }
        public static void CriminalCaseEmail(string refno, string recepients, string subject, string body, string[] path)
        {
            string clientemail = "", adaemail = "";
            string[] recp = recepients.Split(',');
            CaseContext db = new CaseContext();
            CriminalCase cc = db.CriminalCases.Find(refno);
            Client c = new UserContext().Clients.Find(cc.clientid);
            clientemail = c.emailid.Decrypt();
            if (!string.IsNullOrEmpty(cc.adaemail4))
                adaemail = cc.adaemail4.Decrypt();
            else if (!string.IsNullOrEmpty(cc.adaemail3))
                adaemail = cc.adaemail3.Decrypt();
            else if (!string.IsNullOrEmpty(cc.adaemail2))
                adaemail = cc.adaemail2.Decrypt();
            else
                adaemail = cc.adaemail1.Decrypt();
            string bodywithoutfooter = body;
            body = body + Footer();
            string r = "";
            if (recp.Contains("1"))
            {
                Email.SendEmail(clientemail, subject, body, path);
                r = "Client";
            }
            if (recp.Contains("2"))
            {
                if (!string.IsNullOrEmpty(r))
                    r += ", ADA";
                else
                    r = "ADA";
                Email.SendEmail(adaemail, subject, body, path);
            }
            ActionLogs.CriminalCase(cc, "E", r, subject, bodywithoutfooter, path);
        }
        public static void CriminalCaseSMS(string refno, string body)
        {
            CaseContext db = new CaseContext();
            CriminalCase cc = db.CriminalCases.Find(refno);
            SMS.SendSMS(body, cc.cellphone.Decrypt());
            ActionLogs.CriminalCase(cc, "S", "Client", "SMS Sent", body, null);
        }
        public static void BankruptcyCaseEmail(string refno, string recepients, string subject, string body, string[] path)
        {
            string clientemail = "";
            string[] recp = recepients.Split(',');
            CaseContext db = new CaseContext();
            BankruptcyCase cc = db.BankruptcyCases.Find(refno);
            Client c = new UserContext().Clients.Find(cc.clientid);
            clientemail = c.emailid.Decrypt();
            string bodywithoutfooter = body;
            body = body + Footer();
            string r = "";
            if (recp.Contains("1"))
            {
                Email.SendEmail(clientemail, subject, body, path);
                r = "Client";
            }
            ActionLogs.BankruptcyCase(cc, "E", r, subject, bodywithoutfooter, path);
        }
        public static void BankruptcyCaseSMS(string refno, string body)
        {
            CaseContext db = new CaseContext();
            BankruptcyCase cc = db.BankruptcyCases.Find(refno);
            SMS.SendSMS(body, cc.cellphone.Decrypt());
            ActionLogs.BankruptcyCase(cc, "S", "Client", "NA", body, null);
        }
        public static void ImmigrationCaseEmail(string refno, string recepients, string subject, string body, string[] path)
        {
            string clientemail = "";
            string[] recp = recepients.Split(',');
            CaseContext db = new CaseContext();
            ImmigrationCase cc = db.ImmigrationCases.Find(refno);
            Client c = new UserContext().Clients.Find(cc.clientid);
            clientemail = c.emailid.Decrypt();
            string bodywithoutfooter = body;
            body = body + Footer();
            string r = "";
            if (recp.Contains("1"))
            {
                Email.SendEmail(clientemail, subject, body, path);
                r = "Client";
            }
            ActionLogs.ImmigrationCase(cc, "E", r, subject, bodywithoutfooter, path);
        }
        public static void ImmigrationCaseSMS(string refno, string body)
        {
            CaseContext db = new CaseContext();
            ImmigrationCase cc = db.ImmigrationCases.Find(refno);
            SMS.SendSMS(body, cc.cellphone.Decrypt());
            ActionLogs.ImmigrationCase(cc, "S", "Client", "NA", body, null);
        }
        public static void CriminalTaskAssigned(CriminalTask ct)
        {
            CaseContext db = new CaseContext();
            CriminalCase cc = db.CriminalCases.Find(ct.refno);
            string color = "";
            if ((ct.daterequired - DateTime.Now).TotalDays > 30)
            {
                color = "Green";
            }
            else if ((ct.daterequired - DateTime.Now).TotalDays > 15)
            {
                color = "Yellow";
            }
            else if ((ct.daterequired - DateTime.Now).TotalDays > 7)
            {
                color = "Orange";
            }
            else if ((ct.daterequired - DateTime.Now).TotalDays > 3)
            {
                color = "Pink";
            }
            else
            {
                color = "Red";
            }
            string smsbody = "", emailbody = "";
            smsbody = color + " - " + cc.fullname.Decrypt() + "(" + cc.refno + ")" + "-" + ct.daterequired.ApplyDateCorrection() + " Check your Email";
            emailbody = color + " - " + cc.fullname.Decrypt() + "(" + cc.refno + ")" + "-" + ct.daterequired.ApplyDateCorrection() + ct.task;
            string[] ad = ct.adminsassigned.Split(',');
            string admins = "";
            int id = 0;
            for (int i = 0; i < ad.Length; i++)
            {
                Int32.TryParse(ad[i], out id);
                Admin a = new UserContext().Admins.Find(id).Decrypt();
                if (!string.IsNullOrEmpty(admins))
                    admins = admins + ", " + a.fullname;
                else
                    admins += a.fullname;
                SMS.SendSMS(smsbody, a.cellphone);
                Email.SendEmail(a.emailid, "Task Assigned", emailbody, null);
            }
            ActionLogs.CriminalCase(cc, "T", admins, "Task Alert", smsbody, null);
        }
        public static void CriminalTaskStatusChanged(CriminalCase cc)
        {
            DateTime dt = DateTime.Parse("01/01/2016");
            TaskContext db = new TaskContext();
            IEnumerable<CriminalTask> ctlist = db.CriminalTasks.Where(x => x.refno == cc.refno && x.datecompleted <= dt);
            string color = "";
            foreach (var ct in ctlist)
            {
                if ((ct.daterequired - DateTime.Now).TotalDays > 30)
                {
                    color = "Green";
                }
                else if ((ct.daterequired - DateTime.Now).TotalDays == 30)
                {
                    color = "Yellow";
                }
                else if ((ct.daterequired - DateTime.Now).TotalDays == 15)
                {
                    color = "Orange";
                }
                else if ((ct.daterequired - DateTime.Now).TotalDays == 7)
                {
                    color = "Pink";
                }
                else if ((ct.daterequired - DateTime.Now).TotalDays <= 3)
                {
                    color = "Red";
                }
                string smsbody = "", emailbody = "";
                smsbody = color + " - " + cc.fullname.Decrypt() + "(" + cc.refno + ")" + "-" + ct.daterequired.ApplyDateCorrection() + " Check your Email";
                emailbody = color + " - " + cc.fullname.Decrypt() + "(" + cc.refno + ")" + "-" + ct.daterequired.ApplyDateCorrection() + ct.task;
                string[] ad = ct.adminsassigned.Split(',');
                string admins = "";
                int id = 0;
                for (int i = 0; i < ad.Length; i++)
                {
                    Int32.TryParse(ad[i], out id);
                    Admin a = new UserContext().Admins.Find(id).Decrypt();
                    if (!string.IsNullOrEmpty(admins))
                        admins = admins + ", " + a.fullname;
                    else
                        admins += a.fullname;
                    if (DateTime.Now.Hour == 16 && color == "Red")
                    {
                        SMS.SendSMS(smsbody, a.cellphone);
                        Email.SendEmail(a.emailid, "Task Pending", emailbody, null);
                        ActionLogs.CriminalCase(cc, "T", admins, "Task Pending", smsbody, null);
                    }
                    else
                    {
                        SMS.SendSMS(smsbody, a.cellphone);
                        Email.SendEmail(a.emailid, "Task Pending", emailbody, null);
                        ActionLogs.CriminalCase(cc, "T", admins, "Task Pending", smsbody, null);
                        if ((ct.daterequired - DateTime.Now).TotalDays == 3)
                        {
                            Admin sa = new UserContext().Admins.Find(1);
                            SMS.SendSMS(smsbody, sa.cellphone.Decrypt());
                            Email.SendEmail(sa.emailid.Decrypt(), "Task Pending", emailbody, null);
                        }
                    }
                }
            }
        }
        public static void CriminalTaskComplete(CriminalTask ct)
        {
            CaseContext db = new CaseContext();
            CriminalCase cc = db.CriminalCases.Find(ct.refno);
            Admin sa = new UserContext().Admins.Find(1);
            SMS.SendSMS("Task " + ct.task + " for " + cc.fullname.Decrypt() + "(" + cc.refno + ") is complete.", sa.cellphone.Decrypt());
            Email.SendEmail(sa.emailid.Decrypt(), "Task Completed", "Task " + ct.task + " for " + cc.fullname.Decrypt() + "(" + cc.refno + ") is complete. Comments - " + ct.notes, null);
        }
        public static void CriminalMotions(CriminalCase cc)
        {
            if (DateTime.Now.Hour != 8)
                return;
            int defflag = 0, ptrflag = 0, decflag = 0;
            if ((cc.motion - DateTime.Now).TotalDays <= 3 && cc.motion >= DateTime.Today)
            {
                defflag = 1;
            }
            string smsbody = "", emailbody = "";
            smsbody = " motion is due for " + cc.fullname.Decrypt() + "(" + cc.refno + ")";
            emailbody = " motion is due for " + cc.fullname.Decrypt() + "(" + cc.refno + ")";
            string smsbody1 = "", emailbody1 = "";
            Admin sa = new UserContext().Admins.Find(1);
            string[] ad = cc.adminsassigned.Split(',');
            string admins = "";
            int id = 0;
            for (int i = 0; i < ad.Length; i++)
            {
                Int32.TryParse(ad[i], out id);
                Admin a = new UserContext().Admins.Find(id).Decrypt();
                if (!string.IsNullOrEmpty(admins))
                    admins = admins + ", " + a.fullname;
                else
                    admins += a.fullname;
                if (defflag == 1)
                {
                    smsbody1 = "Def." + smsbody;
                    emailbody1 = "Def." + emailbody;
                    SMS.SendSMS(smsbody1, a.cellphone);
                    Email.SendEmail(a.emailid, "Def. Motion Alert", emailbody1, null);
                }
            }
            if (defflag == 1)
            {
                smsbody1 = "Def." + smsbody;
                emailbody1 = "Def." + emailbody;
                ActionLogs.CriminalCase(cc, "M", admins, "Def Motion", smsbody1, null);
                SMS.SendSMS(smsbody1, sa.cellphone.Decrypt());
                Email.SendEmail(sa.emailid.Decrypt(), "Def. Motion Alert", emailbody1, null);
            }
        }
        public static void CriminalCourtDateEntered(CriminalCase cc, int flag = 0)
        {
            if (DateTime.Now.Hour != 8)
                return;
            Client c = new UserContext().Clients.Find(cc.clientid);
            CriminalAction ca = new ActionContext().CriminalActions.Where(x => x.refno == cc.refno).OrderByDescending(x => x.dateaction).FirstOrDefault();
            if (Object.ReferenceEquals(ca, null) || Object.ReferenceEquals(c, null))
                return;
            string smsbody = "", emailbody = "";
            if (c.langpref == "S")
            {
                smsbody = cc.fullname + " su proxima cita de corte es " + ca.nextcourtdate.ApplyDateCorrection() + " " + cc.county.ToCountyName() + " County " + cc.court.ToCourtName() + " Part " + ca.part + " sala " + ca.room + " a las " + ca.nextcourtdate.ToString("hh:mm tt") + ". Por favor de llegar 30 minutos antes del tiempo pautado.";
                emailbody = "Querida " + cc.fullname + ",<br/><br/> Su proxima cita de corte es para " + ca.nextcourtdate.ApplyDateCorrection() + " en " + cc.county.ToCountyName() + " County " + cc.court.ToCourtName() + "Corte, Parte " + ca.part + ", sala " + ca.room + " a las " + ca.nextcourtdate.ToString("hh:mm tt") + ". Por favor de llegar 30 minutos antes del tiempo pautado." + Footer();
            }
            else
            {
                smsbody = cc.fullname + " next court date " + ca.nextcourtdate.ApplyDateCorrection() + " in " + cc.county.ToCountyName() + " County " + cc.court.ToCourtName() + " Part " + ca.part + " Room " + ca.room + " at " + ca.nextcourtdate.ToString("hh:mm tt") + ". Please arrive at least 30 minutes ahead of scheduled.";
                emailbody = "Dear " + cc.fullname + ",<br/><br/> Your next court date is on " + ca.nextcourtdate.ApplyDateCorrection() + " in " + cc.county.ToCountyName() + " County " + cc.court.ToCourtName() + " Part " + ca.part + " Room " + ca.room + " at " + ca.nextcourtdate.ToString("hh:mm tt") + ". Please arrive at least 30 minutes ahead of scheduled." + Footer();
            }
            string asmsbody = cc.fullname + " Next court date " + ca.nextcourtdate.ApplyDateCorrection() + " in " + cc.county.ToCountyName() + " County " + cc.court.ToCourtName() + " Part " + ca.part + " Room " + ca.room + " at " + ca.nextcourtdate.ToString("hh:mm tt") + ". Please arrive at least 30 minutes ahead of scheduled.";
            string aemailbody = cc.fullname + "Next court date is on " + ca.nextcourtdate.ApplyDateCorrection() + " in " + cc.county.ToCountyName() + " County " + cc.court.ToCourtName() + " Part " + ca.part + " Room " + ca.room + " at " + ca.nextcourtdate.ToString("hh:mm tt") + ". Please arrive at least 30 minutes ahead of scheduled." + Footer();
            Admin sa = new UserContext().Admins.Find(1);
            if (flag == 0 || (ca.nextcourtdate - DateTime.Now).TotalDays == 7 || (ca.nextcourtdate - DateTime.Now).TotalDays == 0)
            {
                SMS.SendSMS(smsbody, sa.cellphone.Decrypt());
                Email.SendEmail(sa.emailid.Decrypt(), "CourtDate Alert", emailbody, null);
                ActionLogs.CriminalCase(cc, "A", "Client", "CourtDate Alert", smsbody, null);
                SMS.SendSMS(asmsbody, sa.cellphone.Decrypt());
                Email.SendEmail(sa.emailid.Decrypt(), "CourtDate Alert", aemailbody, null);
            }
        }
        public static void BankruptcyTaskAssigned(BankruptcyTask ct)
        {
            CaseContext db = new CaseContext();
            BankruptcyCase cc = db.BankruptcyCases.Find(ct.refno);
            string color = "";
            if ((ct.daterequired - DateTime.Now).TotalDays > 30)
            {
                color = "Green";
            }
            else if ((ct.daterequired - DateTime.Now).TotalDays > 15)
            {
                color = "Yellow";
            }
            else if ((ct.daterequired - DateTime.Now).TotalDays > 7)
            {
                color = "Orange";
            }
            else if ((ct.daterequired - DateTime.Now).TotalDays > 3)
            {
                color = "Pink";
            }
            else
            {
                color = "Red";
            }
            string smsbody = "", emailbody = "";
            smsbody = color + " - " + cc.fullname.Decrypt() + "(" + cc.refno + ")" + "-" + ct.daterequired.ApplyDateCorrection() + " Check your Email";
            emailbody = color + " - " + cc.fullname.Decrypt() + "(" + cc.refno + ")" + "-" + ct.daterequired.ApplyDateCorrection() + ct.task;
            string[] ad = ct.adminsassigned.Split(',');
            string admins = "";
            int id = 0;
            for (int i = 0; i < ad.Length; i++)
            {
                Int32.TryParse(ad[i], out id);
                Admin a = new UserContext().Admins.Find(id).Decrypt();
                if (!string.IsNullOrEmpty(admins))
                    admins = admins + ", " + a.fullname;
                else
                    admins += a.fullname;
                SMS.SendSMS(smsbody, a.cellphone);
                Email.SendEmail(a.emailid, "Task Assigned", emailbody, null);
            }
            ActionLogs.BankruptcyCase(cc, "T", admins, "Task Alert", smsbody, null);
        }
        public static void BankruptcyTaskStatusChanged(BankruptcyCase cc)
        {
            DateTime dt = DateTime.Parse("01/01/2016");
            TaskContext db = new TaskContext();
            IEnumerable<BankruptcyTask> ctlist = db.BankruptcyTasks.Where(x => x.refno == cc.refno && x.datecompleted <= dt);
            string color = "";
            foreach (var ct in ctlist)
            {
                if ((ct.daterequired - DateTime.Now).TotalDays > 30)
                {
                    color = "Green";
                }
                else if ((ct.daterequired - DateTime.Now).TotalDays == 30)
                {
                    color = "Yellow";
                }
                else if ((ct.daterequired - DateTime.Now).TotalDays == 15)
                {
                    color = "Orange";
                }
                else if ((ct.daterequired - DateTime.Now).TotalDays == 7)
                {
                    color = "Pink";
                }
                else if ((ct.daterequired - DateTime.Now).TotalDays <= 3)
                {
                    color = "Red";
                }
                string smsbody = "", emailbody = "";
                smsbody = color + " - " + cc.fullname.Decrypt() + "(" + cc.refno + ")" + "-" + ct.daterequired.ApplyDateCorrection() + " Check your Email";
                emailbody = color + " - " + cc.fullname.Decrypt() + "(" + cc.refno + ")" + "-" + ct.daterequired.ApplyDateCorrection() + ct.task;
                string[] ad = ct.adminsassigned.Split(',');
                string admins = "";
                int id = 0;
                for (int i = 0; i < ad.Length; i++)
                {
                    Int32.TryParse(ad[i], out id);
                    Admin a = new UserContext().Admins.Find(id).Decrypt();
                    if (!string.IsNullOrEmpty(admins))
                        admins = admins + ", " + a.fullname;
                    else
                        admins += a.fullname;
                    if (DateTime.Now.Hour == 16 && color == "Red")
                    {
                        SMS.SendSMS(smsbody, a.cellphone);
                        Email.SendEmail(a.emailid, "Task Pending", emailbody, null);
                        ActionLogs.BankruptcyCase(cc, "T", admins, "Task Pending", smsbody, null);
                    }
                    else
                    {
                        SMS.SendSMS(smsbody, a.cellphone);
                        Email.SendEmail(a.emailid, "Task Pending", emailbody, null);
                        ActionLogs.BankruptcyCase(cc, "T", admins, "Task Pending", smsbody, null);
                        if ((ct.daterequired - DateTime.Now).TotalDays == 3)
                        {
                            Admin sa = new UserContext().Admins.Find(1);
                            SMS.SendSMS(smsbody, sa.cellphone.Decrypt());
                            Email.SendEmail(sa.emailid.Decrypt(), "Task Pending", emailbody, null);
                        }
                    }
                }
            }
        }
        public static void BankruptcyTaskComplete(BankruptcyTask ct)
        {
            CaseContext db = new CaseContext();
            BankruptcyCase cc = db.BankruptcyCases.Find(ct.refno);
            Admin sa = new UserContext().Admins.Find(1);
            SMS.SendSMS("Task " + ct.task + " for " + cc.fullname.Decrypt() + "(" + cc.refno + ") is complete.", sa.cellphone.Decrypt());
            Email.SendEmail(sa.emailid.Decrypt(), "Task Completed", "Task " + ct.task + " for " + cc.fullname.Decrypt() + "(" + cc.refno + ") is complete. Comments - " + ct.notes, null);
        }
        public static void ImmigrationTaskAssigned(ImmigrationTask ct)
        {
            CaseContext db = new CaseContext();
            ImmigrationCase cc = db.ImmigrationCases.Find(ct.refno);
            string color = "";
            if ((ct.daterequired - DateTime.Now).TotalDays > 30)
            {
                color = "Green";
            }
            else if ((ct.daterequired - DateTime.Now).TotalDays > 15)
            {
                color = "Yellow";
            }
            else if ((ct.daterequired - DateTime.Now).TotalDays > 7)
            {
                color = "Orange";
            }
            else if ((ct.daterequired - DateTime.Now).TotalDays > 3)
            {
                color = "Pink";
            }
            else
            {
                color = "Red";
            }
            string smsbody = "", emailbody = "";
            smsbody = color + " - " + cc.fullname.Decrypt() + "(" + cc.refno + ")" + "-" + ct.daterequired.ApplyDateCorrection() + " Check your Email";
            emailbody = color + " - " + cc.fullname.Decrypt() + "(" + cc.refno + ")" + "-" + ct.daterequired.ApplyDateCorrection() + ct.task;
            string[] ad = ct.adminsassigned.Split(',');
            string admins = "";
            int id = 0;
            for (int i = 0; i < ad.Length; i++)
            {
                Int32.TryParse(ad[i], out id);
                Admin a = new UserContext().Admins.Find(id).Decrypt();
                if (!string.IsNullOrEmpty(admins))
                    admins = admins + ", " + a.fullname;
                else
                    admins += a.fullname;
                SMS.SendSMS(smsbody, a.cellphone);
                Email.SendEmail(a.emailid, "Task Assigned", emailbody, null);
            }
            ActionLogs.ImmigrationCase(cc, "T", admins, "Task Alert", smsbody, null);
        }
        public static void ImmigrationTaskStatusChanged(ImmigrationCase cc)
        {
            DateTime dt = DateTime.Parse("01/01/2016");
            TaskContext db = new TaskContext();
            IEnumerable<ImmigrationTask> ctlist = db.ImmigrationTasks.Where(x => x.refno == cc.refno && x.datecompleted <= dt);
            string color = "";
            foreach (var ct in ctlist)
            {
                if ((ct.daterequired - DateTime.Now).TotalDays > 30)
                {
                    color = "Green";
                }
                else if ((ct.daterequired - DateTime.Now).TotalDays == 30)
                {
                    color = "Yellow";
                }
                else if ((ct.daterequired - DateTime.Now).TotalDays == 15)
                {
                    color = "Orange";
                }
                else if ((ct.daterequired - DateTime.Now).TotalDays == 7)
                {
                    color = "Pink";
                }
                else if ((ct.daterequired - DateTime.Now).TotalDays <= 3)
                {
                    color = "Red";
                }
                string smsbody = "", emailbody = "";
                smsbody = color + " - " + cc.fullname.Decrypt() + "(" + cc.refno + ")" + "-" + ct.daterequired.ApplyDateCorrection() + " Check your Email";
                emailbody = color + " - " + cc.fullname.Decrypt() + "(" + cc.refno + ")" + "-" + ct.daterequired.ApplyDateCorrection() + ct.task;
                string[] ad = ct.adminsassigned.Split(',');
                string admins = "";
                int id = 0;
                for (int i = 0; i < ad.Length; i++)
                {
                    Int32.TryParse(ad[i], out id);
                    Admin a = new UserContext().Admins.Find(id).Decrypt();
                    if (!string.IsNullOrEmpty(admins))
                        admins = admins + ", " + a.fullname;
                    else
                        admins += a.fullname;
                    if (DateTime.Now.Hour == 16 && color == "Red")
                    {
                        SMS.SendSMS(smsbody, a.cellphone);
                        Email.SendEmail(a.emailid, "Task Pending", emailbody, null);
                        ActionLogs.ImmigrationCase(cc, "T", admins, "Task Pending", smsbody, null);
                    }
                    else
                    {
                        SMS.SendSMS(smsbody, a.cellphone);
                        Email.SendEmail(a.emailid, "Task Pending", emailbody, null);
                        ActionLogs.ImmigrationCase(cc, "T", admins, "Task Pending", smsbody, null);
                        if ((ct.daterequired - DateTime.Now).TotalDays == 3)
                        {
                            Admin sa = new UserContext().Admins.Find(1);
                            SMS.SendSMS(smsbody, sa.cellphone.Decrypt());
                            Email.SendEmail(sa.emailid.Decrypt(), "Task Pending", emailbody, null);
                        }
                    }
                }
            }
        }
        public static void ImmigrationTaskComplete(ImmigrationTask ct)
        {
            CaseContext db = new CaseContext();
            ImmigrationCase cc = db.ImmigrationCases.Find(ct.refno);
            Admin sa = new UserContext().Admins.Find(1);
            SMS.SendSMS("Task " + ct.task + " for " + cc.fullname.Decrypt() + "(" + cc.refno + ") is complete.", sa.cellphone.Decrypt());
            Email.SendEmail(sa.emailid.Decrypt(), "Task Completed", "Task " + ct.task + " for " + cc.fullname.Decrypt() + "(" + cc.refno + ") is complete. Comments - " + ct.notes, null);
        }
        private static string Footer(int flag = 1)
        {
            string greet = "";
            if (flag == 0)
                greet = "Gracias";
            else
                greet = "Thank You";
            return "<br/><br/><br/><p><span style=\"font-size: 10pt;\">" + greet + ", </span></p>" +
                   "<p><span style=\"font-size: 10pt;\">Heriberto A. Cabrera, Esq. </span></p>" +
                   "<p><span style=\"font-size: 10pt;\"><a href=\"tel:718-439-3600\">718-439-3600</a></span></p>" +
                   "<p><span style=\"font-size: 10pt;\"><strong> P.S. Please note our <span style=\"color: #ff0000;\">NEW ADDRESS</span> below; phone/fax/email remain the</strong></span></p>" +
                   "<p><span style=\"color: #3366ff; font-size: 10pt;\"><em>Heriberto Cabrera & Associates • Attorneys at Law</em></span></p>" +
                   "<p><span style=\"font-size: 10pt;\"><a href=\"mailto:info@gonylaw.com\">info@gonylaw.com</a> • <a href=\"http://www.gonylaw.com\">www.gonylaw.com </a></span></p>" +
                   "<p><span style=\"font-size: 10pt;\"><span style=\"color: #ff0000;\"><strong>480 39th St., 2nd Fl</strong></span>, <span style=\"color: #3366ff;\">Brooklyn, NY 11232</span> </span></p>" +
                   "<p><span style=\"color: #3366ff; font-size: 10pt;\">P (718) 439-3600 • F (718) 439-1452 </span></p>" +
                   "<p><span style=\"color: #3366ff; font-size: 10pt;\">Immigration • Commercial • Criminal • Personal Injury • Accidents • Divorce • Real Estate • Bankruptcy </span></p>" +
                   "<p><span style=\"color: #3366ff; font-size: 10pt;\">Inmigración • Comercial • Criminal • Heridas • Accidentes • Divorcio • Bienes Raíces • Bancarrota </span></p>" +
                   "<p><span style=\"font-size: 8pt;\">CONFIDENTIALITY NOTICE: The information contained in this transmission may be privileged and confidential, and is intended only for the use of the individual or entity named above. If the reader of this message is not the intended recipient, you are hereby notified that any dissemination, distribution or copying of this communication is strictly prohibited. If you have received this transmission in error, please immediately reply to the sender that you have received this communication in error and then delete it. Thank you. </span></p>" +
                   "<p><span style=\"font-size: 8pt;\">CIRCULAR 230 NOTICE: To comply with U.S. Treasury Department and IRS regulations, we are required to advise you that, unless expressly stated otherwise, any U.S. federal tax advice contained in this transmittal, is not intended or written to be used, and cannot be used, by any person for the purpose of (i) avoiding penalties under the U.S. Internal Revenue Code, or (ii) promoting, marketing or recommending to another party any transaction or matter addressed in this e-mail or attachment.</span></p>";
        }
    }
}