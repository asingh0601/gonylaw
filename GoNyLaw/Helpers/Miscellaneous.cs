﻿using GoNyLaw.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace GoNyLaw.Helpers
{
    public static class Miscellaneous
    {
        public static int[] GetEligibleChildren(Children ch,int age)
        {
            foreach (var propertyInfo in ch.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(ch, null) == null)
                    {
                        propertyInfo.SetValue(ch, string.Empty, null);
                    }
                }
            }
            string ch1_yob, ch2_yob, ch3_yob, ch4_yob, ch5_yob, ch6_yob;
            int yy = 0, ctr = 0, n = 0;
            ch1_yob = "";
            ch2_yob = "";
            ch3_yob = "";
            ch4_yob = "";
            ch5_yob = "";
            ch6_yob = "";
            Int32.TryParse(ch.no_children, out n);
            int[] a = new int[6];
            int[] e = new int[8];
            if (ch.c1_dob.Length > 3)
                ch1_yob = ch.c1_dob.Substring(ch.c1_dob.Length - 4, 4);
            if (ch.c2_dob.Length > 3)
                ch2_yob = ch.c2_dob.Substring(ch.c2_dob.Length - 4, 4);
            if (ch.c3_dob.Length > 3)
                ch3_yob = ch.c3_dob.Substring(ch.c3_dob.Length - 4, 4);
            if (ch.c4_dob.Length > 3)
                ch4_yob = ch.c4_dob.Substring(ch.c4_dob.Length - 4, 4);
            if (ch.c5_dob.Length > 3)
                ch5_yob = ch.c5_dob.Substring(ch.c5_dob.Length - 4, 4);
            if (ch.c6_dob.Length > 3)
                ch6_yob = ch.c6_dob.Substring(ch.c6_dob.Length - 4, 4);
            Int32.TryParse(ch1_yob, out a[0]);
            Int32.TryParse(ch2_yob, out a[1]);
            Int32.TryParse(ch3_yob, out a[2]);
            Int32.TryParse(ch4_yob, out a[3]);
            Int32.TryParse(ch5_yob, out a[4]);
            Int32.TryParse(ch6_yob, out a[5]);
            Int32.TryParse(DateTime.Now.Year.ToString(), out yy);
            for (int i = 0; i < 6; i++)
            {
                if (yy - a[i] <= age)
                {
                    e[i+1] = 1;
                    ctr++;
                }
                else
                {
                    e[i+1] = 0;
                }
            }
            e[0] = n;
            e[7] = ctr;
            return e;
        }
        public static int GetNumberOfDays(this string d)
        {
            if (d == null) return 0;
            if (d == "Y") return 1;
            if (d == "T") return 3;
            if (d == "W") return 7;
            if (d == "M") return 30;
            if (d == "Q") return 120;
            if (d == "H") return 180;
            if (d == "S") return 365;
            return 0;
        }
    }
}