﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoNyLaw.Models;

namespace GoNyLaw.Helpers
{
    public static class CaseMerger
    {
        public static string GetCaseString(int id)
        {
            string str = "";
            Client c = new UserContext().Clients.Find(id);
            bool criminal = new CaseContext().CriminalCases.Any(x => x.clientid == id);
            bool bankruptcy = new CaseContext().BankruptcyCases.Any(x => x.clientid == id);
            bool immigration = new CaseContext().ImmigrationCases.Any(x => x.clientid == id);
            bool personalInjury=false;// = new CaseContext().PersonalInjuries.Any(x => x.clientid == id);
            if (criminal)
            {
                if (string.IsNullOrEmpty(str))
                    str = "C";
                else
                    str += ", C";
            }
            else if(bankruptcy)
            {
                if (string.IsNullOrEmpty(str))
                    str = "B";
                else
                    str += ", B";
            }
            else if (immigration)
            {
                if (string.IsNullOrEmpty(str))
                    str = "I";
                else
                    str += ", I";
            }
            else if (personalInjury)
            {
                if (string.IsNullOrEmpty(str))
                    str = "PI";
                else
                    str += ", PI";
            }
            return str;
        }
    }
}