﻿using GoNyLaw.Models;
using Ical.Net;
using Ical.Net.DataTypes;
using Ical.Net.Interfaces.DataTypes;
using Ical.Net.Serialization;
using Ical.Net.Serialization.iCalendar.Serializers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace GoNyLaw.Helpers
{
    public class iCalendar
    {
        public static void CreateEvent()
        {
            if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + @"\iCalendar\calendar.ics"))
                File.Delete(System.AppDomain.CurrentDomain.BaseDirectory + @"\iCalendar\calendar.ics");
            File.Create(System.AppDomain.CurrentDomain.BaseDirectory + @"\iCalendar\calendar.ics").Dispose();
            TextWriter tw = new StreamWriter(System.AppDomain.CurrentDomain.BaseDirectory + @"\iCalendar\calendar.ics", true);
            var calendar = new Calendar();
            IEnumerable<CriminalCase> cclist = new CaseContext().CriminalCases.Where(x => x.archived == 0);
            IEnumerable<BankruptcyCase> bclist = new CaseContext().BankruptcyCases.Where(x => x.archived == 0);
            IEnumerable<PotentialClient> pclist = new UserContext().PotentialClients;
            foreach (var cc in cclist)
            {
                string judge = "", part = "", room = "";
                if (!string.IsNullOrEmpty(cc.judge4))
                {
                    judge = cc.judge4.Decrypt();
                    part = cc.part4.Decrypt();
                    room = cc.room4.Decrypt();
                }
                else if (!string.IsNullOrEmpty(cc.judge3))
                {
                    judge = cc.judge3.Decrypt();
                    part = cc.part3.Decrypt();
                    room = cc.room3.Decrypt();
                }
                else if (!string.IsNullOrEmpty(cc.judge2))
                {
                    judge = cc.judge2.Decrypt();
                    part = cc.part2.Decrypt();
                    room = cc.room2.Decrypt();
                }
                else
                {
                    judge = cc.judge1.Decrypt();
                    part = cc.part1.Decrypt();
                    room = cc.room1.Decrypt();
                }
                CriminalAction ca = new ActionContext().CriminalActions.Where(x => x.refno == cc.refno).OrderByDescending(x => x.dateaction).FirstOrDefault();
                if(!cc.motion.IsMinDate())
                {
                    DateTime eventtime = cc.motion;
                    var eventstart = new CalDateTime(new DateTime(eventtime.Year, eventtime.Month, eventtime.Day, 14, 00, 00, DateTimeKind.Utc));
                    var eventend = new CalDateTime(new DateTime(eventtime.Year, eventtime.Month, eventtime.Day, 14, 30, 00, DateTimeKind.Utc));
                    var e1 = new Event
                    {
                        Summary = "Def Motion for " + cc.fullname.Decrypt(),
                        Description = "Def Motion for " + cc.fullname.Decrypt() +
                                      "\\nRef. No. - " + cc.refno +
                                      "\\nCell Phone - " + cc.cellphone.Decrypt() +
                                      "\\nCounty - " + cc.county.ToCountyName() +
                                      "\\nCourt Name - " + cc.court.ToCourtName() +
                                      "\\nJudge - " + judge +
                                      "\\nPart - " + part +
                                      "\\nRoom - " + room,
                        Start = eventstart,
                        End = eventend,
                        Categories = { "Pink Category" }
                    };
                    calendar.Events.Add(e1);
                }                                
                if (!Object.ReferenceEquals(ca, null))
                {
                    if (!ca.nextcourtdate.IsMinDate())
                    {
                        DateTime eventtime = ca.nextcourtdate;
                        var eventstart = new CalDateTime(new DateTime(eventtime.Year, eventtime.Month, eventtime.Day, eventtime.AddHours(5).Hour, eventtime.Minute, 00, DateTimeKind.Utc));
                        var eventend = new CalDateTime(new DateTime(eventtime.Year, eventtime.Month, eventtime.Day, eventtime.AddHours(5).Hour, eventtime.AddMinutes(30).Minute, 00, DateTimeKind.Utc));
                        var e = new Event
                        {
                            Summary = "Court Date for " + cc.fullname.Decrypt(),
                            Description = "Court Date for " + cc.fullname.Decrypt() +
                                      "\\nRef. No. - " + cc.refno +
                                      "\\nCell Phone - " + cc.cellphone.Decrypt() +
                                      "\\nCounty - " + cc.county.ToCountyName() +
                                      "\\nCourt Name - " + cc.court.ToCourtName() +
                                      "\\nJudge - " + judge +
                                      "\\nPart - " + part +
                                      "\\nRoom - " + room,
                            Start = eventstart,
                            End = eventend,
                            Categories = { "Red Category" }
                        };
                        calendar.Events.Add(e);
                    }
                }
            }
            foreach(var bc in bclist)
            {
                if (!bc.courtdatetime.IsMinDate())
                {
                    DateTime eventtime = bc.courtdatetime;
                    var eventstart = new CalDateTime(new DateTime(eventtime.Year, eventtime.Month, eventtime.Day, eventtime.AddHours(5).Hour, eventtime.Minute, 00, DateTimeKind.Utc));
                    var eventend = new CalDateTime(new DateTime(eventtime.Year, eventtime.Month, eventtime.Day, eventtime.AddHours(5).Hour, eventtime.AddMinutes(30).Minute, 00, DateTimeKind.Utc));
                    var e = new Event
                    {
                        Summary = "Court Date for " + bc.fullname.Decrypt(),
                        Description = "Other Order for " + bc.fullname.Decrypt() +
                                  "\\nRef. No. - " + bc.refno +
                                  "\\nCell Phone - " + bc.cellphone.Decrypt() +
                                  "\\nCourt Name - " + bc.court.ToCourtName(),
                        Start = eventstart,
                        End = eventend,
                        Categories = { "Blue Category" }
                    };
                    calendar.Events.Add(e);
                }
            }
            foreach(var pc in pclist)
            {
                if (!pc.appointment.Date.IsMinDate())
                {
                    DateTime eventtime = pc.appointment;
                    var eventstart = new CalDateTime(new DateTime(eventtime.Year, eventtime.Month, eventtime.Day, eventtime.AddHours(5).Hour, eventtime.Minute, 00, DateTimeKind.Utc));
                    var eventend = new CalDateTime(new DateTime(eventtime.Year, eventtime.Month, eventtime.Day, eventtime.AddHours(5).Hour, eventtime.AddMinutes(30).Minute, 00, DateTimeKind.Utc));
                    var e = new Event
                    {
                        Summary = "Appointment for " + pc.fullname.Decrypt(),
                        Description = "Appointment for " + pc.fullname.Decrypt() +
                                  "\\nCell Phone - " + pc.cellphone.Decrypt()+
                                  "\\nCase Details - " + pc.casedetails.Decrypt() +
                                  "\\nAppointment Details - " + pc.appointmentdetails.Decrypt(),
                        Start = eventstart,
                        End = eventend,
                        Categories = { "Green Category" }
                    };
                    calendar.Events.Add(e);
                }
            }
            calendar.AddProperty("X-PUBLISHED-TTL", "PT30M");
            calendar.AddProperty("REFRESH-INTERVAL;VALUE=DURATION:", "PT30M");
            var serializer = new CalendarSerializer(new SerializationContext());
            var serializedCalendar = serializer.SerializeToString(calendar);
            string output = serializer.SerializeToString(calendar);
            var bytes = Encoding.UTF8.GetBytes(output);
            tw.WriteLine(output);
            tw.Close();
        }
    }
}