﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoNyLaw.Helpers
{
    public class HttpsPlayer : RequireHttpsAttribute
    {
        protected override void HandleNonHttpsRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.Request.Url.Host.Contains("localhost") && !filterContext.HttpContext.Request.Url.AbsoluteUri.ToLower().Contains("calendarwebdisplay"))
            {
                base.HandleNonHttpsRequest(filterContext);
            }
        }
    }
}