﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoNyLaw.Helpers
{
    public static class Maths
    {
        public static int RoundOff(this double num)
        {
            if (num >= ((int)num + 0.5))
                return (int)num + 1;
            else
                return (int)num;
        }
        public static int GetRandomNumber()
        {
            Random rand = new Random((int)DateTime.Now.Ticks);
            int numIterations = 0;
            numIterations = rand.Next(111111, 999999);
            return numIterations;
        }
        public static string SMSCode()
        {
            Random rand = new Random((int)DateTime.Now.Ticks);
            int numIterations = 0;
            numIterations = rand.Next(1111, 9999);
            return numIterations.ToString();
        }
    }
}