﻿using GoNyLaw.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace GoNyLaw.Helpers
{
    public class ChildCareCalculations
    {
        private double adjusted_gross_plaintiff = 0.0;
        private double adjusted_gross_defendant = 0.0;
        private double combined_parental_income = 0.0;
        private double applicable_combined_parental_income = 0.0;
        private double excess_amount = 0.0;
        private double p_percent = 0.0;
        private double d_percent = 0.0;
        private double plaintiff_child_support = 0.0;
        private double defendant_child_support = 0.0;
        private double combined_child_support = 0.0;
        private double babysit = 0.0;
        private double unreimbursed = 0.0;
        private double p_low_income = 0.0;
        private double d_low_income = 0.0;
        private double app_per = 0.0;
        private double addon = 0.0;
        private double nytax_percent = 0.0, fica_social_percent = 0.0, fica_medicare_percent = 0.0, upper_slab = 0.0, lower_slab = 0.0, lowest_slab = 0.0, p_income = 0.0, d_income = 0.0;
        double fica_social_plaintiff = 0.0, fica_social_defendant = 0.0, fica_medicare_plaintiff = 0.0, fica_medicare_defendant = 0.0, nytax_plaintiff = 0.0, nytax_defendant = 0.0;
        double one_child, two_child, three_child, four_child, five_child;
        private ChildCalc cc;
        private DivorceCase dc;
        public ChildCareCalculations(DivorceCase dc1)
        {
            dc = dc1;
            ChildCalcContext cdb = new ChildCalcContext();
            cc = cdb.ChildCalc.Where(x => x.id == "1").FirstOrDefault();
            int no_children=0;
            double.TryParse(cc.ny_tax, out nytax_percent);
            double.TryParse(dc.h.unreimbursed, out unreimbursed);
            double.TryParse(cc.fica_social, out fica_social_percent);
            double.TryParse(cc.fica_medicare, out fica_medicare_percent);
            double.TryParse(dc.p.income, out p_income);
            double.TryParse(dc.d.income, out d_income);
            double.TryParse(cc.upper_slab, out upper_slab);
            double.TryParse(cc.lower_slab, out lower_slab);
            double.TryParse(cc.lowest_slab, out lowest_slab);
            double.TryParse(cc.one_child, out one_child);
            double.TryParse(cc.two_child, out two_child);
            double.TryParse(cc.three_child, out three_child);
            double.TryParse(cc.four_child, out four_child);
            double.TryParse(cc.five_child, out five_child);
            if(!ReferenceEquals(dc.co, null))
            int.TryParse(dc.ch.no_children, out no_children);
            double.TryParse(dc.h.babysit, out babysit);
            if (no_children == 1)
            {
                app_per = one_child;
            }
            else if (no_children == 2)
            {
                app_per = two_child;
            }
            else if (no_children == 3)
            {
                app_per = three_child;
            }
            else if (no_children == 4)
            {
                app_per = four_child;
            }
            else if (no_children >= 5)
            {
                app_per = five_child;
            }
            nytax_plaintiff = p_income * nytax_percent / 100;
            nytax_defendant = d_income * nytax_percent / 100;
            fica_social_plaintiff = fica_social_percent * p_income / 100;
            fica_social_defendant = fica_social_percent * d_income / 100;
            fica_medicare_plaintiff = fica_medicare_percent * p_income / 100;
            fica_medicare_defendant = fica_medicare_percent * d_income / 100;
            adjusted_gross_plaintiff = p_income - (nytax_plaintiff + fica_social_plaintiff + fica_medicare_plaintiff);
            adjusted_gross_defendant = d_income - (nytax_defendant + fica_social_defendant + fica_medicare_defendant);
            combined_parental_income = adjusted_gross_plaintiff + adjusted_gross_defendant;
            p_percent = Math.Round(adjusted_gross_plaintiff / combined_parental_income, 2) * 100;
            d_percent = 100 - p_percent;
            if (combined_parental_income > upper_slab)
            {
                excess_amount = combined_parental_income - upper_slab;
                applicable_combined_parental_income = upper_slab;
            }
            else
            {
                applicable_combined_parental_income = combined_parental_income;
            }
            combined_child_support = (applicable_combined_parental_income * app_per / 100).RoundOff();
            plaintiff_child_support = (p_percent * combined_child_support / 100).RoundOff();
            defendant_child_support = (d_percent * combined_child_support / 100).RoundOff();
            if (excess_amount == 0)
            {
                p_low_income = adjusted_gross_plaintiff - (p_percent * combined_child_support / 100);
                d_low_income = adjusted_gross_defendant - (d_percent * combined_child_support / 100);
                if (p_low_income <= lowest_slab)
                {
                    plaintiff_child_support = 300.00;
                }
                else if (p_low_income > lowest_slab && p_low_income <= lower_slab)
                {
                    if (adjusted_gross_plaintiff - lower_slab > 600.00)
                        plaintiff_child_support = adjusted_gross_plaintiff - lower_slab;
                    else
                        plaintiff_child_support = 600.00;
                }
                if (d_low_income <= lowest_slab)
                {
                        defendant_child_support = 300.00;
                }
                else if (d_low_income > lowest_slab && d_low_income <= lower_slab)
                {
                    if (adjusted_gross_defendant - lower_slab > 600.00)
                        defendant_child_support = adjusted_gross_defendant - lower_slab;
                    else
                        defendant_child_support = 600.00;
                }
            }
            if (dc.ch.c1_lives == "D" && dc.h.health_cover == "P")
                addon = d_percent * (babysit + unreimbursed) / 100;
            else if (dc.ch.c1_lives == "P" && dc.h.health_cover == "D")
                addon = p_percent * (babysit + unreimbursed) / 100;
            if (dc.ch.c1_lives == "P" && dc.h.health_cover == "P")
                addon = d_percent * (babysit - unreimbursed) / 100;
            else if (dc.ch.c1_lives == "D" && dc.h.health_cover == "D")
                addon = p_percent * (babysit - unreimbursed) / 100;
        }
        public ChildCalc GetChildCalcData()
        {
            cc.upper_slab = upper_slab.ToString("n0");
            cc.lower_slab = lower_slab.ToString("n0");
            cc.lowest_slab = lowest_slab.ToString("n0");
            return cc;
        }
        public string GetDeductionP()
        {
            return (nytax_plaintiff + fica_social_plaintiff + fica_medicare_plaintiff).ToString("n0");
        }
        public string GetDeductionD()
        {
            return (nytax_defendant + fica_social_defendant + fica_medicare_defendant).ToString("n0");
        }
        public string GetIncomeP()
        {
            return p_income.ToString("n0");
        }
        public string GetIncomeD()
        {
            return d_income.ToString("n0");
        }
        public string GetBabysit()
        {
            return babysit.ToString("n0");
        }
        public string GetHealthCover()
        {
            return unreimbursed.ToString("n0");
        }
        public int GetApplicablePercent()
        {
            return (int)app_per;
        }
        public string GetNYCP()
        {
            return nytax_plaintiff.ToString("n0");
        }
        public string GetNYCD()
        {
            return nytax_defendant.ToString("n0");
        }
        public string GetFICAMP()
        {
            return fica_medicare_plaintiff.ToString("n0");
        }
        public string GetFICAMD()
        {
            return fica_medicare_defendant.ToString("n0");
        }
        public string GetFICASP()
        {
            return fica_social_plaintiff.ToString("n0");
        }
        public string GetFICASD()
        {
            return fica_social_defendant.ToString("n0");
        }
        public string GetAdjustedGrossP()
        {
            return adjusted_gross_plaintiff.ToString("n0");
        }
        public string GetAdjustedGrossD()
        {
            return adjusted_gross_defendant.ToString("n0");
        }
        public string GetAdjustedGrossPayor()
        {
            if (dc.ch.c1_lives == "D")
                return adjusted_gross_plaintiff.ToString("n0");
            else
                return adjusted_gross_defendant.ToString("n0");
        }
        public string GetAdjustedGrossPayee()
        {
            if (dc.ch.c1_lives == "P")
                return adjusted_gross_plaintiff.ToString("n0");
            else
                return adjusted_gross_defendant.ToString("n0");
        }
        public string GetCombinedParentalIncome()
        {
            return combined_parental_income.ToString("n0");
        }
        public string GetPPercent()
        {
            return p_percent.ToString("n0");
        }
        public string GetDPercent()
        {
            return d_percent.ToString("n0");
        }
        public string GetNCPPercent()
        {
            if (dc.ch.c1_lives == "D")
                return p_percent.ToString("n0");
            else
                return d_percent.ToString("n0");
        }
        public string GetCPPercent()
        {
            if (dc.ch.c1_lives == "P")
                return p_percent.ToString("n0");
            else
                return d_percent.ToString("n0");
        }
        public string GetNCPLowIncome()
        {
            if (dc.ch.c1_lives == "D")
                return p_low_income.ToString("n0");
            else
                return d_low_income.ToString("n0");
        }
        public string GetNCPIncomeMinusSSR()
        {
            double a = adjusted_gross_plaintiff - lower_slab;
            double b = adjusted_gross_defendant - lower_slab;
            if (dc.ch.c1_lives == "D")
            {
                if (a < 0)
                    return "-$" + (a * -1).ToString("n0");
                else
                    return "$" + (a).ToString("n0");
            }
            else
            {
                if (b < 0)
                    return "-$" + (b * -1).ToString("n0");
                else
                    return "$" + (b).ToString("n0");
            }
        }
        public string GetCombinedChildSupport()
        {
            return combined_child_support.ToString("n0");
        }
        public string GetNCPUnadjustedChildSupport()
        {
            if (dc.ch.c1_lives == "D")
                return (p_percent * combined_child_support / 100).ToString("n0");
            else
                return (d_percent * combined_child_support / 100).ToString("n0");
        }
        public string GetNCPAdjustedChildSupport()
        {
            if (dc.ch.c1_lives == "D")
                return plaintiff_child_support.ToString("n0");
            else
                return defendant_child_support.ToString("n0");
        }
        public string GetNCPBabysit()
        {
            if (dc.ch.c1_lives == "D")
                return (p_percent * babysit / 100).ToString("n0");
            else
                return (d_percent * babysit / 100).ToString("n0");
        }
        public string GetNCPHealth()
        {
            if (dc.ch.c1_lives == "P" && dc.h.health_cover == "P")
                return (d_percent * unreimbursed / 100).ToString("n0");
            else if (dc.ch.c1_lives == "D" && dc.h.health_cover == "D")
                return (p_percent * unreimbursed / 100).ToString("n0");
            return "0";
        }
        public string GetCPHealth()
        {
            if (dc.ch.c1_lives == "D" && dc.h.health_cover == "P")
                return (d_percent * unreimbursed / 100).ToString("n0");
            else if (dc.ch.c1_lives == "P" && dc.h.health_cover == "D")
                return (p_percent * unreimbursed / 100).ToString("n0");
            return "0";
        }
        public string GetHealthCareAdjustment()
        {
            if (dc.ch.c1_lives == "D" && dc.h.health_cover == "P")
                return "+$" + (d_percent * unreimbursed / 100).ToString("n0");
            else if (dc.ch.c1_lives == "P" && dc.h.health_cover == "D")
                return "+$" + (p_percent * unreimbursed / 100).ToString("n0");
            if (dc.ch.c1_lives == "P" && dc.h.health_cover == "P")
                return "-$" + (d_percent * unreimbursed / 100).ToString("n0");
            else if (dc.ch.c1_lives == "D" && dc.h.health_cover == "D")
                return "-$" + (p_percent * unreimbursed / 100).ToString("n0");
            return "0";
        }
        public string GetAnswerHealthYes()
        {
            if ((dc.h.plan_holder == "D" && dc.ch.c1_lives == "P") || (dc.h.plan_holder == "P" && dc.ch.c1_lives == "D"))
                return "X";
            return "";
        }
        public string GetAnswerHealthNo()
        {
            if ((dc.h.plan_holder == "D" && dc.ch.c1_lives == "D") || (dc.h.plan_holder == "P" && dc.ch.c1_lives == "P"))
                return "X";
            return "";
        }
        public int GetNCPChildSupportWithAddOnValue()
        {
            if (dc.ch.c1_lives == "D")
            {
                if (plaintiff_child_support == 300.00)
                    return (int)plaintiff_child_support;
                else
                    return (int)(plaintiff_child_support + addon);
            }
            else
            {
                if (defendant_child_support == 300.00)
                    return (int)defendant_child_support;
                else
                    return (int)(defendant_child_support + addon);
            }
        }
        public string GetNCPChildSupportWithAddOn()
        {
            if (dc.ch.c1_lives == "D")
            {
                if (plaintiff_child_support == 300.00)
                    return plaintiff_child_support.ToString("n0");
                else
                    return (plaintiff_child_support + addon).ToString("n0");
            }
            else
            {
                if (defendant_child_support == 300.00)
                    return defendant_child_support.ToString("n0");
                else
                    return (defendant_child_support + addon).ToString("n0");
            }
        }
        public int GetNCPMonthlyChildSupportWithAddOnValue()
        {
            if (dc.ch.c1_lives == "D")
            {
                if (plaintiff_child_support == 300.00)
                    return (int)plaintiff_child_support / 12;
                else
                    return (int)(plaintiff_child_support + addon) / 12;
            }
            else
            {
                if (defendant_child_support == 300.00)
                    return (int)defendant_child_support / 12;
                else
                    return (int)(defendant_child_support + addon) / 12;
            }
        }
        public string GetNCPMonthlyChildSupportWithAddOn()
        {
            if (dc.ch.c1_lives == "D")
            {
                if (plaintiff_child_support == 300.00)
                    return (plaintiff_child_support / 12).ToString("n0");
                else
                    return ((plaintiff_child_support + addon) / 12).ToString("n0");
            }
            else
            {
                if (defendant_child_support == 300.00)
                    return (defendant_child_support / 12).ToString("n0");
                else
                    return ((defendant_child_support + addon) / 12).ToString("n0");
            }
        }
        public string GetAddOn()
        {
            return addon.ToString("n0");
        }
        public string GetExampleCalc()
        {
            return (upper_slab * two_child / 100).ToString("n0");
        }
        public string GetExcessAmount()
        {
            return excess_amount.ToString("n0");
        }
        public string GetExcessChildSupport()
        {
            return (excess_amount * app_per / 100).ToString("n0");
        }
        public string GetNCPExcessChildSupport()
        {
            if (dc.ch.c1_lives == "D")
                return (excess_amount * app_per * p_percent / 10000).ToString("n0");
            else
                return (excess_amount * app_per * d_percent / 10000).ToString("n0");
        }
        public string GetOneP()
        {
            return (one_child).ToString();
        }
        public string GetTwoP()
        {
            return (two_child).ToString();
        }
        public string GetThreeP()
        {
            return (three_child).ToString();
        }
        public string GetFourP()
        {
            return (four_child).ToString();
        }
        public string GetFiveP()
        {
            return (five_child).ToString();
        }
        public string GetUpperSlab()
        {
            return (upper_slab).ToString("n0");
        }
        public string GetCP()
        {
            if (dc.ch.c1_lives == "D")
                return "Defendant";
            else
                return "Plaintiff";
        }
        public string GetNCP()
        {
            if (dc.ch.c1_lives == "P")
                return "Defendant";
            else
                return "Plaintiff";
        }
    }
}