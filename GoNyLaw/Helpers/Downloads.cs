﻿using GoNyLaw.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GoNyLaw.Helpers
{
    public static class Downloads
    {
        private static Microsoft.Office.Interop.Word.Application appWord;
        public static void Get(string folder, string filename, string refno, int flag = 0)
        {
            int type = 1;
            if (refno.Length > 1)
                refno = "_" + refno;
            DocumentConfigContext db = new DocumentConfigContext();
            DocumentConfig dconfig = db.DocumentConfig.Where(x => x.id == 1).FirstOrDefault();
            Int32.TryParse(dconfig.type, out type);
            if (flag == 1)
            {
                type = 2;
            }
            if (type == 1)
            {
                Documents.ConvertToPdf(folder + "\\" + filename + ".docx", folder + "\\" + filename + ".pdf");
                HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + filename + refno + ".pdf");
                HttpContext.Current.Response.TransmitFile(folder + "\\" + filename + ".pdf");
                HttpContext.Current.Response.Flush();
                appWord = new Microsoft.Office.Interop.Word.Application();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(appWord);
                File.Delete(folder + "\\" + filename + ".docx");
                File.Delete(folder + "\\" + filename + ".pdf");
            }
            else
            {
                HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + filename + refno + ".docx");
                HttpContext.Current.Response.TransmitFile(folder + "\\" + filename + ".docx");
                HttpContext.Current.Response.Flush();
                File.Delete(folder + "\\" + filename + ".docx");
            }
            HttpContext.Current.Response.End();
        }
        public static void GetPDF(string folder, string filename, string refno)
        {
            if (refno.Length > 1)
                refno = "_" + refno;
            HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + filename + refno + ".pdf");
            HttpContext.Current.Response.TransmitFile(folder + "\\" + filename + ".pdf");
            HttpContext.Current.Response.Flush();
            File.Delete(folder + "\\" + filename + ".pdf");
            HttpContext.Current.Response.End();
        }
        public static void GetBypass(string folder, string filename, string refno)
        {
            int type = 1;
            if (refno.Length > 1)
                refno = "_" + refno;
            DocumentConfigContext db = new DocumentConfigContext();
            DocumentConfig dconfig = db.DocumentConfig.Where(x => x.id == 1).FirstOrDefault();
            Int32.TryParse(dconfig.type, out type);
            if (File.Exists(folder + @"\AdminUploads\" + filename + ".docx"))
            {
                if (type == 1)
                {
                    Documents.ConvertToPdf(folder + @"\AdminUploads\" + filename + ".docx", folder + @"\AdminUploads\" + filename + ".pdf");
                    HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + filename + refno + ".pdf");
                    HttpContext.Current.Response.TransmitFile(folder + @"\AdminUploads\" + filename + ".pdf");
                    HttpContext.Current.Response.Flush();
                    File.Delete(folder + @"\AdminUploads\" + filename + ".pdf");
                }
                else
                {
                    HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + filename + refno + ".docx");
                    HttpContext.Current.Response.TransmitFile(folder + @"\AdminUploads\" + filename + ".docx");
                    HttpContext.Current.Response.Flush();
                }
                HttpContext.Current.Response.End();
            }
            else if (File.Exists(folder + @"\AdminUploads\" + filename + ".pdf"))
            {
                HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + filename + refno + ".pdf");
                HttpContext.Current.Response.TransmitFile(folder + @"\AdminUploads\" + filename + ".pdf");
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
        }
    }
}