﻿using GoNyLaw.Models;
using iTextSharp.text.pdf;
using Xceed.Words.NET;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;

namespace GoNyLaw.Helpers
{
    public class Documents
    {
        public static LawOfficeDetail GetLawOfficeDetails()
        {
            LawOfficeDetailContext ldb = new LawOfficeDetailContext();
            LawOfficeDetail ld = ldb.LawOfficeDetail.Where(x => x.id == "1").FirstOrDefault();
            return ld;
        }
        public static void ConvertToPdf(string input, string output)
        {
            Microsoft.Office.Interop.Word._Application oWord = new Microsoft.Office.Interop.Word.Application();
            oWord.Visible = false;
            object oMissing = System.Reflection.Missing.Value;
            object isVisible = true;
            object readOnly = false;
            object oInput = input;
            object oOutput = output;
            object oFormat = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatPDF;
            Microsoft.Office.Interop.Word._Document oDoc = oWord.Documents.Open(ref oInput, ref oMissing, ref readOnly, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref isVisible, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
            oDoc.Activate();
            oDoc.SaveAs(ref oOutput, ref oFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
            oWord.Quit(ref oMissing, ref oMissing, ref oMissing);
        }
        //common

        public static void Divorce_AffidavitOfService(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Affidavit_of_Service.docx"))
            {
                File.Delete(folder + "\\Affidavit_of_Service.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Affidavit_of_Service.docx"))
            {
                {
                    Paragraph p1 = document.InsertParagraph();
                    Paragraph p2 = document.InsertParagraph();
                    Paragraph p3 = document.InsertParagraph();
                    Paragraph p4 = document.InsertParagraph();
                    p1.Append("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p1.AppendLine(pname).MyStyle();
                    p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p1.AppendLine("\t\t\t\t\t\t\tAFFIDAVIT OF SERVICE").MyStyleB();
                    p1.AppendLine("\t\t-against-\n").MyStyle();
                    p1.AppendLine(dname).MyStyle();
                    p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p1.AppendLine("---------------------------------------------------------------------X").MyStyle();
                    p1.AppendLine("STATE OF NEW YORK, COUNTY OF                  , ss.\n").MyStyle();
                    p1.AppendLine("\t______________________________ being duly sworn, says:").MyStyle();
                    p1.AppendLine("\n\t1. I am not a party to this action, am over 18 years of age and reside at:").MyStyle();
                    p1.AppendLine("\n\t2. On                               , " + DateTime.Now.Year.ToString() + ", at            .M., at").MyStyle();
                    if (dc.casetype == "2")
                        p2.AppendLine("\n\tI served the within Summons with Notice, and notice of automatic orders, and notice of guideline maintenance, and the notice of continuation of health coverage on " + dname + ", the Defendant named by delivering true copies on the Defendant personally. In addition I served a copy of the Child Support Standards Chart.").MyStyle();
                    else
                        p2.AppendLine("\n\tI served the within Summons with Notice, and notice of automatic orders, and notice of guideline maintenance on " + dname + ", the Defendant named by delivering true copies on the Defendant personally. In addition I served a copy of the Notice of Continuation of Health Care Coverage.").MyStyle();
                    p2.Alignment = Alignment.both;
                    p3.AppendLine("\t3. The notice required by the Domestic Relations Law, Section 232 -- \"ACTION FOR DIVORCE\" -- was legibly printed on the face of the Summons served on the Defendant.\n").MyStyle();
                    p3.AppendLine("\t4. I knew the person so served to be the person described in the Summons as the Defendant. My knowledge of the Defendant and how I acquired it are as follows:\n").MyStyle();
                    p3.AppendLine("\t\t[ ]  I have known the Defendant for        years and ").MyStyle();
                    p3.AppendLine("\t\t\t\t\t\t     OR").MyStyle();
                    p3.AppendLine("\t\t[ ]  I identified the Defendant by a photograph annexed to this affidavit which was given to me by the Plaintiff.").MyStyle();
                    p3.AppendLine("\t\t\t\t\t\t     OR").MyStyle();
                    p3.AppendLine("\t\t[ ]  Plaintiff accompanied me and pointed out the Defendant.").MyStyle();
                    p3.AppendLine("\t\t\t\t\t\t     OR").MyStyle();
                    if (dc.d.sex == "F")
                        p3.AppendLine("\t\t[ ]  I asked the person served if she was the person named in the Summons and Defendant admitted being the person so named.").MyStyle();
                    else
                        p3.AppendLine("\t\t[ ]  I asked the person served if he was the person named in the Summons and Defendant admitted being the person so named.").MyStyle();
                    p4.InsertPageBreakBeforeSelf();
                    p4.AppendLine("\t5. Deponent describes the individual served as follows:").MyStyle();
                    p4.AppendLine("\nSex").MyStyleU();
                    p4.Append(":  [] Male  [] Female").MyStyle();
                    p4.AppendLine("Height").MyStyleU();
                    p4.Append(":  [] Under 5'  [] 5'0\"-5'3\"  [] 5'4\"-5'8\"  [] 5'9\"-6'0\"  [] Over 6'").MyStyle();
                    p4.AppendLine("Weight").MyStyleU();
                    p4.Append(":  [] Under 100 Lbs  [] 100-130 Lbs  [] 131-160 Lbs  [] 161-200 Lbs  [] Over 200 Lbs").MyStyle();
                    p4.AppendLine("Age").MyStyleU();
                    p4.Append(":  [] 14-20 Yrs  [] 21-35  [] 36-50 Yrs  [] 51-65 Yrs  [] Over 65").MyStyle();
                    p4.AppendLine("Hair Color").MyStyleU();
                    p4.Append(":  [] Black  [] Brown  [] Blond  [] Grey  [] Red  [] White  [] Balding  [] Bald").MyStyle();
                    p4.AppendLine("Color of Skin").MyStyleU();
                    p4.Append(" - describe color:").MyStyle();
                    p4.AppendLine("Other Identifying Features").MyStyleU();
                    p4.Append(", if any:").MyStyle();
                    if (dc.d.sex == "F")
                        p4.AppendLine("\n\n\t6. At the time I served the Defendant, I asked her whether she was in the military service of this state, any other state or this nation, and the Defendant responded in the negative.").MyStyle();
                    else
                        p4.AppendLine("\n\n\t6. At the time I served the Defendant, I asked him whether he was in the military service of this state, any other state or this nation, and the Defendant responded in the negative.").MyStyle();
                    p4.AppendLine("\n\t\t\t\t\t\t____________________________________").MyStyle();
                    p4.AppendLine("\t\t\t\t\t\tName:").MyStyle();
                    p4.AppendLine("\nSubscribed and sworn to before me").MyStyle();
                    p4.AppendLine("on").MyStyle();
                    p4.AppendLine("\n__________________________").MyStyle();
                    p4.AppendLine("\t    Notary Public").MyStyle();
                    p4.AppendLine("My commission expires on").MyStyle();
                    p4.AppendLine("\n(Form UD-3 - 1/25/16)").MyStyle();
                    document.Save();
                }
            }
        }
        public static void Divorce_NoteOfIssue(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Note_Of_Issue.docx"))
            {
                File.Delete(folder + "\\Note_Of_Issue.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Note_Of_Issue.docx"))
            {
                {
                    Paragraph p1 = document.InsertParagraph();
                    Paragraph p2 = document.InsertParagraph();
                    p1.Append("NOTE OF ISSUE - UNCONTESTED DIVORCE").MyStyleB();
                    p1.Alignment = Alignment.center;
                    p2.AppendLine("\t\t\t\t\t\t\t\t*************************").MyStyle();
                    p2.AppendLine("\t\t\t\t\t\t\t\t*\t\t\t\t*").MyStyle();
                    p2.AppendLine("\t\t\t\t\t\t\t\t*\t\t\t\t*").MyStyle();
                    p2.AppendLine("\t\t\t\t\t\t\t\t*\t\t\t\t*").MyStyle();
                    p2.AppendLine("\t\t\t\t\t\t\t\t*\t\t\t\t*").MyStyle();
                    p2.AppendLine("\t\t\t\t\t\t\t\t*\t\t\t\t*").MyStyle();
                    p2.AppendLine("\t\t\t\t\t\t\t\t*************************").MyStyle();
                    p2.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p2.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p2.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p2.AppendLine(pname).MyStyle();
                    p2.AppendLine("\n\t\tPlaintiff,").MyStyle();
                    p2.AppendLine("\n\t\t-against-\n").MyStyle();
                    p2.AppendLine(dname).MyStyle();
                    p2.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p2.AppendLine("---------------------------------------------------------------------X").MyStyle();
                    p2.AppendLine("            NO TRIAL").MyStyle();
                    p2.AppendLine("            FILED BY: GoNyLaw").MyStyle();
                    p2.AppendLine("                                4482 Broadway").MyStyle();
                    p2.AppendLine("                                New York, New York - 10040").MyStyle();
                    p2.AppendLine("                                Phone - 718-492-8822").MyStyle();
                    DateTime index_date;
                    DateTime.TryParseExact(dc.index_date, "MM/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out index_date);
                    if (index_date.Year > 1900)
                        p2.AppendLine("            DATE SUMMONS FILED: " + index_date.ToString("MMMM dd, yyyy")).MyStyle();
                    else
                        p2.AppendLine("            DATE SUMMONS FILED:").MyStyle();
                    if (dc.date_summons.HasValue && dc.date_summons.Value.Year > 1900)
                        p2.AppendLine("            DATE SUMMONS SERVED: " + dc.date_summons.Value.ToString("MMMM dd, yyyy")).MyStyle();
                    else
                        p2.AppendLine("            DATE SUMMONS SERVED:").MyStyle();
                    p2.AppendLine();
                    p2.Append("            DATE ISSUE JOINED: ").MyStyle();
                    if (dc.waiver_default == "1")
                        p2.Append("NOT JOINED - Waiver").MyStyleB();
                    else if (dc.waiver_default == "2")
                        p2.Append("NOT JOINED - Default").MyStyleB();
                    p2.AppendLine();
                    p2.Append("            NATURE OF ACTION: ").MyStyle();
                    p2.Append("UNCONTESTED DIVORCE").MyStyleB();
                    p2.AppendLine();
                    p2.Append("            RELIEF: ").MyStyle();
                    p2.Append("ABSOLUTE DIVORCE\n").MyStyleB();
                    p2.AppendLine();
                    p2.AppendLine();
                    p2.AppendLine("\t\t\t\t\t\t____________________________________").MyStyle();
                    p2.AppendLine("\t\t\t\t\t\t" + dc.p.name.ToTitleCase()).MyStyle();
                    p2.AppendLine("\t\t\t\t\t\tPlaintiff Pro-se").MyStyle();
                    p2.AppendLine("\t\t\t\t\t\t" + dc.p.street.ToTitleCase()).MyStyle();
                    p2.AppendLine("\t\t\t\t\t\t" + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + " - " + dc.p.zip).MyStyle();
                    p2.AppendLine("\t\t\t\t\t\t" + dc.p.phone).MyStyle();
                    p2.AppendLine();
                    p2.AppendLine("Defendant: " + dname).MyStyle();
                    p2.AppendLine("Office and P.O. Address:").MyStyle();
                    p2.AppendLine("\t\t" + dc.d.street.ToTitleCase() + ",").MyStyle();
                    if (dc.d.country == "US")
                        p2.AppendLine("\t\t" + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " - " + dc.d.zip).MyStyle();
                    else
                        p2.AppendLine("\t\t" + dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName()).MyStyle();
                    p2.AppendLine();
                    p2.AppendLine("(Form UD-9 - 9/11)").MyStyle();
                    document.Save();
                }
            }
        }
        public static void Divorce_NoticeOfEntry(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            LawOfficeDetail ld = GetLawOfficeDetails();
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Notice_Of_Entry.docx"))
            {
                File.Delete(folder + "\\Notice_Of_Entry.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Notice_Of_Entry.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tNOTICE OF ENTRY").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                p1.AppendLine("PLEASE TAKE NOTICE that the attached is a true copy of a Judgment of Divorce in this matter that was entered in the Office of the Clerk of the Supreme Court, New York County, on \n\n").MyStyle();
                p1.AppendLine("\nDated:").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t____________________________________").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t" + dc.p.name.ToTitleCase()).MyStyle();
                p1.AppendLine("\t\t\t\t\t\tPlaintiff Pro-Se").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t" + dc.p.street.ToTitleCase()).MyStyle();
                p1.AppendLine("\t\t\t\t\t\t" + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + " " + dc.p.zip).MyStyle();
                p1.AppendLine("\t\t\t\t\t\t" + dc.p.phone).MyStyle();
                p1.AppendLine("TO:" + dname).MyStyle();
                p1.AppendLine("Defendant").MyStyle();
                p1.AppendLine(dc.d.street.ToTitleCase()).MyStyle();
                if (dc.d.country == "US")
                    p1.AppendLine(dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " - " + dc.d.zip).MyStyle();
                else
                    p1.AppendLine(dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName()).MyStyle();
                p1.AppendLine("\n\n(Form UD-14 - 5/99)").MyStyle();
                document.Save();
            }
        }
        public static void Divorce_Authorization(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            //int amount = dc.case_id.PaymentDetails(1).GetAmount();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Authorization.docx"))
            {
                File.Delete(folder + "\\Authorization.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Authorization.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK\n").MyStyleB();
                p1.AppendLine("COUNTY OF NEW YORK\n").MyStyleB();
                p1.AppendLine("COUNTY CLERK / MATRIMONIAL CLERK\n\n").MyStyleB();
                p1.AppendLine(pname + " v. " + dname + "\n").MyStyle();
                p1.AppendLine("Index No: " + dc.index_no).MyStyle();
                p2.AppendLine("A U T H O R I Z A T I O N").MyStyleB();
                p2.Alignment = Alignment.center;
                p3.AppendLine("\n\nI, " + pname + ", am the Plaintiff in the within matrimonial action and hereby authorize the County Clerk and the Matrimonial Clerk or his/her designed(s) to allow, " + ld.messenger + ", to do the following on my behalf, in connection with my matrimonial action:\n").MyStyle();
                p3.AppendLine("1. To file the necessary papers in connection with my matrimonial action.\n").MyStyle();
                p3.AppendLine("2. To examine, inspect and/or make copies of said papers.\n").MyStyle();
                p3.AppendLine("3. To obtain a certified copy of the final judgment.\n\n\n").MyStyle();
                p3.AppendLine("\t\t\t\t\t\t____________________________________").MyStyle();
                p3.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                p3.AppendLine("Subscribed and sworn to before me\t\tPlaintiff Pro-se").MyStyle();
                p3.AppendLine("on").MyStyle();
                p3.AppendLine();
                p3.AppendLine("__________________________").MyStyle();
                p3.AppendLine("\t    Notary Public").MyStyle();
                p3.AppendLine("My commission expires on").MyStyle();
                document.Save();
            }
        }
        public static void Divorce_Part130Certification(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Part_130_Certification.docx"))
            {
                File.Delete(folder + "\\Part_130_Certification.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Part_130_Certification.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tPART 130 CERTIFICATION").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X\n").MyStyle();
                p2.AppendLine("CERTIFICATION:  I, " + dc.p.name.ToTitleCase() + ", am the Plaintiff Pro-se, and I hereby certify that all of the papers that I have served, filed or submitted to the Court in this divorce action are not frivolous as defined in subsection (c) of Sec. 130-1.1 of the Rules of the Chief Administrator of the Courts.").MyStyle();
                p2.Alignment = Alignment.both;
                p3.AppendLine("\nDated:").MyStyle();
                p3.AppendLine("\n\n\t\t\t\t\t\t____________________________________").MyStyle();
                p3.AppendLine("\t\t\t\t\t\t" + dc.p.name.ToTitleCase()).MyStyle();
                p3.AppendLine("Subscribed and sworn to before me\t\t\t\tPlaintiff Pro-se").MyStyle();
                p3.AppendLine("\n__________________________").MyStyle();
                p3.AppendLine("\tNotary Public").MyStyle();
                p3.AppendLine("My commission expires on").MyStyle();
                p3.AppendLine("\n(Form UD-12 - 5/99)").MyStyle();
                document.Save();
            }
        }
        public static void Divorce_RemovalOfBarriers(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Removal_Of_Barriers.docx"))
            {
                File.Delete(folder + "\\Removal_Of_Barriers.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Removal_Of_Barriers.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\tSWORN STATEMENT OF REMOVAL").MyStyleB();
                p1.AppendLine("\t\t\t\t\t\tOF BARRIERS TO REMARRIAGE").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X").MyStyle();
                p1.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                p2.AppendLine("I, " + pname + ", state under penalty of perjury that the parties' marriage was solemnized by a minister, clergyman or leader of the Society of Ethical Culture, and that to the best of my knowledge I have taken all steps solely within my power to remove any barriers to the Defendant's remarriage following the divorce.").MyStyle();
                p2.Alignment = Alignment.both;
                p3.AppendLine("\n\n\n\t\t\t\t\t\t____________________________________").MyStyle();
                p3.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                p3.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                p3.AppendLine("on").MyStyle();
                p3.AppendLine("\n__________________________").MyStyle();
                p3.AppendLine("\t    Notary Public").MyStyle();
                p3.AppendLine("My commission expires on").MyStyle();
                p3.AppendLine("\n(Form UD-4 - 5/99)").MyStyle();
                document.Save();
            }
        }
        public static void Divorce_NoSocialPlaintiff(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Affidavit_of_NoSSN_Plaintiff.docx"))
            {
                File.Delete(folder + "\\Affidavit_of_NoSSN_Plaintiff.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Affidavit_of_NoSSN_Plaintiff.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                p1.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                p1.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                p1.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                p1.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                p1.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                p1.AppendLine("\n\t3. The Plaintiff does not currently have nor has the Plaintiff ever been issued a Social Security Card or Number.").MyStyle();
                p1.AppendLine("\n\t4. The Plaintiff does not anticipate obtaining a Social Security Card or Number in the near future.").MyStyle();
                p2.AppendLine("\tWHEREFORE").MyStyleB();
                p2.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing the Plaintiff’s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                p2.Alignment = Alignment.both;
                p3.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                p3.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                p3.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                p3.AppendLine("on").MyStyle();
                p3.AppendLine("\n__________________________").MyStyle();
                p3.AppendLine("\t    Notary Public").MyStyle();
                p3.AppendLine("My commission expires on").MyStyle();
                document.Save();
            }
        }
        public static void Divorce_NoSocialChildren(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Affidavit_of_NoSSN_Children.docx"))
            {
                File.Delete(folder + "\\Affidavit_of_NoSSN_Children.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Affidavit_of_NoSSN_Children.docx"))
            {
                Paragraph p1;
                Paragraph p2;
                Paragraph p3;
                Paragraph p4;
                Paragraph p5;
                Paragraph p6;
                Paragraph p7;
                Paragraph p8;
                Paragraph p9;
                Paragraph p10;
                Paragraph p11;
                Paragraph p12;
                Paragraph p13;
                Paragraph p14;
                Paragraph p15;
                Paragraph p16;
                Paragraph p17;
                Paragraph p18;
                int no_of_children = 0;
                Int32.TryParse(dc.ch.no_children, out no_of_children);
                if (no_of_children > 0)
                {
                    p1 = document.InsertParagraph();
                    p2 = document.InsertParagraph();
                    p3 = document.InsertParagraph();
                    p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p1.AppendLine(pname).MyStyle();
                    p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p1.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                    p1.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                    p1.AppendLine("\t\t-against-\n").MyStyle();
                    p1.AppendLine(dname).MyStyle();
                    p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p1.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                    p1.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                    p1.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                    p1.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                    p1.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                    p1.AppendLine("\n\t3. The Child of the marriage namely, " + dc.ch.c1_name.ToTitleCase() + ", born " + dc.ch.c1_dob.ToDateLong() + " does not currently have nor has " + dc.ch.c1_name.ToTitleCase() + " ever been issued a Social Security Card or Number.").MyStyle();
                    p1.AppendLine("\n\t4. The Plaintiff does not anticipate that the child will obtain a Social Security Card or Number in the near future.").MyStyle();
                    p2.AppendLine("\tWHEREFORE").MyStyleB();
                    p2.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing " + dc.ch.c1_name.ToTitleCase() + "'s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                    p2.Alignment = Alignment.both;
                    p3.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                    p3.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p3.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                    p3.AppendLine("on").MyStyle();
                    p3.AppendLine("\n__________________________").MyStyle();
                    p3.AppendLine("\t    Notary Public").MyStyle();
                    p3.AppendLine("My commission expires on").MyStyle();
                }
                if (no_of_children > 1)
                {
                    p4 = document.InsertParagraph();
                    p5 = document.InsertParagraph();
                    p6 = document.InsertParagraph();
                    p4.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p4.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p4.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p4.AppendLine(pname).MyStyle();
                    p4.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p4.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                    p4.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                    p4.AppendLine("\t\t-against-\n").MyStyle();
                    p4.AppendLine(dname).MyStyle();
                    p4.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p4.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                    p4.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                    p4.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                    p4.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                    p4.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                    p4.AppendLine("\n\t3. The Child of the marriage namely, " + dc.ch.c2_name.ToTitleCase() + ", born " + dc.ch.c2_dob.ToDateLong() + " does not currently have nor has " + dc.ch.c2_name.ToTitleCase() + " ever been issued a Social Security Card or Number.").MyStyle();
                    p4.AppendLine("\n\t4. The Plaintiff does not anticipate that the child will obtain a Social Security Card or Number in the near future.").MyStyle();
                    p5.AppendLine("\tWHEREFORE").MyStyleB();
                    p5.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing " + dc.ch.c2_name.ToTitleCase() + "'s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                    p5.Alignment = Alignment.both;
                    p6.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                    p6.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p6.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                    p6.AppendLine("on").MyStyle();
                    p6.AppendLine("\n__________________________").MyStyle();
                    p6.AppendLine("\t    Notary Public").MyStyle();
                    p6.AppendLine("My commission expires on").MyStyle();
                }
                if (no_of_children > 2)
                {
                    p7 = document.InsertParagraph();
                    p8 = document.InsertParagraph();
                    p9 = document.InsertParagraph();
                    p7.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p7.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p7.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p7.AppendLine(pname).MyStyle();
                    p7.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p7.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                    p7.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                    p7.AppendLine("\t\t-against-\n").MyStyle();
                    p7.AppendLine(dname).MyStyle();
                    p7.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p7.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                    p7.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                    p7.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                    p7.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                    p7.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                    p7.AppendLine("\n\t3. The Child of the marriage namely, " + dc.ch.c3_name.ToTitleCase() + ", born " + dc.ch.c3_dob.ToDateLong() + " does not currently have nor has " + dc.ch.c3_name.ToTitleCase() + " ever been issued a Social Security Card or Number.").MyStyle();
                    p7.AppendLine("\n\t4. The Plaintiff does not anticipate that the child will obtain a Social Security Card or Number in the near future.").MyStyle();
                    p8.AppendLine("\tWHEREFORE").MyStyleB();
                    p8.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing " + dc.ch.c3_name.ToTitleCase() + "'s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                    p8.Alignment = Alignment.both;
                    p9.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                    p9.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p9.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                    p9.AppendLine("on").MyStyle();
                    p9.AppendLine("\n__________________________").MyStyle();
                    p9.AppendLine("\t    Notary Public").MyStyle();
                    p9.AppendLine("My commission expires on").MyStyle();
                }
                if (no_of_children > 3)
                {
                    p10 = document.InsertParagraph();
                    p11 = document.InsertParagraph();
                    p12 = document.InsertParagraph();
                    p10.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p10.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p10.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p10.AppendLine(pname).MyStyle();
                    p10.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p10.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                    p10.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                    p10.AppendLine("\t\t-against-\n").MyStyle();
                    p10.AppendLine(dname).MyStyle();
                    p10.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p10.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                    p10.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                    p10.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                    p10.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                    p10.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                    p10.AppendLine("\n\t3. The Child of the marriage namely, " + dc.ch.c4_name.ToTitleCase() + ", born " + dc.ch.c4_dob.ToDateLong() + " does not currently have nor has " + dc.ch.c4_name.ToTitleCase() + " ever been issued a Social Security Card or Number.").MyStyle();
                    p10.AppendLine("\n\t4. The Plaintiff does not anticipate that the child will obtain a Social Security Card or Number in the near future.").MyStyle();
                    p11.AppendLine("\tWHEREFORE").MyStyleB();
                    p11.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing " + dc.ch.c4_name.ToTitleCase() + "'s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                    p11.Alignment = Alignment.both;
                    p12.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                    p12.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p12.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                    p12.AppendLine("on").MyStyle();
                    p12.AppendLine("\n__________________________").MyStyle();
                    p12.AppendLine("\t    Notary Public").MyStyle();
                    p12.AppendLine("My commission expires on").MyStyle();
                }
                if (no_of_children > 4)
                {
                    p13 = document.InsertParagraph();
                    p14 = document.InsertParagraph();
                    p15 = document.InsertParagraph();
                    p13.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p13.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p13.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p13.AppendLine(pname).MyStyle();
                    p13.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p13.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                    p13.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                    p13.AppendLine("\t\t-against-\n").MyStyle();
                    p13.AppendLine(dname).MyStyle();
                    p13.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p13.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                    p13.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                    p13.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                    p13.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                    p13.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                    p13.AppendLine("\n\t3. The Child of the marriage namely, " + dc.ch.c5_name.ToTitleCase() + ", born " + dc.ch.c5_dob.ToDateLong() + " does not currently have nor has " + dc.ch.c5_name.ToTitleCase() + " ever been issued a Social Security Card or Number.").MyStyle();
                    p13.AppendLine("\n\t4. The Plaintiff does not anticipate that the child will obtain a Social Security Card or Number in the near future.").MyStyle();
                    p14.AppendLine("\tWHEREFORE").MyStyleB();
                    p14.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing " + dc.ch.c5_name.ToTitleCase() + "'s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                    p14.Alignment = Alignment.both;
                    p15.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                    p15.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p15.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                    p15.AppendLine("on").MyStyle();
                    p15.AppendLine("\n__________________________").MyStyle();
                    p15.AppendLine("\t    Notary Public").MyStyle();
                    p15.AppendLine("My commission expires on").MyStyle();
                }
                if (no_of_children > 5)
                {
                    p16 = document.InsertParagraph();
                    p17 = document.InsertParagraph();
                    p18 = document.InsertParagraph();
                    p16.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p16.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p16.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p16.AppendLine(pname).MyStyle();
                    p16.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p16.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                    p16.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                    p16.AppendLine("\t\t-against-\n").MyStyle();
                    p16.AppendLine(dname).MyStyle();
                    p16.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p16.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                    p16.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                    p16.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                    p16.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                    p16.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                    p16.AppendLine("\n\t3. The Child of the marriage namely, " + dc.ch.c6_name.ToTitleCase() + ", born " + dc.ch.c6_dob.ToDateLong() + " does not currently have nor has " + dc.ch.c6_name.ToTitleCase() + " ever been issued a Social Security Card or Number.").MyStyle();
                    p16.AppendLine("\n\t4. The Plaintiff does not anticipate that the child will obtain a Social Security Card or Number in the near future.").MyStyle();
                    p17.AppendLine("\tWHEREFORE").MyStyleB();
                    p17.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing " + dc.ch.c6_name.ToTitleCase() + "'s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                    p17.Alignment = Alignment.both;
                    p18.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                    p18.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p18.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                    p18.AppendLine("on").MyStyle();
                    p18.AppendLine("\n__________________________").MyStyle();
                    p18.AppendLine("\t    Notary Public").MyStyle();
                    p18.AppendLine("My commission expires on").MyStyle();
                }
                document.Save();
            }
        }
        public static void Divorce_RefuseSocialChildren(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Affidavit_of_NoSSN_Children.docx"))
            {
                File.Delete(folder + "\\Affidavit_of_NoSSN_Children.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Affidavit_of_RefuseSSN_Children.docx"))
            {
                Paragraph p1;
                Paragraph p2;
                Paragraph p3;
                Paragraph p4;
                Paragraph p5;
                Paragraph p6;
                Paragraph p7;
                Paragraph p8;
                Paragraph p9;
                Paragraph p10;
                Paragraph p11;
                Paragraph p12;
                Paragraph p13;
                Paragraph p14;
                Paragraph p15;
                Paragraph p16;
                Paragraph p17;
                Paragraph p18;
                int no_of_children = 0;
                Int32.TryParse(dc.ch.no_children, out no_of_children);
                if (no_of_children > 0)
                {
                    p1 = document.InsertParagraph();
                    p2 = document.InsertParagraph();
                    p3 = document.InsertParagraph();
                    p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p1.AppendLine(pname).MyStyle();
                    p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p1.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                    p1.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                    p1.AppendLine("\t\t-against-\n").MyStyle();
                    p1.AppendLine(dname).MyStyle();
                    p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p1.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                    p1.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                    p1.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                    p1.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                    p1.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                    p1.AppendLine("\n\t3. The Child of the marriage namely, " + dc.ch.c1_name.ToTitleCase() + ", born " + dc.ch.c1_dob.ToDateLong() + " does have a Social Security Number but the custodial parent refuses to provide said number.").MyStyle();
                    p1.AppendLine("\n\t4. The Plaintiff does not anticipate that the child will obtain a Social Security Card or Number in the near future.").MyStyle();
                    p2.AppendLine("\tWHEREFORE").MyStyleB();
                    p2.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing " + dc.ch.c1_name.ToTitleCase() + "'s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                    p2.Alignment = Alignment.both;
                    p3.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                    p3.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p3.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                    p3.AppendLine("on").MyStyle();
                    p3.AppendLine("\n__________________________").MyStyle();
                    p3.AppendLine("\t    Notary Public").MyStyle();
                    p3.AppendLine("My commission expires on").MyStyle();
                }
                if (no_of_children > 1)
                {
                    p4 = document.InsertParagraph();
                    p5 = document.InsertParagraph();
                    p6 = document.InsertParagraph();
                    p4.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p4.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p4.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p4.AppendLine(pname).MyStyle();
                    p4.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p4.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                    p4.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                    p4.AppendLine("\t\t-against-\n").MyStyle();
                    p4.AppendLine(dname).MyStyle();
                    p4.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p4.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                    p4.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                    p4.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                    p4.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                    p4.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                    p4.AppendLine("\n\t3. The Child of the marriage namely, " + dc.ch.c2_name.ToTitleCase() + ", born " + dc.ch.c2_dob.ToDateLong() + " does have a Social Security Number but the custodial parent refuses to provide said number.").MyStyle();
                    p4.AppendLine("\n\t4. The Plaintiff does not anticipate that the child will obtain a Social Security Card or Number in the near future.").MyStyle();
                    p5.AppendLine("\tWHEREFORE").MyStyleB();
                    p5.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing " + dc.ch.c2_name.ToTitleCase() + "'s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                    p5.Alignment = Alignment.both;
                    p6.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                    p6.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p6.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                    p6.AppendLine("on").MyStyle();
                    p6.AppendLine("\n__________________________").MyStyle();
                    p6.AppendLine("\t    Notary Public").MyStyle();
                    p6.AppendLine("My commission expires on").MyStyle();
                }
                if (no_of_children > 2)
                {
                    p7 = document.InsertParagraph();
                    p8 = document.InsertParagraph();
                    p9 = document.InsertParagraph();
                    p7.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p7.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p7.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p7.AppendLine(pname).MyStyle();
                    p7.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p7.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                    p7.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                    p7.AppendLine("\t\t-against-\n").MyStyle();
                    p7.AppendLine(dname).MyStyle();
                    p7.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p7.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                    p7.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                    p7.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                    p7.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                    p7.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                    p7.AppendLine("\n\t3. The Child of the marriage namely, " + dc.ch.c3_name.ToTitleCase() + ", born " + dc.ch.c3_dob.ToDateLong() + " does have a Social Security Number but the custodial parent refuses to provide said number.").MyStyle();
                    p7.AppendLine("\n\t4. The Plaintiff does not anticipate that the child will obtain a Social Security Card or Number in the near future.").MyStyle();
                    p8.AppendLine("\tWHEREFORE").MyStyleB();
                    p8.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing " + dc.ch.c3_name.ToTitleCase() + "'s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                    p8.Alignment = Alignment.both;
                    p9.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                    p9.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p9.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                    p9.AppendLine("on").MyStyle();
                    p9.AppendLine("\n__________________________").MyStyle();
                    p9.AppendLine("\t    Notary Public").MyStyle();
                    p9.AppendLine("My commission expires on").MyStyle();
                }
                if (no_of_children > 3)
                {
                    p10 = document.InsertParagraph();
                    p11 = document.InsertParagraph();
                    p12 = document.InsertParagraph();
                    p10.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p10.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p10.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p10.AppendLine(pname).MyStyle();
                    p10.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p10.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                    p10.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                    p10.AppendLine("\t\t-against-\n").MyStyle();
                    p10.AppendLine(dname).MyStyle();
                    p10.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p10.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                    p10.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                    p10.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                    p10.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                    p10.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                    p10.AppendLine("\n\t3. The Child of the marriage namely, " + dc.ch.c4_name.ToTitleCase() + ", born " + dc.ch.c4_dob.ToDateLong() + " does have a Social Security Number but the custodial parent refuses to provide said number.").MyStyle();
                    p10.AppendLine("\n\t4. The Plaintiff does not anticipate that the child will obtain a Social Security Card or Number in the near future.").MyStyle();
                    p11.AppendLine("\tWHEREFORE").MyStyleB();
                    p11.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing " + dc.ch.c4_name.ToTitleCase() + "'s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                    p11.Alignment = Alignment.both;
                    p12.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                    p12.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p12.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                    p12.AppendLine("on").MyStyle();
                    p12.AppendLine("\n__________________________").MyStyle();
                    p12.AppendLine("\t    Notary Public").MyStyle();
                    p12.AppendLine("My commission expires on").MyStyle();
                }
                if (no_of_children > 4)
                {
                    p13 = document.InsertParagraph();
                    p14 = document.InsertParagraph();
                    p15 = document.InsertParagraph();
                    p13.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p13.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p13.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p13.AppendLine(pname).MyStyle();
                    p13.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p13.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                    p13.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                    p13.AppendLine("\t\t-against-\n").MyStyle();
                    p13.AppendLine(dname).MyStyle();
                    p13.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p13.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                    p13.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                    p13.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                    p13.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                    p13.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                    p13.AppendLine("\n\t3. The Child of the marriage namely, " + dc.ch.c5_name.ToTitleCase() + ", born " + dc.ch.c5_dob.ToDateLong() + " does have a Social Security Number but the custodial parent refuses to provide said number.").MyStyle();
                    p13.AppendLine("\n\t4. The Plaintiff does not anticipate that the child will obtain a Social Security Card or Number in the near future.").MyStyle();
                    p14.AppendLine("\tWHEREFORE").MyStyleB();
                    p14.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing " + dc.ch.c5_name.ToTitleCase() + "'s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                    p14.Alignment = Alignment.both;
                    p15.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                    p15.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p15.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                    p15.AppendLine("on").MyStyle();
                    p15.AppendLine("\n__________________________").MyStyle();
                    p15.AppendLine("\t    Notary Public").MyStyle();
                    p15.AppendLine("My commission expires on").MyStyle();
                }
                if (no_of_children > 5)
                {
                    p16 = document.InsertParagraph();
                    p17 = document.InsertParagraph();
                    p18 = document.InsertParagraph();
                    p16.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p16.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p16.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p16.AppendLine(pname).MyStyle();
                    p16.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p16.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                    p16.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                    p16.AppendLine("\t\t-against-\n").MyStyle();
                    p16.AppendLine(dname).MyStyle();
                    p16.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p16.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                    p16.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                    p16.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:").MyStyle();
                    p16.AppendLine("\n\t1. I am the Plaintiff in the above captioned action.").MyStyle();
                    p16.AppendLine("\n\t2. This is a matrimonial action.").MyStyle();
                    p16.AppendLine("\n\t3. The Child of the marriage namely, " + dc.ch.c6_name.ToTitleCase() + ", born " + dc.ch.c6_dob.ToDateLong() + " does have a Social Security Number but the custodial parent refuses to provide said number.").MyStyle();
                    p16.AppendLine("\n\t4. The Plaintiff does not anticipate that the child will obtain a Social Security Card or Number in the near future.").MyStyle();
                    p17.AppendLine("\tWHEREFORE").MyStyleB();
                    p17.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing " + dc.ch.c6_name.ToTitleCase() + "'s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                    p17.Alignment = Alignment.both;
                    p18.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                    p18.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p18.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                    p18.AppendLine("on").MyStyle();
                    p18.AppendLine("\n__________________________").MyStyle();
                    p18.AppendLine("\t    Notary Public").MyStyle();
                    p18.AppendLine("My commission expires on").MyStyle();
                }
                document.Save();
            }
        }
        public static void Divorce_NoSocialDefendant(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Affidavit_of_NoSSN_Defendant.docx"))
            {
                File.Delete(folder + "\\Affidavit_of_NoSSN_Defendant.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Affidavit_of_NoSSN_Defendant.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                p1.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                p1.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                if (dc.waiver_default == "2")
                {
                    p1.AppendLine("\tI, " + pname + ", am the Plaintiff, being duly sworn, depose and say that:\n").MyStyle();
                    p1.AppendLine("\t1. I am the Plaintiff in the above captioned action.\n").MyStyle();
                }
                else
                {
                    p1.AppendLine("\tI, " + dname + ", am the Defendant, being duly sworn, depose and say that:\n").MyStyle();
                    p1.AppendLine("\t1. I am the Defendant in the above captioned action.\n").MyStyle();
                }
                p1.AppendLine("\t2. This is a matrimonial action.\n").MyStyle();
                p1.AppendLine("\t3. The Defendant does not currently have nor has the Defendant ever been issued a Social Security Card or Number.\n").MyStyle();
                p1.AppendLine("\t4. The Defendant does not anticipate obtaining a Social Security Card or Number in the near future. \n").MyStyle();
                p2.Append("\tWHEREFORE").MyStyleB();
                p2.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing the Defendant’s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                p2.Alignment = Alignment.both;
                p3.AppendLine("\n\n\t\t\t\t\t\t___________________________________").MyStyle();
                if (dc.waiver_default == "2")
                {
                    p3.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                    p3.AppendLine("\t\t\t\t\t\tPlaintiff Pro-se").MyStyle();
                }
                else
                {
                    p3.AppendLine("\t\t\t\t\t\t" + dname).MyStyle();
                    p3.AppendLine("\t\t\t\t\t\tDefendant").MyStyle();
                }
                p3.AppendLine("Subscribed and sworn to before me").MyStyle();
                p3.AppendLine("on").MyStyle();
                p3.AppendLine("\n__________________________").MyStyle();
                p3.AppendLine("\t    Notary Public").MyStyle();
                p3.AppendLine("My commission expires on").MyStyle();
                document.Save();
            }
        }
        public static void Divorce_RefuseSocialDefendant(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Affidavit_Of_RefuseSSN_Defendant.docx"))
            {
                File.Delete(folder + "\\Affidavit_Of_RefuseSSN_Defendant.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Affidavit_Of_RefuseSSN_Defendant.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\t\tAFFIDAVIT OF NO SOCIAL").MyStyleB();
                p1.AppendLine("\t\t\t\t\t\t\t\t      SECURITY NUMBER").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X\n\n").MyStyle();
                p1.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                p1.AppendLine("\tI, " + pname + ", the Plaintiff, being duly sworn, depose and say that:\n").MyStyle();
                p1.AppendLine("\t1. I am the Plaintiff in the above captioned action.\n").MyStyle();
                p1.AppendLine("\t2. This is a matrimonial action.\n").MyStyle();
                p1.AppendLine("\t3. The Defendant does have a Social Security Number but refuses to provide said number to the Plaintiff Pro-se.\n\n").MyStyle();
                p2.Append("\tWHEREFORE").MyStyleB();
                p2.Append(", I respectfully request this court excuse " + pname + ", the Plaintiff, for not providing the Defendant’s Social Security Number and grant the requested relief sought herein and such other and further relief as this Court may deem just and proper.").MyStyle();
                p2.Alignment = Alignment.both;
                p3.AppendLine("\n\n\t\t\t\t\t\t______________________________________").MyStyle();
                p3.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                p3.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                p3.AppendLine("on").MyStyle();
                p3.AppendLine();
                p3.AppendLine("__________________________").MyStyle();
                p3.AppendLine("\t    Notary Public").MyStyle();
                p3.AppendLine("My commission expires on").MyStyle();
                document.Save();
            }
        }
        public static void Divorce_CertificateofDissolution(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Certificate_of_Dissolution.pdf"))
            {
                File.Delete(folder + "\\Certificate_of_Dissolution.pdf");
            }
            string pdfTemplate = @"D:\home\GoNyLaw\Documents\certificate_of_dissolution.pdf";
            string newFile = folder + @"\Certificate_of_Dissolution.pdf";
            PdfReader pdfReader = new PdfReader(pdfTemplate);
            PdfReader.unethicalreading = true;
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
            AcroFields pdfFormFields = pdfStamper.AcroFields;
            string[] pname_parts = dc.p.name.ToTitleCase().Split(' ');
            string[] dname_parts = dc.d.name.ToTitleCase().Split(' ');
            string p_first = "", p_middle = "", p_last = "", d_first = "", d_middle = "", d_last = "";
            string p_education = "", d_education = "";
            if (dc.p.education == "E0")
                p_education = "00";
            else if (dc.p.education == "E1")
                p_education = "01";
            else if (dc.p.education == "E2")
                p_education = "02";
            else if (dc.p.education == "E3")
                p_education = "03";
            else if (dc.p.education == "E4")
                p_education = "04";
            else if (dc.p.education == "E5")
                p_education = "05";
            else if (dc.p.education == "E6")
                p_education = "06";
            else if (dc.p.education == "E7")
                p_education = "07";
            else if (dc.p.education == "E8")
                p_education = "08";
            else if (dc.p.education == "H1")
                p_education = "09";
            else if (dc.p.education == "H2")
                p_education = "10";
            else if (dc.p.education == "H3")
                p_education = "11";
            else if (dc.p.education == "H4")
                p_education = "12";
            else if (dc.p.education == "C1")
                p_education = "13";
            else if (dc.p.education == "C2")
                p_education = "14";
            else if (dc.p.education == "C3")
                p_education = "15";
            else if (dc.p.education == "C4")
                p_education = "16";
            else if (dc.p.education == "C5")
                p_education = "17";
            if (dc.d.education == "E0")
                d_education = "00";
            else if (dc.d.education == "E1")
                d_education = "01";
            else if (dc.d.education == "E2")
                d_education = "02";
            else if (dc.d.education == "E3")
                d_education = "03";
            else if (dc.d.education == "E4")
                d_education = "04";
            else if (dc.d.education == "E5")
                d_education = "05";
            else if (dc.d.education == "E6")
                d_education = "06";
            else if (dc.d.education == "E7")
                d_education = "07";
            else if (dc.d.education == "E8")
                d_education = "08";
            else if (dc.d.education == "H1")
                d_education = "09";
            else if (dc.d.education == "H2")
                d_education = "10";
            else if (dc.d.education == "H3")
                d_education = "11";
            else if (dc.d.education == "H4")
                d_education = "12";
            else if (dc.d.education == "C1")
                d_education = "13";
            else if (dc.d.education == "C2")
                d_education = "14";
            else if (dc.d.education == "C3")
                d_education = "15";
            else if (dc.d.education == "C4")
                d_education = "16";
            else if (dc.d.education == "C5")
                d_education = "17";
            p_first = pname_parts[0];
            if (pname_parts.Length == 2)
            {
                p_middle = "";
                p_last = pname_parts[1];
            }
            if (pname_parts.Length > 2)
            {
                p_middle = pname_parts[1];
                for (int i = 2; i < pname_parts.Length; i++)
                {
                    if (string.IsNullOrWhiteSpace(p_last))
                        p_last += pname_parts[i];
                    else
                        p_last += (" " + pname_parts[i]);
                }
            }
            d_first = dname_parts[0];
            if (dname_parts.Length == 2)
            {
                d_middle = "";
                d_last = dname_parts[1];
            }
            if (dname_parts.Length > 2)
            {
                d_middle = dname_parts[1];
                for (int i = 2; i < dname_parts.Length; i++)
                {
                    if (string.IsNullOrWhiteSpace(d_last))
                        d_last += dname_parts[i];
                    else
                        d_last += (" " + dname_parts[i]);
                }
            }
            pdfFormFields.SetFieldProperty("2168.A.Name.First", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.Name.Middle", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.Name.Last", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.SSN", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.DOB.Month", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.DOB.Day", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.DOB.Year", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.Birth.State", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.Sex", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.Residence.State", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.Residence.County", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.Residence.Locality.Name", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.Residence.Street", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.A.Attorney.Name", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Name.First", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Name.Middle", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Name.Last", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.SSN", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.DOB.Month", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.DOB.Day", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.DOB.Year", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Birth.State", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Sex", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Residence.State", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Residence.County", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Residence.Locality.CTV", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Residence.Locality.Name", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Residence.Street", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Residence.CTV.Limits", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Attorney.Name", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.B.Attorney.Address", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.Marriage.CTV", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.Marriage.County", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.Marriage.State", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.Marriage.DOM.Month", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.Marriage.DOM.Day", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.Marriage.DOM.Year", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.Marriage.Children.Born", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.Marriage.Children.Under18", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.Marriage.Decree.Type", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.Marriage.Decree.County", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.Marriage.Decree.Court", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.CI.C.Race", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.CI.C.Marriage.Number", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.CI.C.Education", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.CI.D.Race", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.CI.D.Marriage.Number", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.CI.D.Education", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.CI.Plaintiff", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.CI.Granted", "textsize", 10f, null);
            pdfFormFields.SetFieldProperty("2168.CI.Grounds", "textsize", 10f, null);
            pdfFormFields.SetField("2168.A.Name.First", p_first);
            pdfFormFields.SetField("2168.A.Name.Middle", p_middle);
            pdfFormFields.SetField("2168.A.Name.Last", p_last);
            pdfFormFields.SetField("2168.A.Name.Birth", "");
            pdfFormFields.SetField("2168.A.SSN", dc.p.ssn);
            pdfFormFields.SetField("2168.A.DOB.Month", dc.p.dob.Substring(0, 2));
            pdfFormFields.SetField("2168.A.DOB.Day", dc.p.dob.Substring(3, 2));
            pdfFormFields.SetField("2168.A.DOB.Year", dc.p.dob.Substring(6, 4));
            pdfFormFields.SetField("2168.A.Birth.State", dc.p.place_birth.ToTitleCase());
            pdfFormFields.SetField("2168.A.Sex", dc.p.sex);
            pdfFormFields.SetField("2168.A.Residence.State", dc.p.state.ToStateName());
            pdfFormFields.SetField("2168.A.Residence.County", dc.p.county.ToTitleCase());
            pdfFormFields.SetField("2168.A.Residence.Locality.CTV", "City");
            pdfFormFields.SetField("2168.A.Residence.Locality.Name", dc.p.city.ToTitleCase());
            if (dc.p.country == "US")
                pdfFormFields.SetField("2168.A.Residence.Street", dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state + "-" + dc.p.zip);
            else
                pdfFormFields.SetField("2168.A.Residence.Street", dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.country.ToCountryName());
            pdfFormFields.SetField("2168.A.Residence.CTV.Limits", "Yes");
            pdfFormFields.SetField("2168.A.Residence.Locality.Town", "");
            pdfFormFields.SetField("2168.A.Attorney.Name", "Pro-Se");
            pdfFormFields.SetField("2168.A.Attorney.Address", "");
            pdfFormFields.SetField("2168.B.Name.First", d_first);
            pdfFormFields.SetField("2168.B.Name.Middle", d_middle);
            pdfFormFields.SetField("2168.B.Name.Last", d_last);
            pdfFormFields.SetField("2168.B.Name.Birth", "");
            pdfFormFields.SetField("2168.B.SSN", dc.d.ssn);
            pdfFormFields.SetField("2168.B.DOB.Month", dc.d.dob.Substring(0, 2));
            pdfFormFields.SetField("2168.B.DOB.Day", dc.d.dob.Substring(3, 2));
            pdfFormFields.SetField("2168.B.DOB.Year", dc.d.dob.Substring(6, 4));
            pdfFormFields.SetField("2168.B.Birth.State", dc.d.place_birth.ToTitleCase());
            pdfFormFields.SetField("2168.B.Sex", dc.d.sex);
            pdfFormFields.SetField("2168.B.Residence.State", dc.d.state.ToStateName());
            pdfFormFields.SetField("2168.B.Residence.County", dc.d.county.ToTitleCase());
            pdfFormFields.SetField("2168.B.Residence.Locality.CTV", "City");
            pdfFormFields.SetField("2168.B.Residence.Locality.Name", dc.d.city.ToTitleCase());
            if (dc.d.country == "US")
                pdfFormFields.SetField("2168.B.Residence.Street", dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state + "-" + dc.d.zip);
            else
                pdfFormFields.SetField("2168.B.Residence.Street", dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName());
            pdfFormFields.SetField("2168.B.Residence.CTV.Limits", "Yes");
            pdfFormFields.SetField("2168.B.Residence.Locality.Town", "");
            pdfFormFields.SetField("2168.B.Attorney.Name", "NONE");
            pdfFormFields.SetField("2168.B.Attorney.Address", "NONE");
            pdfFormFields.SetField("2168.Marriage.CTV", dc.m.city.ToTitleCase());
            pdfFormFields.SetField("2168.Marriage.County", dc.m.county.ToTitleCase());
            pdfFormFields.SetField("2168.Marriage.State", dc.m.state.ToStateName());
            pdfFormFields.SetField("2168.Marriage.DOM.Month", dc.m.marriage_date.Substring(0, 2));
            pdfFormFields.SetField("2168.Marriage.DOM.Day", dc.m.marriage_date.Substring(3, 2));
            pdfFormFields.SetField("2168.Marriage.DOM.Year", dc.m.marriage_date.Substring(6, 4));
            pdfFormFields.SetField("2168.Marriage.Separation.Monty", "");
            pdfFormFields.SetField("2168.Marriage.Separation.Year", "");
            if (dc.casetype == "2")
            {
                pdfFormFields.SetField("2168.Marriage.Children.Born", dc.ch.no_children);
                pdfFormFields.SetField("2168.Marriage.Children.Under18", dc.ch.no_children);
            }
            else
            {
                pdfFormFields.SetField("2168.Marriage.Children.Born", "0");
                pdfFormFields.SetField("2168.Marriage.Children.Under18", "0");
            }
            pdfFormFields.SetField("2168.Marriage.Decree.Rendered.Monty", "");
            pdfFormFields.SetField("2168.Marriage.Decree.Rendered.Day", "");
            pdfFormFields.SetField("2168.Marriage.Decree.Rendered.Year", "");
            pdfFormFields.SetField("2168.Marriage.Decree.Entered.Month", "");
            pdfFormFields.SetField("2168.Marriage.Decree.Entered.Day", "");
            pdfFormFields.SetField("2168.Marriage.Decree.Entered.Year", "");
            pdfFormFields.SetField("2168.Marriage.Decree.Type", "UNCONTESTED DIVORCE");
            pdfFormFields.SetField("2168.Marriage.Decree.County", "NEW YORK");
            pdfFormFields.SetField("2168.Marriage.Decree.Court", "SUPREME COURT OF THE STATE OF NEW YORK");
            if (dc.p.race != "O")
                pdfFormFields.SetField("2168.CI.C.Race", DropDownData.race.Where(x => x.Value == dc.p.race).FirstOrDefault().Text);
            else
                pdfFormFields.SetField("2168.CI.C.Race", dc.p.race.ToTitleCase());
            pdfFormFields.SetField("2168.CI.C.Marriage.Number", DropDownData.nomarriage.Where(x => x.Value == dc.p.marriage_no).FirstOrDefault().Text.ToUpperCase());
            pdfFormFields.SetField("2168.CI.C.Marriage.Death.Number", "");
            pdfFormFields.SetField("2168.CI.C.Marriage.Death.None", "");
            pdfFormFields.SetField("2168.CI.C.Marriage.Dissolution.Number", "");
            pdfFormFields.SetField("2168.CI.C.Marriage.Dissolution.None", "");
            pdfFormFields.SetField("2168.CI.C.Education", p_education);
            if (dc.d.race != "O" && !string.IsNullOrEmpty(dc.d.race))
                pdfFormFields.SetField("2168.CI.D.Race", DropDownData.race.Where(x => x.Value == dc.d.race).FirstOrDefault().Text);
            else
                pdfFormFields.SetField("2168.CI.D.Race", dc.d.race.ToTitleCase());
            pdfFormFields.SetField("2168.CI.D.Marriage.Number", DropDownData.nomarriage.Where(x => x.Value == dc.d.marriage_no).FirstOrDefault().Text.ToUpperCase());
            pdfFormFields.SetField("2168.CI.D.Marriage.Death.Number", "");
            pdfFormFields.SetField("2168.CI.D.Marriage.Death.None", "");
            pdfFormFields.SetField("2168.CI.D.Marriage.Dissolution.Number", "");
            pdfFormFields.SetField("2168.CI.D.Marriage.Dissolution.None", "");
            pdfFormFields.SetField("2168.CI.D.Education", d_education);
            pdfFormFields.SetField("2168.CI.Plaintiff", dc.p.name.ToTitleCase());
            pdfFormFields.SetField("2168.CI.Granted", dc.p.name.ToTitleCase());
            if (dc.r.grounds == "1")
                pdfFormFields.SetField("2168.CI.Grounds", "DRL Section 170 subd. (7)");
            else if (dc.r.grounds == "2")
                pdfFormFields.SetField("2168.CI.Grounds", "DRL Section 170 subd. (2)");
            else if (dc.r.grounds == "3")
                pdfFormFields.SetField("2168.CI.Grounds", "DRL Section 170 subd. (2)");
            else if (dc.r.grounds == "4")
                pdfFormFields.SetField("2168.CI.Grounds", "DRL Section 170 subd. (1)");
            pdfStamper.FormFlattening = false;
            pdfStamper.Close();
        }
        public static void Divorce_RJI(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\RJI.pdf"))
            {
                File.Delete(folder + "\\RJI.pdf");
            }
            string pdfTemplate = @"D:\home\GoNyLaw\Documents\RJI.pdf";
            string newFile = folder + @"\RJI.pdf";
            PdfReader pdfReader = new PdfReader(pdfTemplate);
            PdfReader.unethicalreading = true;
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
            AcroFields pdfFormFields = pdfStamper.AcroFields;
            int number_children = 0;
            if (dc.casetype == "2")
                Int32.TryParse(dc.ch.no_children, out number_children);
            string[] pname_parts = dc.p.name.ToTitleCase().Split(' ');
            string[] dname_parts = dc.d.name.ToTitleCase().Split(' ');
            string p_first = "", p_middle = "", p_last = "", d_first = "", d_middle = "", d_last = "";
            p_first = pname_parts[0];
            if (pname_parts.Length == 2)
            {
                p_middle = "";
                p_last = pname_parts[1];
            }
            if (pname_parts.Length > 2)
            {
                p_middle = pname_parts[1];
                for (int i = 2; i < pname_parts.Length; i++)
                {
                    if (string.IsNullOrWhiteSpace(p_last))
                        p_last += pname_parts[i];
                    else
                        p_last += (" " + pname_parts[i]);
                }
            }
            d_first = dname_parts[0];
            if (dname_parts.Length == 2)
            {
                d_middle = "";
                d_last = dname_parts[1];
            }
            if (dname_parts.Length > 2)
            {
                d_middle = dname_parts[1];
                for (int i = 2; i < dname_parts.Length; i++)
                {
                    if (string.IsNullOrWhiteSpace(d_last))
                        d_last += dname_parts[i];
                    else
                        d_last += (" " + dname_parts[i]);
                }
            }
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Court_Type[0]", "Supreme");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Court_County[0]", dc.county_venue.ToTitleCase());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Index_No[0]", dc.index_no);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Date_Index_Issued[0]", dc.index_date);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Caption_Plaintiff[0]", dc.p.name.ToUpperCase());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Caption_Defendant[0]", dc.d.name.ToUpperCase());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Summons_Filed[0]", "43");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Summons_Filed_Date[0]", dc.index_date);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Summons_Served[0]", "41");
            if (dc.date_summons.HasValue)
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Summons_Served_Date[0]", (dc.date_summons.Value).ToString("MM/dd/yyyy"));
            else
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Summons_Served_Date[0]", "");
            if (dc.casetype == "2" && number_children > 0)
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Children_Under_18[0]", "45");
            else
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Children_Under_18[0]", "46");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Other[0]", "1");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Other__specify[0]", "Uncontested Divorce");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Plaintiff_Unrepresented[0]", "1");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Plaintiff_Last_Name[0]", p_last);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Plaintiff_First_Name[0]", p_first);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Plaintiff_Middle_Name[0]", p_middle);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Plaintiff_Attorney_Street_Address[0]", dc.p.street.ToTitleCase());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Plaintiff_Attorney_City[0]", dc.p.city.ToTitleCase());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Plaintiff_Attorney_State[0]", dc.p.state.ToStateName());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Plaintiff_Attorney_ZipCode[0]", dc.p.zip);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Plaintiff_Attorney_Phone[0]", "");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Defendant_Unrepresented[0]", "1");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Defendant_Last_Name[0]", d_last);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Defendant_First_Name[0]", d_first);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Defendant_Middle_Name[0]", d_middle);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Defendant_Attorney_Street_Address[0]", dc.d.street.ToTitleCase());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Defendant_Attorney_City[0]", dc.d.city.ToTitleCase());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Defendant_Attorney_State[0]", dc.d.state.ToStateName());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Defendant_Attorney_ZipCode[0]", dc.d.zip);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Defendant_Attorney_Phone[0]", "");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Issue_Joined[0]", "2");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].PRINT_OR_TYPE_NAME[0]", dc.p.name.ToUpperCase());
            pdfStamper.FormFlattening = false;
            pdfStamper.Close();
        }



        //Divorce Without Children
        public static void Divorce1_Summons(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Summons_With_Notice.docx"))
            {
                File.Delete(folder + "\\Summons_With_Notice.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Summons_With_Notice.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = document.InsertParagraph();
                Paragraph p13 = document.InsertParagraph();
                Paragraph p14 = document.InsertParagraph();
                Paragraph p15 = document.InsertParagraph();
                Paragraph p16 = document.InsertParagraph();
                Paragraph p17 = document.InsertParagraph();
                Paragraph p18 = document.InsertParagraph();
                Paragraph p19 = document.InsertParagraph();
                Paragraph p20 = document.InsertParagraph();
                Paragraph p21 = document.InsertParagraph();
                Paragraph p22 = document.InsertParagraph();
                Paragraph p23 = document.InsertParagraph();
                Paragraph p24 = document.InsertParagraph();
                Paragraph p25 = document.InsertParagraph();
                Paragraph p26 = document.InsertParagraph();
                Paragraph p27 = document.InsertParagraph();
                Paragraph p28 = document.InsertParagraph();
                Paragraph p29 = document.InsertParagraph();
                Paragraph p30 = document.InsertParagraph();
                Paragraph p31 = document.InsertParagraph();
                Paragraph p32 = document.InsertParagraph();
                p1.Append("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("-------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine("\t\t\t\t\t\t  Date Summons Filed: " + dc.index_date.ToDateLong()).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\t\t\t\t\t\t      Plaintiff Designates " + venue.ToTitleCase() + " County").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t      as the place of trial.").MyStyle();
                if (dc.basis_venue == "P")
                    p1.AppendLine("\t\t\t\t\t\t      The basis of venue is Plaintiff's Residence").MyStyle();
                else if (dc.basis_venue == "D")
                    p1.AppendLine("\t\t\t\t\t\t      The basis of venue is Defendant's Residence").MyStyle();
                else
                    p1.AppendLine("\t\t\t\t\t\t      The basis of venue is " + basis).MyStyle();
                p1.AppendLine("\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tSUMMONS WITH NOTICE").MyStyleB();
                p1.AppendLine("\t\t-against-").MyStyle();
                if (dc.basis_venue == "D")
                {
                    p1.AppendLine("\t\t\t\t\t\t\tDefendant resides at").MyStyle();
                    if (dc.p.country == "US")
                    {
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.d.street.ToTitleCase()).MyStyle();
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " " + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.d.street.ToTitleCase()).MyStyle();
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.d.city + ", " + dc.d.country.ToCountryName()).MyStyle();
                    }
                }
                else
                {
                    p1.AppendLine("\t\t\t\t\t\t\tPlaintiff resides at").MyStyle();
                    if (dc.p.country == "US")
                    {
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.p.street.ToTitleCase()).MyStyle();
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.state.ToStateName() + " " + dc.p.zip).MyStyle();
                    }
                    else
                    {
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.p.street.ToTitleCase()).MyStyle();
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.country.ToCountryName()).MyStyle();
                    }
                }
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("--------------------------------------------------------X").MyStyle();
                p2.Append("\nACTION FOR DIVORCE").MyStyleB();
                p2.Alignment = Alignment.center;
                p3.AppendLine("\nTo the above named Defendant:").MyStyle();
                p4.Append("\nYOU ARE HEREBY SUMMONED ").MyStyleB();
                p4.Append("to serve a notice of appearance on the Plaintiff's Attorney within twenty (20) days after the service of this summons, exclusive of the day of service (or within thirty (30) days after the service is complete if this summons is not personally delivered to you within the State of New York); and in case of your failure to appear, judgment will be taken against you by default for the relief demanded in the notice set forth below.").MyStyle();
                p4.Alignment = Alignment.both;
                p5.AppendLine("\n\nDated: ").MyStyle();
                p5.AppendLine("\n\t\t\t\t\t\t__________________________________").MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + dc.p.name.ToTitleCase()).MyStyle();
                p5.AppendLine("\t\t\t\t\t\tPlaintiff Pro-se").MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + dc.p.street).MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + dc.p.city + ", " + dc.p.state.ToStateName() + " " + dc.p.zip).MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + dc.p.phone).MyStyle();
                p6.AppendLine("NOTICE:").MyStyleB();
                if (dc.r.grounds == "1")
                    p6.Append(" The nature of this action is to dissolve the marriage between the parties, on the grounds: DRL Section 170 subd. (7) - the relationship between Plaintiff and Defendant has broken down irretrievably for a period of at least six months.").MyStyle();
                else if (dc.r.grounds == "2")
                    p6.Append(" The nature of this action is to dissolve the marriage between the parties, on the grounds: DRL Section 170 subd. (2) - the abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                else if (dc.r.grounds == "3")
                    p6.Append(" The nature of this action is to dissolve the marriage between the parties, on the grounds: DRL Section 170 subd. (2) - the constructive abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                else if (dc.r.grounds == "4")
                    p6.Append(" The nature of this action is to dissolve the marriage between the parties, on the grounds: DRL Section 170 subd. (1) - the cruel and inhuman treatment of the Plaintiff by the Defendant.").MyStyle();
                p6.Alignment = Alignment.both;
                p7.InsertPageBreakBeforeSelf();
                p7.AppendLine("The relief sought is a judgment of absolute divorce in favor of the Plaintiff dissolving the marriage between the parties in this action. The nature of any ancillary or additional relief demanded is:").MyStyle();
                p7.IndentationBefore = 1.3f;
                p7.Alignment = Alignment.both;
                p8.AppendLine("That the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance and support.").MyStyle();
                p8.IndentationBefore = 1.3f;
                p8.Alignment = Alignment.both;
                p9.AppendLine("That the parties do not require maintenance and no claim will be made by either party for maintenance. I am not seeking maintenance as payee as described in the Notice of Guideline Maintenance.").MyStyle();
                p9.IndentationBefore = 1.3f;
                p9.Alignment = Alignment.both;
                p10.AppendLine("That the parties do not require payment of counsel and experts' fees and expenses.").MyStyle();
                p10.IndentationBefore = 1.3f;
                p10.Alignment = Alignment.both;
                p11.AppendLine("That both parties may resume the use of any prior surname.").MyStyle();
                p11.IndentationBefore = 1.3f;
                p11.Alignment = Alignment.both;
                p12.AppendLine("That the Court grant such other and further relief as the Court may deem fit and proper.").MyStyle();
                p12.IndentationBefore = 1.3f;
                p12.Alignment = Alignment.both;
                p13.AppendLine("The parties have divided up the marital property and no claim will be made by either party under equitable distribution.").MyStyle();
                p13.IndentationBefore = 1.3f;
                p13.Alignment = Alignment.both;
                p14.AppendLine("Notice of Automatic Orders pursuant to DRL Sec. 236(B)(2) and Notice Concerning Continuation of Health Care Coverage pursuant to DRL Sec. 255(1) and Notice of Guideline Maintenance pursuant to the Maintenance Guidelines Law ([S. 5678/A. 7645], Chapter 269, Laws of 2015) accompany this summons.").MyStyle();
                p14.Alignment = Alignment.both;
                p15.AppendLine("\n(Form UD-1 - 1/25/16)").MyStyle();
                p15.Alignment = Alignment.both;
                p16.InsertPageBreakBeforeSelf();
                p17.AppendLine("NOTICE OF ENTRY OF AUTOMATIC ORDERS (D.R.L. 236) Rev. 1/13").MyStyleB();
                p17.AppendLine("FAILURE TO COMPLY WITH THESE ORDERS MAY BE DEEMED").MyStyleB();
                p17.AppendLine("A CONTEMPT OF COURT").MyStyleB();
                p17.Alignment = Alignment.center;
                p18.Append("\n\nPURSUANT TO the Uniform Rules of the Trial Courts and DOMESTIC RELATIONS LAW Section 236, Part B, Section 2, both you and your spouse (the parties) are bound by the following").MyStyle();
                p18.Append(" AUTOMATIC ORDERS").MyStyleB();
                p18.Append(", which have been entered against you and your spouse in your divorce action pursuant to 22 NYCRR Section 202.16(a) and which shall  remain in full force and effect during the pendency of the action, unless terminated, modified or amended by further order of the court or upon written agreement between the parties:").MyStyle();
                p18.Alignment = Alignment.both;
                p19.AppendLine("(1)  ORDERED:  Neither party shall sell, transfer, encumber, conceal, assign, remove or in any way dispose of, without the consent of the other party in writing, or by order of the court, any property (including, but not limited to, real estate, personal property, cash accounts, stocks, mutual funds, bank accounts, cars and boats) individually or jointly held by the parties, except in the usual course of business, for customary and usual household expenses or for reasonable attorney's fee in connection with this action.").MyStyle();
                p19.Alignment = Alignment.both;
                p20.AppendLine("(2)  ORDERED:  Neither party shall transfer, encumber, assign, remove, withdraw or in any way dispose of any tax deferred funds, stocks or other assets held in any individual retirement accounts, 401k accounts, profit sharing plans, Keogh accounts, or any other pension or retirement account and the parties shall further refrain from applying for or requesting the payment of retirement benefits or annuity payments of any kind, without the consent of the other party in writing, or upon further order of the court; except that any party who is already in pay status may continue to receive such payments thereunder.").MyStyle();
                p20.Alignment = Alignment.both;
                p21.AppendLine("(3)  ORDERED:  Neither party shall incur unreasonable debts hereafter, including, but not limited to, further borrowing against any credit line secured by the family residence, further encumbrancing any assets, or unreasonably using credit cards or cash advances against credit cards, except in the usual course of business or for customary or usual housing expenses, or for reasonable attorney's fees in connection with this action.").MyStyle();
                p21.Alignment = Alignment.both;
                p22.AppendLine("(4)  ORDERED:  Neither party shall cause the other party or the children of the marriage to be removed from any existing medical, hospital and dental insurance coverage and each party shall maintain the existing medical, hospital and dental insurance coverage in full force and effect.").MyStyle();
                p22.Alignment = Alignment.both;
                p23.AppendLine("(5)  ORDERED:  Neither party shall change the beneficiaries of any existing life insurance policies and each party shall maintain the existing life insurance, automobile insurance, homeowners and renters insurance policies in full force and effect.").MyStyle();
                p23.Alignment = Alignment.both;
                p24.AppendLine("IMPORTANT NOTE:  ").MyStyleB();
                p24.Append("After service of Summons with Notice or Summons and Complaint for divorce, if you or your spouse wishes to modify or dissolve the automatic orders, you must ask the court for approval to do so, or enter into a written modification agreement with your spouse duly signed and acknowledged before a notary public.").MyStyle();
                p24.Alignment = Alignment.both;
                p25.InsertPageBreakBeforeSelf();
                p25.AppendLine("NOTICE CONCERNING CONTINUATION OF").MyStyleB();
                p25.AppendLine("HEALTH CARE COVERAGE").MyStyleB();
                p25.AppendLine("(Required by Section 255(1) of the Domestic Relations Law)").MyStyle();
                p25.Alignment = Alignment.center;
                p26.AppendLine("PLEASE TAKE NOTICE that once a judgment of divorce is signed in this action, both you and your spouse may or may not continue to be eligible for coverage under each other's health insurance plan, depending on the terms of the plan.").MyStyleB();
                p26.Alignment = Alignment.both;
                p27.InsertPageBreakBeforeSelf();
                p27.Append("NOTICE OF GUIDELINE MAINTENANCE").MyStyleB();
                p27.Alignment = Alignment.center;
                p29.AppendLine("If your divorce was commenced on or after January 25, 2016, this Notice is required to be given to you by the Supreme Court of the county where your divorce was filed to comply with the Maintenance Guidelines Law ([S. 5678/A. 7645], Chapter 269, Laws of 2015) because you may not have counsel in this action to advise you.").MyStyle();
                p29.Append(" It does not mean that your spouse (the person you are married to) is seeking or offering an award of \"Maintenance\" in this action. \"Maintenance\" means the amount to be paid to the other spouse for support after the divorce is final.").MyStyleB();
                p29.Alignment = Alignment.both;
                p30.AppendLine("You are hereby given notice that under the Maintenance Guidelines Law (Chapter 269, Laws of 2015), there is an obligation to award the guideline amount of maintenance on income up to $178,000 to be paid by the party with the higher income (the maintenance payor) to the party with the lower income (the maintenance payee) according to a formula, unless the parties agree otherwise or waive this right.  Depending on the incomes of the parties, the obligation might fall on either the Plaintiff or Defendant in the action.").MyStyle();
                p30.Alignment = Alignment.both;
                p31.AppendLine("There are two formulas to determine the amount of the obligation. If you and your spouse have no children, the higher formula will apply. If there are children of the marriage, the lower formula will apply, but only if the maintenance payor is paying child support to the other spouse who has the children as the custodial parent. Otherwise the higher formula will apply.").MyStyle();
                p31.Alignment = Alignment.both;
                p32.AppendLine("Lower Formula").MyStyleB();
                p32.AppendLine("1. Multiply Maintenance Payor's Income by 20%.").MyStyle();
                p32.AppendLine("2. Multiply Maintenance Payee's Income by 25%.").MyStyle();
                p32.AppendLine("Subtract Line 2 from Line 1 = ").MyStyle();
                p32.Append("Result 1").MyStyleB();
                p32.AppendLine("Subtract Maintenance Payor's Income from 40 % of Combined Income* = ").MyStyle();
                p32.Append("Result 2").MyStyleB();
                p32.AppendLine("Enter the lower of ").MyStyle();
                p32.Append("Result 1").MyStyleB();
                p32.Append(" or ").MyStyle();
                p32.Append("Result 2").MyStyleB();
                p32.Append(", but if less than or equal to zero, enter zero.").MyStyle();
                p32.AppendLine("THIS IS THE CALCULATED GUIDELINE AMOUNT OF MAINTENANCE WITH THE LOWER FORMULA.").MyStyleB();
                p32.AppendLine("\nHigher Formula").MyStyleB();
                p32.AppendLine("1. Multiply Maintenance Payor's Income by 30%.").MyStyle();
                p32.AppendLine("2. Multiply Maintenance Payee's Income by 20%.").MyStyle();
                p32.AppendLine("Subtract Line 2 from Line 1 = ").MyStyle();
                p32.Append("Result 1").MyStyleB();
                p32.AppendLine("Subtract Maintenance Payor's Income from 40 % of Combined Income* = ").MyStyle();
                p32.Append("Result 2").MyStyleB();
                p32.AppendLine("Enter the lower of ").MyStyle();
                p32.Append("Result 1").MyStyleB();
                p32.Append(" or ").MyStyle();
                p32.Append("Result 2").MyStyleB();
                p32.Append(", but if less than or equal to zero, enter zero.").MyStyle();
                p32.AppendLine("THIS IS THE CALCULATED GUIDELINE AMOUNT OF MAINTENANCE WITH THE HIGHER FORMULA.").MyStyleB();
                p32.AppendLine("\n* Combined Income equals Maintenance Payor's Income up to $178,000 plus Maintenance Payee's Income.").MyStyleB();
                p32.AppendLine("\nNote: The Court will determine how long maintenance will be paid in accordance with the statute.").MyStyleB();
                p32.AppendLine("\n(Eff. 1/25/16)").MyStyle();
                document.Save();
            }
        }
        public static void Divorce1_VerifiedComplaint(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Verified_Complaint.docx"))
            {
                File.Delete(folder + "\\Verified_Complaint.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Verified_Complaint.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = document.InsertParagraph();
                Paragraph p13 = document.InsertParagraph();
                Paragraph p14 = document.InsertParagraph();
                Paragraph p15 = document.InsertParagraph();
                Paragraph p16 = document.InsertParagraph();
                Paragraph p17 = document.InsertParagraph();
                Paragraph p18 = document.InsertParagraph();
                Paragraph p19 = document.InsertParagraph();
                p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("------------------------------------------------------------X           Index No: " + dc.index_no).MyStyle();
                p1.AppendLine("\n" + pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tVERIFIED COMPLAINT").MyStyleB();
                p1.AppendLine("\t\t\t\t\t\t\tACTION FOR DIVORCE").MyStyleB();
                p1.AppendLine("\t\t\t\t-against-").MyStyle();
                p1.AppendLine("\n" + dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("------------------------------------------------------------X").MyStyle();
                p1.AppendLine("\n\t\tThe Plaintiff, " + dc.p.name.ToTitleCase() + ", complaining of the Defendant, alleges the following:").MyStyle();
                p1.AppendLine("\n\t\tFIRST:  ").MyStyleB();
                p1.Append("The parties are over the age of 18 years.").MyStyle();
                p2.AppendLine("\t\tSECOND:  ").MyStyleB();
                p2.Append("This court has jurisdiction to hear this action for divorce. ").MyStyle();
                if (dc.r.applicable == "1")
                {
                    p2.Append("The Plaintiff has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "2")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "3")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "4")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "5")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "6")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "7")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "8")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                p2.Alignment = Alignment.both;
                p3.AppendLine("\t\tTHIRD:  ").MyStyleB();
                if (dc.m.country == "US")
                    p3.Append("The Plaintiff and Defendant were married to each other on " + dc.m.marriage_date.ToDateLong() + " in the City of " + dc.m.city.ToTitleCase() + ", the County of " + dc.m.county.ToTitleCase() + " and the State of " + dc.m.state.ToStateName() + ".").MyStyle();
                else
                    p3.Append("The Plaintiff and Defendant were married to each other on " + dc.m.marriage_date.ToDateLong() + " in the City of " + dc.m.city.ToTitleCase() + " and the Country of " + dc.m.country.ToCountryName() + ".").MyStyle();
                if (dc.m.marriage_clergyman == "C")
                    p3.Append(" The marriage was not performed by a clergyman, minister or leader of the Society for Ethical Culture.").MyStyle();
                p3.Alignment = Alignment.both;
                if (dc.m.marriage_clergyman == "R")
                    p4.AppendLine("\n\t\tThe marriage was performed by a clergyman, minister or leader of the Society for Ethical Culture.  To the best of the Plaintiff's knowledge the Plaintiff has taken all steps solely within the power of the Plaintiff to remove any barrier to the Defendant's remarriage.").MyStyle();
                p4.Alignment = Alignment.both;
                p5.AppendLine("\t\tFOURTH:  ").MyStyleB();
                p5.Append("There is no child as a result of this marriage and no child is expected.").MyStyle();
                if (dc.p.country == "US")
                    p5.AppendLine("\t\tThe Plaintiff resides at " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.state.ToStateName() + " " + dc.p.zip).MyStyle();
                else
                    p5.AppendLine("\t\tThe Plaintiff resides at " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.country.ToCountryName()).MyStyle();
                if (dc.d.country == "US")
                    p5.AppendLine("\t\tThe Defendant resides at " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " " + dc.d.zip).MyStyle();
                else
                    p5.AppendLine("\t\tThe Defendant resides at " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName()).MyStyle();
                p5.AppendLine("\nThe parties are covered by the following group health plans:").MyStyle();
                p5.AppendLine("\nPlaintiff").MyStyleU();
                p5.AppendLine("\n\tGroup Health Plan:  NOT APPLICABLE").MyStyle();
                p5.AppendLine("\nDefendant").MyStyleU();
                p5.AppendLine("\n\tGroup Health Plan:  NOT APPLICABLE").MyStyle();
                p6.AppendLine("\t\tFIFTH:  ").MyStyleB();
                p6.Append("The grounds for divorce are as follows:  ").MyStyle();
                if (dc.r.grounds == "1")
                {
                    p6.Append("Irretrievable Breakdown of the Relationship for at Least Six Months (DRL Sec. 170(7))").MyStyleU();
                    p6.Append(":  The relationship between the Plaintiff and Defendant has broken down irretrievably for a period of at least six months.").MyStyle();
                }
                else if (dc.r.grounds == "2")
                {
                    if (dc.p.country == "US")
                        p6.Append("Abandonment (DRL Sec. 170(2)): Commencing on or about " + dc.a.adate.ToDateLong() + " and continuing for a period of more than one (1) year immediately prior to the commencement of this action, the Defendant left the marital residence of the parties located at " + dc.a.street.ToTitleCase() + ", " + dc.a.city.ToTitleCase() + ", " + dc.a.state.ToStateName() + " and did not return. Such absence was without cause or justification, and was without the Plaintiff's consent.").MyStyleU();
                    else
                        p6.Append("Abandonment (DRL Sec. 170(2)): Commencing on or about " + dc.a.adate.ToDateLong() + " and continuing for a period of more than one (1) year immediately prior to the commencement of this action, the Defendant left the marital residence of the parties located at " + dc.a.street.ToTitleCase() + ", " + dc.a.city.ToTitleCase() + ", " + dc.a.country.ToCountryName() + " and did not return. Such absence was without cause or justification, and was without the Plaintiff's consent.").MyStyleU();
                }
                else if (dc.r.grounds == "3")
                {
                    p6.Append("Constructive Abandonment (Cessation of Sexual Relations) at least for One Year (DRL Sec. 170 subdc.d. (2))").MyStyleU();
                    p6.Append(":  The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "4")
                {
                    p6.Append("Cruel And Inhuman Treatment (DRL Sec. 170 subdc.d. (1))").MyStyleU();
                    p6.Append(":  The cruel and inhuman treatment of the Plaintiff by the Defendant.").MyStyle();
                }
                p6.Alignment = Alignment.both;
                p7.AppendLine("\t\tSIXTH:  ").MyStyleB();
                p7.Append("There is no judgment in any court for a divorce and no other matrimonial action for divorce between the parties is pending in this Court or in any other court of competent jurisdiction.").MyStyle();
                p7.Alignment = Alignment.both;
                p8.AppendLine("\t\tSEVENTH:  ").MyStyleB();
                p8.Append("Neither the Wife nor the Husband needs maintenance.").MyStyle();
                p9.AppendLine("\t\tWHEREFORE").MyStyleB();
                p9.Append(", the Plaintiff demands judgment against the Defendant, dissolving the marriage between the parties to this action and granting the following relief:").MyStyle();
                p9.Alignment = Alignment.both;
                p10.AppendLine("That the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance and support.").MyStyle();
                p10.IndentationBefore = 1.3f;
                p10.Alignment = Alignment.both;
                if (dc.waiver_default == "1")
                    p11.AppendLine("That the parties do not require maintenance and no claim will be made by either party for maintenance. Neither party is seeking maintenance as payee as described in the Notice of Guideline Maintenance as already agreed to in our written Affidavit of Plaintiff and Affidavit of Defendant.").MyStyle();
                else
                    p11.AppendLine("That the parties do not require maintenance and no claim will be made by either party for maintenance. I am not seeking maintenance as payee as described in the Notice of Guideline Maintenance.").MyStyle();
                p11.IndentationBefore = 1.3f;
                p11.Alignment = Alignment.both;
                p12.AppendLine("That the parties do not require payment of counsel and experts' fees and expenses.").MyStyle();
                p12.IndentationBefore = 1.3f;
                p12.Alignment = Alignment.both;
                p13.AppendLine("That both parties may resume the use of any prior surname.").MyStyle();
                p13.IndentationBefore = 1.3f;
                p13.Alignment = Alignment.both;
                p14.AppendLine("That the Court grant such other and further relief as the Court may deem fit and proper.").MyStyle();
                p14.IndentationBefore = 1.3f;
                p14.Alignment = Alignment.both;
                p15.AppendLine("The parties have divided up the marital property and no claim will be made by either party under equitable distribution.").MyStyle();
                p15.IndentationBefore = 1.3f;
                p15.Alignment = Alignment.both;
                p16.AppendLine("\nDated: ").MyStyle();
                p16.AppendLine("\n\t\t\t\t\t\t______________________________").MyStyle();
                p16.AppendLine("\t\t\t\t\t\t" + dc.p.name.ToTitleCase()).MyStyle();
                p16.AppendLine("\t\t\t\t\t\tPlaintiff Pro-se").MyStyle();
                p16.AppendLine("\t\t\t\t\t\t" + dc.p.street).MyStyle();
                p16.AppendLine("\t\t\t\t\t\t" + dc.p.city + ", " + dc.p.state.ToStateName() + " " + dc.p.zip).MyStyle();
                p16.AppendLine("\t\t\t\t\t\t" + dc.p.phone).MyStyle();
                p17.InsertPageBreakBeforeSelf();
                p17.AppendLine("\nSTATE OF NEW YORK, COUNTY OF\t\t,ss.\n").MyStyle();
                p18.AppendLine("I, " + pname + ", am the Plaintiff in the within action for a divorce. I have read the foregoing Complaint and know the contents thereof. The contents of the Complaint are true to my own knowledge, except as to those matters therein stated to be alleged upon information and belief, and as to those matters I believe them to be true.").MyStyle();
                p18.Alignment = Alignment.both;
                p19.AppendLine("\n\n\n\t\t\t\t\t\t\t\t________________________").MyStyle();
                p19.AppendLine("\t\t\t\t\t\t\t\t" + pname).MyStyle();
                p19.AppendLine("Subscribed and sworn to before me\t\t\t\tPlaintiff Pro-se").MyStyle();
                p19.AppendLine("on").MyStyle();
                p19.AppendLine("\n__________________________").MyStyle();
                p19.AppendLine("\t    Notary Public").MyStyle();
                p19.AppendLine("My commission expires on").MyStyle();
                p19.AppendLine();
                p19.AppendLine("(Form UD-2 - 1/25/16)").MyStyle();
                document.Save();
            }
        }
        public static void Divorce1_AffidavitOfRegularity(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Affidavit_Of_Regularity.docx"))
            {
                File.Delete(folder + "\\Affidavit_Of_Regularity.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Affidavit_Of_Regularity.docx"))
            {
                {
                    Paragraph p1 = document.InsertParagraph();
                    p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p1.AppendLine(pname).MyStyle();
                    p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p1.AppendLine("\t\t\t\t\t\t\tAFFIDAVIT OF REGULARITY").MyStyleB();
                    p1.AppendLine("\t\t-against-\n").MyStyle();
                    p1.AppendLine(dname).MyStyle();
                    p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p1.AppendLine("---------------------------------------------------------------------X").MyStyle();
                    p1.AppendLine("STATE OF NEW YORK, COUNTY OF                  , ss.\n").MyStyle();
                    p1.AppendLine("\t\tI, " + dc.p.name.ToTitleCase() + ", the undersigned Plaintiff Pro-se, respectfully show:").MyStyle();
                    p1.AppendLine("\n\t1. I am the Plaintiff herein.\n").MyStyle();
                    p1.AppendLine("\t2. This is a matrimonial action.\n").MyStyle();
                    if (dc.r.summons_usa == "Y")
                    {
                        if (dc.waiver_default == "1")
                            p1.AppendLine("\t3. The Summons with Notice, Notice of Automatic Orders and Notice of Guideline Maintenance were personally served upon the Defendant herein, within the State of New York as appears by the affidavit of the Defendant submitted herewith.\n").MyStyle();
                        if (dc.waiver_default == "2")
                            p1.AppendLine("\t3. The Summons with Notice, Notice of Automatic Orders and Notice of Guideline Maintenance were personally served upon the Defendant herein, within the State of New York as appears by the affidavit of Service submitted herewith.\n").MyStyle();
                    }
                    else
                    {
                        if (dc.waiver_default == "1")
                            p1.AppendLine("\t3. The Summons with Notice, Notice of Automatic Orders and Notice of Guideline Maintenance were personally served upon the Defendant herein, outside the State of New York as appears by the affidavit of the Defendant submitted herewith.\n").MyStyle();
                        if (dc.waiver_default == "2")
                            p1.AppendLine("\t3. The Summons with Notice, Notice of Automatic Orders and Notice of Guideline Maintenance were personally served upon the Defendant herein, outside the State of New York as appears by the affidavit of Service submitted herewith.\n").MyStyle();
                    }
                    if (dc.waiver_default == "1")
                        p1.AppendLine("\t4. The Defendant has appeared in this action and executed an affidavit agreeing that this matter be placed on the matrimonial calendar immediately.\n\n").MyStyle();
                    if (dc.waiver_default == "2")
                        p1.AppendLine("\t4. The Defendant is in default for failure to serve a notice of appearance or failure to answer the complaint served in this action in due time, and the time to answer has not been extended by stipulation, court order or otherwise.\n\n").MyStyle();
                    p1.Append("\tWHEREFORE, ").MyStyleB();
                    p1.Append("I respectfully request that this action be placed on the undefended matrimonial calendar.\n").MyStyle();
                    p1.AppendLine("\tI state under penalty of perjury that the statements herein made are true, except as to such statements as are based upon information and belief, which statements I believe to be true.").MyStyle();
                    p1.AppendLine("\n\n\t\t\t\t\t\t____________________________________").MyStyle();
                    p1.AppendLine("\t\t\t\t\t\t" + dc.p.name.ToTitleCase()).MyStyle();
                    p1.AppendLine("Subscribed and sworn to before me\t\tPlaintiff Pro-Se").MyStyle();
                    p1.AppendLine("on").MyStyle();
                    p1.AppendLine("\n__________________________").MyStyle();
                    p1.AppendLine("\tNotary Public").MyStyle();
                    p1.AppendLine("My commission expires on").MyStyle();
                    p1.AppendLine("\n(Form UD-5 – 1/25/16)").MyStyle();
                    document.Save();
                }
            }
        }
        public static void Divorce1_AffidavitOfPlaintiff(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Affidavit_Of_Plaintiff.docx"))
            {
                File.Delete(folder + "\\Affidavit_Of_Plaintiff.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Affidavit_Of_Plaintiff.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = document.InsertParagraph();
                Paragraph p13 = document.InsertParagraph();
                Paragraph p14 = document.InsertParagraph();
                Paragraph p15 = document.InsertParagraph();
                Paragraph p16 = document.InsertParagraph();
                Paragraph p17 = document.InsertParagraph();
                Paragraph p18 = document.InsertParagraph();
                Paragraph p19 = document.InsertParagraph();
                Paragraph p20 = document.InsertParagraph();
                Paragraph p20_1 = document.InsertParagraph();
                Paragraph p21 = document.InsertParagraph();
                Paragraph p22 = document.InsertParagraph();
                p1.Append("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tAFFIDAVIT OF PLAINTIFF").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X").MyStyle();
                p1.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                p1.AppendLine("\t\t" + pname + ", being duly sworn, says:\n").MyStyle();
                if (dc.p.country == "US")
                    p2.Append("\t1. The Plaintiff, " + pname + ", resides at " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.state.ToStateName() + " - " + dc.p.zip + " and was born on " + dc.p.dob.ToDateLong() + ". ").MyStyle();
                else
                    p2.Append("\t1. The Plaintiff, " + pname + ", resides at " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.country.ToCountryName() + " and was born on " + dc.p.dob.ToDateLong() + ". ").MyStyle();
                if (dc.p.check_ssn == "Y")
                    p2.Append("The Social Security Number of the Plaintiff is " + dc.p.ssn + ". ").MyStyle();
                else if (dc.p.check_ssn == "N")
                {
                    p2.Append("The Plaintiff ").MyStyle();
                    p2.Append("doesn't").MyStyleU();
                    p2.Append(" have a Social Security Number. ").MyStyle();
                }
                if (dc.d.country == "US")
                    p2.Append("The Defendant, " + dname + ", resides at " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " - " + dc.d.zip + " and was born on " + dc.d.dob.ToDateLong() + ". ").MyStyle();
                else
                    p2.Append("The Defendant, " + dname + ", resides at " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName() + " and was born on " + dc.d.dob.ToDateLong() + ". ").MyStyle();
                if (dc.d.check_ssn == "Y")
                    p2.Append("The Social Security Number of the Defendant is " + dc.d.ssn + ". ").MyStyle();
                else if (dc.d.check_ssn == "N")
                {
                    p2.Append("The Defendant ").MyStyle();
                    p2.Append("doesn't").MyStyleU();
                    p2.Append(" have a Social Security Number. ").MyStyle();
                }
                else if (dc.d.check_ssn == "R")
                {
                    p2.Append("The Defendant ").MyStyle();
                    p2.Append("refused").MyStyleU();
                    p2.Append(" to provide the Social Security Number. ").MyStyle();
                }
                p2.Append("The Plaintiff and Defendant were both 18 years of age or over when this action was commenced.").MyStyle();
                p2.Alignment = Alignment.both;
                p3.AppendLine("\t2. This court has jurisdiction to hear this action for divorce. ").MyStyle();
                if (dc.r.applicable == "1")
                    p3.Append("The Plaintiff has resided in New York State for a continuous period in excess of two years ").MyStyle();
                if (dc.r.applicable == "2")
                    p3.Append("The Defendant has resided in New York State for a continuous period in excess of two years ").MyStyle();
                if (dc.r.applicable == "3")
                    p3.Append("The Plaintiff and Defendant were married in New York State and the Plaintiff has resided in New York State for a continuous period in excess of one year ").MyStyle();
                if (dc.r.applicable == "4")
                    p3.Append("The Plaintiff and Defendant were married in New York State and the Defendant has resided in New York State for a continuous period in excess of one year").MyStyle();
                if (dc.r.applicable == "5")
                    p3.Append("The Plaintiff and the Defendant have lived in New York State as husband and wife and the Plaintiff has resided in New York State for a continuous period in excess of one year ").MyStyle();
                if (dc.r.applicable == "6")
                    p3.Append("The Plaintiff and the Defendant have lived in New York State as husband and wife and the Defendant has resided in New York State for a continuous period in excess of one year ").MyStyle();
                if (dc.r.applicable == "7")
                    p3.Append("The act upon which the divorce is founded occurred in New York State and a party has resided in New York State for 1 year ").MyStyle();
                if (dc.r.applicable == "8")
                    p3.Append("The act upon which the divorce is founded occurred in New York State and both parties reside in New York state ").MyStyle();
                p3.Append("immediately preceding the commencement of this action.").MyStyle();
                p3.Alignment = Alignment.both;
                if (dc.m.country == "US")
                    p4.AppendLine("\t3. I married the Defendant on " + dc.m.marriage_date.ToDateLong() + ", in the City of " + dc.m.city.ToTitleCase() + ", the County of " + dc.m.county.ToTitleCase() + " and the State of " + dc.m.state.ToStateName() + ". The marriage was").MyStyle();
                else
                    p4.Append("\t3. I married the Defendant on " + dc.m.marriage_date.ToDateLong() + ", in the City of " + dc.m.city + ", the Country of " + dc.m.country.ToCountryName() + ". The marriage was").MyStyle();
                if (dc.m.marriage_clergyman == "C")
                {
                    p4.Append(" not").MyStyleU();
                }
                p4.Append(" performed by a clergyman, minister or leader of the Society for Ethical Culture.").MyStyle();
                if (dc.m.marriage_clergyman == "R")
                    p4.Append(" To the best of my knowledge I have taken all steps solely within my power to remove all barriers to the Defendant's remarriage following the divorce.").MyStyle();
                if (dc.m.marriage_clergyman == "C")
                    p4.Append(" No steps have to be taken by either party to remove any barriers to either party's remarriage. I waive any requirement of a filing by the Defendant of a statement, pursuant to Section 253 of the Domestic Relations Law, confirming removal of barriers to my remarriage.").MyStyle();
                p4.Alignment = Alignment.both;
                p5.AppendLine("\t4. There is no child as a result of this marriage and no child is expected.").MyStyle();
                p5.AppendLine();
                p5.AppendLine("The parties are covered by the following group health plans:").MyStyle();
                p5.AppendLine();
                p5.AppendLine("Plaintiff").MyStyleU();
                p5.AppendLine();
                p5.AppendLine("\tGroup Health Plan:  NOT APPLICABLE").MyStyle();
                p5.AppendLine();
                p5.AppendLine("Defendant").MyStyleU();
                p5.AppendLine();
                p5.AppendLine("\tGroup Health Plan:  NOT APPLICABLE").MyStyle();
                p6.AppendLine("I fully understand that upon the entrance of a judgment of divorce, I may no longer be allowed to receive health coverage under my former spouse's health insurance plan. I may be entitled to purchase health insurance on my own through a COBRA option, if available and otherwise I may be required to secure my own health insurance.").MyStyle();
                p6.Alignment = Alignment.both;
                if (dc.r.grounds == "1")
                {
                    p7.AppendLine("\t5. The grounds for divorce are as follows: ").MyStyle();
                    p7.Append("Irretrievable Breakdown of the Relationship for at Least Six Months (DRL Sec. 170(7))").MyStyleU();
                    p7.Append(":  The relationship between the Plaintiff and Defendant has broken down irretrievably for a period of at least six months.").MyStyle();
                }
                else if (dc.r.grounds == "2")
                {
                    p7.AppendLine("\t5. The grounds for divorce are as follows:  ").MyStyle();
                    p7.Append("Abandonment for at least One Year (DRL Sec. 170 subd. (2))").MyStyleU();
                    p7.Append(":  The abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "3")
                {
                    p7.AppendLine("\t5. The grounds for divorce are as follows:  ").MyStyle();
                    p7.Append("Constructive Abandonment (Cessation of Sexual Relations) at least for One Year (DRL Sec. 170 subd. (2))").MyStyleU();
                    p7.Append(":  The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "4")
                {
                    p7.AppendLine("\t5. The grounds for divorce are as follows:  ").MyStyle();
                    p7.Append("Cruel And Inhuman Treatment (DRL Sec. 170 subd. (1))").MyStyleU();
                    p7.Append(":  The cruel and inhuman treatment of the Plaintiff by the Defendant.").MyStyle();
                }
                p7.Alignment = Alignment.both;
                p8.AppendLine("\t6a. In addition to the dissolution of the marriage, I am seeking the following relief:").MyStyle();
                p9.AppendLine("That the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance and support.").MyStyle();
                p9.IndentationBefore = 1.3f;
                p9.Alignment = Alignment.both;
                p10.AppendLine("That the parties do not require maintenance and no claim will be made by either party for maintenance. I am not seeking maintenance as payee as described in the Notice of Guideline Maintenance.").MyStyle();
                p10.IndentationBefore = 1.3f;
                p10.Alignment = Alignment.both;
                p11.AppendLine("That the parties do not require payment of counsel and experts' fees and expenses.").MyStyle();
                p11.IndentationBefore = 1.3f;
                p11.Alignment = Alignment.both;
                p12.AppendLine("That both parties may resume the use of any prior surname.").MyStyle();
                p12.IndentationBefore = 1.3f;
                p12.Alignment = Alignment.both;
                p13.AppendLine("That the Court grant such other and further relief as the Court may deem fit and proper.").MyStyle();
                p13.IndentationBefore = 1.3f;
                p13.Alignment = Alignment.both;
                p14.AppendLine("The parties have divided up the marital property and no claim will be made by either party under equitable distribution.").MyStyle();
                p14.IndentationBefore = 1.3f;
                p14.Alignment = Alignment.both;
                if (dc.r.grounds == "1")
                    p15.AppendLine("\t6b. DRL Sec. 170(7)").MyStyle();
                else if (dc.r.grounds == "2")
                    p15.AppendLine("\t6b. DRL Sec. 170(2)").MyStyle();
                else if (dc.r.grounds == "3")
                    p15.AppendLine("\t6b. DRL Sec. 170(2)").MyStyle();
                else if (dc.r.grounds == "4")
                    p15.AppendLine("\t6b. DRL Sec. 170(1)").MyStyle();
                if (dc.waiver_default == "1")
                    p15.Append(" is the ground alleged and the Plaintiff hereby affirms that all economic issues of equitable distribution of marital property, the payment or waiver of spousal support and the payment of counsel and experts' fees and expenses, have been resolved by the parties by written agreement or written waiver. The Defendant has signed a separate affidavit confirming this. Issues of custody, visitation and child support have not been raised or addressed since there are no unemancipated children of the marriage.").MyStyle();
                if (dc.waiver_default == "2")
                    p15.Append(" is the ground alleged, and the Plaintiff hereby affirms that all economic issues of equitable distribution of marital property, the payment or waiver of spousal support, and the payment of counsel and experts' fees and expenses, are specified above and in the Summons with Notice and are to be determined by the court and are to be incorporated into the Judgment of Divorce.Issues of custody, visitation and child support have not been raised or addressed since there are no unemancipated children of the marriage.").MyStyle();
                p15.Alignment = Alignment.both;
                if (dc.waiver_default == "1")
                    p16.AppendLine("\t7. The Defendant is not in the active military service of the United States, New York or any other state or territory. I have submitted with these papers an Affidavit of Defendant which states that the Defendant is not in the active military service of the United States, New York or any other state or territory.").MyStyle();
                if (dc.waiver_default == "2")
                    p16.AppendLine("\t7. The Defendant is not in the active military service of the United States, New York or any other state or territory. I know this because he admitted it to the process server on " + dc.date_summons.ToDateLong()).MyStyle();
                p16.Alignment = Alignment.both;
                p17.AppendLine("\t8. I am not receiving Public Assistance. To my knowledge the Defendant is not receiving Public Assistance.").MyStyle();
                p17.Alignment = Alignment.both;
                p18.AppendLine("\t9. No other matrimonial action is pending in any other court and the marriage has not been terminated by any prior decree of any court of competent jurisdiction.").MyStyle();
                p18.Alignment = Alignment.both;
                if (dc.p.sex == "F")
                    p19.AppendLine("\t10. The Plaintiff's prior surname is " + dc.p.maiden_name.ToTitleCase().ToTitleCase() + " and the Plaintiff wishes to be able to resume use of said prior surname.").MyStyle();
                if (dc.d.sex == "F")
                    p19.AppendLine("\t10. The Defendant's prior surname is " + dc.d.maiden_name.ToTitleCase().ToTitleCase() + " and the Defendant wishes to be able to resume use of said prior surname.").MyStyle();
                p19.Alignment = Alignment.both;
                p20.AppendLine("\t11. I acknowledge receipt of the Notice of Guideline Maintenance from the Court pursuant to DRL 236 B(6), Chapter 269 of the Laws of 2015, which was served with the Summons.").MyStyle();
                p20.Alignment = Alignment.both;
                p20_1.AppendLine("12. I have been provided a copy of the Notice Concerning Continuation of Health Care Coverage.I fully understand that upon the entrance of a judgment of divorce, I may no longer be allowed to receive health coverage under my former spouse &#39;s health insurance plan. I may be entitled to purchase health insurance on my own through a COBRA option, if available, and otherwise I may be required to secure my own health insurance.").MyStyle();
                p20_1.Alignment = Alignment.both;
                p21.InsertPageBreakBeforeSelf();
                p21.AppendLine("\tWHEREFORE").MyStyleB();
                p21.Append(", I respectfully request that a judgment be entered for the relief sought and such other relief as the Court deems fitting and proper.").MyStyle();
                p21.Alignment = Alignment.both;
                p22.AppendLine("\n\n\n\t\t\t\t\t\t____________________________________").MyStyle();
                p22.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                p22.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                p22.AppendLine("on").MyStyle();
                p22.AppendLine();
                p22.AppendLine("__________________________").MyStyle();
                p22.AppendLine("\t    Notary Public").MyStyle();
                p22.AppendLine("My commission expires on").MyStyle();
                p22.AppendLine();
                p22.AppendLine("(Form UD-6 - 1/25/16)").MyStyle();
                document.Save();
            }
        }
        public static void Divorce1_AffidavitOfDefendant(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Affidavit_Of_Defendant.docx"))
            {
                File.Delete(folder + "\\Affidavit_Of_DefendantPlaintiff.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Affidavit_Of_Defendant.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = document.InsertParagraph();
                Paragraph p13 = document.InsertParagraph();
                p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tAFFIDAVIT OF DEFENDANT").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X").MyStyle();
                p1.AppendLine("STATE OF NEW YORK, COUNTY OF                , ss.\n").MyStyle();
                p1.AppendLine("\t\t" + dname + ", being duly sworn, says:\n\n").MyStyle();
                if (dc.d.country == "US")
                    p2.Append("\t1. I am the Defendant in this action for divorce and I am over the age of 18 years. I reside at " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " - " + dc.d.zip + ".").MyStyle();
                else
                    p2.Append("\t1. I am the Defendant in this action for divorce and I am over the age of 18 years. I reside at " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName() + ".").MyStyle();
                if (dc.d.check_ssn == "Y")
                    p2.Append(" My Social Security Number is " + dc.d.ssn).MyStyle();
                p2.Alignment = Alignment.both;
                if (dc.date_summons.HasValue && !dc.date_summons.Value.IsMinDate())
                    p3.AppendLine("\t2. I admit service of the Summons with Notice for divorce on " + dc.date_summons.ToDateLong() + ", based upon the following grounds: ").MyStyle();
                else
                    p3.AppendLine("\t2. I admit service of the Summons with Notice for divorce on \t\t, based upon the following grounds: ").MyStyle();
                if (dc.r.grounds == "1")
                {
                    p3.Append("(DRL Sec. 170(7))").MyStyleU();
                    p3.Append(":  The relationship between the Plaintiff and Defendant has broken down irretrievably for a period of at least six months.").MyStyle();
                }
                else if (dc.r.grounds == "2")
                {
                    p3.Append("(DRL Sec. 170 subd. (2))").MyStyleU();
                    p3.Append(":  The abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "3")
                {
                    p3.Append("(DRL Sec. 170 subd. (2))").MyStyleU();
                    p3.Append(":  The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "4")
                {
                    p3.Append("(DRL Sec. 170 subd. (1))").MyStyleU();
                    p3.Append(":  The cruel and inhuman treatment of the Plaintiff by the Defendant.").MyStyle();
                }
                p3.Append(" I also admit service of the Notice of Automatic Orders, the Notice of Guideline Maintenance and the Notice of Continuation of Health Care Coverage which accompanied the Summons with Notice.").MyStyle();
                p3.Alignment = Alignment.both;
                p4.AppendLine("\t3. I appear in this action; however, I do not intend to respond to the summons or answer the complaint and I waive the twenty (20) or thirty (30) day period provided by law to respond to the summons or answer the complaint.  I waive the forty (40) day waiting period to place this matter on the calendar and I hereby consent to this action being placed on the uncontested divorce calendar immediately.").MyStyle();
                p4.Alignment = Alignment.both;
                p5.AppendLine("\t4. I am not a member of the military service of the United States, New York or any other state or territory.").MyStyle();
                p5.Alignment = Alignment.both;
                p6.AppendLine("\t5. I waive the service of all further papers in this action except for a copy of the final Judgment of Divorce. I specifically waive the service upon me of the Note of Issue, Request for Judicial Intervention, Barriers to Remarriage Affidavit, proposed Findings of Fact and Conclusions of Law, proposed Judgment of Divorce, Notice of Settlement and any other notices, pleadings and papers in this action. I, however, specifically reserve the right to receive a copy of any Judgment as ultimately may be granted in this action. I further waive all statutory waiting periods prior to judgment.").MyStyle();
                p6.Alignment = Alignment.both;
                p7.AppendLine("\t6. I am not seeking equitable distribution. I understand that I may be prevented from further asserting my right to equitable distribution.").MyStyle();
                p7.Alignment = Alignment.both;
                p8.AppendLine("\t7. I am not seeking maintenance as payee as described in the Notice of Guideline Maintenance, and I agree that neither party shall be obligated to provide maintenance. I make this agreement as an agreement regarding maintenance pursuant to DRL 236(B)(3).").MyStyle();
                p8.Alignment = Alignment.both;
                if (dc.r.grounds == "1")
                    p9.AppendLine("\t8. The relationship of the Plaintiff and Defendant has broken down irretrievably for a period of more than six months.").MyStyle();
                else if (dc.r.grounds == "2")
                    p9.AppendLine("\t8. The abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                else if (dc.r.grounds == "3")
                    p9.AppendLine("\t8. The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                else if (dc.r.grounds == "4")
                    p9.AppendLine("\t8. The cruel and inhuman treatment of the Plaintiff by the Defendant.").MyStyle();
                p9.Append(" All economic issues of equitable distribution of marital property, the payment or waiver of spousal support, and the payment of counsel and experts & fees and expenses, have been resolved by the parties in our Affidavit of Plaintiff and Affidavit of Defendant and are to be incorporated into the Judgment of Divorce.Issues of custody, visitation and child support have not been raised or addressed since there are no unemancipated children of the marriage.").MyStyle();
                p9.Alignment = Alignment.both;
                if (dc.m.marriage_clergyman == "C")
                    p10.AppendLine("\t9. The marriage was not performed by a clergyman, minister or leader of the Society for Ethical Culture").MyStyle();
                else if (dc.m.marriage_clergyman == "R")
                    p10.Append("\t9. I will take or have taken all steps solely within my power to remove any barriers to the Plaintiff's remarriage.").MyStyle();
                p10.Alignment = Alignment.both;
                p11.AppendLine("\t10. I acknowledge receipt of the Notice of Guideline Maintenance from the Court pursuant to DRL 236 B(6), Chapter 269 of the Laws of 2015, which was served with the Summons.").MyStyle();
                p11.Alignment = Alignment.both;
                p12.AppendLine("\t11. I have been provided a copy of the Notice Concerning Continuation of Health Care Coverage. I fully understand that upon the entrance of a judgment of divorce, I may no longer be allowed to receive health coverage under my former spouse's health insurance plan.  I may be entitled to purchase health insurance on my own through a COBRA option, if available, and otherwise I may be required to secure my own health insurance.").MyStyle();
                p12.Alignment = Alignment.both;
                p13.AppendLine("\n\n\t\t\t\t\t\t____________________________________").MyStyle();
                p13.AppendLine("\t\t\t\t\t\t" + dname).MyStyle();
                p13.AppendLine("Subscribed and sworn to before me\t\tDefendant").MyStyle();
                p13.AppendLine("on").MyStyle();
                p13.AppendLine("\n__________________________").MyStyle();
                p13.AppendLine("\t    Notary Public").MyStyle();
                p13.AppendLine("My commission expires on").MyStyle();
                p13.AppendLine("\n(Form UD-7 - 3/1/16)").MyStyle();
                document.Save();
            }
        }
        public static void Divorce1_FindingsOfFact(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Findings_Of_Fact_Conclusions.docx"))
            {
                File.Delete(folder + "\\Findings_Of_Fact_Conclusions.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Findings_Of_Fact_Conclusions.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = document.InsertParagraph();
                Paragraph p13 = document.InsertParagraph();
                Paragraph p14 = document.InsertParagraph();
                Paragraph p15 = document.InsertParagraph();
                Paragraph p16 = document.InsertParagraph();
                Paragraph p17 = document.InsertParagraph();
                Paragraph p18 = document.InsertParagraph();
                Paragraph p19 = document.InsertParagraph();
                Paragraph p20 = document.InsertParagraph();
                Paragraph p21 = document.InsertParagraph();
                Paragraph p22 = document.InsertParagraph();
                Paragraph p23 = document.InsertParagraph();
                Paragraph p24 = document.InsertParagraph();
                Paragraph p25 = document.InsertParagraph();
                Paragraph p26 = document.InsertParagraph();
                Paragraph p27 = document.InsertParagraph();
                Paragraph p28 = document.InsertParagraph();
                Paragraph p29 = document.InsertParagraph();
                Paragraph p30 = document.InsertParagraph();
                Paragraph p31 = document.InsertParagraph();
                Paragraph p32 = document.InsertParagraph();
                Paragraph p33 = document.InsertParagraph();
                p1.AppendLine("\t\t\t\t          At the Matrimonial/IAS Part      of the New York ").MyStyle();
                p1.AppendLine("\t\t\t\t          Supreme Court at the Courthouse, " + venue.ToTitleCase() + " County,").MyStyle();
                p1.AppendLine("\t\t\t\t          on the        day of                     , " + DateTime.Now.Year).MyStyle();
                p1.AppendLine("Present:").MyStyle();
                p1.AppendLine("Hon.\t\t\t\t\tJustice").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\t\t      Calendar No: ").MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tFINDINGS OF FACT AND").MyStyleB();
                p1.AppendLine("\t\t\t\t\t\t\t CONCLUSIONS OF LAW").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X").MyStyle();
                p2.AppendLine("\n\tThe issues of this action having been submitted to me as one of the Justices/Referees of this Court at Part                      hereof, held in and for the County of New York on                                           and having considered the allegations and proofs of the respective parties and due deliberation having been had thereon.").MyStyle();
                p2.Alignment = Alignment.both;
                p2.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p3.AppendLine("\tNOW").MyStyleB();
                p3.Append(", after reading and considering the papers submitted, I do hereby make the following findings of essential facts which I deem established by the evidence and reach the following conclusions of law.").MyStyle();
                p3.Alignment = Alignment.both;
                p3.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p4.AppendLine("FINDINGS OF FACT").MyStyleB();
                p4.Alignment = Alignment.center;
                p4.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p5.AppendLine("\tFIRST:").MyStyleB();
                p5.Append(" The Plaintiff and Defendant were both 18 years of age or over when this action was commenced.").MyStyle();
                p5.Alignment = Alignment.both;
                p5.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p6.AppendLine("\tSECOND:").MyStyleB();
                p6.Append(" This court has jurisdiction to hear this action for divorce. ").MyStyle();
                if (dc.r.applicable == "1")
                    p6.Append("The Plaintiff has resided in New York State for a continuous period in excess of two years ").MyStyle();
                if (dc.r.applicable == "2")
                    p6.Append("The Defendant has resided in New York State for a continuous period in excess of two years ").MyStyle();
                if (dc.r.applicable == "3")
                    p6.Append("The Plaintiff and Defendant were married in New York State and the Plaintiff has resided in New York State for a continuous period in excess of one year ").MyStyle();
                if (dc.r.applicable == "4")
                    p6.Append("The Plaintiff and Defendant were married in New York State and the Defendant has resided in New York State for a continuous period in excess of one year").MyStyle();
                if (dc.r.applicable == "5")
                    p6.Append("The Plaintiff and the Defendant have lived in New York State as husband and wife and the Plaintiff has resided in New York State for a continuous period in excess of one year ").MyStyle();
                if (dc.r.applicable == "6")
                    p6.Append("The Plaintiff and the Defendant have lived in New York State as husband and wife and the Defendant has resided in New York State for a continuous period in excess of one year ").MyStyle();
                if (dc.r.applicable == "7")
                    p6.Append("The act upon which the divorce is founded occurred in New York State and a party has resided in New York State for 1 year ").MyStyle();
                if (dc.r.applicable == "8")
                    p6.Append("The act upon which the divorce is founded occurred in New York State and both parties reside in New York state ").MyStyle();
                p6.Append("immediately preceding the commencement of this action.").MyStyle();
                p6.Alignment = Alignment.both;
                p6.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p7.AppendLine("\tTHIRD:").MyStyleB();
                if (dc.m.marriage_clergyman == "R")
                {
                    if (dc.m.country == "US")
                        p7.Append(" The Plaintiff and Defendant were married on " + dc.m.marriage_date.ToDateLong() + ", in the City of " + dc.m.city.ToTitleCase() + ", the County of " + dc.m.county.ToTitleCase() + " and the State of " + dc.m.state.ToStateName() + ", in a religious ceremony.").MyStyle();
                    else
                        p7.Append(" The Plaintiff and Defendant were married on " + dc.m.marriage_date.ToDateLong() + ", in the City of " + dc.m.city.ToTitleCase() + " and the Country of " + dc.m.country.ToTitleCase() + ", in a religious ceremony.").MyStyle();
                }
                else
                {
                    if (dc.m.country == "US")
                        p7.Append(" The Plaintiff and Defendant were married on " + dc.m.marriage_date.ToDateLong() + ", in the City of " + dc.m.city.ToTitleCase() + ", the County of " + dc.m.county.ToTitleCase() + " and the State of " + dc.m.state.ToStateName() + ", in a civil ceremony.").MyStyle();
                    else
                        p7.Append(" The Plaintiff and Defendant were married on " + dc.m.marriage_date.ToDateLong() + ", in the City of " + dc.m.city.ToTitleCase() + " and the Country of " + dc.m.country.ToTitleCase() + ", in a civil ceremony.").MyStyle();
                }
                p7.Alignment = Alignment.both;
                p7.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p8.AppendLine("\tFOURTH:").MyStyleB();
                p8.Append(" No decree, judgment or order of divorce, annulment or dissolution of marriage has been granted to either party against the other in any court of competent jurisdiction of this state or any other state, territory or country and there is no other action for divorce, annulment or dissolution of marriage by either party against the other pending in any court.").MyStyle();
                p8.Alignment = Alignment.both;
                p8.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p9.AppendLine("\tFIFTH:").MyStyleB();
                if (dc.waiver_default == "1")
                {
                    if (dc.d.sex == "F")
                        p9.Append(" This action was commenced by filing the Summons with Notice with the County Clerk on " + dc.index_date.ToDateLong() + ". The Defendant was served personally with the Summons with Notice, Notice of Automatic Orders, Notice of Continuation of Healthcare Coverage and Notice of Guideline Maintenance. The Defendant has appeared and waived her right to answer.").MyStyle();
                    else
                        p9.Append(" This action was commenced by filing the Summons with Notice with the County Clerk on " + dc.index_date.ToDateLong() + ". The Defendant was served personally with the Summons with Notice, Notice of Automatic Orders, Notice of Continuation of Healthcare Coverage and Notice of Guideline Maintenance. The Defendant has appeared and waived his right to answer.").MyStyle();
                }
                if (dc.waiver_default == "2")
                    p9.Append(" This action was commenced by filing the Summons with Notice with the County Clerk on " + dc.index_date.ToDateLong() + ". The Defendant was served personally with the Summons with Notice, Notice of Automatic Orders, Notice of Continuation of Healthcare Coverage and Notice of Guideline Maintenance. The Defendant has defaulted in appearance.").MyStyle();
                p9.Alignment = Alignment.both;
                p9.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p10.AppendLine("\tSIXTH:").MyStyleB();
                p10.Append(" The Defendant is not in the military service of the United States, New York or any other state or territory.").MyStyle();
                p10.Alignment = Alignment.both;
                p10.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p11.AppendLine("\tSEVENTH:").MyStyleB();
                p11.Append(" There is no child as a result of this marriage and no child is expected.").MyStyle();
                p11.Alignment = Alignment.both;
                p11.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p12.AppendLine("\tEIGHTH:").MyStyleB();
                p12.Append(" The grounds for divorce which are alleged in the Verified Complaint were proved as follows: ").MyStyle();
                if (dc.r.grounds == "1")
                {
                    p12.Append("Irretrievable Breakdown of the Relationship for at Least Six Months (DRL Sec. 170(7))").MyStyleU();
                    p12.Append(":  The relationship between the Plaintiff and Defendant has broken down irretrievably for a period of at least six months.").MyStyle();
                }
                else if (dc.r.grounds == "2")
                {
                    p12.Append("Abandonment for at least One Year (DRL Sec. 170 subd. (2))").MyStyleU();
                    p12.Append(":  The abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "3")
                {
                    p12.Append("Constructive Abandonment (Cessation of Sexual Relations) at least for One Year (DRL Sec. 170 subd. (2))").MyStyleU();
                    p12.Append(":  The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "4")
                {
                    p12.Append("Cruel And Inhuman Treatment (DRL Sec. 170 subd. (1))").MyStyleU();
                    p12.Append(":  The cruel and inhuman treatment of the Plaintiff by the Defendant.").MyStyle();
                }
                if (dc.waiver_default == "1")
                    p12.Append(" Both the Plaintiff and Defendant have so stated under oath.").MyStyle();
                if (dc.waiver_default == "2")
                    p12.Append(" The Plaintiff have so stated under oath.").MyStyle();
                p12.Alignment = Alignment.both;
                p12.SetLineSpacing(LineSpacingType.Line, 1.5f);
                if (dc.m.marriage_clergyman == "R")
                {
                    p13.AppendLine("\tNINTH:").MyStyleB();
                    if (dc.p.sex == "F")
                        p13.Append(" A sworn statement pursuant to DRL Sec. 253 that the Plaintiff has taken all steps within her power to remove all barriers to the Defendant's remarriage following the divorce was served on the Defendant.").MyStyle();
                    else
                        p13.Append(" A sworn statement pursuant to DRL Sec. 253 that the Plaintiff has taken all steps within his power to remove all barriers to the Defendant's remarriage following the divorce was served on the Defendant.").MyStyle();
                }
                else
                {
                    p13.AppendLine("\tNINTH:").MyStyleB();
                    p13.Append(" A sworn statement as to the removal of barriers to remarriage is not required because the parties were married in a civil ceremony.").MyStyle();
                }
                p13.Alignment = Alignment.both;
                p13.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p14.AppendLine("\tTENTH:").MyStyleB();
                p14.Append(" Neither party is seeking maintenance from the other.").MyStyle();
                p14.Alignment = Alignment.both;
                p14.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p15.AppendLine("\tELEVENTH:").MyStyleB();
                p15.Append(" Equitable Distribution is not an issue.").MyStyle();
                p15.Alignment = Alignment.both;
                p15.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p16.AppendLine("\tTWELVTH:").MyStyleB();
                p16.Append(" There is no unemancipated child of the marriage").MyStyle();
                p16.Alignment = Alignment.both;
                p16.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p17.AppendLine("\tTHIRTEENTH:").MyStyleB();
                if (dc.p.country == "US")
                    p17.Append(" The Plaintiff's address is " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.state.ToStateName() + " - " + dc.p.zip).MyStyle();
                else
                    p17.Append(" The Plaintiff's address is " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.country.ToCountryName()).MyStyle();
                if (dc.p.check_ssn == "Y")
                    p17.Append(" and Social Security Number is " + dc.p.ssn + ".").MyStyle();
                else
                    p17.Append(" and the Plaintiff doesn't have a Social Security Number.").MyStyle();
                if (dc.d.country == "US")
                    p17.Append(" The Defendant's address is " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " - " + dc.d.zip).MyStyle();
                else
                    p17.Append(" The Defendant's address is " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName()).MyStyle();
                if (dc.d.check_ssn == "Y")
                    p17.Append(" and Social Security Number is " + dc.d.ssn + ".").MyStyle();
                else if (dc.d.check_ssn == "N")
                    p17.Append(" and the Defendant doesn't have a Social Security Number.").MyStyle();
                else if (dc.d.check_ssn == "R")
                    p17.Append(" and the Defendant refused to provide the Social Security Number.").MyStyle();
                p17.Alignment = Alignment.both;
                p17.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p18.Append("There are no unemancipated children of the marriage.").MyStyle();
                p18.AppendLine("The parties are covered by the following group health plans:").MyStyle();
                p18.AppendLine("\nPlaintiff").MyStyleU();
                p18.AppendLine("\tGroup Health Plan:  NOT APPLICABLE").MyStyle();
                p18.AppendLine("\nDefendant").MyStyleU();
                p18.AppendLine("\tGroup Health Plan:  NOT APPLICABLE").MyStyle();
                p18.SetLineSpacing(LineSpacingType.Line, 1.5f);
                if (dc.p.sex == "M")
                {
                    p19.AppendLine("\tFOURTEENTH:").MyStyleB();
                    p19.Append(" The Defendant may resume use of the prior surname: " + dc.d.maiden_name.ToTitleCase().ToUpperCase()).MyStyle();
                }
                else
                {
                    p19.AppendLine("\tFOURTEENTH:").MyStyleB();
                    p19.Append(" The Plaintiff may resume use of the prior surname: " + dc.p.maiden_name.ToTitleCase().ToUpperCase()).MyStyle();
                }
                p19.Alignment = Alignment.both;
                p19.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p20.AppendLine("\tFIFTEENTH:").MyStyleB();
                p20.Append(" Compliance with DRL Sec. 255 (1) and (2) has been satisfied as follows:  There is no agreement between the parties. Each party has been provided notice as required by DRL Sec. 255(1).").MyStyle();
                p20.Alignment = Alignment.both;
                p20.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p21.AppendLine("\tSIXTEENTH:").MyStyleB();
                p21.Append(" The Judgment of Divorce incorporates all ancillary issues, including the payment of counsel and experts' fees and expenses, which issues:").MyStyle();
                p21.Alignment = Alignment.both;
                p21.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p22.AppendLine("\t[  ] were settled by written settlement/separation agreement").MyStyle();
                p22.AppendLine("\t[  ] were settled by oral settlement/separation on the record").MyStyle();
                if (dc.waiver_default == "1")
                    p22.AppendLine("\t[X] were settled by written waivers or affidavits (the Affidavit of Plaintiff and the Affidavit of Defendant)").MyStyle();
                if (dc.waiver_default == "2")
                    p22.AppendLine("\t[  ] were settled by written waivers or affidavits (the Affidavit of Plaintiff and the Affidavit of Defendant)").MyStyle();
                p22.AppendLine("\t[  ] were determined by the Court ").MyStyle();
                p22.AppendLine("\t[  ] were determined by the Family Court order (custody, visitation or child support and/or spousal support issues only)").MyStyle();
                p22.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p23.Append("\t[  ] are not to be incorporated into the Judgment of Divorce, in that neither party to the divorce has contested any such issues based on the Affidavit of Plaintiff (which Defendant has not contested).").MyStyle();
                p23.Alignment = Alignment.both;
                p23.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p24.InsertPageBreakBeforeSelf();
                p24.Append("CONCLUSIONS OF LAW").MyStyleB();
                p24.Alignment = Alignment.center;
                p25.AppendLine("\n\tFIRST:").MyStyleB();
                p25.Append(" Residence as required by DRL Sec. 230 has been satisfied.").MyStyle();
                p25.Alignment = Alignment.both;
                p25.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p26.AppendLine("\tSECOND:").MyStyleB();
                p26.Append(" The requirements of DRL Sec. 255 have been satisfied.").MyStyle();
                p26.Alignment = Alignment.both;
                p26.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p27.AppendLine("\tTHIRD:").MyStyleB();
                p27.Append(" The requirements of DRL Sec. 240 1(a) including the Records Checking Requirements in DRL Sec. 240 1(a-1) have been satisfied.").MyStyle();
                p27.Alignment = Alignment.both;
                p27.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p28.AppendLine("\tFOURTH:").MyStyleB();
                p28.Append(" The requirements of DRL Sec. 240(1-b) have been satisfied.").MyStyle();
                p28.Alignment = Alignment.both;
                p28.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p29.AppendLine("\tFIFTH:").MyStyleB();
                p29.Append(" The requirements of DRL Sec. 236(B)(2)(b) have been satisfied.").MyStyle();
                p29.Alignment = Alignment.both;
                p29.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p30.AppendLine("\tSIXTH:").MyStyleB();
                p30.Append(" The requirements of DRL Sec. 236(B)(6) have been satisfied.").MyStyle();
                p30.Alignment = Alignment.both;
                p30.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p31.AppendLine("\tSEVENTH:").MyStyleB();
                if (dc.r.grounds == "1")
                    p31.Append(" DRL Sec. 170 subd. (7)").MyStyle();
                else if (dc.r.grounds == "2")
                    p31.Append(" DRL Sec. 170 subd. (2)").MyStyle();
                else if (dc.r.grounds == "3")
                    p31.Append(" DRL Sec. 170 subd. (2)").MyStyle();
                else if (dc.r.grounds == "4")
                    p31.Append(" DRL Sec. 170 subd. (1)").MyStyle();
                p31.Append(" is the ground alleged and all economic issues of equitable distribution of marital property, the payment or waiver of spousal support, the payment of child support, the payment of counsel and experts' fees and expenses, as well as custody and visitation with infant children of the marriage, have been resolved by the parties or determined by the Court and incorporated into the Judgment.").MyStyle();
                p31.Alignment = Alignment.both;
                p31.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p32.AppendLine("\tEIGHTH:").MyStyleB();
                p32.Append(" The Plaintiff is entitled to a judgment of divorce on the grounds of ").MyStyle();
                if (dc.r.grounds == "1")
                {
                    p32.Append("(DRL Sec. 170(7))").MyStyle();
                    p32.Append(" - The relationship between the Plaintiff and Defendant has broken down irretrievably for a period of at least six months,").MyStyle();
                }
                else if (dc.r.grounds == "2")
                {
                    p32.Append("(DRL Sec. 170 subd. (2))").MyStyle();
                    p32.Append(" - The abandonment of the Plaintiff by the Defendant for a period of more than one year,").MyStyle();
                }
                else if (dc.r.grounds == "3")
                {
                    p32.Append("(DRL Sec. 170 subd. (2))").MyStyle();
                    p32.Append(" - The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year,").MyStyle();
                }
                else if (dc.r.grounds == "4")
                {
                    p32.Append("(DRL Sec. 170 subd. (1))").MyStyle();
                    p32.Append(" - The cruel and inhuman treatment of the Plaintiff by the Defendant,").MyStyle();
                }
                p32.Append(" and granting the incidental relief awarded.").MyStyle();
                p32.Alignment = Alignment.both;
                p32.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p33.AppendLine("Dated:\n\n\n").MyStyle();
                p33.AppendLine("\t\t\t\t\t\t\t\t_______________________").MyStyle();
                p33.AppendLine("\t\t\t\t\t\t\t\tJ.S.C./Referee\n\n").MyStyle();
                p33.AppendLine("(Form UD-10 – Rev.3/1/16) ").MyStyle();
                p33.SetLineSpacing(LineSpacingType.Line, 1.5f);
                document.Save();
            }
        }
        public static void Divorce1_Judgement(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Judgement_Of_Divorce.docx"))
            {
                File.Delete(folder + "\\Judgement_Of_Divorce.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Judgement_Of_Divorce.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = document.InsertParagraph();
                Paragraph p13 = document.InsertParagraph();
                Paragraph p14 = document.InsertParagraph();
                Paragraph p15 = document.InsertParagraph();
                Paragraph p16 = document.InsertParagraph();
                Paragraph p17 = document.InsertParagraph();
                Paragraph p17_1 = document.InsertParagraph();
                Paragraph p18 = document.InsertParagraph();
                Paragraph p19 = document.InsertParagraph();
                Paragraph p20 = document.InsertParagraph();
                p1.AppendLine("\t\t\t\t          At the Matrimonial/IAS Part      of the New York ").MyStyle();
                p1.AppendLine("\t\t\t\t          Supreme Court at the Courthouse, " + venue.ToTitleCase() + " County,").MyStyle();
                p1.AppendLine("\t\t\t\t          on the        day of                     , " + DateTime.Now.Year).MyStyle();
                p1.AppendLine("Present:").MyStyle();
                p1.AppendLine("Hon.\t\t\t\t\tJustice").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\t\t\tCalendar No: ").MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tJUDGEMENT OF DIVORCE").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X").MyStyle();
                p2.AppendLine("This action was submitted to the referee/this Court for consideration on the\tday of\t\t, " + DateTime.Now.Year + ".").MyStyle();
                p2.IndentationBefore = 1.3f;
                p2.Alignment = Alignment.both;
                p2.SetLineSpacing(LineSpacingType.Line, 1.5f);
                if (dc.r.summons_usa == "Y")
                    p3.Append("\tThe Defendant was served personally within the State of New York.").MyStyle();
                else
                    p3.Append("\tThe Defendant was served personally outside the State of New York.").MyStyle();
                p3.Alignment = Alignment.both;
                p3.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p4.Append("The Plaintiff presented a Summons with Notice and Affidavit of Plaintiff constituting the facts of the matter.").MyStyle();
                p4.IndentationBefore = 1.3f;
                p4.Alignment = Alignment.both;
                p4.SetLineSpacing(LineSpacingType.Line, 1.5f);
                if (dc.waiver_default == "1")
                {
                    if (dc.d.sex == "F")
                        p5.Append("\tThe Defendant has appeared and waived her right to answer.").MyStyle();
                    else
                        p5.Append("\tThe Defendant has appeared and waived his right to answer.").MyStyle();
                }
                if (dc.waiver_default == "2")
                    p5.Append("\tThe Defendant has defaulted in appearance.").MyStyle();
                p5.Alignment = Alignment.both;
                p5.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p6.Append("\tThe Court accepted written proof of non-military service.").MyStyle();
                p6.Alignment = Alignment.both;
                p6.SetLineSpacing(LineSpacingType.Line, 1.5f);
                if (dc.p.country == "US")
                    p7.Append("\tThe Plaintiff's address is " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.state.ToStateName() + " - " + dc.p.zip).MyStyle();
                else
                    p7.Append("\tThe Plaintiff's address is " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.country.ToCountryName()).MyStyle();
                if (dc.p.check_ssn == "Y")
                    p7.Append(" and Social Security Number is " + dc.p.ssn + ".").MyStyle();
                else
                    p7.Append(" and the Plaintiff doesn't have a Social Security Number.").MyStyle();
                if (dc.d.country == "US")
                    p7.Append(" The Defendant's address is " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " - " + dc.d.zip).MyStyle();
                else
                    p7.Append(" The Defendant's address is " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName()).MyStyle();
                if (dc.d.check_ssn == "Y")
                    p7.Append(" and Social Security Number is " + dc.d.ssn + ".").MyStyle();
                else if (dc.d.check_ssn == "N")
                    p7.Append(" and the Defendant doesn't have a Social Security Number.").MyStyle();
                else if (dc.d.check_ssn == "R")
                    p7.Append(" and the Defendant refused to provide the Social Security Number.").MyStyle();
                p7.Alignment = Alignment.both;
                p7.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p8.AppendLine("\tNow").MyStyleB();
                p8.Append(" on motion of " + dc.p.name.ToTitleCase() + ", the Plaintiff Pro-se, it is:").MyStyle();
                p8.Alignment = Alignment.both;
                p8.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p9.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p9.Append(" that the Referee's Report, if any, is confirmed and it is further").MyStyle();
                p9.Alignment = Alignment.both;
                p9.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p10.AppendLine("\tORDERED, ADJUDGED AND DECREED").MyStyleB();
                p10.Append(" that the Plaintiff is hereby granted a judgment of divorce against the Defendant and that the marriage between the Plaintiff, " + pname + " and the Defendant " + dname + ", is hereby dissolved by reason of").MyStyle();
                if (dc.r.grounds == "1")
                    p10.Append(":  The relationship between the Plaintiff and Defendant has broken down irretrievably for a period of at least six months, pursuant to DRL Sec. 170(7), ").MyStyle();
                else if (dc.r.grounds == "2")
                    p10.Append(":  The abandonment of the Plaintiff by the Defendant for a period of more than one year, pursuant to DRL Sec. 170 subd. (2), ").MyStyle();
                else if (dc.r.grounds == "3")
                    p10.Append(":  The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year, pursuant to DRL Sec. 170 subd. (2), ").MyStyle();
                else if (dc.r.grounds == "4")
                    p10.Append(":  The cruel and inhuman treatment of the Plaintiff by the Defendant, pursuant to DRL Sec. 170 subd. (1), ").MyStyle();
                if (dc.waiver_default == "1")
                    p10.Append("and both the Plaintiff and Defendant have so stated under oath in their affidavits; and it is further").MyStyle();
                if (dc.waiver_default == "2")
                    p10.Append("and the Plaintiff has so stated under oath in the Affidavit of Plaintiff; and it is further").MyStyle();
                p10.Alignment = Alignment.both;
                p10.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p11.AppendLine("The requirements of DRL Sec. 240 1(a-1) have been met and the Court having considered the results of said inquiries, it is further").MyStyleB();
                p11.Alignment = Alignment.both;
                p11.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p12.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p12.Append(" that there are no court orders with regard to custody, visitation or maintenance to be continued; and it is further").MyStyle();
                p12.Alignment = Alignment.both;
                p12.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p13.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p13.Append(" that no maintenance was awarded because [X] neither party seeks maintenance, OR[ ] the Guideline Award of Maintenance under the Maintenance Guidelines Law(Ch. 269, L. 2015) was zero, OR[ ] the Court has denied the request for maintenance, ").MyStyle();
                p13.Alignment = Alignment.both;
                p13.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p14.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p14.Append(" that pursuant to the [] parties’ Settlement Agreement dated __________________ OR [] the court’s decision after trial, all parties shall duly execute all documents necessary to formally transfer title to real estate or co-op shares to the [] Plaintiff OR [] Defendant as set forth in the [] parties’ Settlement Agreement OR [] the court’s decision after trial, including, without limitation, an appropriate deed or other conveyance of title, and all other forms necessary to record such deed or other title documents (including the satisfaction or refinance of any mortgage if necessary) to convey ownership of the marital residence located at _______________________, no later than ______________________; OR [X] Not applicable; and it is further").MyStyle();
                p14.Alignment = Alignment.both;
                p14.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p15.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p15.Append(" that the Settlement Agreement entered into between the parties on the day of               , an original OR a transcript of which is on file with this Court and incorporated herein by reference, shall survive and shall not be merged into this judgment, and the parties are hereby directed to comply with all legally enforceable terms and conditions of said agreement as if such terms and conditions were set forth in their entirety herein; and it is further").MyStyle();
                p15.Alignment = Alignment.both;
                p15.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p16.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p16.Append(" that the Supreme Court shall retain jurisdiction to hear any applications to enforce the provisions of said Settlement Agreement or to enforce or modify the provisions of this judgment, provided the court retains jurisdiction of the matter concurrently with the Family Court for the purpose of specifically enforcing, such of the provisions of that (separation agreement)(stipulation agreement) as are capable of specific enforcement, to the extent permitted by law, and of modifying such judgment with respect to maintenance, support, custody or visitation to the extent permitted by law , or both; and it is further").MyStyle();
                p16.Alignment = Alignment.both;
                p16.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p17.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p17.Append(" that any applications brought in Supreme Court to enforce the provisions of said Settlement Agreement or to enforce or modify the provisions of this Judgment shall be brought in a County wherein one of the parties reside; provided that if there are minor children of the marriage, such applications shall be brought in a County wherein one of the parties or the child or children reside, except, in the discretion of the judge, for good cause. Good cause applications shall be made by motion or order to show cause. Where the address of either party and any child or children is unknown and not a matter of public record, or is subject to an existing confidentiality order pursuant to DRL § 254 or FCA § 154-b, such applications may be brought in the County where the Judgment was entered; and it is further").MyStyle();
                p17.Alignment = Alignment.both;
                p17.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p17_1.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p17_1.Append(" that both parties are authorized to resume the use of any prior surname, and it is further").MyStyle();
                p17_1.Alignment = Alignment.both;
                p17_1.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p18.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                if (dc.p.sex == "F")
                    p18.Append(" that the Plaintiff may resume use of the prior surname, " + dc.p.maiden_name.ToTitleCase() + " and it is further").MyStyle();
                else
                    p18.Append(" that the Defendant may resume use of the prior surname, " + dc.d.maiden_name.ToTitleCase() + " and it is further").MyStyle();
                p18.Alignment = Alignment.both;
                p18.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p19.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p19.Append(" that the Defendant shall be served with a copy of this Judgment, with notice of entry, by the Plaintiff within 20 days of such entry.").MyStyle();
                p19.Alignment = Alignment.both;
                p19.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p20.AppendLine("Dated:").MyStyle();
                p20.AppendLine("\t\t\t\t\t\t ENTER:\n\n").MyStyle();
                p20.AppendLine("\t\t\t\t\t\t\t\t___________________").MyStyle();
                p20.AppendLine("\t\t\t\t\t\t\t\tJ.S.C./Referee\n\n").MyStyle();
                p20.AppendLine("\t\t\t\t\t\t\t\t___________________").MyStyle();
                p20.AppendLine("\t\t\t\t\t\t\t\tCLERK\n").MyStyle();
                p20.AppendLine("(Form UD-11 - Eff. 5/31/18)").MyStyle();
                p20.SetLineSpacing(LineSpacingType.Line, 1.5f);
                document.Save();
            }
        }
        public static void Divorce2_UCS111(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\UCS-111.pdf"))
            {
                File.Delete(folder + "\\UCS-111.pdf");
            }
            string pdfTemplate = @"D:\home\GoNyLaw\Documents\UCS-111.pdf";
            string newFile = folder + @"\UCS-111.pdf";
            PdfReader pdfReader = new PdfReader(pdfTemplate);
            PdfReader.unethicalreading = true;
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
            AcroFields pdfFormFields = pdfStamper.AcroFields;
            ChildCareCalculations ccc = new ChildCareCalculations(dc);
            pdfFormFields.SetField("Supreme Court", "Yes");
            pdfFormFields.SetField("County", dc.county_venue.ToTitleCase());
            pdfFormFields.SetField("Index Number", dc.index_no);
            pdfFormFields.SetField("Date Action Commenced", dc.index_date);
            pdfFormFields.SetField("Number of Children Subject to Child Support Order", dc.ch.no_children);
            pdfFormFields.SetFieldProperty("Annual Gross Income Adjusted for Maintenance - Plaintiff", "textsize", 8f, null);
            pdfFormFields.SetFieldProperty("Annual Gross Income Adjusted for Maintenance - Defendant", "textsize", 8f, null);
            pdfFormFields.SetFieldProperty("Amount of Child Support Payment By Plaintiff annually", "textsize", 8f, null);
            pdfFormFields.SetFieldProperty("Amount of Child Support Payment By Defendant annually", "textsize", 8f, null);
            pdfFormFields.SetField("Annual Gross Income Adjusted for Maintenance - Plaintiff", ccc.GetAdjustedGrossP());
            pdfFormFields.SetField("Annual Gross Income Adjusted for Maintenance - Defendant", ccc.GetAdjustedGrossD());
            if (dc.ch.c1_lives == "P")
            {
                pdfFormFields.SetField("Amount of Child Support Payment By Plaintiff annually", "0");
                pdfFormFields.SetField("Amount of Child Support Payment By Defendant annually", ccc.GetNCPChildSupportWithAddOn());
            }
            else
            {
                pdfFormFields.SetField("Amount of Child Support Payment By Plaintiff annually", ccc.GetNCPChildSupportWithAddOn());
                pdfFormFields.SetField("Amount of Child Support Payment By Defendant annually", "0");
            }
            pdfFormFields.SetField("Did the court make a finding that the child support award varied from the Child Support Standards Act amount", "No");
            pdfFormFields.SetField("Maintenance-Spousal Support", "None");
            pdfFormFields.SetField("Value of Maintenance-Spousal Support", "0");
            pdfStamper.FormFlattening = false;
            pdfStamper.Close();
        }
        public static void Divorce2_NYSCaseRegistry(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\NYS_Case_Registry.pdf"))
            {
                File.Delete(folder + "\\NYS_Case_Registry.pdf");
            }
            string pdfTemplate = @"D:\home\GoNyLaw\Documents\case-reg.pdf";
            string newFile = folder + @"\NYS_Case_Registry.pdf";
            PdfReader pdfReader = new PdfReader(pdfTemplate);
            PdfReader.unethicalreading = true;
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
            AcroFields pdfFormFields = pdfStamper.AcroFields;
            string[] pname_parts = dc.p.name.ToTitleCase().Split(' ');
            string[] dname_parts = dc.d.name.ToTitleCase().Split(' ');
            string[] c1name_parts = (dc.ch.c1_name.ToTitleCase() ?? "").Split(' ');
            string[] c2name_parts = (dc.ch.c2_name.ToTitleCase() ?? "").Split(' ');
            string[] c3name_parts = (dc.ch.c3_name.ToTitleCase() ?? "").Split(' ');
            string p_first = "", p_middle = "", p_last = "", d_first = "", d_middle = "", d_last = "", c1_first = "", c1_middle = "", c1_last = "", c2_first = "", c2_middle = "", c2_last = "", c3_first = "", c3_middle = "", c3_last = "";
            bool order_protection = false;
            p_first = pname_parts[0];
            if (pname_parts.Length == 2)
            {
                p_middle = "";
                p_last = pname_parts[1];
            }
            if (pname_parts.Length > 2)
            {
                p_middle = pname_parts[1].Substring(0, 1);
                for (int i = 2; i < pname_parts.Length; i++)
                {
                    if (string.IsNullOrWhiteSpace(p_last))
                        p_last += pname_parts[i];
                    else
                        p_last += (" " + pname_parts[i]);
                }
            }
            d_first = dname_parts[0];
            if (dname_parts.Length == 2)
            {
                d_middle = "";
                d_last = dname_parts[1];
            }
            if (dname_parts.Length > 2)
            {
                d_middle = dname_parts[1].Substring(0, 1);
                for (int i = 2; i < dname_parts.Length; i++)
                {
                    if (string.IsNullOrWhiteSpace(d_last))
                        d_last += dname_parts[i];
                    else
                        d_last += (" " + dname_parts[i]);
                }
            }
            if (c1name_parts.Length > 0)
            {
                c1_first = c1name_parts[0];
                if (c1name_parts.Length == 2)
                {
                    c1_middle = "";
                    c1_last = c1name_parts[1];
                }
                if (c1name_parts.Length > 2)
                {
                    c1_middle = c1name_parts[1].Substring(0, 1);
                    for (int i = 2; i < c1name_parts.Length; i++)
                    {
                        if (string.IsNullOrWhiteSpace(c1_last))
                            c1_last += c1name_parts[i];
                        else
                            c1_last += (" " + c1name_parts[i]);
                    }
                }
            }
            if (c2name_parts.Length > 0)
            {
                c2_first = c2name_parts[0];
                if (c2name_parts.Length == 2)
                {
                    c2_middle = "";
                    c2_last = c2name_parts[1];
                }
                if (c2name_parts.Length > 2)
                {
                    c2_middle = c2name_parts[1].Substring(0, 1);
                    for (int i = 2; i < c2name_parts.Length; i++)
                    {
                        if (string.IsNullOrWhiteSpace(c2_last))
                            c2_last += c2name_parts[i];
                        else
                            c2_last += (" " + c2name_parts[i]);
                    }
                }
            }
            if (c3name_parts.Length > 0)
            {
                c3_first = c3name_parts[0];
                if (c3name_parts.Length == 2)
                {
                    c3_middle = "";
                    c3_last = c3name_parts[1];
                }
                if (c3name_parts.Length > 2)
                {
                    c3_middle = c3name_parts[1].Substring(0, 1);
                    for (int i = 2; i < c3name_parts.Length; i++)
                    {
                        if (string.IsNullOrWhiteSpace(c3_last))
                            c3_last += c3name_parts[i];
                        else
                            c3_last += (" " + c3name_parts[i]);
                    }
                }
            }
            ChildCareCalculations ccc = new ChildCareCalculations(dc);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Court_Type[0]", "Supreme Court");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Court_County[0]", dc.county_venue.ToTitleCase());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Index_Number[0]", dc.index_no);
            if (dc.ch.c1_lives == "P")
            {
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payor_First_Name[0]", d_first);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payor_Last_Name[0]", d_last);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payor_Middle_Initial[0]", d_middle);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payor_SSN[0]", dc.d.ssn);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payor_DOB[0]", dc.d.dob);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payee_First_Name[0]", p_first);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payee_Last_Name[0]", p_last);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payee_Middle_Initial[0]", p_middle);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payee_SSN[0]", dc.p.ssn);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payee_DOB[0]", dc.p.dob);
            }
            else
            {
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payor_First_Name[0]", p_first);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payor_Last_Name[0]", p_last);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payor_Middle_Initial[0]", p_middle);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payor_SSN[0]", dc.p.ssn);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payor_DOB[0]", dc.p.dob);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payee_First_Name[0]", d_first);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payee_Last_Name[0]", d_last);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payee_Middle_Initial[0]", d_middle);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payee_SSN[0]", dc.d.ssn);
                pdfFormFields.SetField("topmostSubform[0].Page1[0].Payee_DOB[0]", dc.d.dob);
            }
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-1_First_Name[0]", c1_first);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-1_Last_Name[0]", c1_last);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-1_Middle_Initial[0]", c1_middle);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-1_SSN[0]", dc.ch.c1_ssn);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-1_DOB[0]", dc.ch.c1_dob);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-2_First_Name[0]", c2_first);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-2_Last_Name[0]", c2_last);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-2_Middle_Initial[0]", c2_middle);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-2_SSN[0]", dc.ch.c2_ssn);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-2_DOB[0]", dc.ch.c2_dob);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-3_First_Name[0]", c3_first);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-3_Last_Name[0]", c3_last);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-3_Middle_Initial[0]", c3_middle);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-3_SSN[0]", dc.ch.c3_ssn);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Child-3_DOB[0]", dc.ch.c3_dob);
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Order_Expiration[0]", "Youngest_Child_21");
            if (dc.co.any_order == "Y" && (dc.co.c1_ordertype == "O" || dc.co.c2_ordertype == "O" || dc.co.c3_ordertype == "O" || dc.co.c4_ordertype == "O"))
                order_protection = true;
            if (order_protection)
            {
                pdfFormFields.SetField("topmostSubform[0].Page1[0].OP_Granted[0]", "Yes");
                if (dc.ch.c1_lives == "P")
                    pdfFormFields.SetField("topmostSubform[0].Page1[0].OP_Granted_Party[0]", "Payee");
                else
                    pdfFormFields.SetField("topmostSubform[0].Page1[0].OP_Granted_Party[0]", "Payor");
            }
            else
                pdfFormFields.SetField("topmostSubform[0].Page1[0].OP_Granted[0]", "No");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Confidential_Address[0]", "No");
            pdfStamper.FormFlattening = false;
            pdfStamper.Close();
        }



        //Divorce With Children
        public static void Divorce2_Summons(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            ChildCareCalculations ccc = new ChildCareCalculations(dc);
            ChildCalc cc = ccc.GetChildCalcData();
            int[] e = Miscellaneous.GetEligibleChildren(dc.ch, 18);
            int no_of_court_orders = 0;
            if (!ReferenceEquals(dc.co, null)) Int32.TryParse(dc.co.no_order, out no_of_court_orders);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Summons_With_Notice.docx"))
            {
                File.Delete(folder + "\\Summons_With_Notice.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Summons_With_Notice.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = null;
                if (e[7] > 0)
                    p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = null;
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y")
                    p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = null;
                Paragraph p13 = document.InsertParagraph();
                Paragraph p14 = document.InsertParagraph();
                Paragraph p15 = document.InsertParagraph();
                Paragraph p16 = document.InsertParagraph();
                Paragraph p17 = document.InsertParagraph();
                Paragraph p18 = document.InsertParagraph();
                Paragraph p19 = document.InsertParagraph();
                Paragraph p20 = document.InsertParagraph();
                Paragraph p21 = document.InsertParagraph();
                Paragraph p22 = document.InsertParagraph();
                Paragraph p23 = document.InsertParagraph();
                Paragraph p24 = document.InsertParagraph();
                Paragraph p25 = document.InsertParagraph();
                Paragraph p26 = document.InsertParagraph();
                Paragraph p27 = document.InsertParagraph();
                Paragraph p28 = document.InsertParagraph();
                Paragraph p29 = document.InsertParagraph();
                Paragraph p30 = document.InsertParagraph();
                Paragraph p31 = document.InsertParagraph();
                Paragraph p32 = document.InsertParagraph();
                Paragraph p33 = document.InsertParagraph();
                Paragraph p34 = document.InsertParagraph();
                Paragraph p35 = document.InsertParagraph();
                Paragraph p36 = document.InsertParagraph();
                Paragraph p37 = document.InsertParagraph();
                Paragraph p38 = document.InsertParagraph();
                p1.Append("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("-------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine("\t\t\t\t\t\t  Date Summons Filed: " + dc.index_date.ToDateLong()).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\t\t\t\t\t\t      Plaintiff Designates " + venue.ToTitleCase() + " County").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t      as the place of trial.").MyStyle();
                if (dc.basis_venue == "P")
                    p1.AppendLine("\t\t\t\t\t\t      The basis of venue is Plaintiff's Residence").MyStyle();
                else if (dc.basis_venue == "D")
                    p1.AppendLine("\t\t\t\t\t\t      The basis of venue is Defendant's Residence").MyStyle();
                else
                    p1.AppendLine("\t\t\t\t\t\t      The basis of venue is " + basis).MyStyle();
                p1.AppendLine("\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tSUMMONS WITH NOTICE").MyStyleB();
                p1.AppendLine("\t\t-against-").MyStyle();
                if (dc.basis_venue == "D")
                {
                    p1.AppendLine("\t\t\t\t\t\t\tDefendant resides at").MyStyle();
                    if (dc.p.country == "US")
                    {
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.d.street.ToTitleCase()).MyStyle();
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " " + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.d.street.ToTitleCase()).MyStyle();
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.d.city + ", " + dc.d.country.ToCountryName()).MyStyle();
                    }
                }
                else
                {
                    p1.AppendLine("\t\t\t\t\t\t\tPlaintiff resides at").MyStyle();
                    if (dc.p.country == "US")
                    {
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.p.street.ToTitleCase()).MyStyle();
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.state.ToStateName() + " " + dc.p.zip).MyStyle();
                    }
                    else
                    {
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.p.street.ToTitleCase()).MyStyle();
                        p1.AppendLine("\t\t\t\t\t\t\t" + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.country.ToCountryName()).MyStyle();
                    }
                }
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("--------------------------------------------------------X").MyStyle();
                p2.Append("\nACTION FOR DIVORCE").MyStyleB();
                p2.Alignment = Alignment.center;
                p3.AppendLine("\nTo the above named Defendant:").MyStyle();
                p4.Append("\nYOU ARE HEREBY SUMMONED ").MyStyleB();
                p4.Append("to serve a notice of appearance on the Plaintiff's Attorney within twenty (20) days after the service of this summons, exclusive of the day of service (or within thirty (30) days after the service is complete if this summons is not personally delivered to you within the State of New York); and in case of your failure to appear, judgment will be taken against you by default for the relief demanded in the notice set forth below.").MyStyle();
                p4.Alignment = Alignment.both;
                p5.AppendLine("\n\nDated: ").MyStyle();
                p5.AppendLine("\n\t\t\t\t\t\t__________________________________").MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + dc.p.name.ToTitleCase()).MyStyle();
                p5.AppendLine("\t\t\t\t\t\tPlaintiff Pro-se").MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + dc.p.street).MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + dc.p.city + ", " + dc.p.state.ToStateName() + " " + dc.p.zip).MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + dc.p.phone).MyStyle();
                p6.AppendLine("NOTICE:").MyStyleB();
                if (dc.r.grounds == "1")
                    p6.Append(" The nature of this action is to dissolve the marriage between the parties, on the grounds: DRL Section 170 subd. (7) - the relationship between Plaintiff and Defendant has broken down irretrievably for a period of at least six months.").MyStyle();
                else if (dc.r.grounds == "2")
                    p6.Append(" The nature of this action is to dissolve the marriage between the parties, on the grounds: DRL Section 170 subd. (2) - the abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                else if (dc.r.grounds == "3")
                    p6.Append(" The nature of this action is to dissolve the marriage between the parties, on the grounds: DRL Section 170 subd. (2) - the constructive abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                else if (dc.r.grounds == "4")
                    p6.Append(" The nature of this action is to dissolve the marriage between the parties, on the grounds: DRL Section 170 subd. (1) - the cruel and inhuman treatment of the Plaintiff by the Defendant.").MyStyle();
                p6.Alignment = Alignment.both;
                p7.InsertPageBreakBeforeSelf();
                p7.Append("The relief sought is a judgment of absolute divorce in favor of the Plaintiff dissolving the marriage between the parties in this action. The nature of any ancillary or additional relief demanded is:").MyStyle();
                p7.Alignment = Alignment.both;
                if (e[7] > 0)
                {
                    if (dc.ch.c1_lives == "P")
                    {
                        if (dc.ch.no_children == "1")
                            p8.AppendLine("That the Plaintiff shall have custody of the child of the marriage under the age of 18, namely ").MyStyle();
                        else
                            p8.AppendLine("That the Plaintiff shall have custody of the children of the marriage under the age of 18, namely ").MyStyle();
                    }
                    else
                    {
                        if (dc.ch.no_children == "1")
                            p8.AppendLine("That the Defendant shall have custody of the child of the marriage under the age of 18, namely ").MyStyle();
                        else
                            p8.AppendLine("That the Defendant shall have custody of the children of the marriage under the age of 18, namely ").MyStyle();
                    }
                    if (e[0] >= 1 && e[1] == 1)
                    {
                        e[7]--;
                        p8.Append(dc.ch.c1_name.ToTitleCase() + " born on " + dc.ch.c1_dob.ToDateLong()).MyStyle();
                        if (e[0] == 1)
                        {
                            p8.Append(".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p8.Append(" and ").MyStyle();

                        }
                        else
                        {
                            p8.Append(", ").MyStyle();

                        }
                    }
                    if (e[0] >= 2 && e[2] == 1)
                    {
                        e[7]--;
                        p8.Append(dc.ch.c2_name.ToTitleCase() + " born on " + dc.ch.c2_dob.ToDateLong()).MyStyle();
                        if (e[0] == 2)
                        {
                            p8.Append(".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p8.Append(" and ").MyStyle();

                        }
                        else
                        {
                            p8.Append(", ").MyStyle();

                        }
                    }
                    if (e[0] >= 3 && e[3] == 1)
                    {
                        e[7]--;
                        p8.Append(dc.ch.c3_name.ToTitleCase() + " born on " + dc.ch.c3_dob.ToDateLong()).MyStyle();
                        if (e[0] == 3)
                        {
                            p8.Append(".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p8.Append(" and ").MyStyle();

                        }
                        else
                        {
                            p8.Append(", ").MyStyle();

                        }
                    }
                    if (e[0] >= 4 && e[4] == 1)
                    {
                        e[7]--;
                        p8.Append(dc.ch.c4_name.ToTitleCase() + " born on " + dc.ch.c4_dob.ToDateLong()).MyStyle();
                        if (e[0] == 4)
                        {
                            p8.Append(".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p8.Append(" and ").MyStyle();

                        }
                        else
                        {
                            p8.Append(", ").MyStyle();

                        }
                    }
                    if (e[0] >= 5 && e[5] == 1)
                    {
                        e[7]--;
                        p8.Append(dc.ch.c5_name.ToTitleCase() + " born on " + dc.ch.c5_dob.ToDateLong()).MyStyle();
                        if (e[0] == 5)
                        {
                            p8.Append(".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p8.Append(" and ").MyStyle();

                        }
                        else
                        {
                            p8.Append(", ").MyStyle();

                        }
                    }
                    if (e[0] >= 6 && e[6] == 1)
                    {
                        p8.Append(dc.ch.c6_name.ToTitleCase() + " born on " + dc.ch.c6_dob.ToDateLong() + ".\n").MyStyle();

                    }
                    p8.IndentationBefore = 1.3f;
                    p8.Alignment = Alignment.both;
                }
                if (dc.ch.c1_lives == "P")
                {
                    if (dc.ch.no_children == "1")
                        p9.AppendLine("That the Defendant shall have reasonable rights of visitation with the child away from the custodial residence.").MyStyle();
                    else
                        p9.AppendLine("That the Defendant shall have reasonable rights of visitation with the children away from the custodial residence.").MyStyle();
                }
                else
                {
                    if (dc.ch.no_children == "1")
                        p9.AppendLine("That the Plaintiff shall have reasonable rights of visitation with the child away from the custodial residence.").MyStyle();
                    else
                        p9.AppendLine("That the Plaintiff shall have reasonable rights of visitation with the children away from the custodial residence.").MyStyle();
                }
                p9.Alignment = Alignment.both;
                p9.IndentationBefore = 1.3f;
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y")
                {
                    if (no_of_court_orders >= 1 && dc.co.c1_ordertype != "O")
                    {
                        p10.AppendLine("That the order, dated " + dc.co.c1_date.ToDateLong() + ", of the " + dc.co.c1_court_name.ToTitleCase().ToTitleCase() + " Court of " + dc.co.c1_county.ToTitleCase() + " County under Docket No. " + dc.co.c1_index + " shall be continued, ").MyStyle();
                        if (no_of_court_orders == 1)
                            p10.Append("and the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance, child support, custody and visitation.").MyStyle();
                    }
                    if (no_of_court_orders >= 2 && dc.co.c2_ordertype != "O")
                    {
                        p10.Append("and the order, dated " + dc.co.c2_date.ToDateLong() + ", of the " + dc.co.c2_court_name.ToTitleCase().ToTitleCase() + " Court of " + dc.co.c2_county.ToTitleCase() + " County under Docket No. " + dc.co.c2_index + " shall be continued, ").MyStyle();
                        if (no_of_court_orders == 2)
                            p10.Append("and the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance, child support, custody and visitation.").MyStyle();
                    }
                    if (no_of_court_orders >= 3 && dc.co.c3_ordertype != "O")
                    {
                        p10.Append("and the order, dated " + dc.co.c3_date.ToDateLong() + ", of the " + dc.co.c3_court_name.ToTitleCase().ToTitleCase() + " Court of " + dc.co.c3_county.ToTitleCase() + " County under Docket No. " + dc.co.c3_index + " shall be continued, ").MyStyle();
                        if (no_of_court_orders == 3)
                            p10.Append("and the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance, child support, custody and visitation.").MyStyle();
                    }
                    if (no_of_court_orders >= 4 && dc.co.c4_ordertype != "O")
                    {
                        p10.Append("and the order, dated " + dc.co.c4_date.ToDateLong() + ", of the " + dc.co.c4_court_name.ToTitleCase().ToTitleCase() + " Court of " + dc.co.c4_county.ToTitleCase() + " County under Docket No. " + dc.co.c4_index + " shall be continued, ").MyStyle();
                        if (no_of_court_orders == 4)
                            p10.Append("and the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance, child support, custody and visitation.").MyStyle();
                    }
                    p10.Alignment = Alignment.both;
                    p10.IndentationBefore = 1.3f;
                }
                p11.AppendLine("That the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance, child support, custody and visitation.").MyStyle();
                p11.IndentationBefore = 1.3f;
                p11.Alignment = Alignment.both;
                p13.AppendLine("That the parties do not require maintenance and no claim will be made by either party for maintenance. I am not seeking maintenance as payee as described in the Notice of Guideline Maintenance.").MyStyle();
                p13.IndentationBefore = 1.3f;
                p13.Alignment = Alignment.both;
                if (no_of_court_orders >= 1)
                {
                    if (dc.co.c1_ordertype == "S")
                    {
                        if (dc.ch.c1_lives == "P")
                            p14.AppendLine("That the Defendant shall pay to the Plaintiff " + dc.co.c1_amount.StringToWords() + " Dollars ($" + dc.co.c1_amount.ToFormattedAmount() + ") " + dc.co.c1_frequency.GetFrequency() + " for child support. ").MyStyle();
                        else
                            p14.AppendLine("That the Plaintiff shall pay to the Defendant " + dc.co.c1_amount.StringToWords() + " Dollars ($" + dc.co.c1_amount.ToFormattedAmount() + ") " + dc.co.c1_frequency.GetFrequency() + " for child support. ").MyStyle();
                    }
                    else if (dc.co.c2_ordertype == "S")
                    {
                        if (dc.ch.c1_lives == "P")
                            p14.AppendLine("That the Defendant shall pay to the Plaintiff " + dc.co.c2_amount.StringToWords() + " Dollars ($" + dc.co.c2_amount.ToFormattedAmount() + ") " + dc.co.c2_frequency.GetFrequency() + " for child support. ").MyStyle();
                        else
                            p14.AppendLine("That the Plaintiff shall pay to the Defendant " + dc.co.c2_amount.StringToWords() + " Dollars ($" + dc.co.c2_amount.ToFormattedAmount() + ") " + dc.co.c2_frequency.GetFrequency() + " for child support. ").MyStyle();
                    }
                    else if (dc.co.c3_ordertype == "S")
                    {
                        if (dc.ch.c1_lives == "P")
                            p14.AppendLine("That the Defendant shall pay to the Plaintiff " + dc.co.c3_amount.StringToWords() + " Dollars ($" + dc.co.c3_amount.ToFormattedAmount() + ") " + dc.co.c3_frequency.GetFrequency() + " for child support. ").MyStyle();
                        else
                            p14.AppendLine("That the Plaintiff shall pay to the Defendant " + dc.co.c3_amount.StringToWords() + " Dollars ($" + dc.co.c3_amount.ToFormattedAmount() + ") " + dc.co.c3_frequency.GetFrequency() + " for child support. ").MyStyle();
                    }
                    else if (dc.co.c4_ordertype == "S")
                    {
                        if (dc.ch.c1_lives == "P")
                            p14.AppendLine("That the Defendant shall pay to the Plaintiff " + dc.co.c4_amount.StringToWords() + " Dollars ($" + dc.co.c4_amount.ToFormattedAmount() + ") " + dc.co.c4_frequency.GetFrequency() + " for child support. ").MyStyle();
                        else
                            p14.AppendLine("That the Plaintiff shall pay to the Defendant " + dc.co.c4_amount.StringToWords() + " Dollars ($" + dc.co.c4_amount.ToFormattedAmount() + ") " + dc.co.c4_frequency.GetFrequency() + " for child support. ").MyStyle();
                    }
                    else
                    {
                        if (dc.ch.c1_lives == "P")
                            p14.AppendLine("That the Defendant shall pay to the Plaintiff " + ccc.GetNCPMonthlyChildSupportWithAddOn().StringToWords() + " Dollars ($" + ccc.GetNCPMonthlyChildSupportWithAddOn() + ") per month for child support. ").MyStyle();
                        else
                            p14.AppendLine("That the Plaintiff shall pay to the Defendant " + ccc.GetNCPMonthlyChildSupportWithAddOn().StringToWords() + " Dollars ($" + ccc.GetNCPMonthlyChildSupportWithAddOn() + ") per month for child support. ").MyStyle();
                    }
                    p14.Alignment = Alignment.both;
                    p14.IndentationBefore = 1.3f;
                }
                else
                {
                    if (!ReferenceEquals(dc.co, null) && dc.ch.c1_lives == "P")
                        p14.AppendLine("That the Defendant shall pay to the Plaintiff " + ccc.GetNCPMonthlyChildSupportWithAddOn().StringToWords() + " Dollars ($" + ccc.GetNCPMonthlyChildSupportWithAddOn() + ") per month for child support. ").MyStyle();
                    else
                        p14.AppendLine("That the Plaintiff shall pay to the Defendant " + ccc.GetNCPMonthlyChildSupportWithAddOn().StringToWords() + " Dollars ($" + ccc.GetNCPMonthlyChildSupportWithAddOn() + ") per month for child support. ").MyStyle();
                    p14.Alignment = Alignment.both;
                    p14.IndentationBefore = 1.3f;
                }
                if (!ReferenceEquals(dc.co, null) && dc.h.plan_holder == "P" || dc.h.plan_holder == "D")
                {
                    if (dc.h.plan_holder == "P")
                    {
                        if (dc.ch.no_children == "1")
                            p15.AppendLine("That the Plaintiff shall provide health insurance benefits to the child until the age of 21 years unless sooner emancipated.").MyStyle();
                        else
                            p15.AppendLine("That the Plaintiff shall provide health insurance benefits to the children until the age of 21 years unless sooner emancipated.").MyStyle();
                    }
                    else
                    {
                        if (dc.ch.no_children == "1")
                            p15.AppendLine("That the Defendant shall provide health insurance benefits to the child until the age of 21 years unless sooner emancipated.").MyStyle();
                        else
                            p15.AppendLine("That the Defendant shall provide health insurance benefits to the children until the age of 21 years unless sooner emancipated.").MyStyle();
                    }
                }
                else
                {
                    if (dc.h.health_cover == "P")
                    {
                        if (dc.ch.no_children == "1")
                            p15.AppendLine("That the Plaintiff shall provide health insurance benefits to the child until the age of 21 years unless sooner emancipated.").MyStyle();
                        else
                            p15.AppendLine("That the Plaintiff shall provide health insurance benefits to the children until the age of 21 years unless sooner emancipated.").MyStyle();
                    }
                    else
                    {
                        if (dc.ch.no_children == "1")
                            p15.AppendLine("That the Defendant shall provide health insurance benefits to the child until the age of 21 years unless sooner emancipated.").MyStyle();
                        else
                            p15.AppendLine("That the Defendant shall provide health insurance benefits to the children until the age of 21 years unless sooner emancipated.").MyStyle();
                    }
                }
                p15.IndentationBefore = 1.3f;
                p15.Alignment = Alignment.both;
                if (dc.ch.c1_lives == "P")
                    p16.AppendLine("That the Defendant shall pay " + ccc.GetNCPPercent() + " percent of the unreimbursed medical expenses of the child until the age of 21 years unless sooner emancipated.").MyStyle();
                if (dc.ch.c1_lives == "D")
                    p16.AppendLine("That the Plaintiff shall pay " + ccc.GetNCPPercent() + " percent of the unreimbursed medical expenses of the child until the age of 21 years unless sooner emancipated.").MyStyle();
                p16.IndentationBefore = 1.3f;
                p16.Alignment = Alignment.both;
                p17.AppendLine("\tThat the parties do not require payment of counsel and experts' fees and expenses.").MyStyle();
                p17.Alignment = Alignment.both;
                p18.AppendLine("\tThat both parties may resume the use of any prior surname.").MyStyle();
                p18.Alignment = Alignment.both;
                p19.AppendLine("That the Court grant such other and further relief as the Court may deem fit and proper.").MyStyle();
                p19.IndentationBefore = 1.3f;
                p19.Alignment = Alignment.both;
                p20.AppendLine("The parties have divided up the marital property and no claim will be made by either party under equitable distribution.").MyStyle();
                p20.IndentationBefore = 1.3f;
                p20.Alignment = Alignment.both;
                p21.AppendLine("Notice of Automatic Orders pursuant to DRL Sec. 236(B)(2) and Notice Concerning Continuation of Health Care Coverage pursuant to DRL Sec. 255(1) and Notice of Guideline Maintenance pursuant to the Maintenance Guidelines Law ([S. 5678/A. 7645], Chapter 269, Laws of 2015) accompany this summons.").MyStyle();
                p21.Alignment = Alignment.both;
                p22.AppendLine("\n(Form UD-1 - 1/25/16)").MyStyle();
                p22.Alignment = Alignment.both;
                p23.InsertPageBreakBeforeSelf();
                p23.AppendLine("NOTICE OF ENTRY OF AUTOMATIC ORDERS (D.R.L. 236) Rev. 1/13").MyStyleB();
                p24.AppendLine("FAILURE TO COMPLY WITH THESE ORDERS MAY BE DEEMED").MyStyleB();
                p24.AppendLine("A CONTEMPT OF COURT").MyStyleB();
                p24.Alignment = Alignment.center;
                p25.Append("\n\nPURSUANT TO the Uniform Rules of the Trial Courts and DOMESTIC RELATIONS LAW Section 236, Part B, Section 2, both you and your spouse (the parties) are bound by the following").MyStyle();
                p25.Append(" AUTOMATIC ORDERS").MyStyleB();
                p25.Append(", which have been entered against you and your spouse in your divorce action pursuant to 22 NYCRR Section 202.16(a) and which shall  remain in full force and effect during the pendency of the action, unless terminated, modified or amended by further order of the court or upon written agreement between the parties:").MyStyle();
                p25.Alignment = Alignment.both;
                p26.AppendLine("(1)  ORDERED:  Neither party shall sell, transfer, encumber, conceal, assign, remove or in any way dispose of, without the consent of the other party in writing, or by order of the court, any property (including, but not limited to, real estate, personal property, cash accounts, stocks, mutual funds, bank accounts, cars and boats) individually or jointly held by the parties, except in the usual course of business, for customary and usual household expenses or for reasonable attorney's fee in connection with this action.").MyStyle();
                p26.Alignment = Alignment.both;
                p27.AppendLine("(2)  ORDERED:  Neither party shall transfer, encumber, assign, remove, withdraw or in any way dispose of any tax deferred funds, stocks or other assets held in any individual retirement accounts, 401k accounts, profit sharing plans, Keogh accounts, or any other pension or retirement account and the parties shall further refrain from applying for or requesting the payment of retirement benefits or annuity payments of any kind, without the consent of the other party in writing, or upon further order of the court; except that any party who is already in pay status may continue to receive such payments thereunder.").MyStyle();
                p27.Alignment = Alignment.both;
                p28.AppendLine("(3)  ORDERED:  Neither party shall incur unreasonable debts hereafter, including, but not limited to, further borrowing against any credit line secured by the family residence, further encumbrancing any assets, or unreasonably using credit cards or cash advances against credit cards, except in the usual course of business or for customary or usual housing expenses, or for reasonable attorney's fees in connection with this action.").MyStyle();
                p28.Alignment = Alignment.both;
                p29.AppendLine("(4)  ORDERED:  Neither party shall cause the other party or the children of the marriage to be removed from any existing medical, hospital and dental insurance coverage and each party shall maintain the existing medical, hospital and dental insurance coverage in full force and effect.").MyStyle();
                p29.Alignment = Alignment.both;
                p30.AppendLine("(5)  ORDERED:  Neither party shall change the beneficiaries of any existing life insurance policies and each party shall maintain the existing life insurance, automobile insurance, homeowners and renters insurance policies in full force and effect.").MyStyle();
                p30.Alignment = Alignment.both;
                p31.AppendLine("IMPORTANT NOTE:  ").MyStyleB();
                p31.Append("After service of Summons with Notice or Summons and Complaint for divorce, if you or your spouse wishes to modify or dissolve the automatic orders, you must ask the court for approval to do so, or enter into a written modification agreement with your spouse duly signed and acknowledged before a notary public.").MyStyle();
                p31.Alignment = Alignment.both;
                p32.InsertPageBreakBeforeSelf();
                p32.AppendLine("NOTICE CONCERNING CONTINUATION OF").MyStyleB();
                p32.AppendLine("HEALTH CARE COVERAGE").MyStyleB();
                p32.AppendLine("(Required by Section 255(1) of the Domestic Relations Law)").MyStyle();
                p32.Alignment = Alignment.center;
                p33.AppendLine("PLEASE TAKE NOTICE that once a judgment of divorce is signed in this action, both you and your spouse may or may not continue to be eligible for coverage under each other's health insurance plan, depending on the terms of the plan.").MyStyleB();
                p33.Alignment = Alignment.both;
                p34.InsertPageBreakBeforeSelf();
                p34.Append("NOTICE OF GUIDELINE MAINTENANCE").MyStyleB();
                p34.Alignment = Alignment.center;
                p35.AppendLine("If your divorce was commenced on or after January 25, 2016, this Notice is required to be given to you by the Supreme Court of the county where your divorce was filed to comply with the Maintenance Guidelines Law ([S. 5678/A. 7645], Chapter 269, Laws of 2015) because you may not have counsel in this action to advise you.").MyStyle();
                p35.Append(" It does not mean that your spouse (the person you are married to) is seeking or offering an award of \"Maintenance\" in this action. \"Maintenance\" means the amount to be paid to the other spouse for support after the divorce is final.").MyStyleB();
                p35.Alignment = Alignment.both;
                p36.AppendLine("You are hereby given notice that under the Maintenance Guidelines Law (Chapter 269, Laws of 2015), there is an obligation to award the guideline amount of maintenance on income up to $178,000 to be paid by the party with the higher income (the maintenance payor) to the party with the lower income (the maintenance payee) according to a formula, unless the parties agree otherwise or waive this right.  Depending on the incomes of the parties, the obligation might fall on either the Plaintiff or Defendant in the action.").MyStyle();
                p36.Alignment = Alignment.both;
                p37.AppendLine("There are two formulas to determine the amount of the obligation. If you and your spouse have no children, the higher formula will apply.  If there are children of the marriage, the lower formula will apply, but only if the maintenance payor is paying child support to the other spouse who has the children as the custodial parent. Otherwise the higher formula will apply.").MyStyle();
                p37.Alignment = Alignment.both;
                p38.AppendLine("Lower Formula").MyStyleB();
                p38.AppendLine("1. Multiply Maintenance Payor's Income by 20%.").MyStyle();
                p38.AppendLine("2. Multiply Maintenance Payee's Income by 25%.").MyStyle();
                p38.AppendLine("Subtract Line 2 from Line 1 = ").MyStyle();
                p38.Append("Result 1").MyStyleB();
                p38.AppendLine("Subtract Maintenance Payor's Income from 40 % of Combined Income* = ").MyStyle();
                p38.Append("Result 2").MyStyleB();
                p38.AppendLine("Enter the lower of ").MyStyle();
                p38.Append("Result 1").MyStyleB();
                p38.Append(" or ").MyStyle();
                p38.Append("Result 2").MyStyleB();
                p38.Append(", but if less than or equal to zero, enter zero.").MyStyle();
                p38.AppendLine("THIS IS THE CALCULATED GUIDELINE AMOUNT OF MAINTENANCE WITH THE LOWER FORMULA.").MyStyleB();
                p38.AppendLine("\nHigher Formula").MyStyleB();
                p38.AppendLine("1. Multiply Maintenance Payor's Income by 30%.").MyStyle();
                p38.AppendLine("2. Multiply Maintenance Payee's Income by 20%.").MyStyle();
                p38.AppendLine("Subtract Line 2 from Line 1 = ").MyStyle();
                p38.Append("Result 1").MyStyleB();
                p38.AppendLine("Subtract Maintenance Payor's Income from 40 % of Combined Income* = ").MyStyle();
                p38.Append("Result 2").MyStyleB();
                p38.AppendLine("Enter the lower of ").MyStyle();
                p38.Append("Result 1").MyStyleB();
                p38.Append(" or ").MyStyle();
                p38.Append("Result 2").MyStyleB();
                p38.Append(", but if less than or equal to zero, enter zero.").MyStyle();
                p38.AppendLine("THIS IS THE CALCULATED GUIDELINE AMOUNT OF MAINTENANCE WITH THE HIGHER FORMULA.").MyStyleB();
                p38.AppendLine("\n* Combined Income equals Maintenance Payor's Income up to $178,000 plus Maintenance Payee's Income.").MyStyleB();
                p38.AppendLine("\nNote: The Court will determine how long maintenance will be paid in accordance with the statute.").MyStyleB();
                p38.AppendLine("\n(Eff. 1/25/16)").MyStyle();
                document.Save();
            }
        }
        public static void Divorce2_VerifiedComplaint(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            ChildCareCalculations ccc = new ChildCareCalculations(dc);
            ChildCalc cc = ccc.GetChildCalcData();
            int[] e = Miscellaneous.GetEligibleChildren(dc.ch, 18);
            int no_of_court_orders = 0;
            if (!ReferenceEquals(dc.co, null))
                Int32.TryParse(dc.co.no_order, out no_of_court_orders);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Verified_Complaint.docx"))
            {
                File.Delete(folder + "\\Verified_Complaint.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Verified_Complaint.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = null;
                if (e[7] > 0)
                    p12 = document.InsertParagraph();
                Paragraph p13 = document.InsertParagraph();
                Paragraph p14 = document.InsertParagraph();
                Paragraph p15 = document.InsertParagraph();
                Paragraph p16 = document.InsertParagraph();
                Paragraph p17 = document.InsertParagraph();
                Paragraph p18 = document.InsertParagraph();
                Paragraph p19 = document.InsertParagraph();
                Paragraph p20 = document.InsertParagraph();
                Paragraph p21 = document.InsertParagraph();
                Paragraph p22 = document.InsertParagraph();
                Paragraph p23 = document.InsertParagraph();
                Paragraph p24 = document.InsertParagraph();
                Paragraph p25 = document.InsertParagraph();
                p1.Append("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("------------------------------------------------------------X           Index No: " + dc.index_no).MyStyle();
                p1.AppendLine("\n" + pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tVERIFIED COMPLAINT").MyStyleB();
                p1.AppendLine("\t\t\t\t\t\t\tACTION FOR DIVORCE").MyStyleB();
                p1.AppendLine("\t\t\t\t-against-").MyStyle();
                p1.AppendLine("\n" + dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("------------------------------------------------------------X").MyStyle();
                p1.AppendLine("\n\t\tThe Plaintiff, " + dc.p.name.ToTitleCase() + ", complaining of the Defendant, alleges the following:").MyStyle();
                p1.AppendLine("\n\t\tFIRST:  ").MyStyleB();
                p1.Append("The parties are over the age of 18 years.").MyStyle();
                p2.AppendLine("\t\tSECOND:  ").MyStyleB();
                p2.Append("This court has jurisdiction to hear this action for divorce. ").MyStyle();
                if (dc.r.applicable == "1")
                {
                    p2.Append("The Plaintiff has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "2")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "3")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "4")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "5")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "6")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "7")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                else if (dc.r.applicable == "8")
                {
                    p2.Append("The Defendant has resided in New York State for a continuous period in excess of two years immediately preceding the commencement of this action.").MyStyle();
                }
                p2.Alignment = Alignment.both;
                p3.AppendLine("\t\tTHIRD:  ").MyStyleB();
                if (dc.m.country == "US")
                    p3.Append("The Plaintiff and Defendant were married to each other on " + dc.m.marriage_date.ToDateLong() + " in the City of " + dc.m.city.ToTitleCase() + ", the County of " + dc.m.county.ToTitleCase() + " and the State of " + dc.m.state.ToStateName() + ".").MyStyle();
                else
                    p3.Append("The Plaintiff and Defendant were married to each other on " + dc.m.marriage_date.ToDateLong() + " in the City of " + dc.m.city.ToTitleCase() + " and the Country of " + dc.m.country.ToCountryName() + ".").MyStyle();
                if (dc.m.marriage_clergyman == "C")
                {
                    p3.Append(" The marriage was ").MyStyle();
                    p3.Append("not").MyStyleU();
                    p3.Append(" performed by a clergyman, minister or leader of the Society for Ethical Culture.").MyStyle();
                }
                if (dc.m.marriage_clergyman == "R")
                    p3.Append(" To the best of the Plaintiff's knowledge the Plaintiff has taken all steps solely within the power of the Plaintiff to remove any barrier to the Defendant's remarriage.").MyStyle();
                p3.Alignment = Alignment.both;
                if (e[0] == 1)
                {
                    p4.AppendLine("\t\tFOURTH:  ").MyStyleB();
                    p4.Append("There is one child as a result of this marriage, namely:").MyStyle();
                }
                else
                {
                    p4.AppendLine("\t\tFOURTH:  ").MyStyleB();
                    p4.Append("There are " + e[0].ToWords() + " children as a result of this marriage, namely:").MyStyle();
                }
                p5.AppendLine("\tName\t\t\tSocial Security No.          Date of Birth          Address").MyStyleB();
                if (e[0] >= 1)
                {
                    if (dc.ch.c1_lives == "P")
                    {
                        p5.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_ssn + " - " + dc.ch.c1_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c1_lives == "D")
                    {
                        p5.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_ssn + " - " + dc.ch.c1_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p5.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_ssn + " - " + dc.ch.c1_dob.ToDateLong() + " - " + dc.ch.t1_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 2)
                {
                    p5.AppendLine();
                    if (dc.ch.c2_lives == "P")
                    {
                        p5.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.c2_ssn + " - " + dc.ch.c2_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c2_lives == "D")
                    {
                        p5.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.c2_ssn + " - " + dc.ch.c2_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p5.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.c2_ssn + " - " + dc.ch.c2_dob.ToDateLong() + " - " + dc.ch.t2_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 3)
                {
                    p5.AppendLine();
                    if (dc.ch.c3_lives == "P")
                    {
                        p5.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.c3_ssn + " - " + dc.ch.c3_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c3_lives == "D")
                    {
                        p5.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.c3_ssn + " - " + dc.ch.c3_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p5.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.c3_ssn + " - " + dc.ch.c3_dob.ToDateLong() + " - " + dc.ch.t3_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 4)
                {
                    p5.AppendLine();
                    if (dc.ch.c4_lives == "P")
                    {
                        p5.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.c4_ssn + " - " + dc.ch.c4_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c4_lives == "D")
                    {
                        p5.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.c4_ssn + " - " + dc.ch.c4_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p5.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.c4_ssn + " - " + dc.ch.c4_dob.ToDateLong() + " - " + dc.ch.t4_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 5)
                {
                    p5.AppendLine();
                    if (dc.ch.c5_lives == "P")
                    {
                        p5.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.c5_ssn + " - " + dc.ch.c5_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c5_lives == "D")
                    {
                        p5.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.c5_ssn + " - " + dc.ch.c5_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p5.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.c5_ssn + " - " + dc.ch.c5_dob.ToDateLong() + " - " + dc.ch.t5_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 6)
                {
                    p5.AppendLine();
                    if (dc.ch.c6_lives == "P")
                    {
                        p5.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.c6_ssn + " - " + dc.ch.c6_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c6_lives == "D")
                    {
                        p5.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.c6_ssn + " - " + dc.ch.c6_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p5.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.c6_ssn + " - " + dc.ch.c6_dob.ToDateLong() + " - " + dc.ch.t6_address.ToTitleCase()).MyStyle();
                    }
                }
                p6.AppendLine("\tThere is no other child as a result of this marriage, and no other child is expected.  All of the children are common to both parties. No child is dependent upon the parties due to a mental or physical disability.").MyStyle();
                p6.Alignment = Alignment.both;
                if (dc.p.country == "US")
                    p7.AppendLine("\t\tThe Plaintiff resides at " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.state.ToStateName() + " " + dc.p.zip).MyStyle();
                else
                    p7.AppendLine("\t\tThe Plaintiff resides at " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.country.ToCountryName()).MyStyle();
                if (dc.d.country == "US")
                    p7.AppendLine("\t\tThe Defendant resides at " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " " + dc.d.zip).MyStyle();
                else
                    p7.AppendLine("\t\tThe Defendant resides at " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName()).MyStyle();
                p7.AppendLine("\nThe parties are covered by the following group health plans:").MyStyle();
                if (dc.h.plan_holder == "P")
                {
                    p7.AppendLine("\nPlaintiff").MyStyleU();
                    p7.AppendLine("\n\tGroup Health Plan: " + dc.h.name.ToTitleCase()).MyStyle();
                    p7.AppendLine("\tAddress: " + dc.h.address).MyStyle();
                    p7.AppendLine("\tIdentification Number: " + dc.h.id_no).MyStyle();
                    if (!string.IsNullOrWhiteSpace(dc.h.plan_admin))
                        p7.AppendLine("\tPlan Administrator: " + dc.h.plan_admin.ToTitleCase()).MyStyle();
                    else
                        p7.AppendLine("\tPlan Administrator: None").MyStyle();
                    string coveragetype = null;
                    if (dc.h.medical == "Y")
                        coveragetype = "Medical";
                    if (dc.h.dental == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Dental";
                        else
                            coveragetype += ", Dental";
                    }
                    if (dc.h.optical == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Optical";
                        else
                            coveragetype += ", Optical";
                    }
                    if (string.IsNullOrWhiteSpace(coveragetype))
                        p7.AppendLine("\tType of Coverage: None").MyStyle();
                    else
                        p7.AppendLine("\tType of Coverage: " + coveragetype).MyStyle();
                    p7.AppendLine("\n\tThe Plaintiff has no other group health plans.").MyStyle();
                    p7.AppendLine("\nDefendant").MyStyleU();
                    p7.AppendLine("\n\tGroup Health Plan:  NONE").MyStyle();
                }
                else if (dc.h.plan_holder == "D")
                {
                    p7.AppendLine("\nPlaintiff").MyStyleU();
                    p7.AppendLine("\n\tGroup Health Plan:  NONE").MyStyle();
                    p7.AppendLine("\nDefendant").MyStyleU();
                    p7.AppendLine("\n\tGroup Health Plan: " + dc.h.name.ToTitleCase()).MyStyle();
                    p7.AppendLine("\tAddress: " + dc.h.address).MyStyle();
                    p7.AppendLine("\tIdentification Number: " + dc.h.id_no).MyStyle();
                    if (!string.IsNullOrWhiteSpace(dc.h.plan_admin))
                        p7.AppendLine("\tPlan Administrator: " + dc.h.plan_admin.ToTitleCase()).MyStyle();
                    else
                        p7.AppendLine("\tPlan Administrator: None").MyStyle();
                    string coveragetype = null;
                    if (dc.h.medical == "Y")
                        coveragetype = "Medical";
                    if (dc.h.dental == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Dental";
                        else
                            coveragetype += ", Dental";
                    }
                    if (dc.h.optical == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Optical";
                        else
                            coveragetype += ", Optical";
                    }
                    if (string.IsNullOrWhiteSpace(coveragetype))
                        p7.AppendLine("\tType of Coverage: None").MyStyle();
                    else
                        p7.AppendLine("\tType of Coverage: " + coveragetype).MyStyle();
                    p7.AppendLine("\n\tThe Defendant has no other group health plans.").MyStyle();
                }
                else
                {
                    p7.AppendLine("\nPlaintiff").MyStyleU();
                    p7.AppendLine("\n\tGroup Health Plan:  NONE").MyStyle();
                    p7.AppendLine("\nDefendant").MyStyleU();
                    p7.AppendLine("\n\tGroup Health Plan:  NONE").MyStyle();
                }
                p8.AppendLine("\t\tFIFTH:  ").MyStyleB();
                p8.Append("The grounds for divorce are as follows:  ").MyStyle();
                if (dc.r.grounds == "1")
                {
                    p8.Append("Irretrievable Breakdown of the Relationship for at Least Six Months (DRL Sec. 170(7))").MyStyleU();
                    p8.Append(":  The relationship between the Plaintiff and Defendant has broken down irretrievably for a period of at least six months.").MyStyle();
                }
                else if (dc.r.grounds == "2")
                {
                    if (dc.p.country == "US")
                        p8.Append("Abandonment (DRL Sec. 170(2)): Commencing on or about " + dc.a.adate.ToDateLong() + " and continuing for a period of more than one (1) year immediately prior to the commencement of this action, the Defendant left the marital residence of the parties located at " + dc.a.street.ToTitleCase() + ", " + dc.a.city.ToTitleCase() + ", " + dc.a.state.ToStateName() + " and did not return. Such absence was without cause or justification, and was without the Plaintiff's consent.").MyStyleU();
                    else
                        p8.Append("Abandonment (DRL Sec. 170(2)): Commencing on or about " + dc.a.adate.ToDateLong() + " and continuing for a period of more than one (1) year immediately prior to the commencement of this action, the Defendant left the marital residence of the parties located at " + dc.a.street.ToTitleCase() + ", " + dc.a.city.ToTitleCase() + ", " + dc.a.country.ToCountryName() + " and did not return. Such absence was without cause or justification, and was without the Plaintiff's consent.").MyStyleU();
                }
                else if (dc.r.grounds == "3")
                {
                    p8.Append("Constructive Abandonment (Cessation of Sexual Relations) at least for One Year (DRL Sec. 170 subdc.d. (2))").MyStyleU();
                    p8.Append(":  The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "4")
                {
                    p8.Append("Cruel And Inhuman Treatment (DRL Sec. 170 subdc.d. (1))").MyStyleU();
                    p8.Append(":  The cruel and inhuman treatment of the Plaintiff by the Defendant.").MyStyle();
                }
                p8.Alignment = Alignment.both;
                p9.AppendLine("\t\tSIXTH:  ").MyStyleB();
                p9.Append("There is no judgment in any court for a divorce and no other matrimonial action for divorce between the parties is pending in this Court or in any other court of competent jurisdiction.").MyStyle();
                p9.Alignment = Alignment.both;
                p10.AppendLine("\t\tSEVENTH:  ").MyStyleB();
                p10.Append("Neither the Wife nor the Husband needs maintenance.").MyStyle();
                p11.AppendLine("\t\tWHEREFORE").MyStyleB();
                p11.Append(", the Plaintiff demands judgment against the Defendant, dissolving the marriage between the parties to this action and granting the following relief:").MyStyle();
                p11.Alignment = Alignment.both;
                if (e[7] > 0)
                {
                    if (dc.ch.c1_lives == "P")
                    {
                        if (dc.ch.no_children == "1")
                            p12.AppendLine("That the Plaintiff shall have custody of the child of the marriage under the age of 18, namely ").MyStyle();
                        else
                            p12.AppendLine("That the Plaintiff shall have custody of the children of the marriage under the age of 18, namely ").MyStyle();
                    }
                    else
                    {
                        if (dc.ch.no_children == "1")
                            p12.AppendLine("That the Defendant shall have custody of the child of the marriage under the age of 18, namely ").MyStyle();
                        else
                            p12.AppendLine("That the Defendant shall have custody of the children of the marriage under the age of 18, namely ").MyStyle();
                    }
                    if (e[0] >= 1 && e[1] == 1)
                    {
                        e[7]--;
                        p12.Append(dc.ch.c1_name.ToTitleCase() + " born on " + dc.ch.c1_dob.ToDateLong()).MyStyle();
                        if (e[0] == 1)
                        {
                            p12.Append(".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p12.Append(" and ").MyStyle();

                        }
                        else
                        {
                            p12.Append(", ").MyStyle();

                        }
                    }
                    if (e[0] >= 2 && e[2] == 1)
                    {
                        e[7]--;
                        p12.Append(dc.ch.c2_name.ToTitleCase() + " born on " + dc.ch.c2_dob.ToDateLong()).MyStyle();
                        if (e[0] == 2)
                        {
                            p12.Append(".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p12.Append(" and ").MyStyle();

                        }
                        else
                        {
                            p12.Append(", ").MyStyle();

                        }
                    }
                    if (e[0] >= 3 && e[3] == 1)
                    {
                        e[7]--;
                        p12.Append(dc.ch.c3_name.ToTitleCase() + " born on " + dc.ch.c3_dob.ToDateLong()).MyStyle();
                        if (e[0] == 3)
                        {
                            p12.Append(".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p12.Append(" and ").MyStyle();

                        }
                        else
                        {
                            p12.Append(", ").MyStyle();

                        }
                    }
                    if (e[0] >= 4 && e[4] == 1)
                    {
                        e[7]--;
                        p12.Append(dc.ch.c4_name.ToTitleCase() + " born on " + dc.ch.c4_dob.ToDateLong()).MyStyle();
                        if (e[0] == 4)
                        {
                            p12.Append(".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p12.Append(" and ").MyStyle();

                        }
                        else
                        {
                            p12.Append(", ").MyStyle();

                        }
                    }
                    if (e[0] >= 5 && e[5] == 1)
                    {
                        e[7]--;
                        p12.Append(dc.ch.c5_name.ToTitleCase() + " born on " + dc.ch.c5_dob.ToDateLong()).MyStyle();
                        if (e[0] == 5)
                        {
                            p12.Append(".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p12.Append(" and ").MyStyle();

                        }
                        else
                        {
                            p12.Append(", ").MyStyle();

                        }
                    }
                    if (e[0] >= 6 && e[6] == 1)
                    {
                        p12.Append(dc.ch.c6_name.ToTitleCase() + " born on " + dc.ch.c6_dob.ToDateLong()).MyStyle();

                    }
                    p12.IndentationBefore = 1.3f;
                    p12.Alignment = Alignment.both;
                }
                if (dc.ch.c1_lives == "D")
                {
                    if (dc.ch.no_children == "1")
                        p13.AppendLine("That the Plaintiff shall have reasonable rights of visitation with the child away from the custodial residence.").MyStyle();
                    else
                        p13.AppendLine("That the Plaintiff shall have reasonable rights of visitation with the children away from the custodial residence.").MyStyle();
                }
                else
                {
                    if (dc.ch.no_children == "1")
                        p13.AppendLine("That the Defendant shall have reasonable rights of visitation with the child away from the custodial residence.").MyStyle();
                    else
                        p13.AppendLine("That the Defendant shall have reasonable rights of visitation with the children away from the custodial residence.").MyStyle();
                }
                p13.Alignment = Alignment.both;
                p13.IndentationBefore = 1.3f;
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y")
                {
                    int co = 0;
                    if (no_of_court_orders >= 1 && dc.co.c1_ordertype != "O")
                    {
                        p14.AppendLine("That the order, dated " + dc.co.c1_date.ToDateLong() + ", of the " + dc.co.c1_court_name.ToTitleCase().ToTitleCase() + " Court of " + dc.co.c1_county.ToTitleCase() + " County under Docket No. " + dc.co.c1_index + " shall be continued, ").MyStyle();
                        co++;
                    }
                    if (no_of_court_orders >= 2 && dc.co.c2_ordertype != "O")
                    {
                        p14.Append("and the order, dated " + dc.co.c2_date.ToDateLong() + ", of the " + dc.co.c2_court_name.ToTitleCase().ToTitleCase() + " Court of " + dc.co.c2_county.ToTitleCase() + " County under Docket No. " + dc.co.c2_index + " shall be continued, ").MyStyle();
                        co++;
                    }
                    if (no_of_court_orders >= 3 && dc.co.c3_ordertype != "O")
                    {
                        p14.Append("and the order, dated " + dc.co.c3_date.ToDateLong() + ", of the " + dc.co.c3_court_name.ToTitleCase().ToTitleCase() + " Court of " + dc.co.c3_county.ToTitleCase() + " County under Docket No. " + dc.co.c3_index + " shall be continued, ").MyStyle();
                        co++;
                    }
                    if (no_of_court_orders >= 4 && dc.co.c4_ordertype != "O")
                    {
                        p14.Append("and the order, dated " + dc.co.c4_date.ToDateLong() + ", of the " + dc.co.c4_court_name.ToTitleCase().ToTitleCase() + " Court of " + dc.co.c4_county.ToTitleCase() + " County under Docket No. " + dc.co.c4_index + " shall be continued, ").MyStyle();
                        co++;
                    }
                    if (co != 0)
                        p14.Append("and the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance, child support, custody and visitation.").MyStyle();
                }
                else
                {
                    p14.AppendLine("That the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance, child support, custody and visitation.").MyStyle();
                }
                p14.Alignment = Alignment.both;
                p14.IndentationBefore = 1.3f;
                if (dc.waiver_default == "1")
                    p15.AppendLine("That the parties do not require maintenance and no claim will be made by either party for maintenance. Neither party is seeking maintenance as payee as described in the Notice of Guideline Maintenance as already agreed to in our written Affidavit of Plaintiff and Affidavit of Defendant.").MyStyle();
                else
                    p15.AppendLine("That the parties do not require maintenance and no claim will be made by either party for maintenance. I am not seeking maintenance as payee as described in the Notice of Guideline Maintenance.").MyStyle();
                p15.IndentationBefore = 1.3f;
                p15.Alignment = Alignment.both;
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y" && (dc.co.c1_ordertype == "S" || dc.co.c2_ordertype == "S" || dc.co.c3_ordertype == "S" || dc.co.c4_ordertype == "S"))
                {
                    if (dc.co.c1_ordertype == "S")
                    {
                        if (dc.ch.c1_lives == "P")
                            p16.AppendLine("That the Defendant shall pay to the Plaintiff " + dc.co.c1_amount.StringToWords() + " Dollars ($" + dc.co.c1_amount.ToFormattedAmount() + ") " + dc.co.c1_frequency.GetFrequency() + " for child support. ").MyStyle();
                        else
                            p16.AppendLine("That the Plaintiff shall pay to the Defendant " + dc.co.c1_amount.StringToWords() + " Dollars ($" + dc.co.c1_amount.ToFormattedAmount() + ") " + dc.co.c1_frequency.GetFrequency() + " for child support. ").MyStyle();
                    }
                    if (dc.co.c2_ordertype == "S")
                    {
                        if (dc.ch.c1_lives == "P")
                            p16.AppendLine("That the Defendant shall pay to the Plaintiff " + dc.co.c2_amount.StringToWords() + " Dollars ($" + dc.co.c2_amount.ToFormattedAmount() + ") " + dc.co.c2_frequency.GetFrequency() + " for child support. ").MyStyle();
                        else
                            p16.AppendLine("That the Plaintiff shall pay to the Defendant " + dc.co.c2_amount.StringToWords() + " Dollars ($" + dc.co.c2_amount.ToFormattedAmount() + ") " + dc.co.c2_frequency.GetFrequency() + " for child support. ").MyStyle();
                    }
                    if (dc.co.c3_ordertype == "S")
                    {
                        if (dc.ch.c1_lives == "P")
                            p16.AppendLine("That the Defendant shall pay to the Plaintiff " + dc.co.c3_amount.StringToWords() + " Dollars ($" + dc.co.c3_amount.ToFormattedAmount() + ") " + dc.co.c3_frequency.GetFrequency() + " for child support. ").MyStyle();
                        else
                            p16.AppendLine("That the Plaintiff shall pay to the Defendant " + dc.co.c3_amount.StringToWords() + " Dollars ($" + dc.co.c3_amount.ToFormattedAmount() + ") " + dc.co.c3_frequency.GetFrequency() + " for child support. ").MyStyle();
                    }
                    if (dc.co.c3_ordertype == "S")
                    {
                        if (dc.ch.c1_lives == "P")
                            p16.AppendLine("That the Defendant shall pay to the Plaintiff " + dc.co.c4_amount.StringToWords() + " Dollars ($" + dc.co.c4_amount.ToFormattedAmount() + ") " + dc.co.c4_frequency.GetFrequency() + " for child support. ").MyStyle();
                        else
                            p16.AppendLine("That the Plaintiff shall pay to the Defendant " + dc.co.c4_amount.StringToWords() + " Dollars ($" + dc.co.c4_amount.ToFormattedAmount() + ") " + dc.co.c4_frequency.GetFrequency() + " for child support. ").MyStyle();
                    }
                }
                else
                {
                    if (dc.ch.c1_lives == "P")
                        p16.AppendLine("That the Defendant shall pay to the Plaintiff " + ccc.GetNCPMonthlyChildSupportWithAddOn().StringToWords() + " Dollars ($" + ccc.GetNCPMonthlyChildSupportWithAddOn() + ") per month for child support. ").MyStyle();
                    else
                        p16.AppendLine("That the Plaintiff shall pay to the Defendant " + ccc.GetNCPMonthlyChildSupportWithAddOn().StringToWords() + " Dollars ($" + ccc.GetNCPMonthlyChildSupportWithAddOn() + ") per month for child support. ").MyStyle();
                }
                p16.Alignment = Alignment.both;
                p16.IndentationBefore = 1.3f;
                if (dc.h.plan_holder == "P" || dc.h.plan_holder == "D")
                {
                    if (dc.h.plan_holder == "P")
                    {
                        if (dc.ch.no_children == "1")
                            p17.AppendLine("That the Plaintiff shall provide health insurance benefits to the child until the age of 21 years unless sooner emancipated.").MyStyle();
                        else
                            p17.AppendLine("That the Plaintiff shall provide health insurance benefits to the children until the age of 21 years unless sooner emancipated.").MyStyle();
                    }
                    else
                    {
                        if (dc.ch.no_children == "1")
                            p17.AppendLine("That the Defendant shall provide health insurance benefits to the child until the age of 21 years unless sooner emancipated.").MyStyle();
                        else
                            p17.AppendLine("That the Defendant shall provide health insurance benefits to the children until the age of 21 years unless sooner emancipated.").MyStyle();
                    }
                }
                else
                {
                    if (dc.h.health_cover == "P")
                    {
                        if (dc.ch.no_children == "1")
                            p17.AppendLine("That the Plaintiff shall provide health insurance benefits to the child until the age of 21 years unless sooner emancipated.").MyStyle();
                        else
                            p17.AppendLine("That the Plaintiff shall provide health insurance benefits to the children until the age of 21 years unless sooner emancipated.").MyStyle();
                    }
                    else
                    {
                        if (dc.ch.no_children == "1")
                            p17.AppendLine("That the Defendant shall provide health insurance benefits to the child until the age of 21 years unless sooner emancipated.").MyStyle();
                        else
                            p17.AppendLine("That the Defendant shall provide health insurance benefits to the children until the age of 21 years unless sooner emancipated.").MyStyle();
                    }
                }
                p17.IndentationBefore = 1.3f;
                p17.Alignment = Alignment.both;
                if (dc.ch.c1_lives == "P")
                    p18.AppendLine("That the Defendant shall pay " + ccc.GetNCPPercent() + " percent of the unreimbursed medical expenses of the child until the age of 21 years unless sooner emancipated.").MyStyle();
                if (dc.ch.c1_lives == "D")
                    p18.AppendLine("That the Plaintiff shall pay " + ccc.GetNCPPercent() + " percent of the unreimbursed medical expenses of the child until the age of 21 years unless sooner emancipated.").MyStyle();
                p18.IndentationBefore = 1.3f;
                p18.Alignment = Alignment.both;
                p19.AppendLine("\tThat the parties do not require payment of counsel and experts' fees and expenses.").MyStyle();
                p19.Alignment = Alignment.both;
                p20.AppendLine("\tThat both parties may resume the use of any prior surname.").MyStyle();
                p20.Alignment = Alignment.both;
                p21.AppendLine("That the Court grant such other and further relief as the Court may deem fit and proper.").MyStyle();
                p21.IndentationBefore = 1.3f;
                p21.Alignment = Alignment.both;
                p22.AppendLine("The parties have divided up the marital property and no claim will be made by either party under equitable distribution.\n").MyStyle();
                p22.IndentationBefore = 1.3f;
                p22.KeepWithNextParagraph();
                p23.AppendLine("Dated: \n").MyStyle();
                p23.AppendLine("\t\t\t\t\t\t______________________________").MyStyle();
                p23.AppendLine("\t\t\t\t\t\t" + dc.p.name.ToTitleCase()).MyStyle();
                p23.AppendLine("\t\t\t\t\t\tPlaintiff Pro-se").MyStyle();
                p23.AppendLine("\t\t\t\t\t\t" + dc.p.street).MyStyle();
                p23.AppendLine("\t\t\t\t\t\t" + dc.p.city + ", " + dc.p.state.ToStateName() + " " + dc.p.zip).MyStyle();
                p23.AppendLine("\t\t\t\t\t\t" + dc.p.phone).MyStyle();
                p24.AppendLine("\nSTATE OF NEW YORK, COUNTY OF\t\t,ss.\n").MyStyle();
                p24.AppendLine("I, " + pname + ", am the Plaintiff in the within action for a divorce. I have read the foregoing Complaint and know the contents thereof. The contents of the Complaint are true to my own knowledge, except as to those matters therein stated to be alleged upon information and belief, and as to those matters I believe them to be true.").MyStyle();
                p24.Alignment = Alignment.both;
                p25.AppendLine("\n\n\n\t\t\t\t\t\t\t\t________________________").MyStyle();
                p25.AppendLine("\t\t\t\t\t\t\t\t" + pname).MyStyle();
                p25.AppendLine("Subscribed and sworn to before me\t\t\t\tPlaintiff Pro-se").MyStyle();
                p25.AppendLine("on").MyStyle();
                p25.AppendLine("\n__________________________").MyStyle();
                p25.AppendLine("\t    Notary Public").MyStyle();
                p25.AppendLine("My commission expires on").MyStyle();
                p25.AppendLine();
                p25.AppendLine("(Form UD-2 - 1/25/16)").MyStyle();
                document.Save();
            }
        }
        public static void Divorce2_AffidavitOfRegularity(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Affidavit_Of_Regularity.docx"))
            {
                File.Delete(folder + "\\Affidavit_Of_Regularity.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Affidavit_Of_Regularity.docx"))
            {
                {
                    Paragraph p1 = document.InsertParagraph();
                    p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                    p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                    p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                    p1.AppendLine(pname).MyStyle();
                    p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                    p1.AppendLine("\t\t\t\t\t\t\tAFFIDAVIT OF REGULARITY").MyStyleB();
                    p1.AppendLine("\t\t-against-\n").MyStyle();
                    p1.AppendLine(dname).MyStyle();
                    p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                    p1.AppendLine("---------------------------------------------------------------------X").MyStyle();
                    p1.AppendLine("STATE OF NEW YORK, COUNTY OF                  , ss.\n").MyStyle();
                    p1.AppendLine("\t\tI, " + dc.p.name.ToTitleCase() + ", the undersigned Plaintiff Pro-se, respectfully show:").MyStyle();
                    p1.AppendLine("\n\t1. I am the Plaintiff herein.\n").MyStyle();
                    p1.AppendLine("\t2. This is a matrimonial action.\n").MyStyle();
                    if (dc.r.summons_usa == "Y")
                    {
                        if (dc.waiver_default == "1")
                            p1.AppendLine("\t3. The Summons with Notice, Notice of Automatic Orders and Notice of Guideline Maintenance were personally served upon the Defendant herein, within the State of New York as appears by the affidavit of the Defendant submitted herewith.\n").MyStyle();
                        if (dc.waiver_default == "2")
                            p1.AppendLine("\t3. The Summons with Notice, Notice of Automatic Orders and Notice of Guideline Maintenance were personally served upon the Defendant herein, within the State of New York as appears by the affidavit of Service submitted herewith.\n").MyStyle();
                    }
                    else
                    {
                        if (dc.waiver_default == "1")
                            p1.AppendLine("\t3. The Summons with Notice, Notice of Automatic Orders and Notice of Guideline Maintenance were personally served upon the Defendant herein, outside the State of New York as appears by the affidavit of the Defendant submitted herewith.\n").MyStyle();
                        if (dc.waiver_default == "2")
                            p1.AppendLine("\t3. The Summons with Notice, Notice of Automatic Orders and Notice of Guideline Maintenance were personally served upon the Defendant herein, outside the State of New York as appears by the affidavit of Service submitted herewith.\n").MyStyle();
                    }
                    if (dc.waiver_default == "1")
                        p1.AppendLine("\t4. The Defendant has appeared in this action and executed an affidavit agreeing that this matter be placed on the matrimonial calendar immediately.\n\n").MyStyle();
                    if (dc.waiver_default == "2")
                        p1.AppendLine("\t4. The Defendant is in default for failure to serve a notice of appearance or failure to answer the complaint served in this action in due time, and the time to answer has not been extended by stipulation, court order or otherwise.\n\n").MyStyle();
                    p1.Append("\tWHEREFORE, ").MyStyleB();
                    p1.Append("I respectfully request that this action be placed on the undefended matrimonial calendar.\n").MyStyle();
                    p1.AppendLine("\tI state under penalty of perjury that the statements herein made are true, except as to such statements as are based upon information and belief, which statements I believe to be true.").MyStyle();
                    p1.AppendLine("\n\n\t\t\t\t\t\t____________________________________").MyStyle();
                    p1.AppendLine("\t\t\t\t\t\t" + dc.p.name.ToTitleCase()).MyStyle();
                    p1.AppendLine("Subscribed and sworn to before me\t\tPlaintiff Pro-Se").MyStyle();
                    p1.AppendLine("on").MyStyle();
                    p1.AppendLine("\n__________________________").MyStyle();
                    p1.AppendLine("\tNotary Public").MyStyle();
                    p1.AppendLine("My commission expires on").MyStyle();
                    p1.AppendLine("\n(Form UD-5 – 1/25/16)").MyStyle();
                    document.Save();
                }
            }
        }
        public static void Divorce2_AffidavitOfPlaintiff(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            ChildCareCalculations ccc = new ChildCareCalculations(dc);
            ChildCalc cc = ccc.GetChildCalcData();
            int[] e = Miscellaneous.GetEligibleChildren(dc.ch, 21);
            int[] f = Miscellaneous.GetEligibleChildren(dc.ch, 18);
            int no_of_court_orders = 0;
            if (!ReferenceEquals(dc.co, null))
                Int32.TryParse(dc.co.no_order, out no_of_court_orders);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Affidavit_Of_Plaintiff.docx"))
            {
                File.Delete(folder + "\\Affidavit_Of_Plaintiff.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Affidavit_Of_Plaintiff.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = null;
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y")
                    p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = document.InsertParagraph();
                Paragraph p13 = document.InsertParagraph();
                Paragraph p14 = document.InsertParagraph();
                Paragraph p15 = document.InsertParagraph();
                Paragraph p16 = null;
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y")
                    p16 = document.InsertParagraph();
                Paragraph p17 = document.InsertParagraph();
                Paragraph p18 = document.InsertParagraph();
                Paragraph p19 = document.InsertParagraph();
                Paragraph p20 = document.InsertParagraph();
                Paragraph p21 = document.InsertParagraph();
                Paragraph p22 = document.InsertParagraph();
                Paragraph p23 = document.InsertParagraph();
                Paragraph p24 = document.InsertParagraph();
                Paragraph p25 = document.InsertParagraph();
                Paragraph p26 = document.InsertParagraph();
                Paragraph p27 = document.InsertParagraph();
                Paragraph p28 = document.InsertParagraph();
                Paragraph p29 = document.InsertParagraph();
                Paragraph p30 = document.InsertParagraph();
                Paragraph p31 = document.InsertParagraph();
                Paragraph p32 = document.InsertParagraph();
                Paragraph p33 = document.InsertParagraph();
                Paragraph p34 = document.InsertParagraph();
                Paragraph p35 = document.InsertParagraph();
                Paragraph p36 = document.InsertParagraph();
                Paragraph p37_1 = document.InsertParagraph();
                Paragraph p37 = document.InsertParagraph();
                Paragraph p38 = document.InsertParagraph();
                Paragraph p39 = document.InsertParagraph();
                p1.Append("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tAFFIDAVIT OF PLAINTIFF").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X").MyStyle();
                p1.AppendLine("STATE OF NEW YORK, COUNTY OF \t\t, ss.\n").MyStyle();
                p1.AppendLine("\t\t" + pname + ", being duly sworn, says:\n").MyStyle();
                if (dc.p.country == "US")
                    p2.Append("\t1. The Plaintiff, " + pname + ", resides at " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.state.ToStateName() + " - " + dc.p.zip + " and was born on " + dc.p.dob.ToDateLong() + ". ").MyStyle();
                else
                    p2.Append("\t1. The Plaintiff, " + pname + ", resides at " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.country.ToCountryName() + " and was born on " + dc.p.dob.ToDateLong() + ". ").MyStyle();
                if (dc.p.check_ssn == "Y")
                    p2.Append("The Social Security Number of the Plaintiff is " + dc.p.ssn + ". ").MyStyle();
                else if (dc.p.check_ssn == "N")
                {
                    p2.Append("The Plaintiff ").MyStyle();
                    p2.Append("doesn't").MyStyleU();
                    p2.Append(" have a Social Security Number. ").MyStyle();
                }
                if (dc.d.country == "US")
                    p2.Append("The Defendant, " + dname + ", resides at " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " - " + dc.d.zip + " and was born on " + dc.d.dob.ToDateLong() + ". ").MyStyle();
                else
                    p2.Append("The Defendant, " + dname + ", resides at " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName() + " and was born on " + dc.d.dob.ToDateLong() + ". ").MyStyle();
                if (dc.d.check_ssn == "Y")
                    p2.Append("The Social Security Number of the Defendant is " + dc.d.ssn + ". ").MyStyle();
                else if (dc.d.check_ssn == "N")
                {
                    p2.Append("The Defendant ").MyStyle();
                    p2.Append("doesn't").MyStyleU();
                    p2.Append(" have a Social Security Number. ").MyStyle();
                }
                else if (dc.d.check_ssn == "R")
                {
                    p2.Append("The Defendant ").MyStyle();
                    p2.Append("refused").MyStyleU();
                    p2.Append(" to provide the Social Security Number. ").MyStyle();
                }
                p2.Append("The Plaintiff and Defendant were both 18 years of age or over when this action was commenced.").MyStyle();
                p2.Alignment = Alignment.both;
                p3.AppendLine("\t2. This court has jurisdiction to hear this action for divorce. ").MyStyle();
                if (dc.r.applicable == "1")
                    p3.Append("The Plaintiff has resided in New York State for a continuous period in excess of two years ").MyStyle();
                if (dc.r.applicable == "2")
                    p3.Append("The Defendant has resided in New York State for a continuous period in excess of two years ").MyStyle();
                if (dc.r.applicable == "3")
                    p3.Append("The Plaintiff and Defendant were married in New York State and the Plaintiff has resided in New York State for a continuous period in excess of one year ").MyStyle();
                if (dc.r.applicable == "4")
                    p3.Append("The Plaintiff and Defendant were married in New York State and the Defendant has resided in New York State for a continuous period in excess of one year").MyStyle();
                if (dc.r.applicable == "5")
                    p3.Append("The Plaintiff and the Defendant have lived in New York State as husband and wife and the Plaintiff has resided in New York State for a continuous period in excess of one year ").MyStyle();
                if (dc.r.applicable == "6")
                    p3.Append("The Plaintiff and the Defendant have lived in New York State as husband and wife and the Defendant has resided in New York State for a continuous period in excess of one year ").MyStyle();
                if (dc.r.applicable == "7")
                    p3.Append("The act upon which the divorce is founded occurred in New York State and a party has resided in New York State for 1 year ").MyStyle();
                if (dc.r.applicable == "8")
                    p3.Append("The act upon which the divorce is founded occurred in New York State and both parties reside in New York state ").MyStyle();
                p3.Append("immediately preceding the commencement of this action.").MyStyle();
                p3.Alignment = Alignment.both;
                if (dc.m.country == "US")
                    p4.AppendLine("\t3. I married the Defendant on " + dc.m.marriage_date.ToDateLong() + ", in the City of " + dc.m.city.ToTitleCase() + ", the County of " + dc.m.county.ToTitleCase() + " and the State of " + dc.m.state.ToStateName() + ". The marriage was").MyStyle();
                else
                    p4.Append("\t3. I married the Defendant on " + dc.m.marriage_date.ToDateLong() + ", in the City of " + dc.m.city.ToTitleCase() + ", the Country of " + dc.m.country.ToCountryName() + ". The marriage was").MyStyle();
                if (dc.m.marriage_clergyman == "C")
                {
                    p4.Append(" not").MyStyleU();
                }
                p4.Append(" performed by a clergyman, minister or leader of the Society for Ethical Culture.").MyStyle();
                if (dc.m.marriage_clergyman == "R")
                    p4.Append(" To the best of my knowledge I have taken all steps solely within my power to remove all barriers to the Defendant's remarriage following the divorce.").MyStyle();
                if (dc.m.marriage_clergyman == "C")
                    p4.Append(" No steps have to be taken by either party to remove any barriers to either party's remarriage. I waive any requirement of a filing by the Defendant of a statement, pursuant to Section 253 of the Domestic Relations Law, confirming removal of barriers to my remarriage.").MyStyle();
                p4.Alignment = Alignment.both;
                if (e[0] == 1)
                {
                    p5.AppendLine("\t4. There is one child as a result of this marriage, namely:").MyStyle();
                }
                else
                {
                    p5.AppendLine("\t4. There are " + e[0].ToWords() + " children as a result of this marriage, namely:").MyStyle();
                }
                p5.AppendLine("\n\tName\t\t\tSocial Security No.          Date of Birth          Address").MyStyleB();
                if (e[0] >= 1)
                {
                    if (dc.ch.c1_lives == "P")
                    {
                        p5.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_ssn + " - " + dc.ch.c1_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c1_lives == "D")
                    {
                        p5.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_ssn + " - " + dc.ch.c1_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p5.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_ssn + " - " + dc.ch.c1_dob.ToDateLong() + " - " + dc.ch.t1_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 2)
                {
                    p5.AppendLine();
                    if (dc.ch.c2_lives == "P")
                    {
                        p5.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.c2_ssn + " - " + dc.ch.c2_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c2_lives == "D")
                    {
                        p5.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.c2_ssn + " - " + dc.ch.c2_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p5.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.c2_ssn + " - " + dc.ch.c2_dob.ToDateLong() + " - " + dc.ch.t2_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 3)
                {
                    p5.AppendLine();
                    if (dc.ch.c3_lives == "P")
                    {
                        p5.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.c3_ssn + " - " + dc.ch.c3_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c3_lives == "D")
                    {
                        p5.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.c3_ssn + " - " + dc.ch.c3_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p5.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.c3_ssn + " - " + dc.ch.c3_dob.ToDateLong() + " - " + dc.ch.t3_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 4)
                {
                    p5.AppendLine();
                    if (dc.ch.c4_lives == "P")
                    {
                        p5.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.c4_ssn + " - " + dc.ch.c4_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c4_lives == "D")
                    {
                        p5.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.c4_ssn + " - " + dc.ch.c4_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p5.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.c4_ssn + " - " + dc.ch.c4_dob.ToDateLong() + " - " + dc.ch.t4_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 5)
                {
                    p5.AppendLine();
                    if (dc.ch.c5_lives == "P")
                    {
                        p5.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.c5_ssn + " - " + dc.ch.c5_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c5_lives == "D")
                    {
                        p5.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.c5_ssn + " - " + dc.ch.c5_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p5.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.c5_ssn + " - " + dc.ch.c5_dob.ToDateLong() + " - " + dc.ch.t5_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 6)
                {
                    p5.AppendLine();
                    if (dc.ch.c6_lives == "P")
                    {
                        p5.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.c6_ssn + " - " + dc.ch.c6_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c6_lives == "D")
                    {
                        p5.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.c6_ssn + " - " + dc.ch.c6_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p5.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.c6_ssn + " - " + dc.ch.c6_dob.ToDateLong() + " - " + dc.ch.t6_address.ToTitleCase()).MyStyle();
                    }
                }
                p5.AppendLine();
                p5.AppendLine("There is no other child as a result of this marriage, and no other child is expected.").MyStyle();
                p6.AppendLine("The present address of each child under the age of 21 and all other places where each child has lived within the last five (5) years are as follows:").MyStyle();
                p6.Alignment = Alignment.both;
                if (e[0] >= 1 && e[1] == 1)
                {
                    if (dc.ch.c1_lives == "P")
                    {
                        p7.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c1_lives == "D")
                    {
                        p7.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p7.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.t1_address.ToTitleCase()).MyStyle();
                    }

                    p7.AppendLine();
                }
                if (e[0] >= 2 && e[2] == 1)
                {
                    if (dc.ch.c2_lives == "P")
                    {
                        p7.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c2_lives == "D")
                    {
                        p7.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p7.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.t2_address.ToTitleCase()).MyStyle();
                    }

                    p7.AppendLine();
                }
                if (e[0] >= 3 && e[3] == 1)
                {
                    if (dc.ch.c3_lives == "P")
                    {
                        p7.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c3_lives == "D")
                    {
                        p7.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p7.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.t3_address.ToTitleCase()).MyStyle();
                    }

                    p7.AppendLine();
                }
                if (e[0] >= 4 && e[4] == 1)
                {
                    if (dc.ch.c4_lives == "P")
                    {
                        p7.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c4_lives == "D")
                    {
                        p7.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p7.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.t4_address.ToTitleCase()).MyStyle();
                    }

                    p7.AppendLine();
                }
                if (e[0] >= 5 && e[5] == 1)
                {
                    if (dc.ch.c5_lives == "P")
                    {
                        p7.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c5_lives == "D")
                    {
                        p7.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p7.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.t5_address.ToTitleCase()).MyStyle();
                    }

                    p7.AppendLine();
                }
                if (e[0] >= 6 && e[6] == 1)
                {
                    if (dc.ch.c6_lives == "P")
                    {
                        p7.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c6_lives == "D")
                    {
                        p7.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p7.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.t6_address.ToTitleCase()).MyStyle();
                    }

                    p7.AppendLine();
                }
                p7.AppendLine("\tName\t\t\t\tOther Address Within Last 5 Years").MyStyleB();
                p7.AppendLine("\t\t\t\t\t\t\tNONE").MyStyle();
                p8.AppendLine("\tThe name(s) and present address(es) of the person(s) with whom each child under the age of 21 has lived within the last five (5) years are:").MyStyle();
                p8.Alignment = Alignment.both;
                p9.AppendLine("\t" + pname + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                p9.AppendLine("\n\t" + dname + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                if (dc.ch.c1_lives == "T")
                    p9.AppendLine("\t" + dc.ch.t1_name + " " + " - " + dc.ch.t1_address).MyStyle();
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y")
                {
                    if (no_of_court_orders >= 1 && dc.co.c1_ordertype != "O")
                    {
                        p10.AppendLine("\tA proceeding was commenced in the " + dc.co.c1_ordertype + " and the order, dated " + dc.co.c1_date.ToDateLong() + ", under Docket No. " + dc.co.c1_index + ", by " + dc.co.c1_court_name.ToTitleCase()).MyStyle();
                        if (no_of_court_orders == 1)
                            p10.Append(" is to be continued.").MyStyle();
                    }
                    if (no_of_court_orders >= 2 && dc.co.c2_ordertype != "O")
                    {
                        p10.Append("and the order, dated " + dc.co.c2_date.ToDateLong() + " under Docket No. " + dc.co.c2_index + ", by " + dc.co.c2_court_name.ToTitleCase()).MyStyle();
                        if (no_of_court_orders == 2)
                            p10.Append(" are to be continued.").MyStyle();
                    }
                    if (no_of_court_orders >= 3 && dc.co.c3_ordertype != "O")
                    {
                        p10.Append(" and the order, dated " + dc.co.c3_date.ToDateLong() + " under Docket No. " + dc.co.c3_index + ", by " + dc.co.c3_court_name.ToTitleCase()).MyStyle();
                        if (no_of_court_orders == 3)
                            p10.Append(" are to be continued.").MyStyle();
                    }
                    if (no_of_court_orders >= 4 && dc.co.c4_ordertype != "O")
                    {
                        p10.Append(" and the order, dated " + dc.co.c4_date.ToDateLong() + " under Docket No. " + dc.co.c4_index + ", by " + dc.co.c4_court_name.ToTitleCase()).MyStyle();
                        if (no_of_court_orders == 4)
                            p10.Append(" are to be continued.").MyStyle();
                    }
                }
                p11.AppendLine("\tI have participated in other litigation concerning the custody of the children in this State or another State.").MyStyle();
                p11.AppendLine("\t\t\t\t\t\t\tYes [ ]   No [X]").MyStyle();
                p11.AppendLine("\tI have information of a custody proceeding concerning custody of the children pending in a court of this or another State.").MyStyle();
                p11.AppendLine("\t\t\t\t\t\t\tYes [ ]   No [X]").MyStyle();
                p11.AppendLine("\tI know of a person who is not a party to this proceeding who has physical custody of the children or claims to have custody or visitation rights with respect to the children.").MyStyle();
                p11.AppendLine("\t\t\t\t\t\t\tYes [ ]   No [X]").MyStyle();
                p11.AppendLine("\nThe parties are covered by the following group health plans:").MyStyle();
                if (dc.h.plan_holder == "P")
                {
                    p11.AppendLine("\nPlaintiff").MyStyleU();
                    p11.AppendLine("\n\tGroup Health Plan: " + dc.h.name.ToTitleCase()).MyStyle();
                    p11.AppendLine("\tAddress: " + dc.h.address).MyStyle();
                    p11.AppendLine("\tIdentification Number: " + dc.h.id_no).MyStyle();
                    if (!string.IsNullOrWhiteSpace(dc.h.plan_admin))
                        p11.AppendLine("\tPlan Administrator: " + dc.h.plan_admin.ToTitleCase()).MyStyle();
                    else
                        p11.AppendLine("\tPlan Administrator: None").MyStyle();
                    string coveragetype = null;
                    if (dc.h.medical == "Y")
                        coveragetype = "Medical";
                    if (dc.h.dental == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Dental";
                        else
                            coveragetype += ", Dental";
                    }
                    if (dc.h.optical == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Optical";
                        else
                            coveragetype += ", Optical";
                    }
                    if (string.IsNullOrWhiteSpace(coveragetype))
                        p11.AppendLine("\tType of Coverage: None").MyStyle();
                    else
                        p11.AppendLine("\tType of Coverage: " + coveragetype).MyStyle();
                    p11.AppendLine("\n\tThe Plaintiff has no other group health plans.").MyStyle();
                    p11.AppendLine("\nDefendant").MyStyleU();
                    p11.AppendLine("\n\tGroup Health Plan:  NONE").MyStyle();
                }
                else if (dc.h.plan_holder == "D")
                {
                    p11.AppendLine("\nPlaintiff").MyStyleU();
                    p11.AppendLine("\n\tGroup Health Plan:  NONE").MyStyle();
                    p11.AppendLine("\nDefendant").MyStyleU();
                    p11.AppendLine("\n\tGroup Health Plan: " + dc.h.name.ToTitleCase()).MyStyle();
                    p11.AppendLine("\tAddress: " + dc.h.address).MyStyle();
                    p11.AppendLine("\tIdentification Number: " + dc.h.id_no).MyStyle();
                    if (!string.IsNullOrWhiteSpace(dc.h.plan_admin))
                        p11.AppendLine("\tPlan Administrator: " + dc.h.plan_admin.ToTitleCase()).MyStyle();
                    else
                        p11.AppendLine("\tPlan Administrator: None").MyStyle();
                    string coveragetype = null;
                    if (dc.h.medical == "Y")
                        coveragetype = "Medical";
                    if (dc.h.dental == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Dental";
                        else
                            coveragetype += ", Dental";
                    }
                    if (dc.h.optical == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Optical";
                        else
                            coveragetype += ", Optical";
                    }
                    if (string.IsNullOrWhiteSpace(coveragetype))
                        p11.AppendLine("\tType of Coverage: None").MyStyle();
                    else
                        p11.AppendLine("\tType of Coverage: " + coveragetype).MyStyle();
                    p11.AppendLine("\n\tThe Defendant has no other group health plans.").MyStyle();
                }
                else
                {
                    p11.AppendLine("\nPlaintiff").MyStyleU();
                    p11.AppendLine("\n\tGroup Health Plan:  NONE").MyStyle();
                    p11.AppendLine("\nDefendant").MyStyleU();
                    p11.AppendLine("\n\tGroup Health Plan:  NONE").MyStyle();
                }
                p12.AppendLine("I fully understand that upon the entrance of a judgment of divorce, I may no longer be allowed to receive health coverage under my former spouse's health insurance plan. I may be entitled to purchase health insurance on my own through a COBRA option, if available and otherwise I may be required to secure my own health insurance.").MyStyle();
                p12.Alignment = Alignment.both;
                if (dc.r.grounds == "1")
                {
                    p13.AppendLine("\t5. The grounds for divorce are as follows: ").MyStyle();
                    p13.Append("Irretrievable Breakdown of the Relationship for at Least Six Months (DRL Sec. 170(7))").MyStyleU();
                    p13.Append(":  The relationship between the Plaintiff and Defendant has broken down irretrievably for a period of at least six months.").MyStyle();
                }
                else if (dc.r.grounds == "2")
                {
                    p13.AppendLine("\t5. The grounds for divorce are as follows:  ").MyStyle();
                    p13.Append("Abandonment for at least One Year (DRL Sec. 170 subd. (2))").MyStyleU();
                    p13.Append(":  The abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "3")
                {
                    p13.AppendLine("\t5. The grounds for divorce are as follows:  ").MyStyle();
                    p13.Append("Constructive Abandonment (Cessation of Sexual Relations) at least for One Year (DRL Sec. 170 subd. (2))").MyStyleU();
                    p13.Append(":  The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "4")
                {
                    p13.AppendLine("\t5. The grounds for divorce are as follows:  ").MyStyle();
                    p13.Append("Cruel And Inhuman Treatment (DRL Sec. 170 subd. (1))").MyStyleU();
                    p13.Append(":  The cruel and inhuman treatment of the Plaintiff by the Defendant.").MyStyle();
                }
                p13.Alignment = Alignment.both;
                p14.AppendLine("\t6a. In addition to the dissolution of the marriage, I am seeking the following relief:").MyStyle();
                if (dc.ch.c1_lives == "P")
                {
                    if (dc.ch.no_children == "1")
                        p14.AppendLine("\nThat the Plaintiff shall have custody of the child of the marriage under the age of 18, namely ").MyStyle();
                    else
                        p14.AppendLine("\nThat the Plaintiff shall have custody of the children of the marriage under the age of 18, namely ").MyStyle();
                }
                if (dc.ch.c1_lives == "D")
                {
                    if (dc.ch.no_children == "1")
                        p14.AppendLine("\nThat the Defendant shall have custody of the child of the marriage under the age of 18, namely ").MyStyle();
                    else
                        p14.AppendLine("\nThat the Defendant shall have custody of the children of the marriage under the age of 18, namely ").MyStyle();
                }
                if (f[0] >= 1 && f[1] == 1)
                {
                    f[7]--;
                    if (f[0] == 1)
                    {
                        p14.Append(dc.ch.c1_name.ToTitleCase() + ", " + dc.ch.c1_dob.ToDateLong() + ".").MyStyle();

                    }
                    else if (f[7] == 1)
                    {
                        p14.Append(dc.ch.c1_name.ToTitleCase() + ", " + dc.ch.c1_dob.ToDateLong() + " and ").MyStyle();

                    }
                    else
                    {
                        p14.Append(dc.ch.c1_name.ToTitleCase() + ", " + dc.ch.c1_dob.ToDateLong() + ", ").MyStyle();

                    }
                }
                if (f[0] >= 2 && f[2] == 1)
                {
                    f[7]--;
                    if (f[0] == 2)
                    {
                        p14.Append(dc.ch.c2_name.ToTitleCase() + ", " + dc.ch.c2_dob.ToDateLong() + ".").MyStyle();

                    }
                    else if (f[7] == 1)
                    {
                        p14.Append(dc.ch.c2_name.ToTitleCase() + ", " + dc.ch.c2_dob.ToDateLong() + " and ").MyStyle();

                    }
                    else
                    {
                        p14.Append(dc.ch.c2_name.ToTitleCase() + ", " + dc.ch.c2_dob.ToDateLong() + ", ").MyStyle();

                    }
                }
                if (f[0] >= 3 && f[3] == 1)
                {
                    f[7]--;
                    if (f[0] == 3)
                    {
                        p14.Append(dc.ch.c3_name.ToTitleCase() + ", " + dc.ch.c3_dob.ToDateLong() + ".").MyStyle();

                    }
                    else if (f[7] == 1)
                    {
                        p14.Append(dc.ch.c3_name.ToTitleCase() + ", " + dc.ch.c3_dob.ToDateLong() + " and ").MyStyle();

                    }
                    else
                    {
                        p14.Append(dc.ch.c3_name.ToTitleCase() + ", " + dc.ch.c3_dob.ToDateLong() + ", ").MyStyle();
                    }
                }
                if (f[0] >= 4 && f[4] == 1)
                {
                    f[7]--;
                    if (f[0] == 4)
                    {
                        p14.Append(dc.ch.c4_name.ToTitleCase() + ", " + dc.ch.c4_dob.ToDateLong() + ".").MyStyle();
                    }
                    else if (f[7] == 1)
                    {
                        p14.Append(dc.ch.c4_name.ToTitleCase() + ", " + dc.ch.c4_dob.ToDateLong() + " and ").MyStyle();
                    }
                    else
                    {
                        p14.Append(dc.ch.c4_name.ToTitleCase() + ", " + dc.ch.c4_dob.ToDateLong() + ", ").MyStyle();
                    }
                }
                if (f[0] >= 5 && f[5] == 1)
                {
                    f[7]--;
                    if (f[0] == 5)
                    {
                        p14.Append(dc.ch.c5_name.ToTitleCase() + ", " + dc.ch.c5_dob.ToDateLong() + ".").MyStyle();
                    }
                    else if (f[7] == 1)
                    {
                        p14.Append(dc.ch.c5_name.ToTitleCase() + ", " + dc.ch.c5_dob.ToDateLong() + " and ").MyStyle();
                    }
                    else
                    {
                        p14.Append(dc.ch.c5_name.ToTitleCase() + ", " + dc.ch.c5_dob.ToDateLong() + ", ").MyStyle();
                    }
                }
                if (f[0] >= 6 && f[6] == 1)
                {
                    p14.Append(dc.ch.c6_name.ToTitleCase() + ", " + dc.ch.c6_dob.ToDateLong() + ".").MyStyle();

                }
                p14.IndentationBefore = 1.3f;
                p14.Alignment = Alignment.both;
                if (dc.ch.c1_lives == "D")
                {
                    if (dc.ch.no_children == "1")
                        p15.AppendLine("That the Plaintiff shall have reasonable rights of visitation with the child away from the custodial residence.").MyStyle();
                    else
                        p15.AppendLine("That the Plaintiff shall have reasonable rights of visitation with the children away from the custodial residence.").MyStyle();
                }
                else
                {
                    if (dc.ch.no_children == "1")
                        p15.AppendLine("That the Defendant shall have reasonable rights of visitation with the child away from the custodial residence.").MyStyle();
                    else
                        p15.AppendLine("That the Defendant shall have reasonable rights of visitation with the children away from the custodial residence.").MyStyle();
                }
                p15.IndentationBefore = 1.3f;
                p15.Alignment = Alignment.both;
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y")
                {
                    if (no_of_court_orders >= 1 && dc.co.c1_ordertype != "O")
                    {
                        p16.AppendLine("That the order, dated " + dc.co.c1_date.ToDateLong() + ", of the " + dc.co.c1_court_name.ToTitleCase() + " Court of " + dc.co.c1_county.ToTitleCase() + " County under Docket No. " + dc.co.c1_index + " shall be continued, ").MyStyle();
                        if (no_of_court_orders == 1)
                            p16.Append("and the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance, child support, custody and visitation.").MyStyle();
                    }
                    if (no_of_court_orders >= 2 && dc.co.c2_ordertype != "O")
                    {
                        p16.Append("and the order, dated " + dc.co.c2_date.ToDateLong() + ", of the " + dc.co.c2_court_name.ToTitleCase() + " Court of " + dc.co.c2_county.ToTitleCase() + " County under Docket No. " + dc.co.c2_index + " shall be continued, ").MyStyle();
                        if (no_of_court_orders == 2)
                            p16.Append("and the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance, child support, custody and visitation.").MyStyle();
                    }
                    if (no_of_court_orders >= 3 && dc.co.c3_ordertype != "O")
                    {
                        p16.Append("and the order, dated " + dc.co.c3_date.ToDateLong() + ", of the " + dc.co.c3_court_name.ToTitleCase() + " Court of " + dc.co.c3_county.ToTitleCase() + " County under Docket No. " + dc.co.c3_index + " shall be continued, ").MyStyle();
                        if (no_of_court_orders == 3)
                            p16.Append("and the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance, child support, custody and visitation.").MyStyle();
                    }
                    if (no_of_court_orders >= 4 && dc.co.c4_ordertype != "O")
                    {
                        p16.Append("and the order, dated " + dc.co.c4_date.ToDateLong() + ", of the " + dc.co.c4_court_name.ToTitleCase() + " Court of " + dc.co.c4_county.ToTitleCase() + " County under Docket No. " + dc.co.c4_index + " shall be continued, ").MyStyle();
                        if (no_of_court_orders == 4)
                            p16.Append("and the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance, child support, custody and visitation.").MyStyle();
                    }
                    p16.Alignment = Alignment.both;
                    p16.IndentationBefore = 1.3f;
                }
                p17.AppendLine("That the Family Court shall have concurrent jurisdiction with the Supreme Court with respect to any future issues of maintenance, child support, custody and visitation.").MyStyle();
                p17.IndentationBefore = 1.3f;
                p17.Alignment = Alignment.both;
                if (dc.waiver_default == "1")
                    p18.AppendLine("That the parties do not require maintenance and no claim will be made by either party for maintenance. Neither party is seeking maintenance as payee as described in the Notice of Guideline Maintenance as already agreed to in our written Affidavit of Plaintiff and Affidavit of Defendant.").MyStyle();
                else
                    p18.AppendLine("That the parties do not require maintenance and no claim will be made by either party for maintenance. I am not seeking maintenance as payee as described in the Notice of Guideline Maintenance.").MyStyle();
                p18.IndentationBefore = 1.3f;
                p18.Alignment = Alignment.both;
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y" && (dc.co.c1_ordertype == "S" || dc.co.c2_ordertype == "S" || dc.co.c3_ordertype == "S" || dc.co.c4_ordertype == "S"))
                {
                    if (dc.co.c1_ordertype == "S")
                    {
                        if (dc.ch.c1_lives == "P")
                            p19.AppendLine("That the Defendant shall pay to the Plaintiff " + dc.co.c1_amount.StringToWords() + " Dollars ($" + dc.co.c1_amount.ToFormattedAmount() + ") " + dc.co.c1_frequency.GetFrequency() + " for child support. ").MyStyle();
                        else
                            p19.AppendLine("That the Plaintiff shall pay to the Defendant " + dc.co.c1_amount.StringToWords() + " Dollars ($" + dc.co.c1_amount.ToFormattedAmount() + ") " + dc.co.c1_frequency.GetFrequency() + " for child support. ").MyStyle();
                    }
                    if (dc.co.c2_ordertype == "S")
                    {
                        if (dc.ch.c1_lives == "P")
                            p19.AppendLine("That the Defendant shall pay to the Plaintiff " + dc.co.c2_amount.StringToWords() + " Dollars ($" + dc.co.c2_amount.ToFormattedAmount() + ") " + dc.co.c2_frequency.GetFrequency() + " for child support. ").MyStyle();
                        else
                            p19.AppendLine("That the Plaintiff shall pay to the Defendant " + dc.co.c2_amount.StringToWords() + " Dollars ($" + dc.co.c2_amount.ToFormattedAmount() + ") " + dc.co.c2_frequency.GetFrequency() + " for child support. ").MyStyle();
                    }
                    if (dc.co.c3_ordertype == "S")
                    {
                        if (dc.ch.c1_lives == "P")
                            p19.AppendLine("That the Defendant shall pay to the Plaintiff " + dc.co.c3_amount.StringToWords() + " Dollars ($" + dc.co.c3_amount.ToFormattedAmount() + ") " + dc.co.c3_frequency.GetFrequency() + " for child support. ").MyStyle();
                        else
                            p19.AppendLine("That the Plaintiff shall pay to the Defendant " + dc.co.c3_amount.StringToWords() + " Dollars ($" + dc.co.c3_amount.ToFormattedAmount() + ") " + dc.co.c3_frequency.GetFrequency() + " for child support. ").MyStyle();
                    }
                    if (dc.co.c3_ordertype == "S")
                    {
                        if (dc.ch.c1_lives == "P")
                            p19.AppendLine("That the Defendant shall pay to the Plaintiff " + dc.co.c4_amount.StringToWords() + " Dollars ($" + dc.co.c4_amount.ToFormattedAmount() + ") " + dc.co.c4_frequency.GetFrequency() + " for child support. ").MyStyle();
                        else
                            p19.AppendLine("That the Plaintiff shall pay to the Defendant " + dc.co.c4_amount.StringToWords() + " Dollars ($" + dc.co.c4_amount.ToFormattedAmount() + ") " + dc.co.c4_frequency.GetFrequency() + " for child support. ").MyStyle();
                    }
                }
                else
                {
                    if (dc.ch.c1_lives == "P")
                        p19.AppendLine("That the Defendant shall pay to the Plaintiff " + ccc.GetNCPMonthlyChildSupportWithAddOn().StringToWords() + " Dollars ($" + ccc.GetNCPMonthlyChildSupportWithAddOn() + ") per month for child support. ").MyStyle();
                    else
                        p19.AppendLine("That the Plaintiff shall pay to the Defendant " + ccc.GetNCPMonthlyChildSupportWithAddOn().StringToWords() + " Dollars ($" + ccc.GetNCPMonthlyChildSupportWithAddOn() + ") per month for child support. ").MyStyle();
                }
                p19.Alignment = Alignment.both;
                p19.IndentationBefore = 1.3f;
                if (dc.h.plan_holder == "P" || dc.h.plan_holder == "D")
                {
                    if (dc.h.plan_holder == "P")
                    {
                        if (dc.ch.no_children == "1")
                            p20.AppendLine("That the Plaintiff shall provide health insurance benefits to the child until the age of 21 years unless sooner emancipated.").MyStyle();
                        else
                            p20.AppendLine("That the Plaintiff shall provide health insurance benefits to the children until the age of 21 years unless sooner emancipated.").MyStyle();
                    }
                    else
                    {
                        if (dc.ch.no_children == "1")
                            p20.AppendLine("That the Defendant shall provide health insurance benefits to the child until the age of 21 years unless sooner emancipated.").MyStyle();
                        else
                            p20.AppendLine("That the Defendant shall provide health insurance benefits to the children until the age of 21 years unless sooner emancipated.").MyStyle();
                    }
                }
                else
                {
                    if (dc.h.health_cover == "P")
                    {
                        if (dc.ch.no_children == "1")
                            p20.AppendLine("That the Plaintiff shall provide health insurance benefits to the child until the age of 21 years unless sooner emancipated.").MyStyle();
                        else
                            p20.AppendLine("That the Plaintiff shall provide health insurance benefits to the children until the age of 21 years unless sooner emancipated.").MyStyle();
                    }
                    else
                    {
                        if (dc.ch.no_children == "1")
                            p20.AppendLine("That the Defendant shall provide health insurance benefits to the child until the age of 21 years unless sooner emancipated.").MyStyle();
                        else
                            p20.AppendLine("That the Defendant shall provide health insurance benefits to the children until the age of 21 years unless sooner emancipated.").MyStyle();
                    }
                }
                p20.IndentationBefore = 1.3f;
                p20.Alignment = Alignment.both;
                if (dc.ch.c1_lives == "P")
                    p21.AppendLine("That the Defendant shall pay " + ccc.GetNCPPercent() + " percent of the unreimbursed medical expenses of the child until the age of 21 years unless sooner emancipated.").MyStyle();
                if (dc.ch.c1_lives == "D")
                    p21.AppendLine("That the Plaintiff shall pay " + ccc.GetNCPPercent() + " percent of the unreimbursed medical expenses of the child until the age of 21 years unless sooner emancipated.").MyStyle();
                p21.IndentationBefore = 1.3f;
                p21.Alignment = Alignment.both;
                p22.AppendLine("\tThat the parties do not require payment of counsel and experts' fees and expenses.").MyStyle();
                p22.Alignment = Alignment.both;
                p23.AppendLine("\tThat both parties may resume the use of any prior surname.").MyStyle();
                p23.Alignment = Alignment.both;
                p24.AppendLine("That the Court grant such other and further relief as the Court may deem fit and proper.").MyStyle();
                p24.Alignment = Alignment.both;
                p24.IndentationBefore = 1.3f;
                p25.AppendLine("The parties have divided up the marital property and no claim will be made by either party under equitable distribution.").MyStyle();
                p25.IndentationBefore = 1.3f;
                p25.Alignment = Alignment.both;
                if (dc.r.grounds == "1")
                    p26.AppendLine("\t6b. DRL Sec. 170(7)").MyStyle();
                else if (dc.r.grounds == "2")
                    p26.AppendLine("\t6b. DRL Sec. 170(2)").MyStyle();
                else if (dc.r.grounds == "3")
                    p26.AppendLine("\t6b. DRL Sec. 170(2)").MyStyle();
                else if (dc.r.grounds == "4")
                    p26.AppendLine("\t6b. DRL Sec. 170(1)").MyStyle();
                if (dc.waiver_default == "1")
                    p26.Append(" is the ground alleged, and the Plaintiff hereby affirms that all economic issues of equitable distribution of marital property, the payment or waiver of spousal support, the payment of child support, and the payment of counsel and experts' fees and expenses, as well as custody and visitation with minor children of the marriage, have been resolved by the parties by written agreement or written waiver. The Defendant has signed a separate affidavit confirming this.").MyStyle();
                if (dc.waiver_default == "2")
                    p26.Append(" is the ground alleged, and the Plaintiff hereby affirms that all economic issues of equitable distribution of marital property, the payment or waiver of spousal support, the payment of child support, and the payment of counsel and experts' fees and expenses, as well as custody and visitation with minor children of the marriage, are specified above and in the Summons with Notice and are to be determined by the court and are to be incorporated in the Judgment of Divorce.").MyStyle();
                p26.Alignment = Alignment.both;
                if (dc.waiver_default == "1")
                    p27.AppendLine("\t7. The Defendant is not in the active military service of the United States, New York or any other state or territory. I have submitted with these papers an Affidavit of Defendant which states that the Defendant is not in the active military service of the United States, New York or any other state or territory.").MyStyle();
                if (dc.waiver_default == "2")
                    p27.AppendLine("\t7. The Defendant is not in the active military service of the United States, New York or any other state or territory. I know this because he admitted it to the process server on " + dc.date_summons.ToDateLong()).MyStyle();
                p27.Alignment = Alignment.both;
                p28.AppendLine("\t8. I am not receiving Public Assistance. To my knowledge the Defendant is not receiving Public Assistance.").MyStyle();
                p28.Alignment = Alignment.both;
                p29.AppendLine("\t9. No other matrimonial action is pending in any other court and the marriage has not been terminated by any prior decree of any court of competent jurisdiction.").MyStyle();
                p29.Alignment = Alignment.both;
                if (dc.ch.c1_lives == "P")
                {
                    if (dc.ch.no_children == "1")
                        p30.AppendLine("\t10. I am the custodial parent of the child entitled to receive child support pursuant to DRL Sec. 236(B)(7)(b). I am aware of the child support enforcement services which are available through the Support Collection Unit, but I decline such services at this time. I am aware that an income deduction order may be issued pursuant to CPLR Sec. 5242(c) without other child support enforcement services and that payment of an administrative fee may be required.").MyStyle();
                    else
                        p30.AppendLine("\t10. I am the custodial parent of the children entitled to receive child support pursuant to DRL Sec. 236(B)(7)(b). I am aware of the child support enforcement services which are available through the Support Collection Unit, but I decline such services at this time. I am aware that an income deduction order may be issued pursuant to CPLR Sec. 5242(c) without other child support enforcement services and that payment of an administrative fee may be required.").MyStyle();
                }
                else
                {
                    if (dc.ch.no_children == "1")
                        p30.AppendLine("\t10. I am not the custodial parent of the child of the marriage.").MyStyle();
                    else
                        p30.AppendLine("\t10. I am not the custodial parent of the children of the marriage.").MyStyle();
                }
                p30.Alignment = Alignment.both;
                p31.AppendLine("Pursuant to DRL Sec. 240 1(a-1) Records Checking Requirements:").MyStyle();
                p32.Append("\n\tAn Order of Protection ").MyStyle();
                p32.Append("has never been ").MyStyleB();
                p32.Append("issued against me, enjoining me or requiring my compliance.").MyStyle();
                p32.Alignment = Alignment.both;
                p33.Append("\n\tAn Order of Protection ").MyStyle();
                p33.Append("has never been ").MyStyleB();
                p33.Append("issued in favor of or protecting me or my children or a member of my household.").MyStyle();
                p33.Alignment = Alignment.both;
                p34.AppendLine("\tList all Family/Criminal Court Docket #'s and Counties, Supreme Court Index #'s and Counties:").MyStyle();
                if (!ReferenceEquals(dc.co, null) && dc.co.c1_ordertype == "O")
                {
                    p34.AppendLine(dc.co.c1_county.ToTitleCase() + " County " + dc.co.c1_court_name.ToTitleCase() + " Index/Docket No: " + dc.co.c1_index).MyStyle();
                }
                if (!ReferenceEquals(dc.co, null) && dc.co.c2_ordertype == "O")
                {
                    p34.AppendLine(dc.co.c2_county.ToTitleCase() + " County " + dc.co.c2_court_name.ToTitleCase() + " Index/Docket No: " + dc.co.c2_index).MyStyle();
                }
                if (!ReferenceEquals(dc.co, null) && dc.co.c3_ordertype == "O")
                {
                    p34.AppendLine(dc.co.c3_county.ToTitleCase() + " County " + dc.co.c3_court_name.ToTitleCase() + " Index/Docket No: " + dc.co.c3_index).MyStyle();
                }
                if (!ReferenceEquals(dc.co, null) && dc.co.c4_ordertype == "O")
                {
                    p34.AppendLine(dc.co.c4_county.ToTitleCase() + " County " + dc.co.c4_court_name.ToTitleCase() + " Index/Docket No: " + dc.co.c4_index).MyStyle();
                }
                p35.AppendLine("\t[  ]  I have been a party in a Child Abuse/Neglect Proceeding (FCA Art. 10).").MyStyle();
                p35.AppendLine("\tList all Family Court Docket #'s and Counties:  N/A").MyStyle();
                p35.AppendLine("\t[X]  I have never been a party in a Child Abuse/Neglect Proceeding (FCA Art. 10).").MyStyle();
                p35.AppendLine("\t[  ]  I am registered under New York State's Sex Offender Registration Act.").MyStyle();
                p35.AppendLine("\tList all names under which you are registered:  N/A").MyStyle();
                p35.AppendLine("\t[X]  I am not registered under New York State's Sex Offender Registration Act.").MyStyle();
                p35.AppendLine("\nUnder DRL Sec. 236B(9)(b) (and FCA Sec. 451) parties have the right to seek a modification of a child support order upon a showing of (i) a substantial change in circumstances, or (ii) that three years have passed since the order was entered, last modified or adjusted, or (iii) there has been a change in either party's gross income by fifteen percent or more since the order was entered, last modified or adjusted.  The Plaintiff and Defendant have not elected to opt out of the above subparagraphs (ii) or (iii).").MyStyle();
                if (dc.p.sex == "F")
                    p36.AppendLine("\t11. The Plaintiff's prior surname is " + dc.p.maiden_name.ToTitleCase() + " and the Plaintiff wishes to be able to resume use of said prior surname.").MyStyle();
                if (dc.d.sex == "F")
                    p36.AppendLine("\t11. The Defendant's prior surname is " + dc.d.maiden_name.ToTitleCase() + " and the Defendant wishes to be able to resume use of said prior surname.").MyStyle();
                p36.Alignment = Alignment.both;
                p37_1.AppendLine("12. I have been provided a copy of the Notice Concerning Continuation of Health Care Coverage.I fully understand that upon the entrance of a judgment of divorce, I may no longer be allowed to receive health coverage under my former spouse &#39;s health insurance plan. I may be entitled to purchase health insurance on my own through a COBRA option, if available, and otherwise I may be required to secure my own health insurance.").MyStyle();
                p37_1.Alignment = Alignment.both;
                p37.AppendLine("\t13. I acknowledge receipt of the Notice of Guideline Maintenance from the Court pursuant to DRL 236 B(6), Chapter 269 of the Laws of 2015, which was served with the Summons.").MyStyle();
                p37.Alignment = Alignment.both;
                p38.AppendLine("\tWHEREFORE").MyStyleB();
                p38.Append(", I respectfully request that a judgment be entered for the relief sought and such other relief as the Court deems fitting and proper.").MyStyle();
                p38.Alignment = Alignment.both;
                p39.AppendLine("\n\n\n\t\t\t\t\t\t____________________________________").MyStyle();
                p39.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                p39.AppendLine("Subscribed and sworn to before me\t\tPlaintiff").MyStyle();
                p39.AppendLine("on").MyStyle();
                p39.AppendLine();
                p39.AppendLine("__________________________").MyStyle();
                p39.AppendLine("\t    Notary Public").MyStyle();
                p39.AppendLine("My commission expires on").MyStyle();
                p39.AppendLine();
                p39.AppendLine("(Form UD-6 - 1/25/16)").MyStyle();
                document.Save();
            }
        }
        public static void Divorce2_AffidavitOfDefendant(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Affidavit_Of_Defendant.docx"))
            {
                File.Delete(folder + "\\Affidavit_Of_DefendantPlaintiff.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Affidavit_Of_Defendant.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = document.InsertParagraph();
                Paragraph p13 = document.InsertParagraph();
                Paragraph p14 = document.InsertParagraph();
                Paragraph p15 = document.InsertParagraph();
                Paragraph p16 = document.InsertParagraph();
                Paragraph p17 = document.InsertParagraph();
                Paragraph p18 = document.InsertParagraph();
                Paragraph p19 = document.InsertParagraph();
                p1.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + venue.ToUpperCase()).MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tAFFIDAVIT OF DEFENDANT").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X").MyStyle();
                p1.AppendLine("STATE OF NEW YORK, COUNTY OF                , ss.\n").MyStyle();
                p1.AppendLine("\t\t" + dname + ", being duly sworn, says:\n\n").MyStyle();
                if (dc.d.country == "US")
                    p2.Append("\t1. I am the Defendant in this action for divorce and I am over the age of 18 years. I reside at " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " - " + dc.d.zip + ".").MyStyle();
                else
                    p2.Append("\t1. I am the Defendant in this action for divorce and I am over the age of 18 years. I reside at " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName() + ".").MyStyle();
                if (dc.d.check_ssn == "Y")
                    p2.Append(" My Social Security Number is " + dc.d.ssn).MyStyle();
                p2.Alignment = Alignment.both;
                if (dc.date_summons.HasValue && !dc.date_summons.Value.IsMinDate())
                    p3.AppendLine("\t2. I admit service of the Summons with Notice for divorce on " + dc.date_summons.ToDateLong() + ", based upon the following grounds: ").MyStyle();
                else
                    p3.AppendLine("\t2. I admit service of the Summons with Notice for divorce on \t\t, based upon the following grounds: ").MyStyle();
                if (dc.r.grounds == "1")
                {
                    p3.Append("(DRL Sec. 170(7))").MyStyleU();
                    p3.Append(":  The relationship between the Plaintiff and Defendant has broken down irretrievably for a period of at least six months.").MyStyle();
                }
                else if (dc.r.grounds == "2")
                {
                    p3.Append("(DRL Sec. 170 subd. (2))").MyStyleU();
                    p3.Append(":  The abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "3")
                {
                    p3.Append("(DRL Sec. 170 subd. (2))").MyStyleU();
                    p3.Append(":  The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "4")
                {
                    p3.Append("(DRL Sec. 170 subd. (1))").MyStyleU();
                    p3.Append(":  The cruel and inhuman treatment of the Plaintiff by the Defendant.").MyStyle();
                }
                p3.Append(" I also admit service of the Notice of Automatic Orders, the Notice of Guideline Maintenance and the Notice of Continuation of Health Care Coverage which accompanied the Summons with Notice.").MyStyle();
                p3.Alignment = Alignment.both;
                p4.AppendLine("\t3. I appear in this action; however, I do not intend to respond to the summons or answer the complaint and I waive the twenty (20) or thirty (30) day period provided by law to respond to the summons or answer the complaint.  I waive the forty (40) day waiting period to place this matter on the calendar and I hereby consent to this action being placed on the uncontested divorce calendar immediately.").MyStyle();
                p4.Alignment = Alignment.both;
                p5.AppendLine("\t4. I am not a member of the military service of the United States, New York or any other state or territory.").MyStyle();
                p5.Alignment = Alignment.both;
                p6.AppendLine("\t5. I waive the service of all further papers in this action except for a copy of the final Judgment of Divorce. I specifically waive the service upon me of the Note of Issue, Request For Judicial Intervention, Barriers to Remarriage Affidavit, proposed Findings of Fact and Conclusions of Law, proposed Judgment of Divorce, Notice of Settlement and any other notices, pleadings and papers in this action. I, however, specifically reserve the right to receive a copy of any Judgment as ultimately may be granted in this action. I further waive all statutory waiting periods prior to judgment.").MyStyle();
                p6.Alignment = Alignment.both;
                p7.AppendLine("\t6. I am not seeking equitable distribution. I understand that I may be prevented from further asserting my right to equitable distribution.").MyStyle();
                p7.Alignment = Alignment.both;
                p8.AppendLine("\t7. I am not seeking maintenance as payee as described in the Notice of Guideline Maintenance, and I agree that neither party shall be obligated to provide maintenance. I make this agreement as an agreement regarding maintenance pursuant to DRL 236(B)(3).").MyStyle();
                p8.Alignment = Alignment.both;
                if (dc.r.grounds == "1")
                    p9.AppendLine("\t8. The relationship of the Plaintiff and Defendant has broken down irretrievably for a period of more than six months.").MyStyle();
                else if (dc.r.grounds == "2")
                    p9.AppendLine("\t8. The abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                else if (dc.r.grounds == "3")
                    p9.AppendLine("\t8. The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                else if (dc.r.grounds == "4")
                    p9.AppendLine("\t8. The cruel and inhuman treatment of the Plaintiff by the Defendant.").MyStyle();
                p9.Append(" All economic issues of equitable distribution of marital property, the payment or waiver of spousal support, and the payment of counsel and experts & fees and expenses, have been resolved by the parties in our Affidavit of Plaintiff and Affidavit of Defendant and are to be incorporated into the Judgment of Divorce.Issues of custody, visitation and child support have not been raised or addressed since there are no unemancipated children of the marriage.").MyStyle();
                p9.Alignment = Alignment.both;
                p10.AppendLine("\t9. The marriage was").MyStyle();
                if (dc.m.marriage_clergyman == "C")
                {
                    p10.Append(" not").MyStyleU();
                }
                p10.Append(" performed by a clergyman, minister or leader of the Society for Ethical Culture.").MyStyle();
                p10.Alignment = Alignment.both;
                p11.AppendLine("\t10. I acknowledge receipt of the Notice of Guideline Maintenance from the Court pursuant to DRL 236 B(6), Chapter 269 of the Laws of 2015, which was served with the Summons.").MyStyle();
                p11.Alignment = Alignment.both;
                if (dc.ch.c1_lives == "D")
                {
                    if (dc.ch.no_children == "1")
                        p12.AppendLine("\t11. I am the custodial parent of the child entitled to receive child support pursuant to DRL Sec. 236(B)(7)(b). I am aware of the child support enforcement services which are available through the Support Collection Unit, but I decline such services at this time. I am aware that an income deduction order may be issued pursuant to CPLR Sec. 5242(c) without other child support enforcement services and that payment of an administrative fee may be required. I consent to the granting of the child support payments and other relief requested in the Summons with Notice.  I have been advised of the provisions of the Child Support Standards Act.  I also have received a copy of the Child Support Standards Chart promulgated by the commissioner of Social Services pursuant to Section 111-i of the Social Services Law.  I have been advised that the basic child support obligation presumptively results in the correct amount of child support.  The child support to which I am consenting conforms with the non-custodial parent's share of the basic child support obligation.").MyStyle();
                    else
                        p12.AppendLine("\t11. I am the custodial parent of the children entitled to receive child support pursuant to DRL Sec. 236(B)(7)(b). I am aware of the child support enforcement services which are available through the Support Collection Unit, but I decline such services at this time. I am aware that an income deduction order may be issued pursuant to CPLR Sec. 5242(c) without other child support enforcement services and that payment of an administrative fee may be required. I consent to the granting of the child support payments and other relief requested in the Summons with Notice.  I have been advised of the provisions of the Child Support Standards Act.  I also have received a copy of the Child Support Standards Chart promulgated by the commissioner of Social Services pursuant to Section 111-i of the Social Services Law.  I have been advised that the basic child support obligation presumptively results in the correct amount of child support.  The child support to which I am consenting conforms with the non-custodial parent's share of the basic child support obligation.").MyStyle();
                }
                else
                {
                    if (dc.ch.no_children == "1")
                        p12.AppendLine("\t11. I am not the custodial parent of the child of the marriage. I consent to the granting of the child support payments and other relief requested in the Summons with Notice.  I have been advised of the provisions of the Child Support Standards Act. I also have received a copy of the Child Support Standards Chart promulgated by the commissioner of Social Services pursuant to Section 111-i of the Social Services Law. I have been advised that the basic child support obligation presumptively results in the correct amount of child support.  The child support to which I am consenting conforms with the non-custodial parent's share of the basic child support obligation.").MyStyle();
                    else
                        p12.AppendLine("\t11. I am not the custodial parent of the children of the marriage. I consent to the granting of the child support payments and other relief requested in the Summons with Notice.  I have been advised of the provisions of the Child Support Standards Act. I also have received a copy of the Child Support Standards Chart promulgated by the commissioner of Social Services pursuant to Section 111-i of the Social Services Law. I have been advised that the basic child support obligation presumptively results in the correct amount of child support.  The child support to which I am consenting conforms with the non-custodial parent's share of the basic child support obligation.").MyStyle();
                }
                p12.Alignment = Alignment.both;
                p13.AppendLine("Pursuant to DRL Sec. 240 1(a-1) Records Checking Requirements:").MyStyle();
                p14.AppendLine("\tAn Order of Protection ").MyStyle();
                p14.Append("has never been ").MyStyle();
                p14.Append("issued against me, enjoining me or requiring my compliance.").MyStyle();
                p14.Alignment = Alignment.both;
                p15.AppendLine("\tAn Order of Protection ").MyStyle();
                p15.Append("has never been ").MyStyle();
                p15.Append("issued in favor of or protecting me or my children or a member of my household.").MyStyle();
                p15.Alignment = Alignment.both;
                p16.AppendLine("\tList all Family/Criminal Court Docket #'s and Counties, Supreme Court Index #'s and Counties:\n").MyStyle();
                if (!ReferenceEquals(dc.co, null) && dc.co.c1_ordertype == "O")
                {
                    p16.AppendLine(dc.co.c1_county.ToTitleCase() + " County " + dc.co.c1_ordertype.ToTitleCase() + " Index/Docket No: " + dc.co.c1_index).MyStyle();
                }
                if (!ReferenceEquals(dc.co, null) && dc.co.c2_ordertype == "O")
                {
                    p16.AppendLine(dc.co.c2_county.ToTitleCase() + " County " + dc.co.c2_ordertype.ToTitleCase() + " Index/Docket No: " + dc.co.c2_index).MyStyle();
                }
                if (!ReferenceEquals(dc.co, null) && dc.co.c3_ordertype == "O")
                {
                    p16.AppendLine(dc.co.c3_county.ToTitleCase() + " County " + dc.co.c3_ordertype.ToTitleCase() + " Index/Docket No: " + dc.co.c3_index).MyStyle();
                }
                if (!ReferenceEquals(dc.co, null) && dc.co.c4_ordertype == "O")
                {
                    p16.AppendLine(dc.co.c4_county.ToTitleCase() + " County " + dc.co.c4_ordertype.ToTitleCase() + " Index/Docket No: " + dc.co.c4_index).MyStyle();
                }
                if (!ReferenceEquals(dc.co, null) && (dc.co.c1_ordertype != "O") && (dc.co.c2_ordertype != "O") && (dc.co.c3_ordertype != "O") && (dc.co.c4_ordertype != "O"))
                    p16.AppendLine("NONE").MyStyle();
                p17.AppendLine("\t[  ]  I have been a party in a Child Abuse/Neglect Proceeding (FCA Art. 10).").MyStyle();
                p17.AppendLine("\tList all Family Court Docket #'s and Counties:  N/A").MyStyle();
                p17.AppendLine("\t[X]  I have never been a party in a Child Abuse/Neglect Proceeding (FCA Art. 10).").MyStyle();
                p17.AppendLine("\n\t[  ]  I am registered under New York State's Sex Offender Registration Act.").MyStyle();
                p17.AppendLine("\tList all names under which you are registered:  N/A").MyStyle();
                p17.AppendLine("\t[X]  I am not registered under New York State's Sex Offender Registration Act.").MyStyle();
                p18.AppendLine("\n12. I have been provided a copy of the Notice Concerning Continuation of Health Care Coverage. I fully understand that upon the entrance of a judgment of divorce, I may no longer be allowed to receive health coverage under my former spouse's health insurance plan. I may be entitled to purchase health insurance on my own through a COBRA option, if available and otherwise I may be required to secure my own health insurance.").MyStyle();
                p18.IndentationBefore = 1.3f;
                p18.Alignment = Alignment.both;
                p19.AppendLine("\n\n\t\t\t\t\t\t____________________________________").MyStyle();
                p19.AppendLine("\t\t\t\t\t\t" + dname).MyStyle();
                p19.AppendLine("Subscribed and sworn to before me\t\tDefendant").MyStyle();
                p19.AppendLine("on").MyStyle();
                p19.AppendLine("\n__________________________").MyStyle();
                p19.AppendLine("\t    Notary Public").MyStyle();
                p19.AppendLine("My commission expires on").MyStyle();
                p19.AppendLine("\n(Form UD-7 - 3/1/16)").MyStyle();
                document.Save();
            }
        }
        public static void Divorce2_FindingsOfFact(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            ChildCareCalculations ccc = new ChildCareCalculations(dc);
            ChildCalc cc = ccc.GetChildCalcData();
            int[] e = Miscellaneous.GetEligibleChildren(dc.ch, 18);
            int[] f = Miscellaneous.GetEligibleChildren(dc.ch, 21);
            int no_of_court_orders = 0;
            if (!ReferenceEquals(dc.co, null))
                Int32.TryParse(dc.co.no_order, out no_of_court_orders);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Findings_Of_Fact_Conclusions.docx"))
            {
                File.Delete(folder + "\\Findings_Of_Fact_Conclusions.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Findings_Of_Fact_Conclusions.docx"))
            {
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = document.InsertParagraph();
                Paragraph p13 = document.InsertParagraph();
                Paragraph p14 = document.InsertParagraph();
                Paragraph p15 = document.InsertParagraph();
                Paragraph p16 = document.InsertParagraph();
                Paragraph p17 = document.InsertParagraph();
                Paragraph p18 = document.InsertParagraph();
                Paragraph p19 = document.InsertParagraph();
                Paragraph p20 = document.InsertParagraph();
                Paragraph p21 = document.InsertParagraph();
                Paragraph p22 = document.InsertParagraph();
                Paragraph p23 = document.InsertParagraph();
                Paragraph p24 = document.InsertParagraph();
                Paragraph p25 = null;
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y" && (dc.co.c1_ordertype == "S" || dc.co.c2_ordertype == "S" || dc.co.c3_ordertype == "S" || dc.co.c4_ordertype == "S"))
                    p25 = document.InsertParagraph();
                Paragraph p26 = document.InsertParagraph();
                Paragraph p27 = document.InsertParagraph();
                Paragraph p28 = document.InsertParagraph();
                Paragraph p29 = document.InsertParagraph();
                Paragraph p30 = document.InsertParagraph();
                Paragraph p31 = document.InsertParagraph();
                Paragraph p32 = document.InsertParagraph();
                Paragraph p33 = document.InsertParagraph();
                Paragraph p34 = document.InsertParagraph();
                Paragraph p35 = document.InsertParagraph();
                Paragraph p36 = document.InsertParagraph();
                Paragraph p37 = document.InsertParagraph();
                Paragraph p38 = document.InsertParagraph();
                Paragraph p39 = document.InsertParagraph();
                Paragraph p40 = document.InsertParagraph();
                Paragraph p41 = document.InsertParagraph();
                p1.AppendLine("\t\t\t\t          At the Matrimonial/IAS Part      of the New York ").MyStyle();
                p1.AppendLine("\t\t\t\t          Supreme Court at the Courthouse, " + venue.ToTitleCase() + " County,").MyStyle();
                p1.AppendLine("\t\t\t\t          on the        day of                     , " + DateTime.Now.Year).MyStyle();
                p1.AppendLine("Present:").MyStyle();
                p1.AppendLine("Hon.\t\t\t\t\tJustice").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\t\t      Calendar No: ").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\t\t      Soc. Sec. No.: " + dc.p.ssn).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\t\tFINDINGS OF FACT AND").MyStyleB();
                p1.AppendLine("\t\t\t\t\t\t\t CONCLUSIONS OF LAW").MyStyleB();
                p1.AppendLine("\t\t-against-\n").MyStyle();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X").MyStyle();
                p2.AppendLine("\n\tThe issues of this action having been submitted to me as one of the Justices/Referees of this Court at Part                      hereof, held in and for the County of New York on                                           and having considered the allegations and proofs of the respective parties and due deliberation having been had thereon.").MyStyle();
                p2.Alignment = Alignment.both;
                p2.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p3.AppendLine("\tNOW").MyStyleB();
                p3.Append(", after reading and considering the papers submitted, I do hereby make the following findings of essential facts which I deem established by the evidence and reach the following conclusions of law.").MyStyle();
                p3.Alignment = Alignment.both;
                p3.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p4.AppendLine("FINDINGS OF FACT").MyStyleB();
                p4.Alignment = Alignment.center;
                p4.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p5.AppendLine("\tFIRST:").MyStyleB();
                p5.Append(" The Plaintiff and Defendant were both 18 years of age or over when this action was commenced.").MyStyle();
                p5.Alignment = Alignment.both;
                p5.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p6.AppendLine("\tSECOND:").MyStyleB();
                p6.Append(" This court has jurisdiction to hear this action for divorce. ").MyStyle();
                if (dc.r.applicable == "1")
                    p6.Append("The Plaintiff has resided in New York State for a continuous period in excess of two years ").MyStyle();
                if (dc.r.applicable == "2")
                    p6.Append("The Defendant has resided in New York State for a continuous period in excess of two years ").MyStyle();
                if (dc.r.applicable == "3")
                    p6.Append("The Plaintiff and Defendant were married in New York State and the Plaintiff has resided in New York State for a continuous period in excess of one year ").MyStyle();
                if (dc.r.applicable == "4")
                    p6.Append("The Plaintiff and Defendant were married in New York State and the Defendant has resided in New York State for a continuous period in excess of one year").MyStyle();
                if (dc.r.applicable == "5")
                    p6.Append("The Plaintiff and the Defendant have lived in New York State as husband and wife and the Plaintiff has resided in New York State for a continuous period in excess of one year ").MyStyle();
                if (dc.r.applicable == "6")
                    p6.Append("The Plaintiff and the Defendant have lived in New York State as husband and wife and the Defendant has resided in New York State for a continuous period in excess of one year ").MyStyle();
                if (dc.r.applicable == "7")
                    p6.Append("The act upon which the divorce is founded occurred in New York State and a party has resided in New York State for 1 year ").MyStyle();
                if (dc.r.applicable == "8")
                    p6.Append("The act upon which the divorce is founded occurred in New York State and both parties reside in New York state ").MyStyle();
                p6.Append("immediately preceding the commencement of this action.").MyStyle();
                p6.Alignment = Alignment.both;
                p6.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p7.AppendLine("\tTHIRD:").MyStyleB();
                if (dc.m.marriage_clergyman == "R")
                {
                    if (dc.m.country == "US")
                        p7.Append(" The Plaintiff and Defendant were married on " + dc.m.marriage_date.ToDateLong() + ", in the City of " + dc.m.city.ToTitleCase() + ", the County of " + dc.m.county.ToTitleCase() + " and the State of " + dc.m.state.ToStateName() + ", in a religious ceremony.").MyStyle();
                    else
                        p7.Append(" The Plaintiff and Defendant were married on " + dc.m.marriage_date.ToDateLong() + ", in the City of " + dc.m.city.ToTitleCase() + " and the Country of " + dc.m.country.ToCountryName() + ", in a religious ceremony.").MyStyle();
                }
                else
                {
                    if (dc.m.country == "US")
                        p7.Append(" The Plaintiff and Defendant were married on " + dc.m.marriage_date.ToDateLong() + ", in the City of " + dc.m.city.ToTitleCase() + ", the County of " + dc.m.county.ToTitleCase() + " and the State of " + dc.m.state.ToStateName() + ", in a civil ceremony.").MyStyle();
                    else
                        p7.Append(" The Plaintiff and Defendant were married on " + dc.m.marriage_date.ToDateLong() + ", in the City of " + dc.m.city.ToTitleCase() + " and the Country of " + dc.m.country.ToCountryName() + ", in a civil ceremony.").MyStyle();
                }
                p7.Alignment = Alignment.both;
                p7.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p8.AppendLine("\tFOURTH:").MyStyleB();
                p8.Append(" No decree, judgment or order of divorce, annulment or dissolution of marriage has been granted to either party against the other in any court of competent jurisdiction of this state or any other state, territory or country and there is no other action for divorce, annulment or dissolution of marriage by either party against the other pending in any court.").MyStyle();
                p8.Alignment = Alignment.both;
                p8.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p9.AppendLine("\tFIFTH:").MyStyleB();
                if (dc.waiver_default == "1")
                {
                    if (dc.d.sex == "F")
                        p9.Append(" This action was commenced by filing the Summons with Notice with the County Clerk on " + dc.index_date.ToDateLong() + ". The Defendant was served personally with the Summons with Notice, Notice of Automatic Orders, Notice of Continuation of Healthcare Coverage and Notice of Guideline Maintenance. The Defendant has appeared and waived her right to answer.").MyStyle();
                    else
                        p9.Append(" This action was commenced by filing the Summons with Notice with the County Clerk on " + dc.index_date.ToDateLong() + ". The Defendant was served personally with the Summons with Notice, Notice of Automatic Orders, Notice of Continuation of Healthcare Coverage and Notice of Guideline Maintenance. The Defendant has appeared and waived his right to answer.").MyStyle();
                }
                if (dc.waiver_default == "2")
                    p9.Append(" This action was commenced by filing the Summons with Notice with the County Clerk on " + dc.index_date.ToDateLong() + ". The Defendant was served personally with the Summons with Notice, Notice of Automatic Orders, Notice of Continuation of Healthcare Coverage and Notice of Guideline Maintainance. The Defendant has defaulted in appearance.").MyStyle();
                p9.Alignment = Alignment.both;
                p9.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p10.AppendLine("\tSIXTH:").MyStyleB();
                p10.Append(" The Defendant is not in the military service of the United States, New York or any other state or territory.").MyStyle();
                p10.Alignment = Alignment.both;
                p10.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p11.AppendLine("\tSEVENTH:").MyStyleB();
                if (e[0] == 1)
                {
                    p11.Append("\tThere is one child as a result of this marriage, namely:").MyStyle();
                }
                else
                {
                    p11.Append("\tThere are " + e[0].ToWords() + " children as a result of this marriage, namely:").MyStyle();
                }
                p11.AppendLine("\tName\t\t\tSocial Security No.          Date of Birth          Address").MyStyleB();
                if (e[0] >= 1)
                {
                    if (dc.ch.c1_lives == "P")
                    {
                        p11.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_ssn + " - " + dc.ch.c1_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c1_lives == "D")
                    {
                        p11.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_ssn + " - " + dc.ch.c1_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p11.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_ssn + " - " + dc.ch.c1_dob.ToDateLong() + " - " + dc.ch.t1_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 2)
                {
                    p11.AppendLine();
                    if (dc.ch.c2_lives == "P")
                    {
                        p11.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.c2_ssn + " - " + dc.ch.c2_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c2_lives == "D")
                    {
                        p11.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.c2_ssn + " - " + dc.ch.c2_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p11.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.c2_ssn + " - " + dc.ch.c2_dob.ToDateLong() + " - " + dc.ch.t2_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 3)
                {
                    p11.AppendLine();
                    if (dc.ch.c3_lives == "P")
                    {
                        p11.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.c3_ssn + " - " + dc.ch.c3_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c3_lives == "D")
                    {
                        p11.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.c3_ssn + " - " + dc.ch.c3_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p11.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.c3_ssn + " - " + dc.ch.c3_dob.ToDateLong() + " - " + dc.ch.t3_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 4)
                {
                    p11.AppendLine();
                    if (dc.ch.c4_lives == "P")
                    {
                        p11.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.c4_ssn + " - " + dc.ch.c4_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c4_lives == "D")
                    {
                        p11.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.c4_ssn + " - " + dc.ch.c4_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p11.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.c4_ssn + " - " + dc.ch.c4_dob.ToDateLong() + " - " + dc.ch.t4_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 5)
                {
                    p11.AppendLine();
                    if (dc.ch.c5_lives == "P")
                    {
                        p11.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.c5_ssn + " - " + dc.ch.c5_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c5_lives == "D")
                    {
                        p11.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.c5_ssn + " - " + dc.ch.c5_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p11.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.c5_ssn + " - " + dc.ch.c5_dob.ToDateLong() + " - " + dc.ch.t5_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 6)
                {
                    p11.AppendLine();
                    if (dc.ch.c6_lives == "P")
                    {
                        p11.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.c6_ssn + " - " + dc.ch.c6_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c6_lives == "D")
                    {
                        p11.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.c6_ssn + " - " + dc.ch.c6_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p11.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.c6_ssn + " - " + dc.ch.c6_dob.ToDateLong() + " - " + dc.ch.t6_address.ToTitleCase()).MyStyle();
                    }
                }
                p11.AppendLine("\n\tThere is no other child as a result of this marriage, and no other child is expected.").MyStyle();
                p11.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p12.AppendLine("\tEIGHTH:").MyStyleB();
                p12.Append(" The grounds for divorce which are alleged in the Verified Complaint were proved as follows: ").MyStyle();
                if (dc.r.grounds == "1")
                {
                    p12.Append("Irretrievable Breakdown of the Relationship for at Least Six Months (DRL Sec. 170(7))").MyStyleU();
                    p12.Append(":  The relationship between the Plaintiff and Defendant has broken down irretrievably for a period of at least six months.").MyStyle();
                }
                else if (dc.r.grounds == "2")
                {
                    p12.Append("Abandonment for at least One Year (DRL Sec. 170 subd. (2))").MyStyleU();
                    p12.Append(":  The abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "3")
                {
                    p12.Append("Constructive Abandonment (Cessation of Sexual Relations) at least for One Year (DRL Sec. 170 subd. (2))").MyStyleU();
                    p12.Append(":  The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year.").MyStyle();
                }
                else if (dc.r.grounds == "4")
                {
                    p12.Append("Cruel And Inhuman Treatment (DRL Sec. 170 subd. (1))").MyStyleU();
                    p12.Append(":  The cruel and inhuman treatment of the Plaintiff by the Defendant.").MyStyle();
                }
                if (dc.waiver_default == "1")
                    p12.Append(" Both the Plaintiff and Defendant have so stated under oath.").MyStyle();
                if (dc.waiver_default == "2")
                    p12.Append(" The Plaintiff has so stated under oath.").MyStyle();
                p12.Alignment = Alignment.both;
                p12.SetLineSpacing(LineSpacingType.Line, 1.25f);
                if (dc.m.marriage_clergyman == "R")
                {
                    p13.AppendLine("\tNINTH:").MyStyleB();
                    if (dc.p.sex == "F")
                        p13.Append(" A sworn statement pursuant to DRL Sec. 253 that the Plaintiff has taken all steps within her power to remove all barriers to the Defendant's remarriage following the divorce was served on the Defendant.").MyStyle();
                    else
                        p13.Append(" A sworn statement pursuant to DRL Sec. 253 that the Plaintiff has taken all steps within his power to remove all barriers to the Defendant's remarriage following the divorce was served on the Defendant.").MyStyle();
                }
                else
                {
                    p13.AppendLine("\tNINTH:").MyStyleB();
                    p13.Append(" A sworn statement as to the removal of barriers to remarriage is not required because the parties were married in a civil ceremony.").MyStyle();
                }
                p13.Alignment = Alignment.both;
                p13.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p14.AppendLine("\tTENTH:").MyStyleB();
                p14.Append(" Neither party is seeking maintenance from the other.").MyStyle();
                p14.Alignment = Alignment.both;
                p14.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p15.AppendLine("\tELEVENTH:").MyStyleB();
                p15.Append(" Equitable Distribution is not an issue.").MyStyle();
                p15.Alignment = Alignment.both;
                p15.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p16.AppendLine("\tTWELVTH:").MyStyleB();
                if (e[7] > 0)
                {
                    if (dc.ch.no_children == "1")
                        p16.Append("\tThe child of the marriage now resides with " + ccc.GetCP() + ". The " + ccc.GetCP() + " is entitled to custody of the child of the marriage under the age of 18, namely ").MyStyle();
                    else
                        p16.Append("\tThe children of the marriage now reside with " + ccc.GetCP() + ". The " + ccc.GetCP() + " is entitled to custody of the children of the marriage under the age of 18, namely ").MyStyle();
                    if (e[0] >= 1 && e[1] == 1)
                    {
                        e[7]--;
                        if (e[0] == 1)
                        {
                            p16.Append(dc.ch.c1_name.ToTitleCase() + ", born on " + dc.ch.c1_dob.ToDateLong() + ".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p16.Append(dc.ch.c1_name.ToTitleCase() + ", born on " + dc.ch.c1_dob.ToDateLong() + " and ").MyStyle();

                        }
                        else
                        {
                            p16.Append(dc.ch.c1_name.ToTitleCase() + ", born on " + dc.ch.c1_dob.ToDateLong() + ", ").MyStyle();

                        }
                    }
                    if (e[0] >= 2 && e[2] == 1)
                    {
                        e[7]--;
                        if (e[0] == 2)
                        {
                            p16.Append(dc.ch.c2_name.ToTitleCase() + ", born on " + dc.ch.c2_dob.ToDateLong() + ".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p16.Append(dc.ch.c2_name.ToTitleCase() + ", born on " + dc.ch.c2_dob.ToDateLong() + " and ").MyStyle();

                        }
                        else
                        {
                            p16.Append(dc.ch.c2_name.ToTitleCase() + ", born on " + dc.ch.c2_dob.ToDateLong() + ", ").MyStyle();

                        }
                    }
                    if (e[0] >= 3 && e[3] == 1)
                    {
                        e[7]--;
                        if (e[0] == 3)
                        {
                            p16.Append(dc.ch.c3_name.ToTitleCase() + ", born on " + dc.ch.c3_dob.ToDateLong() + ".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p16.Append(dc.ch.c3_name.ToTitleCase() + ", born on " + dc.ch.c3_dob.ToDateLong() + " and ").MyStyle();

                        }
                        else
                        {
                            p16.Append(dc.ch.c3_name.ToTitleCase() + ", born on " + dc.ch.c3_dob.ToDateLong() + ", ").MyStyle();

                        }
                    }
                    if (e[0] >= 4 && e[4] == 1)
                    {
                        e[7]--;
                        if (e[0] == 4)
                        {
                            p16.Append(dc.ch.c4_name.ToTitleCase() + ", born on " + dc.ch.c4_dob.ToDateLong() + ".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p16.Append(dc.ch.c4_name.ToTitleCase() + ", born on " + dc.ch.c4_dob.ToDateLong() + " and ").MyStyle();

                        }
                        else
                        {
                            p16.Append(dc.ch.c4_name.ToTitleCase() + ", born on " + dc.ch.c4_dob.ToDateLong() + ", ").MyStyle();

                        }
                    }
                    if (e[0] >= 5 && e[5] == 1)
                    {
                        e[7]--;
                        if (e[0] == 5)
                        {
                            p16.Append(dc.ch.c5_name.ToTitleCase() + ", born on " + dc.ch.c5_dob.ToDateLong() + ".").MyStyle();

                        }
                        else if (e[7] == 1)
                        {
                            p16.Append(dc.ch.c5_name.ToTitleCase() + ", born on " + dc.ch.c5_dob.ToDateLong() + " and ").MyStyle();

                        }
                        else
                        {
                            p16.Append(dc.ch.c5_name.ToTitleCase() + ", born on " + dc.ch.c5_dob.ToDateLong() + ", ").MyStyle();

                        }
                    }
                    if (e[0] >= 6 && e[6] == 1)
                    {
                        p16.Append(dc.ch.c6_name.ToTitleCase() + ", born on " + dc.ch.c6_dob.ToDateLong() + ".").MyStyle();

                    }
                }
                p16.Append(" The " + ccc.GetNCP() + " is entitled to visitation away from the custodial residence.").MyStyle();
                p16.Alignment = Alignment.both;
                p16.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p17.Append("\tAllegations of domestic violence and/or child abuse [  ] were or [  ] were not made in this case. Where such allegations were made the Court [  ] had found that they were supported by a preponderance of the evidence,  and has set forth on the record or in writing how such findings, facts and circumstances were factored into the custody and visitation direction, or [  ] had found that they were not supported by a preponderance of the evidence.").MyStyle();
                p17.Alignment = Alignment.both;
                p17.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p18.AppendLine("\tTHIRTEENTH:").MyStyleB();
                p18.Append(" The award of child support is based upon the following:").MyStyle();
                if (dc.ch.no_children == "1")
                    p18.AppendLine("\t(A)  The child of the marriage entitled to receive support is:").MyStyle();
                else
                    p18.AppendLine("\t(A)  The children of the marriage entitled to receive support are:").MyStyle();
                p18.Alignment = Alignment.both;
                p18.SetLineSpacing(LineSpacingType.Line, 1.25f);
                if (f[0] >= 1 && f[1] == 1)
                {
                    p19.AppendLine("\t\t\tName \t\t\t\t\tDate of Birth").MyStyleB();
                    p19.AppendLine("\t\t\t\t" + dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_dob.ToDateLong()).MyStyle();
                }
                if (f[0] >= 2 && f[2] == 1)
                {
                    p19.AppendLine("\t\t\t" + dc.ch.c2_name.ToTitleCase() + "\t - \t" + dc.ch.c2_dob.ToDateLong()).MyStyle();
                }
                if (f[0] >= 3 && f[3] == 1)
                {
                    p19.AppendLine("\t\t\t" + dc.ch.c3_name.ToTitleCase() + "\t - \t" + dc.ch.c3_dob.ToDateLong()).MyStyle();
                }
                if (f[0] >= 4 && f[4] == 1)
                {
                    p19.AppendLine("\t\t\t" + dc.ch.c4_name.ToTitleCase() + "\t - \t" + dc.ch.c4_dob.ToDateLong()).MyStyle();
                }
                if (f[0] >= 5 && f[5] == 1)
                {
                    p19.AppendLine("\t\t\t" + dc.ch.c5_name.ToTitleCase() + "\t - \t" + dc.ch.c5_dob.ToDateLong()).MyStyle();
                }
                if (f[0] >= 6 && f[5] == 1)
                {
                    p19.AppendLine("\t\t\t" + dc.ch.c6_name.ToTitleCase() + "\t - \t" + dc.ch.c6_dob.ToDateLong()).MyStyle();
                }
                p19.SetLineSpacing(LineSpacingType.Line, 1.25f);
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "N" || (dc.co.c1_ordertype != "S" && dc.co.c2_ordertype != "S" && dc.co.c3_ordertype != "S" && dc.co.c4_ordertype != "S"))
                {
                    if (dc.ch.c1_lives == "P")
                    {
                        p20.AppendLine("(B)  The adjusted gross income of the Plaintiff who is the custodial parent is $" + ccc.GetAdjustedGrossP() + " per year, and the adjusted gross income of the Defendant who is the non-custodial parent is $" + ccc.GetAdjustedGrossD() + " per year. ").MyStyle();
                    }
                    if (dc.ch.c1_lives == "D")
                    {
                        p20.AppendLine("(B)  The adjusted gross income of the Defendant who is the custodial parent is $" + ccc.GetAdjustedGrossD() + " per year, and the adjusted gross income of the Plaintiff who is the non-custodial parent is $" + ccc.GetAdjustedGrossP() + " per year. ").MyStyle();
                    }
                    p20.Append("The combined parental annual income is $" + ccc.GetCombinedParentalIncome() + ". ").MyStyle();
                    p20.Append("The applicable child support percentage is ").MyStyle();
                    if (f[0] == 1)
                        p20.Append(ccc.GetOneP() + " percent. ").MyStyle();
                    if (f[0] == 2)
                        p20.Append(ccc.GetTwoP() + " percent. ").MyStyle();
                    if (f[0] == 3)
                        p20.Append(ccc.GetThreeP() + " percent. ").MyStyle();
                    if (f[0] == 4)
                        p20.Append(ccc.GetFourP() + " percent. ").MyStyle();
                    if (f[0] >= 5)
                        p20.Append(ccc.GetFiveP() + " percent. ").MyStyle();
                    p20.Append("The combined basic child support obligation attributable to both parents is $" + ccc.GetCombinedChildSupport() + " per year on income to $" + ccc.GetUpperSlab() + " and $0 per year on income over $" + ccc.GetUpperSlab() + ". The Plaintiff's pro rata share of the combined parental income is " + ccc.GetPPercent() + " percent, and the Defendant's pro rata share of the combined parental income is " + ccc.GetDPercent() + " percent. The non-custodial parent's pro rata share of the child support obligation on combined income to $" + ccc.GetUpperSlab() + " is $").MyStyle();
                    p20.Append(ccc.GetNCPChildSupportWithAddOn() + " per year or $" + ccc.GetNCPMonthlyChildSupportWithAddOn() + " per month. ").MyStyle();
                    p20.Append("The non-custodial parent's pro rata share of the child support obligation on combined income over $" + ccc.GetUpperSlab() + " is $0 per year or $0 per month. ").MyStyle();
                    p20.Append(" The non-custodial parent's pro rata share of future health care expenses not covered by insurance, child care expenses, educational and other extraordinary expenses is " + ccc.GetNCPPercent() + " percent.  ").MyStyle();
                }
                else
                {
                    if (no_of_court_orders >= 1 && dc.co.c1_ordertype != "O")
                    {
                        p20.AppendLine("\t(B). By order of the " + dc.co.c1_court_name.ToTitleCase() + " Docket No. " + dc.co.c1_index + ", dated " + dc.co.c1_date.ToDateLong()).MyStyle();
                        if (no_of_court_orders == 1)
                        {
                            p20.Append(", the " + ccc.GetNCP() + " was directed to pay " + dc.co.c1_amount.StringToWords() + " Dollars ($" + dc.co.c2_amount.ToFormattedAmount() + ") " + dc.co.c2_frequency.GetFrequency() + " for child support.").MyStyle();
                        }
                    }
                    if (no_of_court_orders >= 2 && dc.co.c2_ordertype != "O")
                    {
                        p20.Append(", and order of the " + dc.co.c2_court_name.ToTitleCase() + " Docket No. " + dc.co.c2_index + ", dated " + dc.co.c2_date.ToDateLong()).MyStyle();
                        if (no_of_court_orders == 2)
                        {
                            p20.Append(", the " + ccc.GetNCP() + " was directed to pay " + dc.co.c2_amount.StringToWords() + " Dollars ($" + dc.co.c2_amount.ToFormattedAmount() + ") " + dc.co.c2_frequency.GetFrequency() + " for child support.").MyStyle();
                        }
                    }
                    if (no_of_court_orders >= 3 && dc.co.c3_ordertype != "O")
                    {
                        p20.Append(", and order of the " + dc.co.c3_court_name.ToTitleCase() + " Docket No. " + dc.co.c3_index + ", dated " + dc.co.c3_date.ToDateLong()).MyStyle();
                        if (no_of_court_orders == 3)
                        {
                            p20.Append(", the " + ccc.GetNCP() + " was directed to pay " + dc.co.c3_amount.StringToWords() + " Dollars ($" + dc.co.c3_amount.ToFormattedAmount() + ") " + dc.co.c3_frequency.GetFrequency() + " for child support.").MyStyle();
                        }
                    }
                    if (no_of_court_orders >= 4 && dc.co.c4_ordertype != "O")
                    {
                        p20.Append(", and order of the " + dc.co.c4_court_name.ToTitleCase() + " Docket No. " + dc.co.c4_index + ", dated " + dc.co.c4_date.ToDateLong()).MyStyle();
                        if (no_of_court_orders == 4)
                        {
                            p20.Append(", the " + ccc.GetNCP() + " was directed to pay " + dc.co.c4_amount.StringToWords() + " Dollars ($" + dc.co.c4_amount.ToFormattedAmount() + ") " + dc.co.c4_frequency.GetFrequency() + " for child support.").MyStyle();
                        }
                        p20.Append(" Said orders shall continue.  The Defendant is aware of the availability of child support enforcement services and has declined such services at this time.").MyStyle();
                    }
                }
                p20.Alignment = Alignment.both;
                p21.AppendLine("\tFOURTEENTH:").MyStyleB();
                if (dc.p.country == "US")
                    p21.Append(" The Plaintiff's address is " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + " - " + dc.p.zip).MyStyle();
                else
                    p21.Append(" The Plaintiff's address is " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.country.ToCountryName()).MyStyle();
                if (dc.p.check_ssn != "N")
                    p21.Append(" and Social Security Number is " + dc.p.ssn + ".").MyStyle();
                else
                    p21.Append(" and the Plaintiff doesn't have a Social Security Number.").MyStyle();
                if (dc.d.country == "US")
                    p21.Append(" The Defendant's address is " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " - " + dc.d.zip).MyStyle();
                else
                    p21.Append(" The Defendant's address is " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName()).MyStyle();
                if (dc.d.check_ssn != "N" && dc.d.check_ssn != "R")
                    p21.Append(" and Social Security Number is " + dc.d.ssn + ".").MyStyle();
                else if (dc.d.check_ssn == "N")
                    p21.Append(" and the Defendant doesn't have a Social Security Number.").MyStyle();
                else if (dc.d.check_ssn == "R")
                    p21.Append(" and the Defendant refused to provide the Social Security Number.").MyStyle();
                p21.AppendLine("The parties are covered by the following group health plans:").MyStyle();
                if (dc.h.plan_holder == "P")
                {
                    p21.AppendLine("\nPlaintiff").MyStyleU();
                    p21.AppendLine("\n\tGroup Health Plan: " + dc.h.name.ToTitleCase()).MyStyle();
                    p21.AppendLine("\tAddress: " + dc.h.address).MyStyle();
                    p21.AppendLine("\tIdentification Number: " + dc.h.id_no).MyStyle();
                    if (!string.IsNullOrWhiteSpace(dc.h.plan_admin))
                        p21.AppendLine("\tPlan Administrator: " + dc.h.plan_admin.ToTitleCase()).MyStyle();
                    else
                        p21.AppendLine("\tPlan Administrator: None").MyStyle();
                    string coveragetype = null;
                    if (dc.h.medical == "Y")
                        coveragetype = "Medical";
                    if (dc.h.dental == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Dental";
                        else
                            coveragetype += ", Dental";
                    }
                    if (dc.h.optical == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Optical";
                        else
                            coveragetype += ", Optical";
                    }
                    if (string.IsNullOrWhiteSpace(coveragetype))
                        p21.AppendLine("\tType of Coverage: None").MyStyle();
                    else
                        p21.AppendLine("\tType of Coverage: " + coveragetype).MyStyle();
                    p21.AppendLine("\n\tThe Plaintiff has no other group health plans.").MyStyle();
                    p21.AppendLine("\nDefendant").MyStyleU();
                    p21.AppendLine("\n\tGroup Health Plan:  NONE").MyStyle();
                }
                else if (dc.h.plan_holder == "D")
                {
                    p21.AppendLine("\nPlaintiff").MyStyleU();
                    p21.AppendLine("\n\tGroup Health Plan:  NONE").MyStyle();
                    p21.AppendLine("\nDefendant").MyStyleU();
                    p21.AppendLine("\n\tGroup Health Plan: " + dc.h.name.ToTitleCase()).MyStyle();
                    p21.AppendLine("\tAddress: " + dc.h.address).MyStyle();
                    p21.AppendLine("\tIdentification Number: " + dc.h.id_no).MyStyle();
                    if (!string.IsNullOrWhiteSpace(dc.h.plan_admin))
                        p21.AppendLine("\tPlan Administrator: " + dc.h.plan_admin.ToTitleCase()).MyStyle();
                    else
                        p21.AppendLine("\tPlan Administrator: None").MyStyle();
                    string coveragetype = null;
                    if (dc.h.medical == "Y")
                        coveragetype = "Medical";
                    if (dc.h.dental == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Dental";
                        else
                            coveragetype += ", Dental";
                    }
                    if (dc.h.optical == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Optical";
                        else
                            coveragetype += ", Optical";
                    }
                    if (string.IsNullOrWhiteSpace(coveragetype))
                        p21.AppendLine("\tType of Coverage: None").MyStyle();
                    else
                        p21.AppendLine("\tType of Coverage: " + coveragetype).MyStyle();
                    p21.AppendLine("\n\tThe Defendant has no other group health plans.").MyStyle();
                }
                else
                {
                    p21.AppendLine("\nPlaintiff").MyStyleU();
                    p21.AppendLine("\n\tGroup Health Plan:  NONE").MyStyle();
                    p21.AppendLine("\nDefendant").MyStyleU();
                    p21.AppendLine("\n\tGroup Health Plan:  NONE").MyStyle();
                }
                if (dc.ch.no_children == "1")
                    p21.AppendLine("\tThe court has determined that " + ccc.GetCP() + " shall be the legally responsible relative and that the child shall be enrolled in the group health care plan of " + dc.h.health_cover.ToPerson() + " as specified above until the age of 21 years.").MyStyle();
                else
                    p21.AppendLine("\tThe court has determined that " + ccc.GetCP() + " shall be the legally responsible relative and that the children shall be enrolled in the group health care plan of " + dc.h.health_cover.ToPerson() + " as specified above until the age of 21 years.").MyStyle();
                p21.SetLineSpacing(LineSpacingType.Line, 1.25f);
                if (dc.p.sex == "M")
                {
                    p22.AppendLine("\tFIFTEENTH:").MyStyleB();
                    p22.Append(" The Defendant may resume use of the prior surname: " + dc.d.maiden_name.ToUpperCase()).MyStyle();
                }
                else
                {
                    p22.AppendLine("\tFIFTEENTH:").MyStyleB();
                    p22.Append(" The Plaintiff may resume use of the prior surname: " + dc.p.maiden_name.ToUpperCase()).MyStyle();
                }
                p22.Alignment = Alignment.both;
                p22.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p23.AppendLine("\tSIXTEENTH:").MyStyleB();
                p23.Append(" Compliance with DRL Sec. 255 (1) and (2) has been satisfied as follows:  There is no agreement between the parties. Each party has been provided notice as required by DRL Sec. 255(1).").MyStyle();
                p23.Alignment = Alignment.both;
                p23.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p24.AppendLine("\tSEVENTEENTH:").MyStyleB();
                p24.Append(" Registry checks were completed pursuant to DRL Sec. 240 1(a-1)").MyStyle();
                p24.Alignment = Alignment.both;
                p24.SetLineSpacing(LineSpacingType.Line, 1.25f);
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y" && (dc.co.c1_ordertype == "S" || dc.co.c2_ordertype == "S" || dc.co.c3_ordertype == "S" || dc.co.c4_ordertype == "S"))
                {
                    p25.AppendLine("\tEIGHTEENTH:  ").MyStyleB();
                    p25.Append("The Family Court entered the following orders: ").MyStyle();
                    if (no_of_court_orders == 1 && dc.co.c1_ordertype != "O")
                        p25.Append(" order, dated " + dc.co.c1_date.ToDateLong() + ", bearing Index No. " + dc.co.c1_index + ",").MyStyle();
                    if (no_of_court_orders == 2 && dc.co.c2_ordertype != "O")
                        p25.Append(" order, dated " + dc.co.c2_date.ToDateLong() + ", bearing Index No. " + dc.co.c2_index + ",").MyStyle();
                    if (no_of_court_orders == 3 && dc.co.c3_ordertype != "O")
                        p25.Append(" order, dated " + dc.co.c3_date.ToDateLong() + ", bearing Index No. " + dc.co.c3_index + ",").MyStyle();
                    if (no_of_court_orders == 4 && dc.co.c4_ordertype != "O")
                        p25.Append(" order, dated " + dc.co.c4_date.ToDateLong() + ", bearing Index No. " + dc.co.c4_index + ",").MyStyle();
                    p25.Alignment = Alignment.both;
                    p25.SetLineSpacing(LineSpacingType.Line, 1.25f);
                    p26.AppendLine("\tNINETEENTH:").MyStyleB();
                }
                else
                {
                    p26.AppendLine("\tEIGHTEENTH:").MyStyleB();
                }
                p26.Append(" The Judgment of Divorce incorporates all ancillary issues, including the payment of counsel and experts' fees and expenses, which issues:").MyStyle();
                p26.Alignment = Alignment.both;
                p26.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p27.AppendLine("\t[  ] were settled by written settlement/separation agreement").MyStyle();
                p27.AppendLine("\t[  ] were settled by oral settlement/separation on the record").MyStyle();
                p27.SetLineSpacing(LineSpacingType.Line, 1.25f);
                if (dc.waiver_default == "1")
                    p28.AppendLine("\t[X] were settled by written waivers or affidavits (the Affidavit of Plaintiff and the Affidavit of Defendant)").MyStyle();
                if (dc.waiver_default == "2")
                    p28.AppendLine("\t[  ] were settled by written waivers or affidavits (the Affidavit of Plaintiff and the Affidavit of Defendant)").MyStyle();
                p28.Alignment = Alignment.both;
                p28.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p29.AppendLine("\t[  ] were determined by the Court ").MyStyle();
                p30.AppendLine("\t[  ] were determined by the Family Court order (custody, visitation or child support and/or spousal support issues only)").MyStyle();
                p30.Alignment = Alignment.both;
                p30.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p31.AppendLine("\t[  ] are not to be incorporated into the Judgment of Divorce, in that neither party to the divorce has contested any such issues based on the Affidavit of Plaintiff (which Defendant has not contested).").MyStyle();
                p31.Alignment = Alignment.both;
                p31.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p32.InsertPageBreakBeforeSelf();
                p32.Append("CONCLUSIONS OF LAW").MyStyleB();
                p32.Alignment = Alignment.center;
                p33.AppendLine("\n\tFIRST:").MyStyleB();
                p33.Append(" Residence as required by DRL Sec. 230 has been satisfied.").MyStyle();
                p33.Alignment = Alignment.both;
                p33.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p34.AppendLine("\tSECOND:").MyStyleB();
                p34.Append(" The requirements of DRL Sec. 255 have been satisfied.").MyStyle();
                p34.Alignment = Alignment.both;
                p34.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p35.AppendLine("\tTHIRD:").MyStyleB();
                p35.Append(" The requirements of DRL Sec. 240 1(a) including the Records Checking Requirements in DRL Sec. 240 1(a-1) have been satisfied.").MyStyle();
                p35.Alignment = Alignment.both;
                p35.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p36.AppendLine("\tFOURTH:").MyStyleB();
                p36.Append(" The requirements of DRL Sec. 240(1-b) have been satisfied.").MyStyle();
                p36.Alignment = Alignment.both;
                p36.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p37.AppendLine("\tFIFTH:").MyStyleB();
                p37.Append(" The requirements of DRL Sec. 236(B)(2)(b) have been satisfied.").MyStyle();
                p37.Alignment = Alignment.both;
                p37.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p38.AppendLine("\tSIXTH:").MyStyleB();
                p38.Append(" The requirements of DRL Sec. 236(B)(6) have been satisfied.").MyStyle();
                p38.Alignment = Alignment.both;
                p38.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p39.AppendLine("\tSEVENTH:").MyStyleB();
                if (dc.r.grounds == "1")
                    p39.Append(" DRL Sec. 170 subd. (7)").MyStyle();
                else if (dc.r.grounds == "2")
                    p39.Append(" DRL Sec. 170 subd. (2)").MyStyle();
                else if (dc.r.grounds == "3")
                    p39.Append(" DRL Sec. 170 subd. (2)").MyStyle();
                else if (dc.r.grounds == "4")
                    p39.Append(" DRL Sec. 170 subd. (1)").MyStyle();
                p39.Append(" is the ground alleged and all economic issues of equitable distribution of marital property, the payment or waiver of spousal support, the payment of child support, the payment of counsel and experts' fees and expenses, as well as custody and visitation with infant children of the marriage, have been resolved by the parties or determined by the Court and incorporated into the Judgment.").MyStyle();
                p39.Alignment = Alignment.both;
                p39.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p40.AppendLine("\tEIGHTH:").MyStyleB();
                p40.Append(" The Plaintiff is entitled to a judgment of divorce on the grounds of ").MyStyle();
                if (dc.r.grounds == "1")
                {
                    p40.Append("(DRL Sec. 170(7))").MyStyle();
                    p40.Append(" - The relationship between the Plaintiff and Defendant has broken down irretrievably for a period of at least six months,").MyStyle();
                }
                else if (dc.r.grounds == "2")
                {
                    p40.Append("(DRL Sec. 170 subd. (2))").MyStyle();
                    p40.Append(" - The abandonment of the Plaintiff by the Defendant for a period of more than one year,").MyStyle();
                }
                else if (dc.r.grounds == "3")
                {
                    p40.Append("(DRL Sec. 170 subd. (2))").MyStyle();
                    p40.Append(" - The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year,").MyStyle();
                }
                else if (dc.r.grounds == "4")
                {
                    p40.Append("(DRL Sec. 170 subd. (1))").MyStyle();
                    p40.Append(" - The cruel and inhuman treatment of the Plaintiff by the Defendant,").MyStyle();
                }
                p40.Append(" and granting the incidental relief awarded.").MyStyle();
                p40.Alignment = Alignment.both;
                p40.SetLineSpacing(LineSpacingType.Line, 1.25f);
                p41.AppendLine("Dated:\n\n\n").MyStyle();
                p41.AppendLine("\t\t\t\t\t\t\t\t_______________________").MyStyle();
                p41.AppendLine("\t\t\t\t\t\t\t\tJ.S.C./Referee\n\n").MyStyle();
                p41.AppendLine("(Form UD-10 – Rev.3/1/16) ").MyStyle();
                p41.SetLineSpacing(LineSpacingType.Line, 1.25f);
                document.Save();
            }
        }
        public static void Divorce2_Judgement(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            LawOfficeDetail ld = GetLawOfficeDetails();
            string pname, dname, basis, venue;
            pname = dc.p.name.ToUpperCase();
            dname = dc.d.name.ToUpperCase();
            basis = StringDecorators.GetBasisVenue(dc);
            venue = StringDecorators.GetCountyVenue(dc);
            ChildCareCalculations ccc = new ChildCareCalculations(dc);
            ChildCalc cc = ccc.GetChildCalcData();
            int[] e = Miscellaneous.GetEligibleChildren(dc.ch, 18);
            int[] f = Miscellaneous.GetEligibleChildren(dc.ch, 21);
            int no_of_court_orders = 0;
            if (!ReferenceEquals(dc.co, null))
                Int32.TryParse(dc.co.no_order, out no_of_court_orders);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (File.Exists(folder + "\\Judgement_Of_Divorce.docx"))
            {
                File.Delete(folder + "\\Judgement_Of_Divorce.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Judgement_Of_Divorce.docx"))
            {
                Paragraph p0 = document.InsertParagraph();
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = document.InsertParagraph();
                Paragraph p13 = document.InsertParagraph();
                Paragraph p14 = document.InsertParagraph();
                Paragraph p15 = document.InsertParagraph();
                Paragraph p16 = document.InsertParagraph();
                Paragraph p17 = document.InsertParagraph();
                Paragraph p18 = document.InsertParagraph();
                Paragraph p19 = document.InsertParagraph();
                Paragraph p20 = document.InsertParagraph();
                Paragraph p21 = document.InsertParagraph();
                Paragraph p22 = document.InsertParagraph();
                Paragraph p22_1 = document.InsertParagraph();
                Paragraph p22_2 = document.InsertParagraph();
                Paragraph p23 = document.InsertParagraph();
                Paragraph p24 = document.InsertParagraph();
                Paragraph p25 = document.InsertParagraph();
                Paragraph p26 = document.InsertParagraph();
                Paragraph p27 = document.InsertParagraph();
                Paragraph p27_1 = document.InsertParagraph();
                Paragraph p28 = document.InsertParagraph();
                Paragraph p29 = document.InsertParagraph();
                Paragraph p30 = document.InsertParagraph();
                Paragraph p31 = document.InsertParagraph();
                Paragraph p32 = document.InsertParagraph();
                p0.AppendLine("\t\t\t\t          At the Matrimonial/IAS Part      of the New York ").MyStyle();
                p0.AppendLine("\t\t\t\t          Supreme Court at the Courthouse, " + venue.ToTitleCase() + " County,").MyStyle();
                p0.AppendLine("\t\t\t\t          on the        day of                     , " + DateTime.Now.Year).MyStyle();
                p0.AppendLine("Present:").MyStyle();
                p0.AppendLine("Hon.\t\t\t\t\tJustice").MyStyle();
                p0.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p0.AppendLine("\t\t\t\t\t\t\t\t\tCalendar No: ").MyStyle();
                p0.AppendLine(pname).MyStyle();
                p0.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p0.AppendLine("\t\t\t\t\t\t\tJUDGEMENT OF DIVORCE").MyStyleB();
                p0.AppendLine("\t\t-against-\n").MyStyle();
                p0.AppendLine(dname).MyStyle();
                p0.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p0.AppendLine("---------------------------------------------------------------------X").MyStyle();
                p1.AppendLine("\t\tEACH PARTY HAS THE RIGHT TO SEEK A MODIFICATION OF THE CHILD SUPPORT ORDER UPON A SHOWING OF (I) A SUBSTANTIAL CHANGE IN THE CIRCUMSTANCES, OR (II) THAT THREE YEARS HAVE PASSED SINCE THE ORDER WAS ENTERED, LAST MODIFIED OR ADJUSTED, OR (III) THERE HAS BEEN A CHANGE IN EITHER PARTY'S GROSS INCOME BY FIFTEEN PERCENT OR MORE SINCE THE ORDER WAS ENTERED, LAST MODIFIED OR ADJUSTED; HOWEVER, IF THE PARTIES HAVE SPECIFICALLY OPTED OUT OF SUBPARAGRAPHS (II) OR (III) OF THIS PARAGRAPH IN A VALIDLY EXECUTED AGREEMENT OR STIPULATION, THEN THAT BASIS TO SEEK MODIFICATION DOES NOT APPLY. THE PARTIES HAVE NOT ELECTED TO OPTED OUT OF SUBPARAGRAPHS (II) OR (III).").MyStyle();
                p1.Alignment = Alignment.both;
                p2.AppendLine("This action was submitted to the referee/this Court for consideration on the\tday of\t\t, " + DateTime.Now.Year + ".").MyStyle();
                p2.IndentationBefore = 1.3f;
                p2.Alignment = Alignment.both;
                p2.SetLineSpacing(LineSpacingType.Line, 1.5f);
                if (dc.r.summons_usa == "Y")
                    p3.Append("\tThe Defendant was served personally within the State of New York.").MyStyle();
                else
                    p3.Append("\tThe Defendant was served personally outside the State of New York.").MyStyle();
                p3.Alignment = Alignment.both;
                p3.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p4.Append("The Plaintiff presented a Summons with Notice and Affidavit of Plaintiff constituting the facts of the matter.").MyStyle();
                p4.IndentationBefore = 1.3f;
                p4.Alignment = Alignment.both;
                p4.SetLineSpacing(LineSpacingType.Line, 1.5f);
                if (dc.waiver_default == "1")
                {
                    if (dc.d.sex == "F")
                        p5.Append("\tThe Defendant has appeared and waived her right to answer.").MyStyle();
                    else
                        p5.Append("\tThe Defendant has appeared and waived his right to answer.").MyStyle();
                }
                if (dc.waiver_default == "2")
                    p5.Append("\tThe Defendant has defaulted in appearance.").MyStyle();
                p5.Alignment = Alignment.both;
                p5.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p6.Append("\tThe Court accepted written proof of non-military service.").MyStyle();
                p6.Alignment = Alignment.both;
                p6.SetLineSpacing(LineSpacingType.Line, 1.5f);
                if (dc.p.country == "US")
                    p7.Append("\tThe Plaintiff's address is " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.state.ToStateName() + " - " + dc.p.zip).MyStyle();
                else
                    p7.Append("\tThe Plaintiff's address is " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase().ToTitleCase() + ", " + dc.p.country.ToCountryName()).MyStyle();
                if (dc.p.check_ssn == "Y")
                    p7.Append(" and Social Security Number is " + dc.p.ssn + ".").MyStyle();
                else
                    p7.Append(" and the Plaintiff doesn't have a Social Security Number.").MyStyle();
                if (dc.d.country == "US")
                    p7.Append(" The Defendant's address is " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + " - " + dc.d.zip).MyStyle();
                else
                    p7.Append(" The Defendant's address is " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.country.ToCountryName()).MyStyle();
                if (dc.d.check_ssn == "Y")
                    p7.Append(" and Social Security Number is " + dc.d.ssn + ".").MyStyle();
                else if (dc.d.check_ssn == "N")
                    p7.Append(" and the Defendant doesn't have a Social Security Number.").MyStyle();
                else if (dc.d.check_ssn == "R")
                    p7.Append(" and the Defendant refused to provide the Social Security Number.").MyStyle();
                p7.Alignment = Alignment.both;
                p7.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p8.AppendLine("\tNow").MyStyleB();
                p8.Append(" on motion of " + dc.p.name.ToTitleCase() + ", the Plaintiff Pro-se, it is:").MyStyle();
                p8.Alignment = Alignment.both;
                p8.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p9.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p9.Append(" that the Referee's Report, if any, is confirmed and it is further").MyStyle();
                p9.Alignment = Alignment.both;
                p9.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p10.AppendLine("\tORDERED, ADJUDGED AND DECREED").MyStyleB();
                p10.Append(" that the application of the Plaintiff is hereby granted to dissolve the marriage between the Plaintiff, " + pname + " and the Defendant " + dname + ", by reason of").MyStyle();
                if (dc.r.grounds == "1")
                    p10.Append(":  The relationship between the Plaintiff and Defendant has broken down irretrievably for a period of at least six months, pursuant to DRL Sec. 170(7), ").MyStyle();
                else if (dc.r.grounds == "2")
                    p10.Append(":  The abandonment of the Plaintiff by the Defendant for a period of more than one year, pursuant to DRL Sec. 170 subd. (2), ").MyStyle();
                else if (dc.r.grounds == "3")
                    p10.Append(":  The constructive abandonment of the Plaintiff by the Defendant for a period of more than one year, pursuant to DRL Sec. 170 subd. (2), ").MyStyle();
                else if (dc.r.grounds == "4")
                    p10.Append(":  The cruel and inhuman treatment of the Plaintiff by the Defendant, pursuant to DRL Sec. 170 subd. (1), ").MyStyle();
                if (dc.waiver_default == "1")
                    p10.Append("and both the Plaintiff and Defendant have so stated under oath in their affidavits; and it is further").MyStyle();
                if (dc.waiver_default == "2")
                    p10.Append("and the Plaintiff has so stated under oath in the Affidavit of Plaintiff; and it is further").MyStyle();
                p10.Alignment = Alignment.both;
                p10.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p11.AppendLine("The requirements of DRL Sec. 240 1(a-1) have been met and the Court having considered the results of said inquiries, it is further").MyStyleB();
                p11.Alignment = Alignment.both;
                p11.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p12.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                if (dc.ch.no_children == "1")
                    p12.Append(" that the " + ccc.GetCP() + " shall have sole custody of the child of the marriage, under the age of 18, namely").MyStyle();
                else
                    p12.Append(" that the " + ccc.GetCP() + " shall have sole custody of the children of the marriage, under the age of 18, namely").MyStyle();
                p12.Alignment = Alignment.both;
                p12.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p13.AppendLine("\tName\t\t\tSocial Security No.          Date of Birth          Address").MyStyleB();
                if (e[0] >= 1)
                {
                    if (dc.ch.c1_lives == "P")
                    {
                        p13.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_ssn + " - " + dc.ch.c1_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c1_lives == "D")
                    {
                        p13.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_ssn + " - " + dc.ch.c1_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p13.AppendLine(dc.ch.c1_name.ToTitleCase() + " - " + dc.ch.c1_ssn + " - " + dc.ch.c1_dob.ToDateLong() + " - " + dc.ch.t1_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 2)
                {
                    p13.AppendLine();
                    if (dc.ch.c2_lives == "P")
                    {
                        p13.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.c2_ssn + " - " + dc.ch.c2_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c2_lives == "D")
                    {
                        p13.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.c2_ssn + " - " + dc.ch.c2_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p13.AppendLine(dc.ch.c2_name.ToTitleCase() + " - " + dc.ch.c2_ssn + " - " + dc.ch.c2_dob.ToDateLong() + " - " + dc.ch.t2_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 3)
                {
                    p13.AppendLine();
                    if (dc.ch.c3_lives == "P")
                    {
                        p13.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.c3_ssn + " - " + dc.ch.c3_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c3_lives == "D")
                    {
                        p13.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.c3_ssn + " - " + dc.ch.c3_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p13.AppendLine(dc.ch.c3_name.ToTitleCase() + " - " + dc.ch.c3_ssn + " - " + dc.ch.c3_dob.ToDateLong() + " - " + dc.ch.t3_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 4)
                {
                    p13.AppendLine();
                    if (dc.ch.c4_lives == "P")
                    {
                        p13.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.c4_ssn + " - " + dc.ch.c4_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c4_lives == "D")
                    {
                        p13.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.c4_ssn + " - " + dc.ch.c4_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p13.AppendLine(dc.ch.c4_name.ToTitleCase() + " - " + dc.ch.c4_ssn + " - " + dc.ch.c4_dob.ToDateLong() + " - " + dc.ch.t4_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 5)
                {
                    p13.AppendLine();
                    if (dc.ch.c5_lives == "P")
                    {
                        p13.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.c5_ssn + " - " + dc.ch.c5_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c5_lives == "D")
                    {
                        p13.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.c5_ssn + " - " + dc.ch.c5_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p13.AppendLine(dc.ch.c5_name.ToTitleCase() + " - " + dc.ch.c5_ssn + " - " + dc.ch.c5_dob.ToDateLong() + " - " + dc.ch.t5_address.ToTitleCase()).MyStyle();
                    }
                }
                if (e[0] >= 6)
                {
                    p13.AppendLine();
                    if (dc.ch.c6_lives == "P")
                    {
                        p13.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.c6_ssn + " - " + dc.ch.c6_dob.ToDateLong() + " - " + dc.p.street.ToTitleCase() + ", " + dc.p.city.ToTitleCase() + ", " + dc.p.state.ToStateName() + "-" + dc.p.zip).MyStyle();
                    }
                    else if (dc.ch.c6_lives == "D")
                    {
                        p13.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.c6_ssn + " - " + dc.ch.c6_dob.ToDateLong() + " - " + dc.d.street.ToTitleCase() + ", " + dc.d.city.ToTitleCase() + ", " + dc.d.state.ToStateName() + "-" + dc.d.zip).MyStyle();
                    }
                    else
                    {
                        p13.AppendLine(dc.ch.c6_name.ToTitleCase() + " - " + dc.ch.c6_ssn + " - " + dc.ch.c6_dob.ToDateLong() + " - " + dc.ch.t6_address.ToTitleCase()).MyStyle();
                    }
                }
                p13.Append("\nThere is no other child as a result of this marriage and no child is expected.").MyStyle();
                p13.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p14.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p14.Append(" that the " + ccc.GetNCP() + " shall have reasonable rights of visitation away from the custodial residence, and it is further").MyStyle();
                p14.Alignment = Alignment.both;
                p14.SetLineSpacing(LineSpacingType.Line, 1.5f);
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y")
                {
                    p15.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                    if (no_of_court_orders >= 1 && dc.co.c1_ordertype != "O")
                    {
                        p15.Append("\tthat the order, dated " + dc.co.c1_date.ToDateLong() + ", of the " + dc.co.c1_court_name.ToTitleCase() + " of " + dc.co.c1_county.ToTitleCase() + " County under Docket No. " + dc.co.c1_index + " shall be continued, ").MyStyle();
                        if (no_of_court_orders == 1)
                            p15.Append(" and a copy of this Judgment shall be served by Plaintiff's attorney upon the Clerk of the Family Court within ten days after the date hereof, and it is further").MyStyle();
                    }
                    if (no_of_court_orders >= 2 && dc.co.c2_ordertype != "O")
                    {
                        p15.Append("and the order, dated " + dc.co.c2_date.ToDateLong() + ", of the " + dc.co.c1_court_name.ToTitleCase() + " of " + dc.co.c2_county.ToTitleCase() + " County under Docket No. " + dc.co.c2_index + " shall be continued, ").MyStyle();
                        if (no_of_court_orders == 2)
                            p15.Append(" and a copy of this Judgment shall be served by Plaintiff's attorney upon the Clerk of the Family Court within ten days after the date hereof, and it is further").MyStyle();
                    }
                    if (no_of_court_orders >= 3 && dc.co.c3_ordertype != "O")
                    {
                        p15.Append("and the order, dated " + dc.co.c3_date.ToDateLong() + ", of the " + dc.co.c1_court_name.ToTitleCase() + " of " + dc.co.c3_county.ToTitleCase() + " County under Docket No. " + dc.co.c3_index + " shall be continued, ").MyStyle();
                        if (no_of_court_orders == 3)
                            p15.Append(" and a copy of this Judgment shall be served by Plaintiff's attorney upon the Clerk of the Family Court within ten days after the date hereof, and it is further").MyStyle();
                    }
                    if (no_of_court_orders >= 4 && dc.co.c4_ordertype != "O")
                    {
                        p15.Append("and the order, dated " + dc.co.c4_date.ToDateLong() + ", of the " + dc.co.c1_court_name.ToTitleCase() + " of " + dc.co.c4_county.ToTitleCase() + " County under Docket No. " + dc.co.c4_index + " shall be continued, ").MyStyle();
                        if (no_of_court_orders == 4)
                            p15.Append(" and a copy of this Judgment shall be served by Plaintiff's attorney upon the Clerk of the Family Court within ten days after the date hereof, and it is further").MyStyle();
                    }
                }
                else
                {
                    p15.AppendLine("ORDERED AND ADJUDGED").MyStyleB();
                    p15.Append(" that a copy of this Judgment shall be served by Plaintiff's attorney upon the Clerk of the Family Court within ten days after the date hereof, There are no court orders with regard to custody, visitation or maintenance to be continued; and it is further").MyStyle();
                }
                p15.Alignment = Alignment.both;
                p15.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p16.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p16.Append(" that there are no court orders with regard to custody, visitation or maintenance to be continued; and it is further").MyStyle();
                p16.Alignment = Alignment.both;
                p16.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p17.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p17.Append(" that no maintenance was awarded because [X] neither party seeks maintenance, OR[ ] the Guideline Award of Maintenance under the Maintenance Guidelines Law(Ch. 269, L. 2015) was zero, OR[ ] the Court has denied the request for maintenance, ").MyStyle();
                p17.Alignment = Alignment.both;
                p17.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p18.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                if (dc.ch.no_children == "1")
                    p18.Append(" that the " + ccc.GetNCP() + " shall pay to the " + ccc.GetCP() + " as and for the support of the parties' child, namely:").MyStyle();
                else
                    p18.Append(" that the " + ccc.GetNCP() + " shall pay to the " + ccc.GetCP() + " as and for the support of the parties' children, namely:").MyStyle();
                p18.Alignment = Alignment.both;
                p18.SetLineSpacing(LineSpacingType.Line, 1.5f);
                if (f[0] >= 1 && f[1] == 1)
                {
                    p19.AppendLine("\tName \t\t\t\tDate of Birth").MyStyleB();
                    p19.AppendLine(dc.ch.c1_name.ToTitleCase() + "\t - \t" + dc.ch.c1_dob.ToDateLong()).MyStyle();
                }
                if (f[0] >= 2 && f[2] == 1)
                {
                    p19.AppendLine(dc.ch.c2_name.ToTitleCase() + "\t - \t" + dc.ch.c2_dob.ToDateLong()).MyStyle();
                }
                if (f[0] >= 3 && f[3] == 1)
                {
                    p19.AppendLine(dc.ch.c3_name.ToTitleCase() + "\t - \t" + dc.ch.c3_dob.ToDateLong()).MyStyle();
                }
                if (f[0] >= 4 && f[4] == 1)
                {
                    p19.AppendLine(dc.ch.c4_name.ToTitleCase() + "\t - \t" + dc.ch.c4_dob.ToDateLong()).MyStyle();
                }
                if (f[0] >= 5 && f[5] == 1)
                {
                    p19.AppendLine(dc.ch.c5_name.ToTitleCase() + "\t - \t" + dc.ch.c5_dob.ToDateLong()).MyStyle();
                }
                if (f[0] >= 6 && f[6] == 1)
                {
                    p19.AppendLine(dc.ch.c6_name.ToTitleCase() + "\t - \t" + dc.ch.c6_dob.ToDateLong()).MyStyle();
                }
                p19.SetLineSpacing(LineSpacingType.Line, 1.5f);
                if (!ReferenceEquals(dc.co, null) && dc.co.any_order == "Y" && (dc.co.c1_ordertype == "S" || dc.co.c2_ordertype == "S" || dc.co.c3_ordertype == "S" || dc.co.c4_ordertype == "S"))
                {
                    if (dc.co.c1_ordertype == "S")
                        p20.AppendLine("the sum of " + dc.co.c1_amount.StringToWords() + " Dollars ($" + dc.co.c1_amount.ToFormattedAmount() + ") " + dc.co.c1_frequency.ExpandFrequency() + " for child support, commencing on                      and " + dc.co.c1_frequency.GetFrequency() + " thereafter, which shall be paid directly to the Plaintiff, in accordance with the Court's decision, and it is further").MyStyle();
                    if (dc.co.c2_ordertype == "S")
                        p20.AppendLine("the sum of " + dc.co.c2_amount.StringToWords() + " Dollars ($" + dc.co.c2_amount.ToFormattedAmount() + ") " + dc.co.c2_frequency.ExpandFrequency() + " for child support, commencing on                      and " + dc.co.c1_frequency.GetFrequency() + " thereafter, which shall be paid directly to the Plaintiff, in accordance with the Court's decision, and it is further").MyStyle();
                    if (dc.co.c3_ordertype == "S")
                        p20.AppendLine("the sum of " + dc.co.c3_amount.StringToWords() + " Dollars ($" + dc.co.c3_amount.ToFormattedAmount() + ") " + dc.co.c3_frequency.ExpandFrequency() + " for child support, commencing on                      and " + dc.co.c1_frequency.GetFrequency() + " thereafter, which shall be paid directly to the Plaintiff, in accordance with the Court's decision, and it is further").MyStyle();
                    if (dc.co.c3_ordertype == "S")
                        p20.AppendLine("the sum of " + dc.co.c4_amount.StringToWords() + " Dollars ($" + dc.co.c4_amount.ToFormattedAmount() + ") " + dc.co.c4_frequency.ExpandFrequency() + " for child support, commencing on                      and " + dc.co.c1_frequency.GetFrequency() + " thereafter, which shall be paid directly to the Plaintiff, in accordance with the Court's decision, and it is further").MyStyle();
                }
                else
                {
                    if (dc.ch.c1_lives == "P")
                        p20.AppendLine("the sum of " + ccc.GetNCPMonthlyChildSupportWithAddOn().StringToWords() + " Dollars ($" + ccc.GetNCPMonthlyChildSupportWithAddOn() + ") per month on the first day of each month for child support, commencing on \t\t\t and on the first day of each month thereafter, which shall be paid directly to the Plaintiff, in accordance with the Court's decision, and it is further").MyStyle();
                    else
                        p20.AppendLine("the sum of " + ccc.GetNCPMonthlyChildSupportWithAddOn().StringToWords() + " Dollars ($" + ccc.GetNCPMonthlyChildSupportWithAddOn() + ") per month on the first day of each month for child support, commencing on \t\t\t and on the first day of each month thereafter, which shall be paid directly to the Defendant, in accordance with the Court's decision, and it is further").MyStyle();
                }
                p20.Alignment = Alignment.both;
                p20.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p21.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                if (dc.h.health_cover == "P")
                    p21.Append(" that the relative legally responsible to supply health insurance benefits is the Plaintiff, and it is further").MyStyle();
                else
                    p21.Append(" that the relative legally responsible to supply health insurance benefits is the Defendant, and it is further").MyStyle();
                p21.Alignment = Alignment.both;
                p21.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p22.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                if (dc.h.plan_holder == "N")
                {
                    if (dc.h.health_cover == "P")
                    {
                        if (dc.ch.no_children == "1")
                            p22.Append(" that the Plaintiff shall apply to the state sponsored health insurance plan and provide health insurance coverage to the child until the age of 21 years unless sooner emancipated, and it is further").MyStyle();
                        else
                            p22.Append(" that the Plaintiff shall apply to the state sponsored health insurance plan and provide health insurance coverage to the children until the age of 21 years unless sooner emancipated, and it is further").MyStyle();
                    }
                    else
                    {
                        if (dc.ch.no_children == "1")
                            p22.Append(" that the Defendant shall apply to the state sponsored health insurance plan and provide health insurance coverage to the child until the age of 21 years unless sooner emancipated, and it is further").MyStyle();
                        else
                            p22.Append(" that the Defendant shall apply to the state sponsored health insurance plan and provide health insurance coverage to the children until the age of 21 years unless sooner emancipated, and it is further").MyStyle();
                    }
                    p22.Alignment = Alignment.both;
                    p22.SetLineSpacing(LineSpacingType.Line, 1.5f);
                }
                else
                {
                    p22.Append(" that the relative legally responsible to supply health insurance benefits is eligible under the following group health insurance plan or plans:").MyStyle();
                    p22.Alignment = Alignment.both;
                    p22.SetLineSpacing(LineSpacingType.Line, 1.5f);
                    p22_1.AppendLine("\tGroup Health Plan: " + dc.h.name.ToTitleCase()).MyStyle();
                    p22_1.AppendLine("\tAddress: " + dc.h.address).MyStyle();
                    p22_1.AppendLine("\tIdentification Number: " + dc.h.id_no).MyStyle();
                    if (!string.IsNullOrWhiteSpace(dc.h.plan_admin))
                        p22_1.AppendLine("\tPlan Administrator: " + dc.h.plan_admin.ToTitleCase()).MyStyle();
                    else
                        p22_1.AppendLine("\tPlan Administrator: None").MyStyle();
                    string coveragetype = null;
                    if (dc.h.medical == "Y")
                        coveragetype = "Medical";
                    if (dc.h.dental == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Dental";
                        else
                            coveragetype += ", Dental";
                    }
                    if (dc.h.optical == "Y")
                    {
                        if (string.IsNullOrWhiteSpace(coveragetype))
                            coveragetype = "Optical";
                        else
                            coveragetype += ", Optical";
                    }
                    if (string.IsNullOrWhiteSpace(coveragetype))
                        p22_1.AppendLine("\tType of Coverage: None").MyStyle();
                    else
                        p22_1.AppendLine("\tType of Coverage: " + coveragetype).MyStyle();
                    if (dc.h.plan_holder == "P")
                        p22_1.AppendLine("\n\tThe Plaintiff has no other group health plans.").MyStyle();
                    else
                        p22_1.AppendLine("\n\tThe Defendant has no other group health plans.").MyStyle();
                    if (dc.ch.no_children == "1")
                        p22_2.AppendLine("And the relative legally responsible to supply health insurance benefits shall provide health insurance coverage to the child until the age of 21 years, and it is further").MyStyle();
                    else
                        p22_2.AppendLine("And the relative legally responsible to supply health insurance benefits shall provide health insurance coverage to the children until the age of 21 years, and it is further").MyStyle();
                    p22_2.Alignment = Alignment.both;
                    p22_2.SetLineSpacing(LineSpacingType.Line, 1.5f);
                }
                p23.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                if (dc.ch.no_children == "1")
                {
                    if (dc.ch.c1_lives == "P")
                        p23.Append(" that the Defendant shall pay " + ccc.GetDPercent() + " percent of the unreimbursed medical expenses of the child until the age of 21 years, and it is further").MyStyle();
                    else if (dc.ch.c1_lives == "D")
                        p23.Append(" that the Plaintiff shall pay " + ccc.GetPPercent() + " percent of the unreimbursed medical expenses of the child until the age of 21 years, and it is further").MyStyle();
                }
                else
                {
                    if (dc.ch.c1_lives == "P")
                        p23.Append(" that the Defendant shall pay " + ccc.GetDPercent() + " percent of the unreimbursed medical expenses of the children until the age of 21 years, and it is further").MyStyle();
                    else if (dc.ch.c1_lives == "D")
                        p23.Append(" that the Plaintiff shall pay " + ccc.GetPPercent() + " percent of the unreimbursed medical expenses of the children until the age of 21 years, and it is further").MyStyle();
                }
                p23.Alignment = Alignment.both;
                p23.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p24.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p24.Append(" that there is no request for maintenance and it is further").MyStyle();
                p24.Alignment = Alignment.both;
                p24.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p25.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p25.Append(" that the Family Court shall be granted concurrent jurisdiction with this Supreme Court with respect to the issues of maintenance, child support, custody and visitation, and it is further").MyStyle();
                p25.Alignment = Alignment.both;
                p25.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p26.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p26.Append(" that both parties are authorized to resume the use of any prior surname, and it is further").MyStyle();
                p26.Alignment = Alignment.both;
                p26.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p27.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                if (dc.p.sex == "F")
                    p27.Append(" that the Plaintiff may resume use of the prior surname, " + dc.p.maiden_name.ToTitleCase() + " and it is further").MyStyle();
                else
                    p27.Append(" that the Defendant may resume use of the prior surname, " + dc.d.maiden_name.ToTitleCase() + " and it is further").MyStyle();
                p27.Alignment = Alignment.both;
                p27.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p27_1.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p27_1.Append(" that pursuant to the [] parties’ Settlement Agreement dated __________________ OR [] the court’s decision after trial, all parties shall duly execute all documents necessary to formally transfer title to real estate or co-op shares to the [] Plaintiff OR [] Defendant as set forth in the [] parties’ Settlement Agreement OR [] the court’s decision after trial, including, without limitation, an appropriate deed or other conveyance of title, and all other forms necessary to record such deed or other title documents (including the satisfaction or refinance of any mortgage if necessary) to convey ownership of the marital residence located at _______________________, no later than ______________________; OR [X] Not applicable; and it is further").MyStyle();
                p27_1.Alignment = Alignment.both;
                p27_1.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p28.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p28.Append(" that the Settlement Agreement entered into between the parties on the day of               , an original OR a transcript of which is on file with this Court and incorporated herein by reference, shall survive and shall not be merged into this judgment, and the parties are hereby directed to comply with all legally enforceable terms and conditions of said agreement as if such terms and conditions were set forth in their entirety herein; and it is further").MyStyle();
                p28.Alignment = Alignment.both;
                p28.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p29.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p29.Append(" that the Supreme Court shall retain jurisdiction to hear any applications to enforce the provisions of said Settlement Agreement or to enforce or modify the provisions of this judgment, provided the court retains jurisdiction of the matter concurrently with the Family Court for the purpose of specifically enforcing, such of the provisions of that (separation agreement)(stipulation agreement) as are capable of specific enforcement, to the extent permitted by law, and of modifying such judgment with respect to maintenance, support, custody or visitation to the extent permitted by law , or both; and it is further").MyStyle();
                p29.Alignment = Alignment.both;
                p29.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p30.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p30.Append(" that any applications brought in Supreme Court to enforce the provisions of said Settlement Agreement or to enforce or modify the provisions of this Judgment shall be brought in a County wherein one of the parties reside; provided that if there are minor children of the marriage, such applications shall be brought in a County wherein one of the parties or the child or children reside, except, in the discretion of the judge, for good cause. Good cause applications shall be made by motion or order to show cause. Where the address of either party and any child or children is unknown and not a matter of public record, or is subject to an existing confidentiality order pursuant to DRL § 254 or FCA § 154-b, such applications may be brought in the County where the Judgment was entered; and it is further").MyStyle();
                p30.Alignment = Alignment.both;
                p30.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p31.AppendLine("\tORDERED AND ADJUDGED").MyStyleB();
                p31.Append(" that the Defendant shall be served with a copy of this Judgment, with notice of entry, by the Plaintiff within 20 days of such entry.").MyStyle();
                p31.Alignment = Alignment.both;
                p31.SetLineSpacing(LineSpacingType.Line, 1.5f);
                p31.KeepWithNextParagraph();
                p32.AppendLine("Dated:").MyStyle();
                p32.AppendLine("\t\t\t\t\t\t ENTER:\n\n").MyStyle();
                p32.AppendLine("\t\t\t\t\t\t\t\t___________________").MyStyle();
                p32.AppendLine("\t\t\t\t\t\t\t\tJ.S.C./Referee\n\n").MyStyle();
                p32.AppendLine("\t\t\t\t\t\t\t\t___________________").MyStyle();
                p32.AppendLine("\t\t\t\t\t\t\t\tCLERK\n").MyStyle();
                p32.AppendLine("(Form UD-11 - Eff. 5/31/18)").MyStyle();
                p32.SetLineSpacing(LineSpacingType.Line, 1.5f);
                document.Save();
            }
        }
        public static void Divorce2_ChildCare(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            string county_venue = StringDecorators.GetCountyVenue(dc);
            string basis_venue = StringDecorators.GetBasisVenue(dc);
            string pname = dc.p.name.ToUpperCase();
            string dname = dc.d.name.ToUpperCase();
            LawOfficeDetail ld = GetLawOfficeDetails();
            ChildCareCalculations ccc = new ChildCareCalculations(dc);
            ChildCalc cc = ccc.GetChildCalcData();
            string p_income = ccc.GetIncomeP();
            string d_income = ccc.GetIncomeD();
            string babysit = ccc.GetBabysit();
            string healthcover = ccc.GetHealthCover();
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (System.IO.File.Exists(folder + "\\Child_Support_Worksheet.docx"))
            {
                System.IO.File.Delete(folder + "\\Child_Support_Worksheet.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Child_Support_Worksheet.docx"))
            {
                Paragraph p0 = document.InsertParagraph();
                Paragraph p1 = document.InsertParagraph();
                Paragraph p2 = document.InsertParagraph();
                Paragraph p3 = document.InsertParagraph();
                Paragraph p4 = document.InsertParagraph();
                Paragraph p5 = document.InsertParagraph();
                Paragraph p6 = document.InsertParagraph();
                Paragraph p7 = document.InsertParagraph();
                Paragraph p8 = document.InsertParagraph();
                Paragraph p9 = document.InsertParagraph();
                Paragraph p10 = document.InsertParagraph();
                Paragraph p11 = document.InsertParagraph();
                Paragraph p12 = document.InsertParagraph();
                Paragraph p13 = document.InsertParagraph();
                Paragraph p14 = document.InsertParagraph();
                Paragraph p15 = document.InsertParagraph();
                Paragraph p16 = document.InsertParagraph();
                Paragraph p17 = document.InsertParagraph();
                Paragraph p18 = document.InsertParagraph();
                Paragraph p19 = document.InsertParagraph();
                Paragraph p20 = document.InsertParagraph();
                Paragraph p21 = document.InsertParagraph();
                Paragraph p22 = document.InsertParagraph();
                Paragraph p23 = document.InsertParagraph();
                Paragraph p24 = document.InsertParagraph();
                Paragraph p25 = document.InsertParagraph();
                Paragraph p26 = document.InsertParagraph();
                Paragraph p27 = document.InsertParagraph();
                Paragraph p28 = document.InsertParagraph();
                Paragraph p29 = document.InsertParagraph();
                Paragraph p30 = document.InsertParagraph();
                Paragraph p31 = document.InsertParagraph();
                Paragraph p32 = document.InsertParagraph();
                Paragraph p33 = document.InsertParagraph();
                Paragraph p34 = document.InsertParagraph();
                Paragraph p35 = document.InsertParagraph();
                Paragraph p36 = document.InsertParagraph();
                Paragraph p37 = document.InsertParagraph();
                Paragraph p38 = document.InsertParagraph();
                Paragraph p39 = document.InsertParagraph();
                Paragraph p40 = document.InsertParagraph();
                p1.Append("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p1.AppendLine("COUNTY OF " + county_venue.ToUpperCase()).MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p1.AppendLine(pname).MyStyle();
                p1.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p1.AppendLine("\t\t\t\t\t\tANNUAL INCOME WORKSHEET").MyStyleB();
                p1.AppendLine("\t\t-against-").MyStyle();
                p1.Append("\t\t\t\t          Form UD-8(1) Eff. 1/25/16\n").MyStyleB();
                p1.AppendLine(dname).MyStyle();
                p1.AppendLine("\n\t\t\tDefendant,").MyStyle();
                p1.AppendLine("---------------------------------------------------------------------X\n").MyStyle();
                p1.AppendLine("\nThis Worksheet is submitted by Plaintiff.").MyStyleB();
                p2.AppendLine("To assist you in making the calculations on this Worksheet, you may use the Maintenance/Child Support Calculators posted on the Court's Divorce Resources website at ").MyStyleB();
                Hyperlink h1 = document.AddHyperlink("http://www.nycourts.gov/divorce/MaintenanceChildSupportTools.shtml", new Uri("http://www.nycourts.gov/divorce/MaintenanceChildSupportTools.shtml"));
                p2.AppendHyperlink(h1).Font("Times New Roman").MyStyleBU();
                p2.Append(".").MyStyleB();
                p2.Alignment = Alignment.both;
                p3.Append("They are provided for your convenience as a tool.  They have been tested with many scenarios to assure accuracy with appropriate entry of data. You may wish to make the calculations yourself on the Appendices to this Worksheet.  Neither this Worksheet nor the Calculators are meant to predict what the Court will order as to maintenance or child support in your case. Comments and questions about this Worksheet or the Calculators should be sent to ").MyStyleB();
                Hyperlink h2 = document.AddHyperlink("NYMatCalc@nycourts.gov", new Uri("mailto:NYMatCalc@nycourts.gov"));
                p3.AppendHyperlink(h2).Font("Times New Roman").MyStyleBU();
                p3.Append(".").MyStyleB();
                p3.Alignment = Alignment.both;
                p4.AppendLine("Complete Income Computations for Plaintiff and Defendant:").MyStyleB();
                p4.AppendLine("\nENTER INCOME OF PARTIES -- CHECK THE BOX TO INDICATE HOW YOU MADE THE CALCULATION:").MyStyleB();
                p4.AppendLine("\n\t[  ] Use the Maintenance/Child Support Calculators posted at:").MyStyle();
                p4.AppendLine("\t").MyStyle();
                Hyperlink h3 = document.AddHyperlink("http://www.nycourts.gov/divorce/MaintenanceChildSupportTools.shtml", new Uri("http://www.nycourts.gov/divorce/MaintenanceChildSupportTools.shtml"));
                p4.AppendHyperlink(h3).Font("Times New Roman").MyStyleU();
                p4.Append(".").MyStyle();
                p4.AppendLine("\n\tOR\n").MyStyle();
                p4.AppendLine("\t[ X ] Use ").MyStyle();
                p4.Append("Appendix A").MyStyleB();
                p4.Append(" to make the calculation").MyStyle();
                p4.AppendLine("\n\t\tA. Enter Plaintiff's Annual Income from ").MyStyle();
                p4.Append("Line 18 of Appendix A").MyStyleB();
                p4.Append(" or ").MyStyle();
                p4.Append("Line 18\n\t\tof Part A of the Calculator").MyStyleB();
                p4.Append(" . . . . . . . . . . . . . . . . . . . . . . . $" + ccc.GetAdjustedGrossP()).MyStyle();
                p4.AppendLine("\n\t\tB. Enter Defendant's Annual Income from ").MyStyle();
                p4.Append("Line 18 of Appendix A").MyStyleB();
                p4.Append(" or ").MyStyle();
                p4.Append("Line 18\n\t\tof Part A of the Calculator").MyStyleB();
                p4.Append(" . . . . . . . . . . . . . . . . . . . . . . . $" + ccc.GetAdjustedGrossD()).MyStyle();
                p4.AppendLine("\n\t\tNOTE: If you do not know your spouses income write \"unknown.\"").MyStyle();
                p5.InsertPageBreakBeforeSelf();
                p5.Append("I have carefully read this statement and attest that it is true and accurate to the best of my knowledge.").MyStyle();
                p5.AppendLine("\n\n\n\n\t\t\t\t\t\t_______________________________").MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                p5.AppendLine("\n\nSubscribed and sworn to before me").MyStyle();
                p5.AppendLine("on").MyStyle();
                p5.AppendLine("\n_________________________________").MyStyle();
                p5.AppendLine("\t\tNotary Public").MyStyle();
                p5.AppendLine("My commission expires on ").MyStyle();
                p5.AppendLine("\n\n\n\t\t\t\t\t\t_______________________________").MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                p5.AppendLine("\t\t\t\t\t\tPlaintiff Pro-se").MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + ld.street + ",").MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + ld.city + ", " + ld.state.ToStateName() + " - " + ld.zip).MyStyle();
                p5.AppendLine("\t\t\t\t\t\t" + ld.telephone).MyStyle();
                p5.AppendLine("\n\n(Form UD-8(1) Eff. 1/25/16)").MyStyle();
                p6.InsertPageBreakBeforeSelf();
                p6.Append("APPENDIX A").Font("Times New Roman").MyStyleBU();
                p6.AppendLine("Annual Income Worksheet").MyStyleB();
                p6.Alignment = Alignment.center;
                p7.Append("\nI. GROSS INCOME").MyStyleB();
                p7.Append(" (Annual Figures Only)").MyStyle();
                p7.AppendLine("\t\t\t\t\t\t\t\t\t      Plaintiff\tDefendant").MyStyle();
                p7.AppendLine("1. Gross (total) income (as should have been or should be\nreported in most recent federal income tax return) . . . . . . . . . . . . . . . .  $" + p_income + "\t$" + d_income).MyStyle();
                p7.AppendLine("2. Investment income (not already included in item 1)\nreduced by amount expended in connection with the investments. . . .").MyStyle();
                p7.AppendLine("3. Income or compensation from the following sources (not already\nincluded in items 1 or 2). . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .").MyStyle();
                p7.AppendLine("\ta. deferred compensation . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ").MyStyle();
                p7.AppendLine("\tb. worker's compensation . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ").MyStyle();
                p7.AppendLine("\tc. disability benefits . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ").MyStyle();
                p7.AppendLine("\td. unemployment insurance benefits. . . . . . . . . . . . . . . . . . . . . ").MyStyle();
                p7.AppendLine("\te. social security benefits . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ").MyStyle();
                p7.AppendLine("\tf. veterans benefits . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ").MyStyle();
                p7.AppendLine("\tg. pensions and retirement benefits . . . . . . . . . . . . . . . . . . . . . ").MyStyle();
                p7.AppendLine("\th. fellowships and stipends . . . . . . . . . . . . . . . . . . . . . . . . . . . . ").MyStyle();
                p7.AppendLine("\ti. annuity payments . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ").MyStyle();
                p7.AppendLine("4. Former income or resources voluntarily reduced . . . . . . . . . . . . . . . ").MyStyle();
                p7.AppendLine("5. Self-employment deductions (not already included in items 1 or 2) . ").MyStyle();
                p7.AppendLine("\ta. depreciation deduction in excess of straight-line . . . . . . . . . .").MyStyle();
                p7.AppendLine("b. entertainment and travel allowances if they reduce personal\n\texpenditures . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ").MyStyle();
                p7.AppendLine("6. Other Income not already listed above (including but not limited to:").MyStyle();
                p7.AppendLine("income from non-income producing assets; employment \"perks\" and reimbursed expenses to the extent that they reduce personal expenses; fringe benefits as a result of employment; money, goods and services provided by friends and relatives) . . . . . . . . . . . . . . . . . . . ").MyStyle();
                p7.AppendLine("7. Income from Income Producing Property distributed or to be distributed pursuant to a final\njudgment of divorce . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .").MyStyle();
                p7.AppendLine("8. ").MyStyle();
                p7.Append("GROSS ANNUAL INCOME").MyStyleB();
                p7.Append(" (Add Lines 1-7) . . . . . . . . . . . . . . .  $" + p_income + "\t$" + d_income).MyStyle();
                p7.AppendLine("\nII.  DEDUCTIONS").MyStyleB();
                p7.Append(" (Annual Figures Only)").MyStyle();
                p7.AppendLine("\t\t\t\t\t\t\t\t\t      Plaintiff\tDefendant").MyStyle();
                p7.AppendLine("9. Unreimbursed employee business expenses (except to extent\nexpenses reduce personal expenditures). . . . . . . . . . . . . . . . . . . . . . . .").MyStyle();
                p7.AppendLine("10. Alimony or maintenance actually paid to non-party spouse pursuant\nto court order or agreement. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .").MyStyle();
                p7.AppendLine("11. Child support actually paid pursuant to court order or agreement\nfor non - party child. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .").MyStyle();
                p7.AppendLine("12. Public assistance . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . \nNote: enter zero unless included in Gross Income").MyStyle();
                p7.AppendLine("13. Supplemental social security Income . . . . . . . . . . . . . . . . . . . . . . . \nNote: enter zero unless included in Gross Income").MyStyle();
                p7.AppendLine("14. N.Y.C. or Yonkers taxes . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .  $" + ccc.GetNYCP() + "\t$" + ccc.GetNYCD()).MyStyle();
                p7.AppendLine("15. Federal Insurance Contributions Act (FICA) Social Security taxes..$" + ccc.GetFICASP() + "\t$" + ccc.GetFICASD()).MyStyle();
                p7.AppendLine("16. Federal Insurance Contributions Act (FICA) Medicare taxes . . . . . $" + ccc.GetFICAMP() + "\t$" + ccc.GetFICAMD()).MyStyle();
                p7.AppendLine("17. ").MyStyle();
                p7.Append("TOTAL ANNUAL DEDUCTIONS").MyStyleB();
                p7.Append(" (Add Lines 8-15) . . . . . . . . . $" + ccc.GetDeductionP() + "\t$" + ccc.GetDeductionD()).MyStyle();
                p7.AppendLine("\nIII. NET INCOME").MyStyleB();
                p7.AppendLine("\n18. ").MyStyle();
                p7.Append("GROSS ANNUAL INCOME").MyStyleB();
                p7.Append(" (Subtract Line 17 from Line 8 and\ninsert on Lines 1A and 1B of the Worksheet). . . . . . . . . . . . . . . . . . . . .  $" + ccc.GetAdjustedGrossP() + "\t$" + ccc.GetAdjustedGrossD()).MyStyle();
                p7.AppendLine("\n\n[APPENDIX A -- Effective 1/25/16, Chapter 269, Laws of New York 2015]").MyStyle();
                p8.InsertPageBreakBeforeSelf();
                p8.Append("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p8.AppendLine("COUNTY OF " + county_venue.ToUpperCase()).MyStyle();
                p8.AppendLine("---------------------------------------------------------------------X       Index No: " + dc.index_no).MyStyle();
                p8.AppendLine(pname).MyStyle();
                p8.AppendLine();
                p8.AppendLine("                           Plaintiff,").MyStyle();
                p8.AppendLine("\t\t\t\t\t\tCHILD SUPPORT WORKSHEET").MyStyleB();
                p8.AppendLine("                  -against-").MyStyle();
                p8.Append("\t\t\t\t       (Form UD-8(3)) Eff. 1/25/16").MyStyleB();
                p8.AppendLine();
                p8.AppendLine(dname).MyStyle();
                p8.AppendLine();
                p8.AppendLine("                           Defendant,").MyStyle();
                p8.AppendLine("---------------------------------------------------------------------X\n").MyStyle();
                p9.AppendLine("To assist you in making the calculations on this Worksheet, you may use the Maintenance/Child Support Calculators posted on the Court's Divorce Resources website at ").MyStyleB();
                Hyperlink h4 = document.AddHyperlink("http://www.nycourts.gov/divorce/MaintenanceChildSupportTools.shtml", new Uri("http://www.nycourts.gov/divorce/MaintenanceChildSupportTools.shtml"));
                p9.AppendHyperlink(h4).Font("Times New Roman").MyStyleBU();
                p9.Append(".").MyStyleB();
                p9.Alignment = Alignment.both;
                p10.Append("They are provided for your convenience as a tool.  They have been tested with many scenarios to assure accuracy with appropriate entry of data. You may wish to make the calculations yourself on the Appendices to this Worksheet.  Neither this Worksheet nor the Calculators are meant to predict what the Court will order as to maintenance or child support in your case. Comments and questions about this Worksheet or the Calculators should be sent to ").MyStyleB();
                Hyperlink h5 = document.AddHyperlink("NYMatCalc@nycourts.gov", new Uri("mailto:NYMatCalc@nycourts.gov"));
                p10.AppendHyperlink(h5).Font("Times New Roman").MyStyleBU();
                p10.Append(".").MyStyleB();
                p10.Alignment = Alignment.both;
                p11.AppendLine("1. This Worksheet was prepared by the Plaintiff.").MyStyle();
                p11.AppendLine("\n2. If you and your spouse have entered into a written agreement about child support check the box below:").MyStyle();
                p11.AppendLine("\n\t[   ] Plaintiff and Defendant have entered into a written agreement about Child\n\tSupport.").MyStyle();
                p11.AppendLine("\n3. If you and your spouse have entered into a written agreement about child support, submit a copy of the agreement to the Court along with the completed Worksheet and check the box below:").MyStyle();
                p11.AppendLine("\n\t[   ] A copy of the written agreement about child support was submitted to the Court\n\twith this Worksheet.").MyStyle();
                p11.AppendLine("\n4. [ X ] If I am not represented by an attorney, I have received a copy of the Child Support Standards Act Chart.").MyStyle();
                p11.AppendLine("\n5. ").MyStyle();
                p11.Append("CALCULATE BASIC ANNUAL CHILD SUPPORT OBLIGATION").MyStyleB();
                p11.AppendLine("\nIf there are unemancipated children of the marriage, calculate the amount of child support that must be paid to the custodial parent by the non-custodial parent.").MyStyle();
                p11.AppendLine("\nA.").MyStyleB();
                p11.Append(" Check the box to indicate how you made the calculation:  ").MyStyle();
                p11.AppendLine("\n[   ] Use the Maintenance/Child Support Calculators posted at the link above and enter the amount from Part C-IV, Line 1 of the Calculator in ").MyStyle();
                p11.Append("Line 5B").MyStyleB();
                p11.Append(" below.").MyStyle();
                p11.AppendLine("\nOR\n").MyStyle();
                p11.AppendLine("[ X ] Use ").MyStyle();
                p11.Append("Appendix G").MyStyleB();
                p11.Append(" to make the calculation and enter the amount from\n").MyStyle();
                p11.Append("Section IV Line 1 of Appendix G").MyStyleB();
                p11.Append(" in ").MyStyle();
                p11.Append("Line 5B").MyStyleB();
                p11.Append(" below.").MyStyle();
                p11.AppendLine("\nB.").MyStyleB();
                p11.Append(" The Annual Basic Child Support Obligation . . . . . . . . . . . . . . $" + ccc.GetNCPChildSupportWithAddOn()).MyStyle();
                p11.AppendLine("\n6. If you believe the Annual Basic Child Support Obligation is unjust and should be changed, list the factors you would like the Court to consider in its decision after reviewing the ").MyStyle();
                p11.Append("10 child support adjustment factors").MyStyleB();
                p11.Append(" in Appendix F.*").MyStyle();
                p11.AppendLine("\n\n\n\n\n\n7. If you would like the Court to award child support on Combined Parental Income in excess of $" + cc.upper_slab + ", please list the factors you would like the Court to consider in its decision, after reviewing the 10 child support adjustment factors in Appendix F.**").MyStyle();
                p11.AppendLine("\n\n\n\n\n8. I have carefully read this statement and attest that it is true and accurate to the best of my knowledge.").MyStyle();
                p11.AppendLine("\n\n\n\n\t\t\t\t\t\t_______________________________").MyStyle();
                p11.AppendLine("\t\t\t\t\t\t" + pname).MyStyle();
                p11.AppendLine("\n\nSubscribed and sworn to before me").MyStyle();
                p11.AppendLine("on").MyStyle();
                p11.AppendLine("\n_________________________________").MyStyle();
                p11.AppendLine("\t\tNotary Public").MyStyle();
                p11.AppendLine("My commission expires on ").MyStyle();
                p12.InsertPageBreakBeforeSelf();
                p12.Append("*  If a party believes that NCP's Annual Child Support Obligation is unjust or inappropriate, the party can ask the Court to order the NCP to pay an adjusted amount after considering ").MyStyle();
                p12.Append("the 10 child support adjustment factors.  The 10 child support adjustment factors pursuant to DRL Section 240(1-b)(f) are listed on Appendix F.").MyStyleB();
                p12.Alignment = Alignment.both;
                p13.AppendLine("**  If the Combined Parental Income exceeds $" + cc.upper_slab + ", the Court may award an additional amount of child support.  In making such decision, the Court will consider the 10 child support adjustment factors and/or the child support percentages as shown for information only on Appendix G, Section I, lines 9-9c and on Part C, line 8 of the Calculators").MyStyle();
                p13.Alignment = Alignment.both;
                p14.AppendLine("SEE APPENDICES F AND G ATTACHED").MyStyle();
                p14.AppendLine("\nAPPENDIX F.").MyStyle();
                p14.AppendLine("10 Child Support Adjustment Factors Where Income Exceeds $" + cc.upper_slab + " or When Considering Adjustment of Award (see DRL 240(1-b)(f))").MyStyle();
                p14.AppendLine("\nAPPENDIX G.").MyStyle();
                p14.AppendLine("Calculation of Annual Basic Child Support Obligation").MyStyle();
                p15.InsertPageBreakBeforeSelf();
                p15.AppendLine("APPENDIX F").Font("Times New Roman").MyStyleBU();
                p15.AppendLine("CHILD SUPPORT WORKSHEET").MyStyle();
                p16.AppendLine("10  CHILD SUPPORT ADJUSTMENT FACTORS PURSUANT TO DRL Section 240(B-1)(F) FOR ADJUSTMENT OF AWARD OR WHERE COMBINED PARENTAL INCOME EXCEEDS $" + cc.upper_slab + ":").MyStyleB();
                p16.Alignment = Alignment.both;
                p17.AppendLine("\n1. The financial resources of the custodial and non-custodial parent, and those of the child;").MyStyle();
                p17.AppendLine("\n2. The physical and emotional health of the child and his/her special needs and aptitudes;").MyStyle();
                p17.AppendLine("\n3. The standard of living the child would have enjoyed had the marriage or household not been dissolved;").MyStyle();
                p17.AppendLine("\n4. The tax consequences to the parties;").MyStyle();
                p17.AppendLine("\n5. The non-monetary contributions that the parents will make toward the care and well-being of the child;").MyStyle();
                p17.AppendLine("\n6. The educational needs of either parent;").MyStyle();
                p17.AppendLine("\n7.  A determination that the gross income of one parent is substantially less than the other parent's gross income;").MyStyle();
                p18.AppendLine("8. The needs of the children of the non-custodial parent for whom the non-custodial parent is providing support who are not subject to the instant action and whose support has not been deducted from income pursuant to subclause (D) of clause (vii) of subparagraph five of paragraph (b) of this subdivision, and the financial resources of any person obligated to support such children, provided, however, that this factor may apply only if the resources available to support such children are less than the resources available to support the children who are subject to the instant action;").MyStyle();
                p18.Alignment = Alignment.both;
                p19.AppendLine("9. Provided that the child is not on public assistance (i) extraordinary expenses incurred by the non-custodial parent in exercising visitation, or (ii) expenses incurred by the non-custodial parent in extended visitation provided that the custodial parent's expenses are substantially reduced as a result thereof; and").MyStyle();
                p19.Alignment = Alignment.both;
                p20.AppendLine("10. Any other factors the Court determines are relevant in each case, the Court shall order the non-custodial parent to pay his or her pro rata share of the basic child support obligation, and may order the non-custodial parent to pay an amount pursuant to paragraph (e) of this subdivision.").MyStyle();
                p20.Alignment = Alignment.both;
                p21.InsertPageBreakBeforeSelf();
                p21.Append("APPENDIX G").Font("Times New Roman").MyStyleBU();
                p21.AppendLine("COMBINED WORKSHEET FOR POST-DIVORCE MAINTENANCE GUIDELINES AND, IF APPLICABLE, CHILD SUPPORT STANDARDS ACT").MyStyle();
                p21.AppendLine("\nCALCULATION OF ANNUAL BASIC CHILD SUPPORT OBLIGATION").MyStyle();
                p21.AppendLine("\nI. ADJUST FOR MAINTENANCE AND COMPUTE BASIC CHILD SUPPORT BEFORE LOW INCOME ADJUSTMENT OR ADD-ONS").MyStyle();
                p21.AppendLine("\n\t1. Enter the amount of the guideline award of maintenance on Income of Maintenance \n\tPayor up to $178, 000 from Line 3B on page 2 of the Worksheet. . . . . . .  $0").MyStyle();
                p21.AppendLine("\n\t2.  Maintenance Payee's Net Annual Income Adjusted for Maintenance\n\t(Line 1 above plus Line 2B from p.1 of Worksheet). . . . . . . . . . . . . . . . .  $" + ccc.GetAdjustedGrossPayee()).MyStyle();
                p21.AppendLine("\n\t3. Maintenance Payor's Net Annual Income Adjusted for Maintenance\n\t(Line 2A from p.1 of Worksheet minus Line1 above). . . . . . . . . . . . . . . .  $" + ccc.GetAdjustedGrossPayor()).MyStyle();
                p21.AppendLine("\n\t4. Combined Parental Income Adjusted for Maintenance\n\t(Total 2 plus 3). . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .  $" + ccc.GetCombinedParentalIncome()).MyStyle();
                p21.AppendLine("\n\t5. Determine whether the Non-Custodial parent (NCP) is the\n\tMaintenance Payor or the Maintenance Payee and enter the Income\n\tof the NCP from Line 2 or 3, whichever applies. . . . . . . . . . . . . . . . . . . .  $" + ccc.GetAdjustedGrossPayor()).MyStyle();
                p21.AppendLine("\tALSO ENTER THIS AMOUNT IN Section II, Line 1").MyStyle();
                p21.AppendLine("\n\t5a. Enter the NCP's Percentage Share of Combined Parental Income . . . . " + ccc.GetNCPPercent() + "%").MyStyle();
                p21.AppendLine("\tNote: Divide Line 5 by Line 4").MyStyle();
                p21.AppendLine("\tNote: The percentage share is sometimes referred to as the \"pro rata share.\"").MyStyle();
                p21.AppendLine("\tYou will use this same percentage for the NCP's share of Mandatory \n\tAdd-on Expenses in Section III below.").MyStyle();
                p21.AppendLine("\n\t5b. Enter the CP's Percentage Share of Combined Parental Income . . . . . " + ccc.GetCPPercent() + "%").MyStyle();
                p21.AppendLine("\tNote:  Divide Custodial Parent (CP)'s Income (from Line 2 or Line\n\t3, whichever applies), by Line 4").MyStyle();
                p21.AppendLine("\n\tNote:  The percentage share is sometimes referred to as the \"pro rata\n\tshare\". You will use this same percentage for the CP's share of\n\tMandatory Health insurance Expenses in Section III below.").MyStyle();
                p21.AppendLine("\n\t6. Enter the percentage that applies based on the number of children . . . . . " + ccc.GetApplicablePercent() + "%").MyStyle();
                p21.AppendLine("\t1 child = " + cc.one_child + "%; 2 children = " + cc.two_child + "%; 3 children = " + cc.three_child + "%; 4 children = " + cc.four_child + "%; \n\t5 children = " + cc.five_child + "% (minimum)").MyStyle();
                p21.AppendLine("\n\t7. Multiply the percentage in Line 6 by Combined Parental Income from\n\tLine 4, but only up to $" + cc.upper_slab + " of Combined Parental Income. . . . . . . . .  $" + ccc.GetCombinedChildSupport()).MyStyle();
                p21.AppendLine("\n\tThis is the Combined Child Support on Combined Income up to $" + cc.upper_slab).MyStyleB();
                p21.AppendLine("\tExample:  If Combined Parental Income in Line 4 is $150,000, and if there are\n\t2 children, multiply $" + cc.upper_slab + " by " + cc.two_child + "% to yield $" + ccc.GetExampleCalc()).MyStyle();
                p21.AppendLine("\n\t8. Multiply amount in Line 7 by percentage in Line 5a . . . . . . . . . . . . . . . $" + ccc.GetNCPUnadjustedChildSupport()).MyStyle();
                p21.AppendLine("\n\tThis is the NCP's Annual Percentage Share of Child Support on Combined\n\tParental Income up to and including $" + cc.upper_slab + ".").MyStyleB();
                p21.AppendLine("\nALSO ENTER THIS AMOUNT IN SECTION II, Line 2.").MyStyle();
                p22.AppendLine("NCP'S ANNUAL BASIC PAYMENT will be the total of Line 8 plus any possible increase at the Court's discretion after consideration of the 10 child support adjustment factors and/or the child support percentage for child support on combined parental income in excess of $" + cc.upper_slab + ", if any.  This is the amount the NCP must pay to the CP for all of the children's costs and expenses, before possible low income adjustment (See Section II), Add On Expenses (see Section III), and possible adjustment at the Court's discretion if the Court finds such amount to be unjust and inappropriate based on consideration of the 10 child support adjustment factors (See Appendix F).").MyStyle();
                p22.Alignment = Alignment.both;
                p23.AppendLine("------------------------------------------------------------------").MyStyle();
                p23.Alignment = Alignment.center;
                p24.AppendLine("\tLines 9-9c below are for information only and are not to be included in the totals in this worksheet.").MyStyle();
                p24.AppendLine("\n\t\t9. Compute Child Support on Combined Parental Income Above $" + cc.upper_slab + ", if\n\t\tany. If there is none, skip to Section II below.").MyStyle();
                p24.AppendLine("\n\t\t9a. If there is Combined Parental Income above $" + cc.upper_slab + ", enter the amount\n\t\tof such Income you asking the Court to use for child support  . .  $" + ccc.GetExcessAmount()).MyStyle();
                p24.AppendLine("\n\t\t9b. Multiply amount in Line 9a by percentage in Line 6").MyStyle();
                p24.AppendLine("\t\tThis is Combined Child Support on Income above $" + cc.upper_slab + " you are asking\n\t\tthe Court to consider for Child Support . . . . . . . . . . . . . . . . . . .    $" + ccc.GetExcessChildSupport()).MyStyle();
                p24.AppendLine("\n\t\t9c. Multiply Line 9b by the percentage in Line 5a").MyStyle();
                p24.AppendLine("\t\tThis is the NCP's Annual Percentage Share of Income Above $" + cc.upper_slab + " that\n\t\tyou are asking the Court to consider for Child Support . . . . . . . .  $" + ccc.GetNCPExcessChildSupport()).MyStyle();
                p24.AppendLine("\nII. DETERMINE WHETHER LOW INCOME EXEMPTION APPLIES").MyStyle();
                p24.AppendLine("\n\t1. NCP's Annual Income(Line 5 of Section I)  . . . . . . . . . . . . . . . . . . . . . .  $" + ccc.GetAdjustedGrossPayor()).MyStyle();
                p24.AppendLine("\n\t2. Basic Child Support Obligation (Line 8 of Section I) . . . . . . . . . . . . . .  $" + ccc.GetNCPUnadjustedChildSupport()).MyStyle();
                p24.AppendLine("\n\t3. Subtract Line 2 from Line 1  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .  $" + ccc.GetNCPLowIncome()).MyStyle();
                p24.AppendLine("\n\tThis is the NCP's Annual Income after the Basic Child Support\n\tObligation. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .              \t$" + ccc.GetNCPLowIncome()).MyStyle();
                p24.AppendLine("\n\t\tIf Line 3 is less than the Self-Support Reserve (SSR) of $" + cc.lower_slab + ", there will be\n\t\ta low income adjustment.").MyStyle();
                p24.AppendLine("\n\t\tIf Line 3 is less than the SSR of $" + cc.lower_slab + " but greater than $" + cc.lowest_slab + " (poverty\n\t\tlevel),child support shall be the greater of $600 or the difference between\n\t\tNCP Income and the SSR of $" + cc.lower_slab + ". Proceed to Line 4a to compute the \n\t\tdifference. Enter the greater of $600 or the difference in Line 4b. (Note: Add-\n\t\ton expenses may apply in the Court's discretion).").MyStyle();
                p24.AppendLine("\n\t\tIf Line 3 is equal to or greater than the Self-Support Reserve (SSR)\n\t\tof $" + cc.lower_slab + ", there will be no low income adjustment. Skip the rest of this\n\t\tsection and proceed to Section III below.").MyStyle();
                p24.AppendLine("\n\t\tIf Line 3 is less than $" + cc.lowest_slab + "(poverty level), the Basic Child Support shall be\n\t\t$300.[However, if the Court finds such amount to be unjust and inappropriate,\n\t\tbased on the factors in DRL Section 240 (1-b)(f), the Court can order the NCP\n\t\tto pay less than $300 per year.]").MyStyle();
                p24.AppendLine("\n\t\tEnter $300 in Line 4b below. Add on Expenses will not apply.").MyStyle();
                p24.AppendLine("\n\t4a. NCP Income minus SSR: Subtract $" + cc.lower_slab + " from amount in Line 1  . .  " + ccc.GetNCPIncomeMinusSSR()).MyStyle();
                p24.AppendLine("\n\t4b. Enter the Basic Child Support Obligation with Low Income \n\tExemption if applicable. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .  $" + ccc.GetNCPAdjustedChildSupport()).MyStyle();
                p24.AppendLine("\n\tIn Line 4b, enter $300 if Line 3 is less than $" + cc.lowest_slab).MyStyle();
                p24.AppendLine("\n\tALSO ENTER THIS AMOUNT ON LINE 5B at page 2 of the Worksheet.").MyStyleB();
                p24.AppendLine("\n\tSkip Section III.").MyStyle();
                p24.AppendLine("\n\tOR\n").MyStyleB();
                p24.AppendLine("\tIn Line 4b, enter the greater of $600 and Line 4a, if Line 3 is greater than $" + cc.lowest_slab + "\n\tbut less than $" + cc.lower_slab + ". Then proceed to Section III.").MyStyle();
                p24.AppendLine("\n\tOR\n").MyStyleB();
                p24.AppendLine("\tIn Line 4b, enter amount from Line 2 if Line 3 is equal to or greater than $" + cc.lowest_slab + ".\n\tThen Proceed to Section III.").MyStyle();
                p25.AppendLine("III. ADD-ON EXPENSES (SKIP THIS SECTION IF THE BASIC CHILD SUPPORT OBLIGATION WITH LOW INCOME EXEMPTION IS $300) IF LINE 3 of SECTION II IS LESS THAN THE SSR BUT GREATER THAN THE POVERTY LEVEL, THE COURT HAS DISCRETION WHETHER OR NOT TO AWARD THE MANDATORY ADD ON EXPENSES (see  DRL 240(1-b)(d)).").MyStyle();
                p25.Alignment = Alignment.both;
                p26.AppendLine("\n\tA. Mandatory Child Care Expenses").MyStyle();
                p26.AppendLine("\n\t\t1. Enter annual cost of child care (child care costs \n\t\tfrom custodial parent's working, or receiving elementary,\n\t\tsecondary or higher education or vocational training leading\n\t\tto employment). . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .  $" + ccc.GetBabysit()).MyStyle();
                p26.AppendLine("\n\t\t2. NCP's Percentage Share of Child Care Expenses\n\t\t(from Line 5a of Section I). . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .   " + ccc.GetNCPPercent()).MyStyle();
                p26.AppendLine("\n\t\t3. NCP's Dollar Share of Child Care Expenses (multiply\n\t\tLine 1 x line 2). . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .  $" + ccc.GetNCPBabysit() + "%").MyStyle();
                p26.AppendLine("\n\tB. Mandatory Health Expenses (health insurance premiums and future unreimbursed\n\thealth-related expenses)").MyStyle();
                p26.AppendLine("\n\t\t4a. NCP's % share of health insurance premiums and future\n\t\tunreimbursed health - related expense. . . . . . . . . . . . . . . . . . . . . . . .  " + ccc.GetNCPPercent() + "%").MyStyle();
                p26.AppendLine("\n\t\t4b. CP's % share of health insurance premiums and future\n\t\tunreimbursed health - related expense. . . . . . . . . . . . . . . . . . . . . . . .  " + ccc.GetCPPercent() + "%").MyStyle();
                p26.AppendLine("\n\t\t5. Annual cost of health insurance for the children . . . . . . . . . . . .  $" + ccc.GetHealthCover()).MyStyle();
                p26.AppendLine("\n\t\t6. Does the NCP provide the Health Insurance?    [ " + ccc.GetAnswerHealthYes() + " ] Yes   [ " + ccc.GetAnswerHealthNo() + " ] No").MyStyle();
                p26.AppendLine("\n\t\t6a. If No, NCP's dollar share of Health Insurance (added to\n\t\tthe Basic Child Support Obligation)(multiply Line 4a x line 5). .  $" + ccc.GetNCPHealth()).MyStyle();
                p26.AppendLine("\n\t\t6b. If yes, CP's dollar Share of Health Insurance (deducted\n\t\tfrom Basic Child Support Obligation)(multiply Line 4b x line 5).  $" + ccc.GetCPHealth()).MyStyle();
                p26.AppendLine("\n\t\t7. Health Care Adjustment (Add amount from Line 6a or\n\t\tsubtract amount from Line 6b, whichever applies). . . . . . . . . . . . " + ccc.GetHealthCareAdjustment()).MyStyle();
                p26.AppendLine("\n\t\t8. Total Mandatory Add-On Expenses (Total Lines 3 and 7) . . . .  $" + ccc.GetAddOn()).MyStyle();
                p26.AppendLine("\n\t\t9. For Information Only, (not to add to the totals in this Worksheet), enter the\n\t\ttotal Discretionary Expenses for Child Care and Education if you are asking\n\t\tthe Court to consider awarding them** . . . . . . . . . . . . . . . . . . . . .  $0").MyStyle();
                p27.AppendLine("Note:  In addition to Mandatory Add-On Expenses in A and B above, the Court may determine and apportion additional Discretionary Expenses for child care expenses, and additional Discretionary Expenses for education.").MyStyle();
                p27.Alignment = Alignment.both;
                p28.AppendLine("\nIV. BASIC ANNUAL CHILD SUPPORT OBLIGATION*").MyStyle();
                p28.AppendLine("\n\tLine 1:  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .  $" + ccc.GetNCPChildSupportWithAddOn()).MyStyle();
                p28.AppendLine("\n\tAdd Line 4b of Section II and Line 8 of Section III, BUT IF LINE 3 of SECTION II\n\tIS LESS THAN THE SSR BUT GREATER THAN THE POVERTY LEVEL, KEEP\n\tIN MIND THAT THE TOTAL MAY BE LOWER AFTER THE COURT DECIDES\n\tWHETHER TO AWARD THE ADD-ON EXPENSES.").MyStyle();
                p28.AppendLine("\n\tThis is the NCP's Annual Basic Payment Adjusted for Low Income.").MyStyle();
                p28.AppendLine("\n\tIf any, plus Add On Expenses and Health Insurance Adjustment, if applicable.").MyStyle();
                p28.AppendLine("\n\tENTER THIS AMOUNT ON LINE 5B of the Worksheet").MyStyle();
                p29.AppendLine("* Note:  Basic Annual Child Support Obligation will also include whatever the Court may order the NCP to pay in child support on combined parental income above $143,000, if any, after considering the 10 child support adjustment factors and/or the child support percentage.").MyStyle();
                p29.Alignment = Alignment.both;
                p30.AppendLine("\n\n(Form UD-8(3) Eff. 1/25/16)").MyStyle();
                document.Save();
            }
        }
        public static void Divorce2_SCUInfoSheet(DivorceCase dc)
        {
            foreach (var propertyInfo in dc.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(dc, null) == null)
                    {
                        propertyInfo.SetValue(dc, string.Empty, null);
                    }
                }
            }
            string county_venue = StringDecorators.GetCountyVenue(dc);
            string basis_venue = StringDecorators.GetBasisVenue(dc);
            string pname = dc.p.name.ToUpperCase();
            string dname = dc.d.name.ToUpperCase();
            LawOfficeDetail ld = GetLawOfficeDetails();
            ChildCareCalculations ccc = new ChildCareCalculations(dc);
            ChildCalc cc = ccc.GetChildCalcData();
            string p_income = ccc.GetIncomeP();
            string d_income = ccc.GetIncomeD();
            string babysit = ccc.GetBabysit();
            string healthcover = ccc.GetHealthCover();
            int[] e = Miscellaneous.GetEligibleChildren(dc.ch, 21);
            var folder = @"D:\home\GoNyLaw\Documents\Divorce\" + dc.case_id;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            if (System.IO.File.Exists(folder + "\\Support_Collection_Unit.docx"))
            {
                System.IO.File.Delete(folder + "\\Support_Collection_Unit.docx");
            }
            using (DocX document = DocX.Create(folder + "\\Support_Collection_Unit.docx"))
            {
                Paragraph p11 = null;
                Paragraph p12 = null;
                Paragraph p13 = null;
                p11 = document.InsertParagraph();
                p12 = document.InsertParagraph();
                p13 = document.InsertParagraph();
                p11.AppendLine("SUPREME COURT OF THE STATE OF NEW YORK").MyStyle();
                p11.AppendLine("COUNTY OF " + county_venue.ToUpperCase()).MyStyle();
                p11.AppendLine("--------------------------------------------------------X Index No." + dc.index_no).MyStyle();
                p11.AppendLine(pname + ",").MyStyle();
                p11.AppendLine("\n\t\t\tPlaintiff,").MyStyle();
                p11.AppendLine("\t\t\t\t\t\t\tSUPPORT COLLECTION UNIT").MyStyleB();
                p11.AppendLine("\t\tagainst,\t\t\t\tINFORMATION SHEET\n").MyStyleB();
                p11.AppendLine(dname + ",").MyStyle();
                p11.AppendLine("\n\t\t\tDefendant.").MyStyle();
                p11.AppendLine("--------------------------------------------------------X").MyStyle();
                p11.AppendLine("\n\nThe following information is required pursuant to Section 240(1) of the Domestic Relations Law.").MyStyle();
                p11.AppendLine("\nPLAINTIFF: " + pname.ToUpperCase()).MyStyle();
                p11.AppendLine("Address: " + dc.p.street + ", " + dc.p.city + ", " + dc.p.state + "-" + dc.p.zip).MyStyle();
                p11.AppendLine("Date of Birth: " + dc.p.dob).MyStyle();
                p11.AppendLine("Social Security Number: " + dc.p.ssn).MyStyle();
                p11.AppendLine("\nDEFENDANT: " + dname.ToUpperCase()).MyStyle();
                p11.AppendLine("Address: " + dc.d.street + ", " + dc.d.city + ", " + dc.d.state + "-" + dc.d.zip).MyStyle();
                p11.AppendLine("Date of Birth: " + dc.d.dob).MyStyle();
                p11.AppendLine("Social Security Number: " + dc.d.ssn).MyStyle();
                p11.AppendLine("\nDate of Marriage: " + dc.m.marriage_date.ToDateLong()).MyStyle();
                p11.AppendLine("Place of Marriage: " + dc.m.city.ToTitleCase() + ", " + dc.m.state.ToStateName()).MyStyle();
                if (dc.ch.c1_lives == "D")
                {
                    if (dc.d.sex == "F")
                        p11.AppendLine("The Wife is the custodial parent and is not receiving public assistance.").MyStyle();
                    else
                        p11.AppendLine("The Husband is the custodial parent and is not receiving public assistance.").MyStyle();
                }
                if (dc.ch.c1_lives == "P")
                {
                    if (dc.p.sex == "F")
                        p11.AppendLine("The Wife is the custodial parent and is not receiving public assistance.").MyStyle();
                    else
                        p11.AppendLine("The Husband is the custodial parent and is not receiving public assistance.").MyStyle();
                }
                p11.AppendLine("\nUNEMANCIPATED CHILDREN:").MyStyleB();
                p11.AppendLine("\n\t\t\t\tName  \t - \t  Date of Birth").MyStyle();
                if (e[1] == 1)
                    p11.AppendLine("\t\t" + dc.ch.c1_name.ToTitleCase() + "\t" + dc.ch.c1_dob.ToDateLong()).MyStyle();
                if (e[2] == 1)
                    p11.AppendLine("\t\t" + dc.ch.c2_name.ToTitleCase() + "\t" + dc.ch.c2_dob.ToDateLong()).MyStyle();
                if (e[3] == 1)
                    p11.AppendLine("\t\t" + dc.ch.c3_name.ToTitleCase() + "\t" + dc.ch.c3_dob.ToDateLong()).MyStyle();
                if (e[4] == 1)
                    p11.AppendLine("\t\t" + dc.ch.c4_name.ToTitleCase() + "\t" + dc.ch.c4_dob.ToDateLong()).MyStyle();
                if (e[5] == 1)
                    p11.AppendLine("\t\t" + dc.ch.c5_name.ToTitleCase() + "\t" + dc.ch.c5_dob.ToDateLong()).MyStyle();
                if (e[6] == 1)
                    p11.AppendLine("\t\t" + dc.ch.c6_name.ToTitleCase() + "\t" + dc.ch.c6_dob.ToDateLong()).MyStyle();
                p11.AppendLine("\nSUPPORT:").MyStyleB();
                p11.AppendLine("\nMaintenance:  None").MyStyle();
                p11.AppendLine("\nChild support: " + ccc.GetNCPMonthlyChildSupportWithAddOn().StringToWords() + " Dollars ($" + ccc.GetNCPMonthlyChildSupportWithAddOn() + ") per month on the first day of each month.").MyStyle();
                if (dc.ch.c1_lives == "D")
                    p11.AppendLine("\n\n\n\n\n\nChild support payments are to be made to Plaintiff through the Support Collection Unit for New York County.").MyStyle();
                if (dc.ch.c1_lives == "P")
                    p11.AppendLine("\n\n\n\n\n\nChild support payments are to be made to Defendant through the Support Collection Unit for New York County.").MyStyle();
                p11.AppendLine("\nTotal Support: ").MyStyle();
                p11.AppendLine("\n\nName and address of non-custodial parent's employer:").MyStyle();
                p11.AppendLine("\n\n\nDated:").MyStyle();
                p11.AppendLine("\n(Form UD-8a - 1/25/16)").MyStyle();
                document.Save();
            }
        }
    }
}