﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class RJIAddendumFix
    {
        public string case_id { get; set; }
        public string pfirstname { get; set; }
        public string plastname { get; set; }
        public string dfirstname { get; set; }
        public string dlastname { get; set; }
        public string c1firstname { get; set; }
        public string c1lastname { get; set; }
        public string c2firstname { get; set; }
        public string c2lastname { get; set; }
        public string c3firstname { get; set; }
        public string c3lastname { get; set; }
        public string c4firstname { get; set; }
        public string c4lastname { get; set; }
        public string c5firstname { get; set; }
        public string c5lastname { get; set; }
    }
}