﻿using GoNyLaw.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GoNyLaw.Helpers
{
    public static class Amounts
    {
        public static void UpdatePayment(string cid, string txnno, int amount)
        {
            if ((cid.Substring(0, 4) == "DIVN") || cid.Substring(0, 4) == "DIVC")
            {
                DivorceCase dc;
                using (CaseContext db = new CaseContext())
                {
                    dc = db.DivorceCases.Find(cid);
                    if (dc.t == null)
                    {
                        dc.t = new Transaction();
                    }
                    if (string.IsNullOrEmpty(dc.t.txn_id1))
                    {
                        dc.t.txn_id1 = txnno;
                        dc.t.amount1 = amount;
                        dc.t.date1 = DateTime.Now.ToString();
                        PaymentConfig p = new SettingsContext().PaymentConfig.Find(1);
                        using (UserContext db1 = new UserContext())
                        {
                            Admin a = db1.Admins.Find(dc.agency_id);
                            db1.Configuration.ValidateOnSaveEnabled = false;
                            db1.SaveChanges();                           
                        }
                    }
                    else
                    {
                        dc.t.txn_id2 = txnno;
                        dc.t.amount2 = amount;
                        dc.t.date2 = DateTime.Now.ToString();
                    }
                    db.SaveChanges();
                }                
            }
        }
        public static bool CheckFullyPaid(string cid)
        {
            if (cid == null || cid == "") return false;
            SettingsContext db = new SettingsContext();
            PaymentConfig p = db.PaymentConfig.Find(1);
            if ((cid.Substring(0, 4) == "DIVN") || cid.Substring(0, 4) == "DIVC")
            {
                UserContext adb = new UserContext();
                CaseContext db1 = new CaseContext();
                DivorceCase dc = db1.DivorceCases.Find(cid);
                Admin ag = adb.Admins.Find(dc.agency_id);
                if (dc.t == null) return false;
                if (cid.Substring(0, 4) == "DIVN" && (dc.t.amount1 + dc.t.amount2) >= p.divorce_no_kids_total)
                    return true;
                else if (cid.Substring(0, 4) == "DIVC" && (dc.t.amount1 + dc.t.amount2) >= p.divorce_with_kids_total)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public static bool CheckPartiallyPaid(string cid)
        {
            if (cid == null || cid == "") return false;
            SettingsContext db = new SettingsContext();
            PaymentConfig p = db.PaymentConfig.Find(1);
            if ((cid.Substring(0, 4) == "DIVN") || cid.Substring(0, 4) == "DIVC")
            {
                CaseContext db1 = new CaseContext();
                DivorceCase dc = db1.DivorceCases.Find(cid);
                if (object.ReferenceEquals(null, dc.t)) return false;
                if (cid.Substring(0, 4) == "DIVN" && dc.t.amount1 >= p.divorce_no_kids_first && dc.t.amount1 <= p.divorce_no_kids_total)
                    return true;
                else if (cid.Substring(0, 4) == "DIVC" && dc.t.amount1 >= p.divorce_with_kids_first && dc.t.amount1 <= p.divorce_with_kids_total)
                    return true;
                else
                    return false;
            }
            else if ((cid.Substring(0, 4) == "CORP") || cid.Substring(0, 4) == "CORN" || (cid.Substring(0, 4) == "CORL"))
            {
                return false;
            }
            return false;
        }
        public static bool CheckFullyPaid(DivorceCase dc, Admin ag, PaymentConfig p)
        {
                if (dc.t == null) return false;
                if (dc.casetype == "1" && (dc.t.amount1 + dc.t.amount2) >= p.divorce_no_kids_total)
                    return true;
                else if (dc.casetype == "2" && (dc.t.amount1 + dc.t.amount2) >= p.divorce_with_kids_total)
                    return true;
                else
                    return false;           
        }
        public static bool CheckPartiallyPaid(DivorceCase dc, Admin ag, PaymentConfig p)
        {
                if (object.ReferenceEquals(null, dc.t)) return false;
                if (dc.casetype == "1" && dc.t.amount1 >= p.divorce_no_kids_first && dc.t.amount1 <= p.divorce_no_kids_total)
                    return true;
                else if (dc.casetype == "2" && dc.t.amount1 >= p.divorce_with_kids_first && dc.t.amount1 <= p.divorce_with_kids_total)
                    return true;
                else
                    return false;
        }
    }
}