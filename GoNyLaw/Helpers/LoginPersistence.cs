﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoNyLaw.Models;

namespace GoNyLaw.Helpers
{
    public class LoginPersistence
    {
        public static void SetUser(string uid)
        {
            HttpCookie authCookie = new HttpCookie("webuser", uid);
            authCookie.Expires = DateTime.MinValue;
            HttpContext.Current.Response.Cookies.Add(authCookie);
        }
        public static void UnsetUser()
        {
            HttpCookie myCookie = new HttpCookie("webuser");
            myCookie.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(myCookie);
            OriginSense.Reset();
        }
        public static string GetUser()
        {
            return HttpContext.Current.Request.Cookies["webuser"].Value;
        }
        public static bool CheckUser()
        {
            if (HttpContext.Current.Request.Cookies["webuser"] != null)
            {
                return true;
            }
            return false;
        }
        public static int getuserid()
        {
            UserContext db = new UserContext();
            string emailid = GetUser();
            return db.Clients.Where(x => x.emailid == emailid).FirstOrDefault().id;
        }
        public static void SetAdmin(string uid, string name)
        {
            HttpCookie authCookie = new HttpCookie("webadmin", uid);
            authCookie.Expires = DateTime.MinValue;
            HttpContext.Current.Response.Cookies.Add(authCookie);
            HttpCookie authCookie1 = new HttpCookie("webadminname", name);
            authCookie1.Expires = DateTime.MinValue;
            HttpContext.Current.Response.Cookies.Add(authCookie1);
        }
        public static void UnsetAdmin()
        {
            HttpCookie myCookie = new HttpCookie("webadmin");
            myCookie.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(myCookie);
            HttpCookie myCookie1 = new HttpCookie("webadminname");
            myCookie1.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(myCookie1);
            OriginSense.Reset();
        }
        public static string GetAdmin()
        {
            return HttpContext.Current.Request.Cookies["webadmin"].Value;
        }
        public static bool CheckAdmin()
        {
            if (HttpContext.Current.Request.Cookies["webadmin"] != null)
            {
                return true;
            }
            return false;
        }
        public static int getadminid()
        {
            UserContext db = new UserContext();
            string emailid = GetAdmin();
            return db.Admins.Where(x => x.emailid == emailid).FirstOrDefault().id;
        }
        public static void SetSuperAdmin(string uid,string name)
        {
            HttpCookie authCookie = new HttpCookie("sa", uid);
            authCookie.Expires = DateTime.MinValue;
            HttpContext.Current.Response.Cookies.Add(authCookie);
            HttpCookie authCookie1 = new HttpCookie("saname", name);
            authCookie1.Expires = DateTime.MinValue;
            HttpContext.Current.Response.Cookies.Add(authCookie1);
        }
        public static void UnsetSuperAdmin()
        {
            HttpCookie myCookie = new HttpCookie("sa");
            myCookie.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(myCookie);
            HttpCookie myCookie1 = new HttpCookie("saname");
            myCookie1.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(myCookie1);
            OriginSense.Reset();
        }
        public static string GetSuperAdmin()
        {
            return HttpContext.Current.Request.Cookies["sa"].Value;
        }
        public static bool CheckSuperAdmin()
        {
            if (HttpContext.Current.Request.Cookies["sa"] != null)
            {
                return true;
            }
            return false;
        }
        public static int getsuperadminid()
        {
            UserContext db = new UserContext();
            string emailid = GetSuperAdmin();
            return db.Admins.Where(x => x.emailid == emailid).FirstOrDefault().id;
        }
    }
}