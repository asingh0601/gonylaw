﻿using GoNyLaw.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Xceed.Words.NET;

namespace GoNyLaw.Helpers
{
    public static class StringDecorators
    {
        public static string ToTitleCase(this string s)
        {
            if (string.IsNullOrEmpty(s)) return s;
            string[] words = s.Split(' ');
            String rest;
            int j;
            for (int i = 0; i < words.Length; i++)
            {
                rest = "";
                if (words[i].Length == 0) continue;
                for (j = 0; j < words[i].Length; j++)
                {
                    if (char.IsDigit(words[i][j]))
                    {
                        rest += words[i][j];
                        continue;
                    }
                    else
                    {
                        rest += char.ToUpper(words[i][j]);
                        break;
                    }
                }
                if (words[i].Length > 1)
                {
                    if (j != words[i].Length)
                        rest += words[i].Substring(j + 1).ToLower();
                }
                words[i] = rest;
            }
            return string.Join(" ", words);
        }
        public static string ToLowerCase(this string s)
        {
            if (string.IsNullOrEmpty(s)) return s;            
            return s.ToLower();
        }
        public static string ToUpperCase(this string s)
        {
            if (string.IsNullOrEmpty(s)) return s;
            return s.ToUpper();
        }
        public static string ToCourtAddress(this string c)
        {
            if (string.IsNullOrEmpty(c)) return "";
            return DropDownData.courtaddress.Where(m => m.Value == c)
                              .First()
                              .Text;
        }
        public static string ToCourtName(this string c)
        {
            if (string.IsNullOrEmpty(c)) return "";
            return DropDownData.court.Where(m => m.Value == c)
                              .First()
                              .Text;
        }
        public static string ToCountyName(this string c)
        {
            if (string.IsNullOrEmpty(c)) return "";
            return DropDownData.county.Where(m => m.Value == c)
                              .First()
                              .Text;
        }
        public static string ToBankruptcyCourtName(this string c)
        {
            if (c == null) return "";
            return DropDownData.bankruptcycourt.Where(m => m.Value == c)
                              .First()
                              .Text;
        }
        public static string ToBankruptcyCourtAddress(this string c)
        {
            if (c == null) return "";
            if(c == "1" || c == "2")
                return DropDownData.easterndistrictcourt.Where(m => m.Value == c)
                              .First()
                              .Text;
            else if (c == "3" || c == "4")
                return DropDownData.southerndistrictcourt.Where(m => m.Value == c)
                              .First()
                              .Text;
            return "";
        }
        public static string RemoveHTMLTags(this string s)
        {
            if (s == null) return s;
            string noHTML = Regex.Replace(s, @"<[^>]+>|&nbsp;", "").Trim();
            string noHTMLNormalised = Regex.Replace(noHTML, @"\s{2,}", " ");
            return noHTMLNormalised;
        }
        public static Paragraph MyStyle(this Paragraph p)
        {
            return p.Font("Times New Roman").FontSize(12);
        }
        public static Paragraph MyStyleB(this Paragraph p)
        {
            return p.Font("Times New Roman").FontSize(12).Bold();
        }
        public static Paragraph MyStyleU(this Paragraph p)
        {
            return p.Font("Times New Roman").FontSize(12).UnderlineStyle(UnderlineStyle.singleLine);
        }
        public static Paragraph MyStyleBU(this Paragraph p)
        {
            return p.Font("Times New Roman").FontSize(12).Bold().UnderlineStyle(UnderlineStyle.singleLine);
        }
        public static string Ordinal(this int num)
        {
            if (num.ToString().EndsWith("11")) return num.ToString() + "th";
            if (num.ToString().EndsWith("12")) return num.ToString() + "th";
            if (num.ToString().EndsWith("13")) return num.ToString() + "th";
            if (num.ToString().EndsWith("1")) return num.ToString() + "st";
            if (num.ToString().EndsWith("2")) return num.ToString() + "nd";
            if (num.ToString().EndsWith("3")) return num.ToString() + "rd";
            return num.ToString() + "th";
        }
        public static string ToDateLong(this string n)
        {
            DateTime dt;
            if (DateTime.TryParseExact(n, "MM/dd/yyyy", null, DateTimeStyles.None, out dt))
            {
                if (!dt.IsMinDate())
                    return dt.ToString("MMMM dd, yyyy");
            }
            return "";
        }
        public static string ToDateLong(this DateTime? dt)
        {
            if (!dt.HasValue) return "";
            DateTime t = dt.Value;
            if (t != null)
            {
                return t.ToString("MMMM dd, yyyy");
            }
            return "";
        }
        public static string ToDateWords(this string n)
        {
            int num;
            Int32.TryParse(n, out num);
            if (num == 1)
                return "First";
            if (num == 2)
                return "Second";
            if (num == 3)
                return "Third";
            if (num == 4)
                return "Fourth";
            if (num == 5)
                return "Fifth";
            if (num == 6)
                return "Sixth";
            if (num == 7)
                return "Seventh";
            if (num == 8)
                return "Eighth";
            if (num == 9)
                return "Nineth";
            if (num == 10)
                return "Tenth";
            if (num == 11)
                return "Eleventh";
            if (num == 12)
                return "Twelth";
            if (num == 13)
                return "Thirteenth";
            if (num == 14)
                return "Fourteenth";
            if (num == 15)
                return "Fifteenth";
            if (num == 16)
                return "Sixteenth";
            if (num == 17)
                return "Seventeenth";
            if (num == 18)
                return "Eighteenth";
            if (num == 19)
                return "Nineteenth";
            if (num == 20)
                return "Twentieth";
            if (num == 21)
                return "Twenty First";
            if (num == 22)
                return "Twenty Second";
            if (num == 23)
                return "Twenty Third";
            if (num == 24)
                return "Twenty Fourth";
            if (num == 25)
                return "Twenty Fifth";
            if (num == 26)
                return "Twenty Sixth";
            if (num == 27)
                return "Twenty Seventh";
            if (num == 28)
                return "Twenty Eighth";
            if (num == 29)
                return "Twenty Nineth";
            if (num == 30)
                return "Thirtieth";
            if (num == 31)
                return "Thirty First";
            return "";
        }
        public static string ToMonthWords(this int n)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(n);
        }
        public static string ToMonthWords(this string n)
        {
            int num;
            Int32.TryParse(n, out num);
            return num.ToMonthWords();
        }
        public static string GetDateNumber(this string d)
        {
            DateTime dt;
            if (DateTime.TryParseExact(d, "MM/dd/yyyy", null, DateTimeStyles.None, out dt))
            {
                return dt.ToString("dd");
            }
            return "";
        }
        public static string GetMonthNumber(this string d)
        {
            DateTime dt;
            if (DateTime.TryParseExact(d, "MM/dd/yyyy", null, DateTimeStyles.None, out dt))
            {
                return dt.ToString("MM");
            }
            return "";
        }
        public static string GetYearNumber(this string d)
        {
            DateTime dt;
            if (DateTime.TryParseExact(d, "MM/dd/yyyy", null, DateTimeStyles.None, out dt))
            {
                return dt.ToString("yyyy");
            }
            return "";
        }
        public static string ToYearWords(this string yr)
        {
            if (yr == null || yr == "")
                yr = "00";
            int n2, n3;
            Int32.TryParse(yr.Substring(0, 2), out n2);
            Int32.TryParse(yr.Substring(2, 2), out n3);
            if (n2 < 20)
                return n2.ToWords() + " Hundred and " + n3.ToWords();
            else
                return "Two Thousand and " + n3.ToWords();
        }
        public static string ToYearWords(this int yr)
        {
            return yr.ToString().ToYearWords();
        }
        public static string ToWords(this int number)
        {
            if (number == 0)
                return "Zero";
            if (number < 0)
                return "minus " + Math.Abs(number).ToWords();
            string words = "";
            if ((number / 1000000) > 0)
            {
                words += (number / 1000000).ToWords() + " Million ";
                number %= 1000000;
            }
            if ((number / 1000) > 0)
            {
                words += (number / 1000).ToWords() + " Thousand ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                words += (number / 100).ToWords() + " Hundred ";
                number %= 100;
            }
            if (number > 0)
            {
                if (words != "")
                    words += "and ";
                var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += " " + unitsMap[number % 10];
                }
            }
            return words;
        }
        public static string ToCountryName(this string c)
        {
            if (string.IsNullOrWhiteSpace(c)) return "";
            return CountryList.LIST_OF_COUNTRIES.Where(x => x.Value == c).FirstOrDefault().Key.ToTitleCase();
        }
        public static string ToStateName(this string c)
        {
            if (c == null) return "";
            return GoNyLaw.Helpers.DropDownData.states.Where(m => m.Value == c)
                              .First()
                              .Text;
        }
        public static string GetBasisVenue(DivorceCase dc)
        {
            string basis = "";
            if (dc.basis_venue == "P")
            {
                basis = "Plaintiff's County";
            }
            else if (dc.basis_venue == "D")
            {
                basis = "Defendant's County";
            }
            else if (dc.basis_venue == "C")
            {
                basis = "CPLR Sec. 509";
            }
            return basis;
        }
        public static string GetCountyVenue(DivorceCase dc)
        {
            string venue = "";
            venue = dc.county_venue.ToTitleCase();
            return venue;
        }
        public static string ExpandFrequency(this string f)
        {
            if (f == "M")
                return "per month";
            else if (f == "W")
                return "per week";
            else if (f == "B")
                return "per two weeks";
            return "";
        }
        public static string GetFrequency(this string f)
        {
            if (f == "M")
                return "on the first day of each month";
            else if (f == "W")
                return "on Monday of each week";
            else if (f == "B")
                return "on Monday of every other week";
            return "";
        }
        public static string StringToWords(this string f)
        {
            if (f == null) return "Zero";
            f = f.Replace(",", "");
            double n;
            Double.TryParse(f, out n);
            return ((int)n).ToWords();
        }
        public static string GetCourtOrderType(this string t)
        {
            if (t == "S")
                return "Child Support";
            else if (t == "C")
                return "Custody";
            else if (t == "V")
                return "Visitation";
            else if (t == "O")
                return "Order of Protection";
            return "";
        }
        public static string ToFormattedAmount(this string s)
        {
            if (s == null) return s;
            double n;
            Double.TryParse(s, out n);
            return ((int)n).ToString("n0");
        }
        public static string ToPerson(this string p)
        {
            if (p == "P")
                return "Plaintiff";
            else
                return "Defendant";
        }
        public static string ToGender(this string g)
        {
            if (g == "F")
                return "Female";
            else
                return "Male";
        }
        public static string GetStatus(this string c)
        {
            if (c == null) return "";
            return GoNyLaw.Helpers.DropDownData.status.Where(m => m.Value == c)
                              .First()
                              .Text;
        }
        public static string PaymentNumberWords(this string p)
        {
            if (p == "1") return "First Part";
            else if (p == "2") return "Second Part";
            else if (p == "F") return "One Time";
            else return "Error";
        }
        public static string GetFirstName(this string n)
        {
            if (n == null) return "";
            n = n.ToTitleCase();
            var names = n.Split(' ');
            if (names.Length == 4)
                return names[0] + " " + names[1];
            else if (names.Length == 3 || names.Length == 2)
                return names[0];
            return n;

        }
        public static string GetLastName(this string n)
        {
            if (n == null) return "";
            n = n.ToTitleCase();
            var names = n.Split(' ');
            if (names.Length == 4)
                return names[2] + " " + names[3];
            else if (names.Length == 3 || names.Length == 2)
                return names[names.Length - 1];
            return n;
        }
        public static string GetIndexPurchasedCode()
        {
            return "6";
        }
        public static string ToUSDate(this DateTime dt)
        {
            if (dt == null) return null;
            else
                return dt.ToString("MM/dd/yyyy");
        }
        public static string HiddenDOB(this DateTime dt)
        {
            if (dt == null) return "";
            return dt.ToString("MM/dd/yyyy").Substring(0, 6) + "XXXX";
        }
        public static string HiddenDOB(this DateTime? dt)
        {
            if (dt == null) return "";
            return dt.Value.ToString("MM/dd/yyyy").Substring(0, 6) + "XXXX";
        }
        public static string HiddenDOB(this string dt)
        {
            if (string.IsNullOrWhiteSpace(dt)) return "";
            return dt.Substring(0, 6) + "XXXX";
        }
        public static string HiddenSSN(this string ssn)
        {
            if (string.IsNullOrWhiteSpace(ssn)) return "";
            return "XXX-XX-" + ssn.Substring(7);
        }
        public static bool HasReceivedDocsStatusChanged(this DivorceCase dc, ref DivorceCase olddc)
        {
            bool response = false;
            if ((dc.allow1 != olddc.allow1) && (dc.allow1 == "3" || dc.allow1 == "4"))
            {
                olddc.received1 = DateTime.Today;
                response = true;
            }
            if ((dc.allow2 != olddc.allow2) && (dc.allow2 == "3" || dc.allow2 == "4"))
            {
                olddc.received2 = DateTime.Today;
                response = true;
            }
            if ((dc.allow3 != olddc.allow3) && (dc.allow3 == "3" || dc.allow3 == "4"))
            {
                olddc.received3 = DateTime.Today;
                response = true;
            }
            if ((dc.allow4 != olddc.allow4) && (dc.allow4 == "3" || dc.allow4 == "4"))
            {
                olddc.received4 = DateTime.Today;
                response = true;
            }
            if ((dc.allow5 != olddc.allow5) && (dc.allow5 == "3" || dc.allow5 == "4"))
            {
                olddc.received5 = DateTime.Today;
                response = true;
            }
            if ((dc.allow6 != olddc.allow6) && (dc.allow6 == "3" || dc.allow6 == "4"))
            {
                olddc.received6 = DateTime.Today;
                response = true;
            }
            if ((dc.allow7 != olddc.allow7) && (dc.allow7 == "3" || dc.allow7 == "4"))
            {
                olddc.received7 = DateTime.Today;
                response = true;
            }
            if ((dc.allow8 != olddc.allow8) && (dc.allow8 == "3" || dc.allow8 == "4"))
            {
                olddc.received8 = DateTime.Today;
                response = true;
            }
            if ((dc.allow9 != olddc.allow9) && (dc.allow9 == "3" || dc.allow9 == "4"))
            {
                olddc.received9 = DateTime.Today;
                response = true;
            }
            if ((dc.allow10 != olddc.allow10) && (dc.allow10 == "3" || dc.allow10 == "4"))
            {
                olddc.received10 = DateTime.Today;
                response = true;
            }
            if ((dc.allow11 != olddc.allow11) && (dc.allow11 == "3" || dc.allow11 == "4"))
            {
                olddc.received11 = DateTime.Today;
                response = true;
            }
            if ((dc.allow12 != olddc.allow12) && (dc.allow12 == "3" || dc.allow12 == "4"))
            {
                olddc.received12 = DateTime.Today;
                response = true;
            }
            if ((dc.allow13 != olddc.allow13) && (dc.allow13 == "3" || dc.allow13 == "4"))
            {
                olddc.received13 = DateTime.Today;
                response = true;
            }
            if ((dc.allow14 != olddc.allow14) && (dc.allow14 == "3" || dc.allow14 == "4"))
            {
                olddc.received14 = DateTime.Today;
                response = true;
            }
            if ((dc.allow15 != olddc.allow15) && (dc.allow15 == "3" || dc.allow15 == "4"))
            {
                olddc.received15 = DateTime.Today;
                response = true;
            }
            if ((dc.allow16 != olddc.allow16) && (dc.allow16 == "3" || dc.allow16 == "4"))
            {
                olddc.received16 = DateTime.Today;
                response = true;
            }
            if ((dc.allow17 != olddc.allow17) && (dc.allow17 == "3" || dc.allow17 == "4"))
            {
                olddc.received17 = DateTime.Today;
                response = true;
            }
            if ((dc.allow18 != olddc.allow18) && (dc.allow18 == "3" || dc.allow18 == "4"))
            {
                olddc.received18 = DateTime.Today;
                response = true;
            }
            return response;
        }

    }
}