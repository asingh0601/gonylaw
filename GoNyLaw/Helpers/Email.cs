﻿using DKIM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;

namespace GoNyLaw.Helpers
{
    public class Email
    {
        public static void SendEmail(string sendto, string subject, string body, string[] attachmentFilename)
        {
            if (string.IsNullOrEmpty(sendto))
                return;
            Attachment attachment;
            ContentDisposition disposition;
            var message = new MailMessage();
            message.To.Add(new MailAddress(sendto));
            message.From = new MailAddress("info@gonylaw.com", "GoNyLaw");
            message.Subject = "GoNyLaw - " + subject;
            message.Body = body;
            message.IsBodyHtml = true;
            var privateKey = PrivateKeySigner.Create(@"-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQC0iv3RSeLYY5Lc6zr+tH7/UvFz3ZxwP1Xv5S4ZOd4ckpxRs0RX
C9JKUQTnlS3aE2QnKmy+lCdk3meNT+W5KgbwIbRbkpcytLO8E3vJ4pnK6Ktlr3+/
BEnnCfxQS1JpCpfnRwezKkj6m6cjMXnU+oE8Gj0pQ9tYhD/fQlPOHmy0nwIDAQAB
AoGAJnRgYZ6SMbLryiBUXYIGds2s3hf6xTHIVSOEcR0RQRa9Mu2zBbNf5DJjkAwH
SybMM0dnCCS4S1TPy5qT/J+GAG3UrvIYdgKo5UzxIXay8VuAxqRgqIOwsbC0UjAv
HRXj6LJWRLA1pBOIkgX/KDwKy3g5p5P5uP2q1N1tjlJkGBkCQQDp6KpcPWXO9uh6
sUTAMUM+DUay3tepaX6KBI8mHkZUzraIJbKuN4OKDz22ki8BE90QPXBzEJYccoHy
rUMZGHlVAkEAxZgSI+TUYK/TGsqWAoI0QCelTUxTBPPb/ocnAvVa1CNDTPV/CUaW
1FPrmFYaOWm2WklgbSX/pulShdX5L8umIwJAINfMY9szGlZM2j1du+au3F2c2ay0
d1Mas2JkfyOtGGdV9dPlPNUKoS32qlXoR6x0U71stptTmHbzjBGL5FAtaQJBAK7O
gUXl/IlpI27xSNMBU65qJJCE9HFOOBMuiDHS1D8Wz0R3b6LKI0fG0ofbrKmcPyoS
/k6xrkGA63Phv+kr1lcCQGk90azkQbut5G+DNVG1jan56DcdWgP9sUKvYgSxB6xx
tK8JpDa8NSbQlJ3wiFI0/1kP2rUzFHVZj41UA0vjNzw=
-----END RSA PRIVATE KEY-----");

            var domainKeySigner = new DomainKeySigner(privateKey, "gonylaw.com", "selector1", new string[] { "From", "To", "Subject", "Mail From" });
            message.DomainKeySign(domainKeySigner);


            var dkimSigner = new DkimSigner(privateKey, "gonylaw.com", "selector1", new string[] { "From", "To", "Subject" });
            message.DkimSign(dkimSigner);
            if (attachmentFilename != null)
            {
                if (!string.IsNullOrEmpty(attachmentFilename[0]))
                {
                    attachment = new Attachment(attachmentFilename[0], MediaTypeNames.Application.Octet);
                    disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(attachmentFilename[0]);
                    disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename[0]);
                    disposition.ReadDate = File.GetLastAccessTime(attachmentFilename[0]);
                    disposition.FileName = Path.GetFileName(attachmentFilename[0]);
                    disposition.Size = new FileInfo(attachmentFilename[0]).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    message.Attachments.Add(attachment);
                }
                if (!string.IsNullOrEmpty(attachmentFilename[1]))
                {
                    attachment = new Attachment(attachmentFilename[1], MediaTypeNames.Application.Octet);
                    disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(attachmentFilename[1]);
                    disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename[1]);
                    disposition.ReadDate = File.GetLastAccessTime(attachmentFilename[1]);
                    disposition.FileName = Path.GetFileName(attachmentFilename[1]);
                    disposition.Size = new FileInfo(attachmentFilename[1]).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    message.Attachments.Add(attachment);
                }
                if (!string.IsNullOrEmpty(attachmentFilename[2]))
                {
                    attachment = new Attachment(attachmentFilename[2], MediaTypeNames.Application.Octet);
                    disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(attachmentFilename[2]);
                    disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename[2]);
                    disposition.ReadDate = File.GetLastAccessTime(attachmentFilename[2]);
                    disposition.FileName = Path.GetFileName(attachmentFilename[2]);
                    disposition.Size = new FileInfo(attachmentFilename[2]).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    message.Attachments.Add(attachment);
                }
                if (!string.IsNullOrEmpty(attachmentFilename[3]))
                {
                    attachment = new Attachment(attachmentFilename[3], MediaTypeNames.Application.Octet);
                    disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(attachmentFilename[3]);
                    disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename[3]);
                    disposition.ReadDate = File.GetLastAccessTime(attachmentFilename[3]);
                    disposition.FileName = Path.GetFileName(attachmentFilename[3]);
                    disposition.Size = new FileInfo(attachmentFilename[3]).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    message.Attachments.Add(attachment);
                }
                if (!string.IsNullOrEmpty(attachmentFilename[4]))
                {
                    attachment = new Attachment(attachmentFilename[4], MediaTypeNames.Application.Octet);
                    disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(attachmentFilename[4]);
                    disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename[4]);
                    disposition.ReadDate = File.GetLastAccessTime(attachmentFilename[4]);
                    disposition.FileName = Path.GetFileName(attachmentFilename[4]);
                    disposition.Size = new FileInfo(attachmentFilename[4]).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    message.Attachments.Add(attachment);
                }
            }
            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "info@gonylaw.com",
                    Password = "Reyna480!"
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp.office365.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.Send(message);
            }
        }
    }
}