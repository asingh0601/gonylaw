﻿using System;
using System.Web;
using Twilio;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace GoNyLaw.Helpers
{
    public class SMS
    {
        private static string AccountSid = "AC598e69e1ae21385f2dbeea350ed86cdd";
        private static string AuthToken = "066f9b35d0d8555c31c4a53ec7ed6af7";
        private static string mynumber = "+18557343600";
        public static int SendSMS(string body, string sendto)
        {
            if (string.IsNullOrEmpty(sendto))
                return -1;
            sendto = "+1" + sendto.Replace(@"-", "");
            body = SpanishAccentRemoval.RemoveDiacritics(body, true);
            TwilioClient.Init(AccountSid, AuthToken);
            var message = MessageResource.Create(
             to: new PhoneNumber(sendto),
             from: new PhoneNumber(mynumber),
             body: body);
            if (message.ErrorMessage != null)
            {
                return -1;
            }
            return 1;
        }
    }
}