﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace GoNyLaw.Helpers
{
    public class Recaptcha
    {
        public static bool Validate(string r)
        {
            using (var wc = new WebClient())
            {
                var validateString = string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", "6LecIRIUAAAAACFgmQUWFzT2IHlBo-bLJlleZnXI", r);
                var recaptcha_result = wc.DownloadString(validateString);
                if (recaptcha_result.ToLower().Contains("true"))
                {
                    return true;
                }
            }
            return false;
        }
    }
}