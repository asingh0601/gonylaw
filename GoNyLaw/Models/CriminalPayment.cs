﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class CriminalPayment : IEnumerable<CriminalPayment>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string refno { get; set; }
        public DateTime datepayment { get; set; }
        public string type { get; set; }
        public int amount { get; set; }
        public string txnid { get; set; }
        public string details { get; set; }
        private List<CriminalPayment> cp;
        public IEnumerator<CriminalPayment> GetEnumerator()
        {
            return cp.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return cp.GetEnumerator();
        }
    }
}