﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace GoNyLaw.Models
{
    public class LawOfficeDetail
    {
        [Key]
        [Required]
        public string id { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string telephone { get; set; }
        public string fax { get; set; }
        public string messenger { get; set; }
    }
    public class LawOfficeDetailContext : DbContext
    {
        public LawOfficeDetailContext() : base("mssqlDB") { }
        public DbSet<LawOfficeDetail> LawOfficeDetail { get; set; }
    }
}