﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class ImmigrationCase : IEnumerable<ImmigrationCase>
    {
        [Key]
        public string refno { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateregister { get; set; }
        public int clientid { get; set; }
        public int adminid { get; set; }
        public string adminsassigned { get; set; }
        public string fullname { get; set; }
        public string alias { get; set; }
        public string cellphone { get; set; }
        public string emailid { get; set; }
        public string contactfullname { get; set; }
        public string contactrelationship { get; set; }
        public string contacttelephone { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public int type { get; set; }
        public string formfile { get; set; }
        public int amount { get; set; }
        public int archived { get; set; }
        private List<ImmigrationCase> ic;
        public IEnumerator<ImmigrationCase> GetEnumerator()
        {
            return ic.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ic.GetEnumerator();
        }
    }
}