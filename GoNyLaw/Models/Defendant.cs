﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class Defendant1
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public DefendantDetail d1 { get; set; }
        public DefendantDetail d2 { get; set; }
        public DefendantDetail d3 { get; set; }
        public DefendantDetail d4 { get; set; }
        public DefendantDetail d5 { get; set; }
        public DefendantDetail d6 { get; set; }
        public DefendantDetail d7 { get; set; }
        public DefendantDetail d8 { get; set; }
        public DefendantDetail d9 { get; set; }
        public DefendantDetail d10 { get; set; }
    }
}