﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class LogContext : DbContext
    {
        public LogContext() : base("mssqldb") { }
        public DbSet<CriminalLog> CriminalLogs { get; set; }
        public DbSet<BankruptcyLog> BankruptcyLogs { get; set; }
        public DbSet<ImmigrationLog> ImmigrationLogs { get; set; }
        public DbSet<PersonalInjuryLog> PersonalInjuryLogs { get; set; }
    }
}