﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class CourtOrder
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string any_order { get; set; }
        public string no_order { get; set; }
        public string c1_ordertype { get; set; }
        public string c1_type { get; set; }
        public string c1_court_name { get; set; }
        public string c1_state { get; set; }
        public string c1_county { get; set; }
        public string c1_date { get; set; }
        public string c1_index { get; set; }
        public string c1_amount { get; set; }
        public string c1_frequency { get; set; }
        public string c2_ordertype { get; set; }
        public string c2_type { get; set; }
        public string c2_court_name { get; set; }
        public string c2_state { get; set; }
        public string c2_county { get; set; }
        public string c2_date { get; set; }
        public string c2_index { get; set; }
        public string c2_amount { get; set; }
        public string c2_frequency { get; set; }
        public string c3_ordertype { get; set; }
        public string c3_type { get; set; }
        public string c3_court_name { get; set; }
        public string c3_state { get; set; }
        public string c3_county { get; set; }
        public string c3_date { get; set; }
        public string c3_index { get; set; }
        public string c3_amount { get; set; }
        public string c3_frequency { get; set; }
        public string c4_ordertype { get; set; }
        public string c4_type { get; set; }
        public string c4_court_name { get; set; }
        public string c4_state { get; set; }
        public string c4_county { get; set; }
        public string c4_date { get; set; }
        public string c4_index { get; set; }
        public string c4_amount { get; set; }
        public string c4_frequency { get; set; }        
    }
}