﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace GoNyLaw.Models
{
    public class ChildCalc
    {
        [Key]
        [Required]
        public string id { get; set; }
        public string one_child { get; set; }
        public string two_child { get; set; }
        public string three_child { get; set; }
        public string four_child { get; set; }
        public string five_child { get; set; }
        public string upper_slab { get; set; }
        public string lower_slab { get; set; }
        public string lowest_slab { get; set; }
        public string ny_tax { get; set; }
        public string fica_social { get; set; }
        public string fica_medicare { get; set; }
    }
    public class ChildCalcContext : DbContext
    {
        public ChildCalcContext() : base("mssqlDB") { }
        public DbSet<ChildCalc> ChildCalc { get; set; }
    }
}
