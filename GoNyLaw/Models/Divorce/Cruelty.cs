﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class Cruelty
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string date1 { get; set; }
        public string place1 { get; set; }
        public string act1 { get; set; }
        public string date2 { get; set; }
        public string place2 { get; set; }
        public string act2 { get; set; }
        public string date3 { get; set; }
        public string place3 { get; set; }
        public string act3 { get; set; }
    }
}