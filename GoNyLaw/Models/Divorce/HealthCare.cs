﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class HealthCare
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string plan_holder { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string id_no { get; set; }
        public string plan_admin { get; set; }
        public string medical { get; set; }
        public string dental { get; set; }
        public string optical { get; set; }
        public string health_cover { get; set; }
        public string unreimbursed { get; set; }
        public string babysit { get; set; }
    }
}