﻿using GoNyLaw.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace GoNyLaw.Models
{
    public class Defendant
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string name { get; set; }
        public string sex { get; set; }
        public string maiden_name { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string county { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string dob { get; set; }
        public string place_birth { get; set; }
        public string check_ssn { get; set; }
        public string ssn { get; set; }
        public string marriage_no { get; set; }
        public string education { get; set; }
        public string race { get; set; }
        public string income{ get; set; }
    }
}