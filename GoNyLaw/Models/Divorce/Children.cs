﻿using GoNyLaw.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class Children
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string no_children { get; set; }
        public string c1_name { get; set; }
        public string c1_sex { get; set; }
        public string c1_lives { get; set; }
        public string t1_name { get; set; }
        public string t1_address { get; set; }
        public string c1_dob { get; set; }
        public string c1_hasssn { get; set; }
        public string c1_ssn { get; set; }
        public string c2_name { get; set; }
        public string c2_sex { get; set; }
        public string c2_lives { get; set; }
        public string t2_name { get; set; }
        public string t2_address { get; set; }
        public string c2_dob { get; set; }
        public string c2_hasssn { get; set; }
        public string c2_ssn { get; set; }
        public string c3_name { get; set; }
        public string c3_sex { get; set; }
        public string c3_lives { get; set; }
        public string t3_name { get; set; }
        public string t3_address { get; set; }
        public string c3_dob { get; set; }
        public string c3_hasssn { get; set; }
        public string c3_ssn { get; set; }
        public string c4_name { get; set; }
        public string c4_sex { get; set; }
        public string c4_lives { get; set; }
        public string t4_name { get; set; }
        public string t4_address { get; set; }
        public string c4_dob { get; set; }
        public string c4_hasssn { get; set; }
        public string c4_ssn { get; set; }
        public string c5_name { get; set; }
        public string c5_sex { get; set; }
        public string c5_lives { get; set; }
        public string t5_name { get; set; }
        public string t5_address { get; set; }
        public string c5_dob { get; set; }
        public string c5_hasssn { get; set; }
        public string c6_hasssn { get; set; }
        public string c5_ssn { get; set; }
        public string c6_name { get; set; }
        public string c6_sex { get; set; }
        public string c6_lives { get; set; }
        public string t6_name { get; set; }
        public string t6_address { get; set; }
        public string c6_dob { get; set; }
        public string c6_ssn { get; set; }
    }
}