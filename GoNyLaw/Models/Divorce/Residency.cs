﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace GoNyLaw.Models
{
    public class Residency
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string summons_usa { get; set; }
        public string applicable { get; set; }
        public string grounds { get; set; }
    }
}