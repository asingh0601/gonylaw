﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class PriorTreatment
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public PriorDetail facility1 { get; set; }
        public PriorDetail facility2 { get; set; }
        public PriorDetail facility3 { get; set; }
        public PriorDetail facility4 { get; set; }
        public PriorDetail facility5 { get; set; }
        
    }
}