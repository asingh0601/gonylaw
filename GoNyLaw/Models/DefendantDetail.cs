﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class DefendantDetail
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string driver_name { get; set; }
        public string driver_address { get; set; }
        public string driver_tel { get; set; }
        public string driver_owner { get; set; }
        public string owner_info { get; set; }
        public string owner_name { get; set; }
        public string owner_address { get; set; }
        public string owner_tel { get; set; }
        public string car_year { get; set; }
        public string car_make { get; set; }
        public string car_model { get; set; }
        public string insurance_policy_no { get; set; }
        public string insurance_policy_limit { get; set; }
        public string insurance_company_name { get; set; }
        public string insurance_company_address { get; set; }
        public string insurance_adjuster { get; set; }
        public string insurance_tel { get; set; }
        public string insurance_email { get; set; }
        public string insurance_no_of_attorney { get; set; }
        public string insurance_attorney_name_1 { get; set; }
        public string insurance_attorney_address_1 { get; set; }
        public string insurance_attorney_tel_1 { get; set; }
        public string insurance_attorney_email_1 { get; set; }
        public string insurance_attorney_name_2 { get; set; }
        public string insurance_attorney_address_2 { get; set; }
        public string insurance_attorney_tel_2 { get; set; }
        public string insurance_attorney_email_2 { get; set; }
        public string insurance_attorney_name_3 { get; set; }
        public string insurance_attorney_address_3 { get; set; }
        public string insurance_attorney_tel_3 { get; set; }
        public string insurance_attorney_email_3 { get; set; }
    }
}