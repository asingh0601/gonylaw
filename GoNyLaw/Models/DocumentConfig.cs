﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class DocumentConfig
    {
            public int id { get; set; }
            public string type { get; set; }
    }
    public class DocumentConfigContext : DbContext
    {
        public DocumentConfigContext() : base("mssqlDB") { }
        public DbSet<DocumentConfig> DocumentConfig { get; set; }
    }
}