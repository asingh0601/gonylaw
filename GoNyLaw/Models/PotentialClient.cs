﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections;
using System.Collections.Generic;

namespace GoNyLaw.Models
{
    public class PotentialClient : IEnumerable<PotentialClient>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateregister { get; set; }
        public string fullname { get; set; }
        public string homephone { get; set; }
        public string cellphone { get; set; }
        public string emailid { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string langpref { get; set; }
        public string initialinterest { get; set; }
        public string casedetails { get; set; }
        public string besttimetocall { get; set; }
        public DateTime appointment { get; set; }
        public string appointmentdetails { get; set; }
        [NotMapped]
        public string recaptcha { get; set; }
        private List<PotentialClient> c;
        public IEnumerator<PotentialClient> GetEnumerator()
        {
            return c.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return c.GetEnumerator();
        }
    }
}