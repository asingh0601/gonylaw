﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class BankruptcyCase : IEnumerable<BankruptcyCase>
    {
        [Key]
        public string refno { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateregister { get; set; }
        public int clientid { get; set; }
        public int adminid { get; set; }
        public string adminsassigned { get; set; }
        public string fullname { get; set; }
        public string alias { get; set; }
        public string cellphone { get; set; }
        public string emailid { get; set; }
        public string contactfullname { get; set; }
        public string contactrelationship { get; set; }
        public string contacttelephone { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateclass1 { get; set; }
        public string fileclass1 { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateclass2 { get; set; }
        public string fileclass2 { get; set; }
        [Column(TypeName = "date")]
        public DateTime datedischargeletter { get; set; }
        public string filedischargeletter { get; set; }
        public string miscfile1 { get; set; }
        public string miscfile2 { get; set; }
        public string miscfile3 { get; set; }
        public string miscfile4 { get; set; }
        public string miscfile5 { get; set; }
        public string fileapplication { get; set; }
        public string court { get; set; }
        public string courtaddress { get; set; }
        public DateTime courtdatetime { get; set; }
        public int amount { get; set; }
        public int archived { get; set; }
        private List<BankruptcyCase> bc;
        public IEnumerator<BankruptcyCase> GetEnumerator()
        {
            return bc.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return bc.GetEnumerator();
        }
    }
}