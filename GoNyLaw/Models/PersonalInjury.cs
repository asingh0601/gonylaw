﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class PersonalInjury : IEnumerable<PersonalInjury>
    {
        [Key]
        public string refno { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateregister { get; set; }
        public int clientid { get; set; }
        public int adminid { get; set; }
        public string adminsassigned { get; set; }
        //Section 1
        public string fullname { get; set; }
        public string address { get; set; }
        public string ssn { get; set; }
        [Column(TypeName = "date")]
        public DateTime dob { get; set; }
        public string civil_status { get; set; }
        public string spouse_name { get; set; }
        public string spouse_address { get; set; }
        public string spouse_phone { get; set; }
        [Column(TypeName = "date")]
        public DateTime divorce_date { get; set; }
        [Column(TypeName = "date")]
        public DateTime widowed_date { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public string emailid { get; set; }       
        public string contactfullname { get; set; }
        public string contactrelationship { get; set; }
        public string contacttelephone { get; set; }
        //Section 2
        public string isemployed { get; set; }
        public string missedjob { get; set; }
        public string daysjobmissed { get; set; }
        public string jobsalary { get; set; }
        public string jobsalaryinterval { get; set; }
        public string paystub { get; set; }
        public string taxreturns { get; set; }
        public string employer_name { get; set; }
        public string employer_address { get; set; }
        public string employer_tel { get; set; }
        public string employer_email { get; set; }
        public string work_record_requested { get; set; }
        [Column(TypeName = "date")]
        public DateTime date_work_requested { get; set; }
        [Column(TypeName = "date")]
        public DateTime date_work_received { get; set; }
        public string certified_copy_subpoenaed { get; set; }
        [Column(TypeName = "date")]
        public DateTime date_subpoenaed { get; set; }
        public string certified_copy_verified { get; set; }
        [Column(TypeName = "date")]
        public DateTime date_certified_verified { get; set; }
        //Section 3
        public DateTime datetimeofaccident { get; set; }
        public string cause { get; set; }
        public string cause_other { get; set; }
        public string accident_location { get; set; }
        public string weather_condition { get; set; }
        public string clientroll { get; set; }
        public string accidentpictures { get; set; }
        public string policereport { get; set; }
        public string list_of_injuries { get; set; }
        public string accidentsummary { get; set; }
        public string prior_accident { get; set; }
        public string prior_accident_date { get; set; }
        public string prior_accident_attorney { get; set; }
        public string prior_attorney_address { get; set; }
        public string prior_attorney_tel { get; set; }
        public string prior_attorney_email { get; set; }
        public string prior_treatment_facilities { get; set; }
        public PriorTreatment facilities { get; set; }
        public string was_award { get; set; }
        public string amount_award { get; set; }
        public string summary_injuries { get; set; }
        //Section 4
        public string ambulancecalled { get; set; }
        public string ambulance_service { get; set; }
        public string emergency_room { get; set; }
        public string ambulance_name { get; set; }
        public string ambulance_address { get; set; }
        public string ambulance_tel { get; set; }
        public string ambulance_summary { get; set; }
        public string ambulance_report { get; set; }
        public string ambulance_report_requestedby { get; set; }
        [Column(TypeName = "date")]
        public DateTime ambulance_report_request { get; set; }
        [Column(TypeName = "date")]
        public DateTime ambulance_report_received { get; set; }
        public string certified_records { get; set; }
        public string certified_records_requestedby { get; set; }
        [Column(TypeName = "date")]
        public DateTime certified_records_request { get; set; }
        [Column(TypeName = "date")]
        public DateTime certified_records_verified { get; set; }
        public string certified_records_verifiedby { get; set; }
        public string admittedhospital { get; set; }
        public int no_of_hospitals { get; set; }
        public string admittedclinic { get; set; }
        public int no_of_clinics { get; set; }
        public string visiteddoctor { get; set; }
        public int no_of_doctors { get; set; }
        public Treatment medical { get; set; }
        public string mri_done { get; set; }
        public int no_of_mri { get; set; }
        public string emg_done { get; set; }
        public int no_of_emg { get; set; }
        public string ekg_done { get; set; }
        public int no_of_ekg { get; set; }
        public string xray_done { get; set; }
        public int no_of_xray { get; set; }
        public Scan scans { get; set; }
        public string ime_done { get; set; }
        public string ime_name { get; set; }
        public string ime_address { get; set; }
        public string ime_phone { get; set; }
        public string ime_email { get; set; }
        [Column(TypeName = "date")]
        public DateTime ime_date { get; set; }
        public string ime_upload { get; set; }
        public string expert_done { get; set; }
        public string expert_area { get; set; }
        public string expert_alreadytreating { get; set; }
        [Column(TypeName = "date")]
        public DateTime expert_date { get; set; }
        public string expert_name { get; set; }
        public string expert_address { get; set; }
        public string expert_phone { get; set; }
        public string expert_email { get; set; }
        public string expert_upload { get; set; }
        //Section 5
        public string anycar { get; set; }
        public string insured_name { get; set; }
        public string insured_address { get; set; }
        public string insurance_policy_no { get; set; }
        public string insurance_policy_limit { get; set; }
        public string insurance_company_name { get; set; }
        public string insurance_company_address { get; set; }
        public string insurance_adjuster { get; set; }
        public string insurance_tel { get; set; }
        public string insurance_email { get; set; }
        public string insurance_attorney_name { get; set; }
        public string insurance_attorney_address { get; set; }
        public string insurance_attorney_tel { get; set; }
        public string insurance_attorney_email { get; set; }
        [Column(TypeName = "date")]
        public DateTime notice_due_date { get; set; }
        [Column(TypeName = "date")]
        public DateTime notice_sent_date { get; set; }
        public string no_of_defendants { get; set; }
        public Defendant1 defendant { get; set; }
        //Section 6
        public string court { get; set; }
        public string court_county { get; set; }
        public string againstcitystate { get; set; }
        
        public int amount_to_pay { get; set; }
        public int amount_paid { get; set; }
        public int archived { get; set; }
        private List<PersonalInjury> pi;
        public IEnumerator<PersonalInjury> GetEnumerator()
        {
            return pi.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return pi.GetEnumerator();
        }
    }
}