﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class TaskContext : DbContext
    {
        public TaskContext() : base("mssqldb") { }
        public DbSet<CriminalTask> CriminalTasks { get; set; }
        public DbSet<BankruptcyTask> BankruptcyTasks { get; set; }
        public DbSet<ImmigrationTask> ImmigrationTasks { get; set; }
        //public DbSet<PersonalInjuryTask> PersonalInjuryTasks { get; set; }
    }
}