﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class CriminalCase : IEnumerable<CriminalCase>
    {
        [Key]
        public string refno { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateregister { get; set; }
        public int clientid { get; set; }
        public int adminid { get; set; }
        public string adminsassigned { get; set; }
        public string fullname{ get; set; }
        public string alias { get; set; }
        public string cellphone { get; set; }
        public string emailid { get; set; }
        public string contactfullname { get; set; }
        public string contactrelationship { get; set; }
        public string contacttelephone { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string court { get; set; }
        public string county { get; set; }
        public string courtaddress { get; set; }
        public string othercourtaddress { get; set; }
        public string docketorindex { get; set; }
        public string docketorindexnumber { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateofarrest { get; set; }
        public DateTime datetimeofarraignment { get; set; }
        public string ada1 { get; set; }
        public string adaphone1 { get; set; }
        public string adaemail1 { get; set; }
        public string ada2 { get; set; }
        public string adaphone2 { get; set; }
        public string adaemail2 { get; set; }
        public string ada3 { get; set; }
        public string adaphone3 { get; set; }
        public string adaemail3 { get; set; }
        public string ada4{ get; set; }
        public string adaphone4 { get; set; }
        public string adaemail4 { get; set; }
        [Column(TypeName = "date")]
        public DateTime motion { get; set; }
        [Column(TypeName = "date")]
        public DateTime ptr { get; set; }
        [Column(TypeName = "date")]
        public DateTime dec { get; set; }
        public string Openfilediscovery { get; set; }
        [Column(TypeName = "date")]
        public DateTime datereceived { get; set; }
        [Column(TypeName = "date")]
        public DateTime mapp { get; set; }
        [Column(TypeName = "date")]
        public DateTime wade { get; set; }
        [Column(TypeName = "date")]
        public DateTime huntley { get; set; }
        [Column(TypeName = "date")]
        public DateTime dunaway { get; set; }
        [Column(TypeName = "date")]
        public DateTime other { get; set; }
        public string judge1 { get; set; }
        public string room1 { get; set; }
        public string part1 { get; set; }
        public string judge2 { get; set; }
        public string room2 { get; set; }
        public string part2 { get; set; }
        public string judge3 { get; set; }
        public string room3 { get; set; }
        public string part3 { get; set; }
        public string judge4 { get; set; }
        public string room4 { get; set; }
        public string part4 { get; set; }
        public int amount { get; set; }
        public int archived { get; set; }
        private List<CriminalCase> cc;
        public IEnumerator<CriminalCase> GetEnumerator()
        {
            return cc.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        { 
            return cc.GetEnumerator();
        }
    }
}