﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class MasterAccount : IEnumerable<MasterAccount>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [Column(TypeName = "date")]
        public DateTime date_of_operation { get; set; }
        public int sub_account_id { get; set; }
        public int check_no { get; set; }
        public string payable_to { get; set; }
        public int transaction_type { get; set; }
        public double transaction_amount { get; set; }
        public double running_balance { get; set; }
        public string remarks { get; set; }
        private List<MasterAccount> ma;
        public IEnumerator<MasterAccount> GetEnumerator()
        {
            return ma.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ma.GetEnumerator();
        }
    }
}