﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class PaymentContext : DbContext
    {
        public PaymentContext() : base("mssqldb") { }
        public DbSet<CriminalPayment> CriminalPayments { get; set; }
        public DbSet<BankruptcyPayment> BankruptcyPayments { get; set; }
        public DbSet<ImmigrationPayment> ImmigrationPayments { get; set; }
        public DbSet<PersonalInjuryPayment> PersonalInjuryPayments { get; set; }

    }
}