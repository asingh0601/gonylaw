﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class CaseContext : DbContext
    {
        public CaseContext() : base("mssqldb") { }
        public DbSet<CriminalCase> CriminalCases { get; set; }
        public DbSet<BankruptcyCase> BankruptcyCases { get; set; }
        public DbSet<ImmigrationCase> ImmigrationCases { get; set; }
        public DbSet<PersonalInjury> PersonalInjuries { get; set; }
        public DbSet<DivorceCase> DivorceCases { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
    }
}