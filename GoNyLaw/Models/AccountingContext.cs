﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class AccountingContext : DbContext
    {
        public AccountingContext() : base("mssqldb") { }
        public DbSet<MasterAccount> MasterAccountData { get; set; }
        public DbSet<SubAccount> SubAccountData { get; set; }
    }
}