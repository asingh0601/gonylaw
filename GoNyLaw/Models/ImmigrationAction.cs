﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class ImmigrationAction : IEnumerable<ImmigrationAction>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string refno { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateaction { get; set; }
        public string part { get; set; }
        public string room { get; set; }
        public string judge { get; set; }
        public string actiontaken { get; set; }
        [Column(TypeName = "date")]
        public DateTime nextcourtdate { get; set; }

        private List<ImmigrationAction> i;
        public IEnumerator<ImmigrationAction> GetEnumerator()
        {
            return i.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return i.GetEnumerator();
        }
    }
}