﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Collections;

namespace GoNyLaw.Models
{
    public class SalesPerson : IEnumerable
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateregister { get; set; }
        public string fullname { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public string emailid { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string county { get; set; }
        public string zip { get; set; }        
        public string password { get; set; }
        [NotMapped]
        public string confirmpassword { get; set; }
        private List<SalesPerson> s;
        public IEnumerator<SalesPerson> GetEnumerator()
        {
            return s.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return s.GetEnumerator();
        }
    }
}