﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class DivorceCase
    {
        [Key]
        [Required]
        public string case_id { get; set; }
        public int agency_id { get; set; }
        [Column(TypeName = "date")]
        public DateTime case_date { get; set; }
        public string casetype { get; set; }
        public int notification_preference { get; set; }
        public string index_no { get; set; }
        public string index_date { get; set; }
        public string basis_venue { get; set; }
        public string county_venue { get; set; }
        public string waiver_default { get; set; }
        public DateTime? date_summons { get; set; }
        public string status { get; set; }
        public DateTime? date_status { get; set; }
        public string comments { get; set; }        
        public string allow1 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received1 { get; set; }
        public string allow2 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received2 { get; set; }
        public string allow3 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received3 { get; set; }
        public string allow4 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received4 { get; set; }
        public string allow5 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received5 { get; set; }
        public string allow6 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received6 { get; set; }
        public string allow7 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received7 { get; set; }
        public string allow8 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received8 { get; set; }
        public string allow9 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received9 { get; set; }
        public string allow10 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received10 { get; set; }
        public string allow11 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received11 { get; set; }
        public string allow12 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received12 { get; set; }
        public string allow13 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received13 { get; set; }
        public string allow14 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received14 { get; set; }
        public string allow15 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received15 { get; set; }
        public string allow16 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received16 { get; set; }
        public string allow17 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received17 { get; set; }
        public string allow18 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received18 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received_co1 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received_co2 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received_co3 { get; set; }
        [Column(TypeName = "date")]
        public DateTime received_co4 { get; set; }
        public string smscode { get; set; }
        public string archived { get; set; }
        public virtual Plaintiff p { get; set; }
        public virtual Defendant d { get; set; }
        public virtual Marriage m { get; set; }
        public virtual Residency r { get; set; }
        public virtual Abandonment a { get; set; }
        public virtual Cruelty c { get; set; }
        public virtual Children ch { get; set; }
        public virtual CourtOrder co { get; set; }
        public virtual HealthCare h { get; set; }
        public virtual Transaction t { get; set; }
    }
}