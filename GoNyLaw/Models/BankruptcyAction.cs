﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class BankruptcyAction : IEnumerable<BankruptcyAction>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string refno { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateaction { get; set; }
        public string part { get; set; }
        public string room { get; set; }
        public string judge { get; set; }
        public string actiontaken { get; set; }
        [Column(TypeName = "date")]
        public DateTime nextcourtdate { get; set; }

        private List<BankruptcyAction> ba;
        public IEnumerator<BankruptcyAction> GetEnumerator()
        {
            return ba.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ba.GetEnumerator();
        }
    }
}