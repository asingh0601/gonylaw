﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class ActionContext : DbContext
    {
        public ActionContext() : base("mssqldb") { }
        public DbSet<CriminalAction> CriminalActions { get; set; }
        public DbSet<BankruptcyAction> BankruptcyActions { get; set; }
        public DbSet<ImmigrationAction> ImmigrationActions { get; set; }
       // public DbSet<PersonalInjuryAction> PersonalInjuryActions { get; set; }
    }
}