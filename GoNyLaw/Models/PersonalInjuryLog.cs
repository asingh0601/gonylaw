﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace GoNyLaw.Models
{
    public class PersonalInjuryLog : IEnumerable<PersonalInjuryLog>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string refno { get; set; }
        public string adminsassigned { get; set; }
        public DateTime dateaction { get; set; }
        public string type { get; set; }
        public string recepient { get; set; }
        public string action { get; set; }
        public string details { get; set; }
        public string filename1 { get; set; }
        public string filename2 { get; set; }
        public string filename3 { get; set; }
        public string filename4 { get; set; }
        public string filename5 { get; set; }
        private List<PersonalInjuryLog> i;
        public IEnumerator<PersonalInjuryLog> GetEnumerator()
        {
            return i.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return i.GetEnumerator();
        }
    }
}