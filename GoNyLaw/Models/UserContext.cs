﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class UserContext : DbContext
    {
        public UserContext() : base("mssqldb") { }
        public DbSet<Client> Clients { get; set; }
        public DbSet<PotentialClient> PotentialClients { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<SalesPerson> SalesPersons { get; set; }
    }
}