﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class Transaction
    {
        [Key]
        [Required]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string txn_id1 { get; set; }
        public int amount1 { get; set; }
        public string date1 { get; set; }
        public string txn_id2 { get; set; }
        public int amount2 { get; set; }
        public string date2 { get; set; }
    }
}