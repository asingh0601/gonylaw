﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections;
using System.Collections.Generic;

namespace GoNyLaw.Models
{
    public class Admin : IEnumerable<Admin>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateregister { get; set; }
        public string fullname { get; set; }
        public string cellphone { get; set; }
        public string emailid { get; set; }
        public string langpref { get; set; }
        public string otp { get; set; }
        public int otpverified { get; set; }
        public bool IsSuperAdmin { get; set; }
        public string password { get; set; }
        [NotMapped]
        [System.ComponentModel.DataAnnotations.Compare("password")]
        public string confirmpassword { get; set; }
        [NotMapped]
        public string recaptcha { get; set; }
        private List<Admin> a;
        public IEnumerator<Admin> GetEnumerator()
        {
            return a.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return a.GetEnumerator();
        }
    }
}