﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class ImmigrationTask : IEnumerable<ImmigrationTask>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string refno { get; set; }
        public string adminsassigned { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateassigned { get; set; }
        [Column(TypeName = "date")]
        public DateTime daterequired { get; set; }
        [Column(TypeName = "date")]
        public DateTime datecompleted { get; set; }
        public string task { get; set; }
        public string notes { get; set; }

        private List<ImmigrationTask> i;
        public IEnumerator<ImmigrationTask> GetEnumerator()
        {
            return i.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return i.GetEnumerator();
        }
    }
}