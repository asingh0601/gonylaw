﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class Treatment
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public TreatmentDetail hospital1 { get; set; }
        public TreatmentDetail hospital2 { get; set; }
        public TreatmentDetail hospital3 { get; set; }
        public TreatmentDetail hospital4 { get; set; }
        public TreatmentDetail hospital5 { get; set; }
        public TreatmentDetail clinic1 { get; set; }
        public TreatmentDetail clinic2 { get; set; }
        public TreatmentDetail clinic3 { get; set; }
        public TreatmentDetail clinic4 { get; set; }
        public TreatmentDetail clinic5 { get; set; }
        public TreatmentDetail doctor1 { get; set; }
        public TreatmentDetail doctor2 { get; set; }
        public TreatmentDetail doctor3 { get; set; }
        public TreatmentDetail doctor4 { get; set; }
        public TreatmentDetail doctor5 { get; set; }
        public TreatmentDetail doctor6 { get; set; }
        public TreatmentDetail doctor7 { get; set; }
        public TreatmentDetail doctor8 { get; set; }
        public TreatmentDetail doctor9 { get; set; }
        public TreatmentDetail doctor10 { get; set; }
    }
}