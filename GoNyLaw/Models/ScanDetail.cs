﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class ScanDetail
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string scan_area { get; set; }
        public string scan_area_other { get; set; }
        public string scan_type { get; set; }
        public string scan_type_other { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        [Column(TypeName = "date")]
        public DateTime reportdate { get; set; }
        public string cd_available { get; set; }
        public string report_upload { get; set; }
        public string scan_report_requestedby { get; set; }
        [Column(TypeName = "date")]
        public DateTime scan_report_request { get; set; }
        [Column(TypeName = "date")]
        public DateTime scan_report_received { get; set; }
        public string certified_records { get; set; }
        public string certified_records_subpoenaedby { get; set; }
        [Column(TypeName = "date")]
        public DateTime certified_records_request { get; set; }
        [Column(TypeName = "date")]
        public DateTime certified_records_verified { get; set; }
        public string certified_records_verifiedby { get; set; }
    }
}