﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class BankruptcyLog : IEnumerable<BankruptcyLog>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string refno { get; set; }
        public string adminsassigned { get; set; }
        public DateTime dateaction { get; set; }
        public string type { get; set; }
        public string recepient { get; set; }
        public string action { get; set; }
        public string details { get; set; }
        public string filename1 { get; set; }
        public string filename2 { get; set; }
        public string filename3 { get; set; }
        public string filename4 { get; set; }
        public string filename5 { get; set; }
        private List<BankruptcyLog> bl;
        public IEnumerator<BankruptcyLog> GetEnumerator()
        {
            return bl.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return bl.GetEnumerator();
        }
    }
}