﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class PaymentConfig
    {
        [Key]
        public int id { get; set; }
        public string ApiLoginID { get; set; }
        public string ApiTransactionKey { get; set; }
        public string Mode { get; set; }
        public int onetimegold { get; set; }
        public int installment_gold_total { get; set; }
        public int platinum { get; set; }
        public int divorce_no_kids_total { get; set; }
        public int divorce_no_kids_platinum { get; set; }
        public int divorce_no_kids_first { get; set; }
        public int divorce_no_kids_second { get; set; }
        public int divorce_with_kids_total { get; set; }
        public int divorce_with_kids_platinum { get; set; }
        public int divorce_with_kids_first { get; set; }
        public int divorce_with_kids_second { get; set; }
        public int corp_diamond { get; set; }
        public int corp_gold { get; set; }
        public int corp_silver { get; set; }
        public int corn_diamond { get; set; }
        public int corn_gold { get; set; }
        public int corn_silver { get; set; }
        public int llc_diamond { get; set; }
        public int llc_gold { get; set; }
        public int llc_silver { get; set; }
        public int bankruptcy_one_no { get; set; }
        public int bankruptcy_one_prop { get; set; }
        public int bankruptcy_joint_no { get; set; }
        public int bankruptcy_joint_prop { get; set; }
    }
}