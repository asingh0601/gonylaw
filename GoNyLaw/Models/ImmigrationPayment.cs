﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class ImmigrationPayment : IEnumerable<ImmigrationPayment>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string refno { get; set; }
        public DateTime datepayment { get; set; }
        public string type { get; set; }
        public int amount { get; set; }
        public string txnid { get; set; }
        public string details { get; set; }
        private List<ImmigrationPayment> i;
        public IEnumerator<ImmigrationPayment> GetEnumerator()
        {
            return i.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return i.GetEnumerator();
        }
    }
}