﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class SettingsContext : DbContext
    {
        public SettingsContext() : base("mssqlDB") { }
        public DbSet<PaymentConfig> PaymentConfig { get; set; }
    }
}