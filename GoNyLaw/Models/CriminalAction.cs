﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class CriminalAction : IEnumerable<CriminalAction>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string refno { get; set; }
        [Column(TypeName = "date")]
        public DateTime dateaction{ get; set; }
        public string part { get; set; }
        public string room { get; set; }
        public string judge { get; set; }
        public string actiontaken { get; set; }
        public DateTime nextcourtdate { get; set; }

        private List<CriminalAction> ca;
        public IEnumerator<CriminalAction> GetEnumerator()
        {
            return ca.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ca.GetEnumerator();
        }
    }
}