﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoNyLaw.Models
{
    public class TreatmentDetail
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string name { get; set; }
        public string address{ get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        [Column(TypeName = "date")]
        public DateTime treatmentstart { get; set; }
        [Column(TypeName = "date")]
        public DateTime treatmentend { get; set; }
        public int totaldays { get; set; }
        public string treatmentsummary { get; set; }
        public string medicalreport { get; set; }
        public string medical_report_requestedby { get; set; }
        [Column(TypeName = "date")]
        public DateTime medical_report_request { get; set; }
        [Column(TypeName = "date")]
        public DateTime medical_report_received { get; set; }
        public string certified_records { get; set; }
        public string certified_records_subpoenaedby { get; set; }
        [Column(TypeName = "date")]
        public DateTime certified_records_request { get; set; }
        [Column(TypeName = "date")]
        public DateTime certified_records_verified { get; set; }
        public string certified_records_verifiedby { get; set; }
    }
}