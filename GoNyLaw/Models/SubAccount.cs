﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class SubAccount : IEnumerable<SubAccount>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [Column(TypeName = "date")]
        public DateTime date_of_creation { get; set; }
        [Column(TypeName = "date")]
        public DateTime date_of_closure { get; set; }
        [Column(TypeName = "date")]
        public DateTime last_financial_updation { get; set; }
        public string account_holder_name { get; set; }
        public string gonylaw_ref_no { get; set; }
        public double current_balance { get; set; }
        private List<SubAccount> sa;
        public IEnumerator<SubAccount> GetEnumerator()
        {
            return sa.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return sa.GetEnumerator();
        }
    }
}