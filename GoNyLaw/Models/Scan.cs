﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoNyLaw.Models
{
    public class Scan
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public ScanDetail mri1 { get; set; }
        public ScanDetail mri2 { get; set; }
        public ScanDetail mri3 { get; set; }
        public ScanDetail mri4 { get; set; }
        public ScanDetail mri5 { get; set; }
        public ScanDetail mri6 { get; set; }
        public ScanDetail mri7 { get; set; }
        public ScanDetail mri8 { get; set; }
        public ScanDetail mri9 { get; set; }
        public ScanDetail mri10 { get; set; }
        public ScanDetail emg1 { get; set; }
        public ScanDetail emg2 { get; set; }
        public ScanDetail emg3 { get; set; }
        public ScanDetail emg4 { get; set; }
        public ScanDetail emg5 { get; set; }
        public ScanDetail emg6 { get; set; }
        public ScanDetail emg7 { get; set; }
        public ScanDetail emg8 { get; set; }
        public ScanDetail emg9 { get; set; }
        public ScanDetail emg10 { get; set; }
        public ScanDetail ekg1 { get; set; }
        public ScanDetail ekg2 { get; set; }
        public ScanDetail ekg3 { get; set; }
        public ScanDetail ekg4 { get; set; }
        public ScanDetail ekg5 { get; set; }
        public ScanDetail ekg6 { get; set; }
        public ScanDetail ekg7 { get; set; }
        public ScanDetail ekg8 { get; set; }
        public ScanDetail ekg9 { get; set; }
        public ScanDetail ekg10 { get; set; }
        public ScanDetail xray1 { get; set; }
        public ScanDetail xray2 { get; set; }
        public ScanDetail xray3 { get; set; }
        public ScanDetail xray4 { get; set; }
        public ScanDetail xray5 { get; set; }
        public ScanDetail xray6 { get; set; }
        public ScanDetail xray7 { get; set; }
        public ScanDetail xray8 { get; set; }
        public ScanDetail xray9 { get; set; }
        public ScanDetail xray10 { get; set; }
    }
}