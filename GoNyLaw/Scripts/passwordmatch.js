﻿$('#password2').keyup(function (e) {
    if ($('#password1').val() !== $('#password2').val()) {
        e.target.setCustomValidity('Passwords do not match');
        e.target.style.color = 'red';
    }
    else {
        e.target.setCustomValidity('');
        e.target.style.color = 'green';
    }
});