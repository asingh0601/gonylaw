﻿$(document).ready(function () {
    $('form').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
    //$(".fancybox").click();
    $('.form-control').tooltip();
    $("#mainnav-menu>li:nth-child(2)").next().addClass("active-link");
    //$("input,select").prop('required', false);
    $('.dates').datetimepicker({ format: 'MM/DD/YYYY' });
    if ($('#pssn').val() != '' && $('#pssn').val() != null)
        $('#ps').show();
    else
        $('#ps').hide();
    if ($('#dssn').val() != '' && $('#dssn').val() != null)
        $('#ds').show();
    else
        $('#ds').hide();
    if ($('#mcountry').val() == 'US')
        $('.mc1').show();
    else
        $('.mc1').hide();
    if (($('#pmaiden').val() != '' && $('#pmaiden').val() != null) || $('#psex').val() == 'F')
        $('#pm').show();
    else
        $('#pm').hide();
    if (($('#dmaiden').val() != '' && $('#dmaiden').val() != null) || $('#dsex').val() == 'F')
        $('#dm').show();
    else
        $('#dm').hide();
    if ($('#pcountry').val() == 'US') {
        $('#pc1').show();
        $('#pc2').show();
        $('#pc3').show();
    }
    else {
        $('#pc1').hide();
        $('#pc2').hide();
        $('#pc3').hide();
    }
    if ($('#dcountry').val() == 'US') {
        $('#dc1').show();
        $('#dc2').show();
        $('#dc3').show();
    }
    else {
        $('#dc1').hide();
        $('#dc2').hide();
        $('#dc3').hide();
    }
    $('#c5').parent().hide();
    $('#c6').parent().hide();
    $('#c7').parent().hide();
    $('#c8').parent().hide();
    $('#c9').parent().hide();
    var chno = parseInt($('#chnumber').val());
    if (isNaN(chno)) {
        chno = 0;
    }
    for (i = 1; i <= 6; i++) {
        if (i > chno)
            $('.ch' + i).hide();
        $('#c' + i + 'hasssn').trigger('change');
        $('#c' + i + 'lives').trigger('change');
    }
    var cono = parseInt($('#conumber').val());
    if (isNaN(cono)) {
        cono = 0;
    }
    for (i = 1; i <= 4; i++) {
        if (i > cono)
            $('.co' + i).hide();
        $('#co' + i + 'ordertype').trigger('change');
    }
    $('#coany').trigger("change");
    $('#hplanholder').trigger("change");
    if ($('#digrounds').val() == "1") {
        $('#c7').html('5');
        $('#c8').html('6');
        $('#c9').html('7');
        $('#c7').parent().show();
        $('#c8').parent().show();
        $('#c9').parent().show();
    }
    if ($('#digrounds').val() == "2" || $('#digrounds').val() == "3") {
        $('#c5').parent().show();
        $('#c7').parent().show();
        $('#c8').parent().show();
        $('#c9').parent().show();
        $('#c7').html('6');
        $('#c8').html('7');
        $('#c9').html('8');
    }
    if ($('#digrounds').val() == "4") {
        $('#c6').parent().show();
        $('#c7').parent().show();
        $('#c8').parent().show();
        $('#c9').parent().show();
        $('#c7').html('6');
        $('#c8').html('7');
        $('#c9').html('8');
    }
    var s = 1;
    $('#nxtBtn').click(function (e) {
        e.preventDefault();
        var curStep = $("#step-" + s),
                    curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select").not(":hidden"),
                    isValid = true;
        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid) {
            $("#step-" + s).hide();
            if (s < 9)
                s++;
            if (s == 2) {
                $('.panel-title').html('Defendant Information');
                $("#c1").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c2").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                $('#bkBtn').show();
            }
            else if (s == 3) {
                $('.panel-title').html('Marriage Information');
                $("#c2").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c3").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            }
            else if (s == 4) {
                $('.panel-title').html('Divorce Information');
                $("#c3").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c4").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            }
            else if (s == 5) {
                if ($('#digrounds').val() == "1") {
                    s += 2;
                    $('.panel-title').html('Children Information');
                    $("#c4").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                    $("#c7").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                }
                else if ($('#digrounds').val() == "4") {
                    s++;
                    $('.panel-title').html('Cruelty Information');
                    $("#c4").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                    $("#c6").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                }
                else {
                    $('.panel-title').html('Abandonment Information');
                    $("#c4").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                    $("#c5").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                }
            }
            else if (s == 6) {
                $('.panel-title').html('Children Information');
                if ($('#digrounds').val() == "2" || $('#digrounds').val() == "3") {
                    $("#c5").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                    $("#c7").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                    s++;
                }
                else {
                    $("#c6").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                    $("#c7").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                }
            }
            else if (s == 7) {
                $('.panel-title').html('Children Information');
                $("#c6").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c7").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            }
            else if (s == 8) {
                $('.panel-title').html('Court Orders Information');
                $("#c7").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c8").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            }
            else if (s == 9) {
                $('.panel-title').html('Healthcare Information');
                $("#c8").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c9").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                $('#nxtBtn').hide();
                $('#sbmtBtn').show();
            }
            $("#step-" + s).show();
        }
        e.stopPropagation();
    });
    $('#sbmtBtn').click(function (e) {
        e.preventDefault();
        var curStep = $("#step-" + s),
                    curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select").not(":hidden"),
                    isValid = true;
        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid) {
            $("input:hidden,select:hidden").prop('required', false);
            $("#__AjaxAntiForgeryForm").submit();
        }
        e.stopPropagation();
    });
    $('#bkBtn').click(function (e) {
        e.preventDefault();
        $('#sbmtBtn').hide();
        $('#nxtBtn').show();
        $("#step-" + s).hide();
        if (s == 2) {
            $('.panel-title').html('Plaintiff Information');
            $("#c2").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c1").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            $('#bkBtn').hide();
        }
        else if (s == 3) {
            $('.panel-title').html('Defendant Information');
            $("#c3").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c2").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
        }
        else if (s == 4) {
            $('.panel-title').html('Marriage Information');
            $("#c4").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c3").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
        }
        else if (s == 5) {
            $('.panel-title').html('Divorce Information');
            $("#c5").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c4").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
        }
        else if (s == 6) {
            $('.panel-title').html('Divorce Information');
            $("#c6").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c4").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            s--;
        }
        else if (s == 7) {
            if ($('#digrounds').val() == "1") {
                $('.panel-title').html('Divorce Information');
                $("#c7").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c4").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                s -= 2;
            }
            else if ($('#digrounds').val() == "4") {
                $('.panel-title').html('Cruelty Information');
                $("#c7").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c6").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            }
            else {
                $('.panel-title').html('Abandonment Information');
                $("#c7").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c5").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                s--;
            }
        }
        else if (s == 8) {
            $('.panel-title').html('Children Information');
            $("#c8").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c7").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
        }
        else if (s == 9) {
            $('.panel-title').html('Court Orders Information');
            $("#c9").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c8").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
        }
        if (s > 1)
            s--;
        $("#step-" + s).show();
        e.stopPropagation();
    });
    $("#c1,#c2,#c3,#c4,#c5,#c6,#c7,#c8,#c9").click(function (e) {
        e.preventDefault();
        var x = parseInt($(this).attr('id').substring(1, 2));
        var j, ctr = 0, z;
        for (j = 1; j < x; j++) {
            var curStep = $("#step-" + j),
                       curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select").not(":hidden"),
                       isValid = true;
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
            if (isValid) {
                ctr++;
            }
        }
        if (ctr == x - 1) {
            if (x == 5) {
                if ($('#digrounds').val() != "2" && $('#digrounds').val() != "3") {
                    return;
                }
            }
            else if (x == 6) {
                if ($('#digrounds').val() != "4") {
                    return;
                }
            }
            for (z = 1; z < 10; z++) {
                $("#step-" + z).hide();
                $("#c" + z).removeClass('btn-primary').addClass('btn-default').attr('disabled');
            }
            if (x == 1) {
                $('.panel-title').html('Plaintiff Information');
            }
            if (x == 2) {
                $('.panel-title').html('Defendant Information');
                $('#bkBtn').show();
            }
            else if (x == 3) {
                $('.panel-title').html('Marriage Information');
            }
            else if (x == 4) {
                $('.panel-title').html('Divorce Information');
            }
            else if (x == 5) {
                if ($('#digrounds').val() == "2" || $('#digrounds').val() == "3") {
                    $('.panel-title').html('Abandonment Information');
                }
            }
            else if (x == 6) {
                if ($('#digrounds').val() == "4") {
                    $('.panel-title').html('Cruelty Information');
                }
            }
            else if (x == 7) {
                $('.panel-title').html('Children Information');
            }
            else if (x == 8) {
                $('.panel-title').html('Court Orders Information');
            }
            else if (x == 9) {
                $('.panel-title').html('HealthCare Information');
                $('#nxtBtn').hide();
                $('#sbmtBtn').show();
            }
            s = x;
            $("#c" + x).removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            if (x != 1)
                $('#bkBtn').show();
            $("#step-" + x).show();
        }
        else {
            var m = parseInt($('.setup-panel').find('.btn-primary').attr('id').substring(1, 2));
            var curStep = $("#step-" + m),
                       curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select").not(":hidden"),
                       isValid = true;
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
        }
        e.stopPropagation();
    });
});
$('#digrounds').change(function () {
    if ($(this).val() == "1") {
        $('#c6').parent().hide();
        $('#c5').parent().hide();
        $('#c7').html('5');
        $('#c8').html('6');
        $('#c9').html('7');
        $('#c7').parent().show();
        $('#c8').parent().show();
        $('#c9').parent().show();
    }
    if ($(this).val() == "2" || $(this).val() == "3") {
        $('#c6').parent().hide();
        $('#c5').parent().show();
        $('#c7').parent().show();
        $('#c8').parent().show();
        $('#c9').parent().show();
        $('#c7').html('6');
        $('#c8').html('7');
        $('#c9').html('8');
    }
    if ($(this).val() == "4") {
        $('#c5').parent().hide();
        $('#c6').parent().show();
        $('#c7').parent().show();
        $('#c8').parent().show();
        $('#c9').parent().show();
        $('#c7').html('6');
        $('#c8').html('7');
        $('#c9').html('8');
    }
});
$('#psex').change(function () {
    if ($('#psex').val() == "M") {
        $('#dsex').val('F');
        $('#pm').hide();
        $('#dm').show();
    }
    if ($('#psex').val() == "F") {
        $('#dsex').val('M');
        $('#pm').show();
        $('#dm').hide();
        ;
    }
});
$('#dsex').change(function () {
    if ($('#dsex').val() == "F") {
        $('#dm').show();
    }
    if ($('#psex').val() == "M") {
        $('#dm').hide();
        ;
    }
});
$('#pcheckssn').change(function () {
    if ($('#pcheckssn').val() == "Y") {
        $('#ps').show();
    }
    if ($('#pcheckssn').val() == "N") {
        $('#ps').hide();
    }
});
$('#dcheckssn').change(function () {
    if ($('#dcheckssn').val() == "Y") {
        $('#ds').show();
    }
    if ($('#dcheckssn').val() == "N" || $('#dcheckssn').val() == "R") {
        $('#ds').hide();
    }
});
$('#pcountry').change(function () {
    if ($('#pcountry').val() == "US") {
        $('#pc1').show();
        $('#pc2').show();
        $('#pc3').show();
    }
    else {
        $('#pc1').hide();
        $('#pc2').hide();
        $('#pc3').hide();
    }
});
$('#dcountry').change(function () {
    if ($('#dcountry').val() == "US") {
        $('#dc1').show();
        $('#dc2').show();
        $('#dc3').show();
    }
    else {
        $('#dc1').hide();
        $('#dc2').hide();
        $('#dc3').hide();
    }
});
$('#mcountry').change(function () {
    if ($('#mcountry').val() == "US") {
        $('.mc1').show();
    }
    else {
        $('.mc1').hide();
    }
});
$('#acountry').change(function () {
    if ($('#acountry').val() == "US") {
        $('#a1').show();
        $('#a2').show();
    }
    else {
        $('#a1').hide();
        $('#a2').hide();
    }
});
$('#chnumber').change(function () {
    if ($(this).val() == '1') {
        $('.ch1').show();
        $('.ch2').hide();
        $('.ch3').hide();
        $('.ch4').hide();
        $('.ch5').hide();
        $('.ch6').hide();
    }
    else if ($(this).val() == '2') {
        $('.ch1').show();
        $('.ch2').show();
        $('.ch3').hide();
        $('.ch4').hide();
        $('.ch5').hide();
        $('.ch6').hide();
    }
    else if ($(this).val() == '3') {
        $('.ch1').show();
        $('.ch2').show();
        $('.ch3').show();
        $('.ch4').hide();
        $('.ch5').hide();
        $('.ch6').hide();
    }
    else if ($(this).val() == '4') {
        $('.ch1').show();
        $('.ch2').show();
        $('.ch3').show();
        $('.ch4').show();
        $('.ch5').hide();
        $('.ch6').hide();
    }
    else if ($(this).val() == '5') {
        $('.ch1').show();
        $('.ch2').show();
        $('.ch3').show();
        $('.ch4').show();
        $('.ch5').show();
        $('.ch6').hide();
    }
    else if ($(this).val() == '6') {
        $('.ch1').show();
        $('.ch2').show();
        $('.ch3').show();
        $('.ch4').show();
        $('.ch5').show();
        $('.ch6').show();
    }
    else {
        $('.ch1').hide();
        $('.ch2').hide();
        $('.ch3').hide();
        $('.ch4').hide();
        $('.ch5').hide();
        $('.ch6').hide();
    }
});
$('#c1lives').change(function () {
    if ($(this).val() == 'T') {
        $('.t1').show();
    }
    else {
        $('.t1').hide();
    }
});
$('#c2lives').change(function () {
    if ($(this).val() == 'T') {
        $('.t2').show();
    }
    else {
        $('.t2').hide();
    }
});
$('#c3lives').change(function () {
    if ($(this).val() == 'T') {
        $('.t3').show();
    }
    else {
        $('.t3').hide();
    }
});
$('#c4lives').change(function () {
    if ($(this).val() == 'T') {
        $('.t4').show();
    }
    else {
        $('.t4').hide();
    }
});
$('#c5lives').change(function () {
    if ($(this).val() == 'T') {
        $('.t5').show();
    }
    else {
        $('.t5').hide();
    }
});
$('#c6lives').change(function () {
    if ($(this).val() == 'T') {
        $('.t6').show();
    }
    else {
        $('.t6').hide();
    }
});
$('#c1hasssn').change(function () {
    if ($(this).val() == 'Y') {
        $('.c1s').show();
    }
    else {
        $('.c1s').hide();
    }
});
$('#c2hasssn').change(function () {
    if ($(this).val() == 'Y') {
        $('.c2s').show();
    }
    else {
        $('.c2s').hide();
    }
});
$('#c3hasssn').change(function () {
    if ($(this).val() == 'Y') {
        $('.c3s').show();
    }
    else {
        $('.c3s').hide();
    }
});
$('#c4hasssn').change(function () {
    if ($(this).val() == 'Y') {
        $('.c4s').show();
    }
    else {
        $('.c4s').hide();
    }
});
$('#c5hasssn').change(function () {
    if ($(this).val() == 'Y') {
        $('.c5s').show();
    }
    else {
        $('.c5s').hide();
    }
});
$('#c6hasssn').change(function () {
    if ($(this).val() == 'Y') {
        $('.c6s').show();
    }
    else {
        $('.c6s').hide();
    }
});
$('input[type=tel]').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3');
        return true;
    }
    this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('input[type=tel]').bind('input', function (e) {
    if ($.isNumeric($(this).val())) {
        $(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3'));
        return true;
    }
});
$('.income').keyup(function (e) {
    this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('.ssn').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        this.value = this.value.replace(/(\d{3})\-?(\d{2})\-?(\d{4})/, '$1-$2-$3');
        return true;
    }
    this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('.zip').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        if ($(this).val().length >= 5 && typeof google != 'undefined') {
            var zip = $(this).val();
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            var pref = 0;
            geocoder.geocode({ 'address': zip }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++) {
                            var types = results[0].address_components[ii].types.join(",");
                            if ((types == "locality,political") && (pref > 1 || pref == 0)) {
                                $('.city').val(results[0].address_components[ii].long_name);
                                pref = 1;
                            }
                            else if ((types == "neighborhood,political") && (pref > 2 || pref == 0)) {
                                if (results[0].address_components[ii].long_name.toLowerCase().indexOf("bronx") >= 0) {
                                    $('.city').val('Bronx');
                                    pref = 1;
                                }
                                else {
                                    $('.city').val(results[0].address_components[ii].long_name);
                                    pref = 2;
                                }
                            }
                            else if (types == "political,sublocality,sublocality_level_1" && pref == 0) {
                                $('.city').val(results[0].address_components[ii].long_name);
                                pref = 3;
                            }
                            if (types == "administrative_area_level_1,political") {
                                $('.state').val(results[0].address_components[ii].short_name);
                            }
                            if (types == "administrative_area_level_2,political") {
                                $('#pcounty').val(results[0].address_components[ii].short_name.replace(" County", ""));
                            }
                        }
                    }
                }
            });
        }
    }
    else
        this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('.zip1').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        if ($(this).val().length >= 5 && typeof google != 'undefined') {
            var zip = $(this).val();
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            var pref = 0;
            geocoder.geocode({ 'address': zip }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++) {
                            var types = results[0].address_components[ii].types.join(",");
                            if ((types == "locality,political") && (pref > 1 || pref == 0)) {
                                $('.city1').val(results[0].address_components[ii].long_name);
                                pref = 1;
                            }
                            else if ((types == "neighborhood,political") && (pref > 2 || pref == 0)) {
                                if (results[0].address_components[ii].long_name.toLowerCase().indexOf("bronx") >= 0) {
                                    $('.city1').val('Bronx');
                                    pref = 1;
                                }
                                else {
                                    $('.city1').val(results[0].address_components[ii].long_name);
                                    pref = 2;
                                }
                            }
                            else if (types == "political,sublocality,sublocality_level_1" && pref == 0) {
                                $('.city1').val(results[0].address_components[ii].long_name);
                                pref = 3;
                            }
                            if (types == "administrative_area_level_1,political") {
                                $('.state1').val(results[0].address_components[ii].short_name);
                            }
                            if (types == "administrative_area_level_2,political") {
                                $('#dcounty').val(results[0].address_components[ii].short_name.replace(" County", ""));
                            }
                        }
                    }
                }
            });
        }
    }
    else
        this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('.zip2').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        if ($(this).val().length >= 5 && typeof google != 'undefined') {
            var zip = $(this).val();
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            var pref = 0;
            geocoder.geocode({ 'address': zip }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++) {
                            var types = results[0].address_components[ii].types.join(",");
                            if ((types == "locality,political") && (pref > 1 || pref == 0)) {
                                $('.city2').val(results[0].address_components[ii].long_name);
                                pref = 1;
                            }
                            else if ((types == "neighborhood,political") && (pref > 2 || pref == 0)) {
                                if (results[0].address_components[ii].long_name.toLowerCase().indexOf("bronx") >= 0) {
                                    $('.city2').val('Bronx');
                                    pref = 1;
                                }
                                else {
                                    $('.city2').val(results[0].address_components[ii].long_name);
                                    pref = 2;
                                }
                            }
                            else if (types == "political,sublocality,sublocality_level_1" && pref == 0) {
                                $('.city2').val(results[0].address_components[ii].long_name);
                                pref = 3;
                            }
                            if (types == "administrative_area_level_1,political") {
                                $('.state2').val(results[0].address_components[ii].short_name);
                            }
                        }
                    }
                }
            });
        }
    }
    else
        this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('.index').keyup(function (e) {
    var d = new Date();
    var y = d.getFullYear();
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        if (this.value.length == 6)
            this.value = this.value.replace(/(\d{6})/, '$1/' + y);
        return true;
    }
    this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('#coany').change(function () {
    if ($(this).val() == 'Y') {
        $('.corder').show();
    }
    else {
        $('.corder').hide();
    }
});
$('#conumber').change(function () {
    if ($(this).val() == '1') {
        $('.co1').show();
        $('.co2').hide();
        $('.co3').hide();
        $('.co4').hide();
    }
    else if ($(this).val() == '2') {
        $('.co1').show();
        $('.co2').show();
        $('.co3').hide();
        $('.co4').hide();
    }
    else if ($(this).val() == '3') {
        $('.co1').show();
        $('.co2').show();
        $('.co3').show();
        $('.co4').hide();
    }
    else if ($(this).val() == '4') {
        $('.co1').show();
        $('.co2').show();
        $('.co3').show();
        $('.co4').show();
    }
    else {
        $('.co1').hide();
        $('.co2').hide();
        $('.co3').hide();
        $('.co4').hide();
    }
});
$('#co1ordertype').change(function () {
    if ($(this).val() == 'O') {
        $('.pr1').show();
        $('.cs1').hide();
    }
    else if ($(this).val() == 'S') {
        $('.cs1').show();
        $('.pr1').hide();
    }
    else {
        $('.pr1').hide();
        $('.cs1').hide();
    }
});
$('#co2ordertype').change(function () {
    if ($(this).val() == 'O') {
        $('.pr2').show();
        $('.cs2').hide();
    }
    else if ($(this).val() == 'S') {
        $('.cs2').show();
        $('.pr2').hide();
    }
    else {
        $('.pr2').hide();
        $('.cs2').hide();
    }
});
$('#co3ordertype').change(function () {
    if ($(this).val() == 'O') {
        $('.pr3').show();
        $('.cs3').hide();
    }
    else if ($(this).val() == 'S') {
        $('.cs3').show();
        $('.pr3').hide();
    }
    else {
        $('.pr3').hide();
        $('.cs3').hide();
    }
});
$('#co4ordertype').change(function () {
    if ($(this).val() == 'O') {
        $('.pr4').show();
        $('.cs4').hide();
    }
    else if ($(this).val() == 'S') {
        $('.cs4').show();
        $('.pr4').hide();
    }
    else {
        $('.pr4').hide();
        $('.cs4').hide();
    }
});
$('#hplanholder').change(function () {
    if ($(this).val() == 'P' || $(this).val() == 'D') {
        $('.hcn1').hide();
        $('.hc1').show();
    }
    else {
        $('.hc1').hide();
        $('.hcn1').show();
    }
});