﻿var start, end, today, viewbage, viewbags;
$(document).ready(function () {
    $('a,.form-control,img').tooltip();
    $('.dates').datetimepicker({ format: 'MM/DD/YYYY' });
    $('.datetime').datetimepicker({
        format: 'MM/DD/YYYY HH:mm A',
    });
    $('.dates > :nth-child(2)').find("input").val(today);
    $(".__AjaxAntiForgeryForm").ajaxForm({
        success: function (responseText, statusText, xhr, $form) {
            $('.fancybox-loading').hide();            
            if (responseText == "False") {
                $('#insufficient-funds').html('Sub-Account or Master Account has INSUFFICIENT FUNDS.');
                return;
            }
            else if (responseText == "cl") {
                $('#account-closed').html('Sub-Account Already Closed.');
                return;
            }
            $(".__AjaxAntiForgeryForm")[0].reset();
            $.fancybox.close();
            location.reload();
        }
    });
    $("#__AjaxAntiForgeryForm-Transaction-Detail").ajaxForm({
        target: "#displayamount",
        success: function (responseText, statusText, xhr, $form) {
            responseText = "$" + responseText;
            $('.fancybox-loading').hide();
        }
    });
    if (viewbags) {
        start = moment(new Date(viewbags));
        end = moment(new Date(viewbage));
    }
    else {
        start = moment().subtract(29, 'days');
        end = moment();
    }
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    cb(start, end);
    $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
        $('#startdate').val(picker.startDate.format('MM/DD/YYYY'));
        $('#enddate').val(picker.endDate.format('MM/DD/YYYY'));
        $('#sortbydate').submit();
    });
});
$('.sbmtBtn').click(function () {
    var targetform = $(this).parents('form:first');
    if (targetform[0].checkValidity())
        $('.fancybox-loading').show();
});
$('.deposit,.withdraw,.transaction-detail,.close-account').click(function () {
    var id = $(this).data("id");
    var name = $(this).data("name");
    var classname = $(this).prop('class');
    if (~classname.indexOf("deposit")) {
        $('#add-funds-name').html(name);
        $('#add-funds-id').val(id);
    }
    else if (~classname.indexOf("withdraw")) {
        $('#withdraw-funds-name').html(name + "'S");
        $('#withdraw-funds-id').val(id);
    }
    else if (~classname.indexOf("close-account")) {
        $('#close-account-name').html(name + "'S");
        $('#close-account-id').val(id);
        $('#close-account-payee').val(name);
    }
    else if (~classname.indexOf("transaction-detail")) {
        $('#transaction-detail-name').html(name);
        $('#transaction-detail-id').val(id);
        $('#displayamount').html('');
        var transactiontypetext = "",operatortype="";
        var dateoperation = $(this).data("dateoperation");
        var checkno = $(this).data("checkno");
        var payableto = $(this).data("payableto");
        var transactiontype = $(this).data("transactiontype");
        var transactionamount = $(this).data("transactionamount");
        var remarks = $(this).data("remarks");
        if (transactiontype == '1') {
            transactiontypetext = "A/C Opening Deposit";
            operatortype = "Depositor:";
        }            
        else if (transactiontype == '2') {
            transactiontypetext = "Deposit";
            operatortype = "Depositor:";
        }         
        else if (transactiontype == '3') {
            transactiontypetext = "Withdrawal";
            operatortype = "Payee:";
        }     
        else if (transactiontype == '4') {
            transactiontypetext = "A/C Closure";
            operatortype = "Payee:";
        }
        $('.fancybox-loading').show();
        $('#__AjaxAntiForgeryForm-Transaction-Detail').submit(); 
        $('#f1').html(dateoperation);
        $('#f2').html(transactiontypetext);
        $('#f3').html("$" + transactionamount);
        if (checkno != '0')
            $('#f4').html(checkno);
        else
            $('#f4').html('');
        $('#f5').html(operatortype);
        $('#f6').html(payableto);
        $('#f7').html(remarks);
    }
});
