﻿$(document).ready(function () {
    if ($("#casetype").val() === 'Civ') {
        $('.criminal').hide();
        $('.civil').show();
    }
    else if ($("#casetype").val() === 'Cri') {
        $('.civil').hide();
        $('.criminal').show();
    }
});
$("#casetype").change(function (e) {
    if ($(this).val() === 'Civ') {
        $('.criminal').hide();
        $('.civil').show();
    }
    else if ($(this).val() === 'Cri') {
        $('.civil').hide();
        $('.criminal').show();
    }
});
$("#criminal").click(function (e) {
    $('#__AjaxAntiForgeryForm').attr('action', '/Criminal/CriminalCaseQuestionnaire').submit();
});
$("#personalinjury").click(function (e) {
    $('#__AjaxAntiForgeryForm').attr('action', '/PersonalInjury/PersonalInjuryQuestionnaire').submit();
});
$("#immigration").click(function (e) {
    $('#__AjaxAntiForgeryForm').attr('action', '/Immigration/ImmigrationCaseQuestionnaire').submit();
});
$("#bankruptcy").click(function (e) {
    $('#__AjaxAntiForgeryForm').attr('action', '/Bankruptcy/BankruptcyPetitionQuestionnaire').submit();
});
