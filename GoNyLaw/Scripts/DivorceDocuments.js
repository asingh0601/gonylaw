﻿$("#mainnav-menu>li:nth-child(2)").next().addClass("active-link");
$(document).ready(function (e) {
    $('img').tooltip();
    $("#xx").hide();
});
$("#defendantdownload").click(function (e) {
    $("#downloaddefendant").submit();
});
$("#defendantnossndownload").click(function (e) {
    $("#downloaddefendantnossn").submit();
});
$("#plaintiffnossndownload").click(function (e) {
    $("#downloadplaintiffnossn").submit();
});
$("#childrennossndownload").click(function (e) {
    $("#downloadchildrennossn").submit();
});
$("#childrenrefusessndownload").click(function (e) {
    $("#downloadchildrenrefusessn").submit();
});
$("#plaintiffdownload").click(function (e) {
    $("#downloadplaintiff").submit();
});
$("#defendantrefusessndownload").click(function (e) {
    $("#downloaddefendantrefusessn").submit();
});
$("#regularitydownload").click(function (e) {
    $("#downloadregularity").submit();
});
$("#servicedownload").click(function (e) {
    $("#downloadservice").submit();
});
$("#authorizationdownload").click(function (e) {
    $("#downloadauthorization").submit();
});
$("#supportsheetdownload").click(function (e) {
    $("#downloadsupportsheet").submit();
});
$("#supportsheetrefdownload").click(function (e) {
    $("#downloadsupportrefsheet").submit();
});
$("#factsdownload").click(function (e) {
    $("#downloadfacts").submit();
});
$("#judgementdownload").click(function (e) {
    $("#downloadjudgement").submit();
});
$("#notedownload").click(function (e) {
    $("#downloadnote").submit();
});
$("#noticedownload").click(function (e) {
    $("#downloadnotice").submit();
});
$("#othersdownload").click(function (e) {
    $("#downloadothers").submit();
});
$("#partdownload").click(function (e) {
    $("#downloadpart").submit();
});
$("#rjidownload").click(function (e) {
    $("#downloadrji").submit();
});
$("#barriersdownload").click(function (e) {
    $("#downloadbarriers").submit();
});
$("#summonsdownload").click(function (e) {
    $("#downloadsummons").submit();
});
$("#verifieddownload").click(function (e) {
    $("#downloadverified").submit();
});