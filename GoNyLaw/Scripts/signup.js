﻿$(document).ready(function () {
    $('.cssload-loader').hide();
    $('.form-control').tooltip();
    $('.dates').datetimepicker({ format: 'MM/DD/YYYY' });
    $('.datetime').datetimepicker({
        format: 'MM/DD/YYYY HH:mm A',
    });
});
$('input[type=tel]').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3');
        return true;
    }
    this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('input[type=tel]').bind('input', function (e) {
    if ($.isNumeric($(this).val())) {
        $(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3'));
        return true;
    }
});
$('#zip').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        if ($(this).val().length >= 5 && typeof google != 'undefined') {
            var zip = $(this).val();
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            var pref = 0;
            geocoder.geocode({ 'address': zip }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++) {
                            var types = results[0].address_components[ii].types.join(",");
                            if ((types == "locality,political") && (pref > 1 || pref == 0)) {
                                $('#city').val(results[0].address_components[ii].long_name);
                                pref = 1;
                            }
                            else if ((types == "neighborhood,political") && (pref > 2 || pref == 0)) {
                                if (results[0].address_components[ii].long_name.toLowerCase().indexOf("bronx") >= 0) {
                                    $('#city').val('Bronx');
                                    pref = 1;
                                }
                                else {
                                    $('#hcity').val(results[0].address_components[ii].long_name);
                                    pref = 2;
                                }
                            }
                            else if (types == "political,sublocality,sublocality_level_1" && pref == 0) {
                                $('#city').val(results[0].address_components[ii].long_name);
                                pref = 3;
                            }
                            if (types == "administrative_area_level_1,political") {
                                $('#state').val(results[0].address_components[ii].short_name);
                            }
                        }
                    }
                }
            });
        }
    }
    else
        this.value = this.value.replace(/[^\-0-9]/g, '');
});