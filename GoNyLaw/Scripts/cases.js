﻿$('.ui-tooltip').tooltip();
$('.details>img.edit').on("click", function (e) {
    $(this).parent().parent().parent().parent().children('.formedit').submit();
});
$('.details>img.view').on("click", function (e) {
    $(this).parent().parent().parent().parent().children('.formview').submit();
});
$('.details>img.logs').on("click", function (e) {
    $(this).parent().parent().parent().parent().children('.formlogs').submit();
});
$('.details>img.miscemail').on("click", function (e) {
    $(this).parent().parent().parent().parent().children('.formmiscemail').submit();
});
$('.clientemail').click(function (e) {
    $(this).parent().parent().parent().parent().children('.formclientemail').submit();
});
$('.adaemail').click(function (e) {
    $(this).parent().parent().parent().parent().children('.formadaemail').submit();
});
$('.judgeemail').click(function (e) {
    $(this).parent().parent().parent().parent().children('.formjudgeemail').submit();
});
$('.clientsms').click(function (e) {
    $(this).parent().parent().parent().parent().children('.formclientsms').submit();
});
$('.details>img.task').on("click", function (e) {
    $(this).parent().parent().parent().parent().children('.formtask').submit();
});
$('.details>img.viewtask').on("click", function (e) {
    $(this).parent().parent().parent().parent().children('.formviewtask').submit();
});
$('.details>img.legal').on("click", function (e) {
    $(this).parent().parent().parent().parent().children('.formlegal').submit();
});
$('.details>img.upayment').on("click", function (e) {
    $(this).parent().parent().parent().parent().children('.formupayment').submit();
});
$('.details>img.vpayment').on("click", function (e) {
    $(this).parent().parent().parent().parent().children('.formvpayment').submit();
});
$('.details>img.delete').on("click", function (e) {
    var origin = $(this);
    bootbox.dialog({
        message: "<i style='font-weight:600'>Press CONFIRM to proceed. Once confirmed, you CAN NOT UNDO this action !!!</i>",
        title: "<h4 style='color:red'>DELETE this Case?</h4>",
        buttons: {
            success: {
                label: "Cancel",
                className: "btn-success",
                callback: function () {

                }
            },
            danger: {
                label: "Confirm",
                className: "btn-danger",
                callback: function () {
                    origin.parent().parent().parent().parent().children('.formdel').submit();
                }
            }
        }
    });
});
