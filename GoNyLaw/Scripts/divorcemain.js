﻿var divorce;
$('.form-control').tooltip();
$('.delete').on("click", function (e) {
    var origin = $(this);
    bootbox.dialog({
        message: "<i style='font-weight:600'>Press CONFIRM to proceed. Once confirmed, you CAN NOT UNDO this action !!!</i>",
        title: "<h4 style='color:red'>DELETE this Case?</h4>",
        buttons: {
            success: {
                label: "Cancel",
                className: "btn-success",
                callback: function () {

                }
            },
            danger: {
                label: "Confirm",
                className: "btn-danger",
                callback: function () {
                    origin.parent().parent().parent().children('.formdel').submit();
                }
            }
        }
    });
});
$('.details>img.view').click(function (e) {
    $(this).parent().parent().parent().children('#formview').submit();
});
$('.details>img.uploads').click(function (e) {
    $(this).parent().parent().parent().children('#formupload').submit();
});
$('.unpaid>img').click(function (e) {
    $(this).parent().parent().parent().children('#formpay').submit();
});
$('.details>img.ack').click(function (e) {
    $(this).parent().parent().parent().children('#formack').submit();
});
$('.details>img.docs').click(function (e) {
    $(this).parent().parent().parent().children('#formdocs').submit();
});
$('input').not('.visible').hide();
$("#mainnav-menu>li:nth-child(2)").next().addClass("active-link");
function DivorceCase(data) {
    var self = this;

    if (!data) {
        data = {};
    }
    self.case_id = ko.observable(data.case_id);
    self.t_txn_id1 = ko.observable(data.t.txn_id1);
    self.t_amount1 = ko.observable(data.t.amount1);
    self.t_date1 = ko.observable(data.t.date1);
    self.t_txn_id2 = ko.observable(data.t.txn_id2);
    self.t_amount2 = ko.observable(data.t.amount2);
    self.t_date2 = ko.observable(data.t.date2);
};
$(document).ready(function () {
    $('.ui-tooltip').tooltip();
    $("a.inline").fancybox({
        'hideOnContentClick': true,
        autoDimensions: true
    });
});
$('.agencyname').click(function (e) {
    $(this).parent().siblings('form').submit();
});
$("#__AjaxAntiForgeryForm").ajaxForm({
    success: function (responseText, statusText, xhr, $form) {
        $('.fancybox-loading').hide();
        if (responseText == "False") {
           return;
        }
        $.fancybox.close();
        $("#__AjaxAntiForgeryForm")[0].reset();
        alert('Transferred !!!')
    }
});
function getecourtstatus(index_no) {
    $('#txtIndex').val(index_no);
    $('#ecourtsform').submit();
}