﻿$('.datetime').on('dp.change', function (e) {
    if (e.oldDate === null) {
        $(this).data('DateTimePicker').date(new Date(e.date._d.setHours(09, 00, 00)));
    }
});
$('.datetime').on('focusout', function (e) {
    var d = $(this).val();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = d.getFullYear() + '/' +
    (month < 10 ? '0' : '') + month + '/' +
    (day < 10 ? '0' : '') + day;
    $(".datetime").val(output + " 09:00");
});
$('input[type=tel]').keyup(function (e) {
    if (e.keyCode > 36 && e.keyCode < 41) {
        return true;
    }
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3');
        return true;
    }
    this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('input[type=tel]').bind('input', function (e) {
    if ($.isNumeric($(this).val())) {
        $(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3'));
        return true;
    }
});
$('.ssn').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        this.value = this.value.replace(/(\d{3})\-?(\d{2})\-?(\d{4})/, '$1-$2-$3');
        return true;
    }
    this.value = this.value.replace(/[^\-0-9]/g, '');
});
$(document).ready(function () {
    $('form').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
    $('.form-control').tooltip();
    $("input,select").prop('required', false);
    $('.dates').datetimepicker({ format: 'MM/DD/YYYY' });
    $('.datetime').datetimepicker({
        format: 'MM/DD/YYYY HH:mm A',
    });
    $('#prior_accident,#ambulancecalled,#admittedhospital,#admittedclinic,#visiteddoctor,#mri_done,#emg_done,#ekg_done,#xray_done,#ime_done,#expert_done,#anycar').val('N');
    var s = 1;
    $('#nxtBtn').click(function (e) {
        e.preventDefault();
        var curStep = $("#step-" + s),
                    curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select").not(":hidden"),
                    isValid = true;
        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid) {
            $("#step-" + s).hide();
            if (s < 7)
                s++;
            if (s == 2) {
                $('.main-title').html('Employment History');
                $("#c1").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c2").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                $('#bkBtn').show();
            }
            else if (s == 3) {
                $('.main-title').html('Accident Information');
                $("#c2").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c3").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            }
            else if (s == 4) {
                $('.main-title').html('Medical Information');
                $("#c3").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c4").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            }
            else if (s == 5) {
                $('.main-title').html('Defendant Information');
                $("#c4").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c5").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            }
            else if (s == 6) {
                $('.main-title').html('Notices & Filings');
                $("#c5").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c6").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                $('#nxtBtn').hide();
                $('#sbmtBtn').show();
            }
            $("#step-" + s).show();
        }
        e.stopPropagation();
    });
    $('#sbmtBtn').click(function (e) {
        e.preventDefault();
        var curStep = $("#step-" + s),
                    curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select").not(":hidden"),
                    isValid = true;
        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid) {
            $("input:hidden,select:hidden").prop('required', false);
            $("#__AjaxAntiForgeryForm").submit();
        }
        e.stopPropagation();
    });
    $('#bkBtn').click(function (e) {
        e.preventDefault();
        $('#sbmtBtn').hide();
        $('#nxtBtn').show();
        $("#step-" + s).hide();
        if (s == 2) {
            $('.main-title').html('Client Information');
            $("#c2").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c1").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            $('#bkBtn').hide();
        }
        else if (s == 3) {
            $('.main-title').html('Employment History');
            $("#c3").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c2").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
        }
        else if (s == 4) {
            $('.main-title').html('Accident Information');
            $("#c4").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c3").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
        }
        else if (s == 5) {
            $('.main-title').html('Medical Information');
            $("#c5").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c4").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
        }
        else if (s == 6) {
            $('.main-title').html('Defendant Information');
            $("#c6").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c5").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
        }
        if (s > 1)
            s--;
        $("#step-" + s).show();
        e.stopPropagation();
    });
    $("#c1,#c2,#c3,#c4,#c5,#c6").click(function (e) {
        e.preventDefault();
        var x = parseInt($(this).attr('id').substring(1, 2));
        var j, ctr = 0, z;
        for (j = 1; j < x; j++) {
            var curStep = $("#step-" + j),
                       curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select").not(":hidden"),
                       isValid = true;
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
            if (isValid) {
                ctr++;
            }
        }
        if (ctr == x - 1) {
            for (z = 1; z < 7; z++) {
                $("#step-" + z).hide();
                $("#c" + z).removeClass('btn-primary').addClass('btn-default').attr('disabled');
            }
            $('#sbmtBtn').hide();
            $('#nxtBtn').show();
            $('#bkBtn').hide();
            if (x == 1) {
                $('.main-title').html('Client Information');
            }
            if (x == 2) {
                $('.main-title').html('Employment History');
                $('#bkBtn').show();
            }
            else if (x == 3) {
                $('.main-title').html('Accident Information');
            }
            else if (x == 4) {
                $('.main-title').html('Medical Information');
            }
            else if (x == 5) {
                $('.main-title').html('Defendant Information');
            }
            else if (x == 6) {
                $('.main-title').html('Notices & Filings');
                $('#sbmtBtn').show();
                $('#nxtBtn').hide();
            }
            s = x;
            $("#c" + x).removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            if (x != 1)
                $('#bkBtn').show();
            $("#step-" + x).show();
        }
        else {
            var m = parseInt($('.setup-panel').find('.btn-primary').attr('id').substring(1, 2));
            var curStep = $("#step-" + m),
                       curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select").not(":hidden"),
                       isValid = true;
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
        }
        e.stopPropagation();
    });
    var collapsed = true;
    $('#collapse-init').click(function () {
        var origin = $(this);
        $('.panel-collapse').each(function () {
            if (collapsed) {
                $(this).addClass('in');
                //$(this).prev('.panel-heading').find(".more-less").removeClass('glyphicon-plus').addClass('glyphicon-minus');
                $(this).prev('.panel-heading').find("a").attr('aria-expanded', 'true');
                origin.text('Collapse All');
            }
            else {
                $(this).removeClass('in');
                //$(this).prev('.panel-heading').find(".more-less").removeClass('glyphicon-minus').addClass('glyphicon-plus');
                $(this).prev('.panel-heading').find("a").attr('aria-expanded', 'false');
                origin.text('Expand All');
            }
        });
        collapsed = (true == collapsed) ? false : true;
    });
});
//function toggleIcon(e) {
//    $(e.target)
//        .prev('.panel-heading')
//        .find(".more-less")
//        .toggleClass('glyphicon-plus glyphicon-minus');
//}
//$('.panel-group').on('hidden.bs.collapse', toggleIcon);
//$('.panel-group').on('shown.bs.collapse', toggleIcon);
var values = { name: '',doa:'' };
$('#fullname').keyup(function () {
    values.name = $(this).val();
}).keyup();
values.watch('name', function (id, oldval, newval) {
    $('#cname').html(newval);
    return newval;
});
$('#civil_status').change(function () {
    $('.married').addClass('hidden');
    $('.divorced').addClass('hidden');
    $('.widowed').addClass('hidden');
    if ($(this).val() == 'M') {
        $('.married').removeClass('hidden');
    }
    else if ($(this).val() == 'D') {
        $('.divorced').removeClass('hidden');
    }
    else if ($(this).val() == 'W') {
        $('.widowed').removeClass('hidden');
    }
});
$('#isemployed').change(function () {
    if ($(this).val() == 'Y') {
        $('.employed').removeClass('hidden');
    }
    else {
        $('.employed').addClass('hidden');
    }
});
$('#missedjob').change(function () {
    if ($(this).val() == 'Y') {
        $('.missedthejob').removeClass('hidden');
    }
    else {
        $('.missedthejob').addClass('hidden');
    }
});
$('#cause').change(function () {
    if ($(this).val() == 'O') {
        $(this).parent().parent().parent().parent().next().removeClass('hidden');
    }
    else {
        $(this).parent().parent().parent().parent().next().addClass('hidden');
    }
});
$('#prior_accident').change(function () {
    if ($(this).val() == 'Y') {
        $('.prioraccidentdetails').removeClass('hidden');
    }
    else {
        $('.prioraccidentdetails').addClass('hidden');
    }
});
$('#prior_treatment_facilities').change(function () {
    var num = parseInt($(this).val());
    for (i = 1; i <= 5; i++) {
        if (i <= num) $('.facility' + i).removeClass('hidden');
        else $('.facility' + i).addClass('hidden');
    }
});
$('#was_award').change(function () {
    if ($(this).val() == 'Y') {
        $(this).parent().parent().parent().parent().next().removeClass('hidden');
    }
    else {
        $(this).parent().parent().parent().parent().next().addClass('hidden');
    }
});
var dateaccident;
$('.datetime').on('dp.change', function (e) {
    dateaccident = new Date(e.date);
    values.doa = ("0" + (dateaccident.getMonth() + 1)).slice(-2) + '/' + ("0" + dateaccident.getDate()).slice(-2) + '/' + dateaccident.getFullYear();
});
values.watch('doa', function (id, oldval, newval) {
    $('#borderdoa').html(newval);
    return newval;
});
$('#ambulancecalled').change(function () {
    if ($(this).val() == 'Y') {
        $('.ambulancedetails').removeClass('hidden');
    }
    else {
        $('.ambulancedetails').addClass('hidden');
    }
});
$('#ambulance_service').change(function () {
    if ($(this).val() == 'E') {
        $(this).parent().parent().parent().parent().next().removeClass('hidden');
    }
    else {
        $(this).parent().parent().parent().parent().next().addClass('hidden');
    }
});
$('#admittedhospital,#admittedclinic,#visiteddoctor').change(function () {
    if ($(this).val() == 'Y') {
        $(this).parent().parent().parent().parent().next().removeClass('hidden');
    }
    else {
        $(this).parent().parent().parent().parent().next().addClass('hidden');
    }
});
$('#no_of_hospitals').change(function () {
    var num = parseInt($(this).val());
    for (i = 1; i <= 5; i++) {
        if (i <= num) $('.hospital' + i).removeClass('hidden');
        else $('.hospital' + i).addClass('hidden');
    }
});
$('#no_of_clinics').change(function () {
    var num = parseInt($(this).val());
    for (i = 1; i <= 5; i++) {
        if (i <= num) $('.clinic' + i).removeClass('hidden');
        else $('.clinic' + i).addClass('hidden');
    }
});
$('#no_of_doctors').change(function () {
    var num = parseInt($(this).val());
    for (i = 1; i <= 10; i++) {
        if (i <= num) $('.doctor' + i).removeClass('hidden');
        else $('.doctor' + i).addClass('hidden');
    }
});
$('#mri_done,#emg_done,#ekg_done,#xray_done,#ime_done,#expert_done,#anycar').change(function () {
    if ($(this).val() == 'Y') {
        $(this).parent().parent().parent().parent().next().removeClass('hidden');
    }
    else {
        $(this).parent().parent().parent().parent().next().addClass('hidden');
    }
});
$('#no_of_mri').change(function () {
    var num = parseInt($(this).val());
    for (i = 1; i <= 10; i++) {
        if (i <= num) $('.mri' + i).removeClass('hidden');
        else $('.mri' + i).addClass('hidden');
    }
});
$('#no_of_emg').change(function () {
    var num = parseInt($(this).val());
    for (i = 1; i <= 10; i++) {
        if (i <= num) $('.emg' + i).removeClass('hidden');
        else $('.emg' + i).addClass('hidden');
    }
});
$('#no_of_ekg').change(function () {
    var num = parseInt($(this).val());
    for (i = 1; i <= 10; i++) {
        if (i <= num) $('.ekg' + i).removeClass('hidden');
        else $('.ekg' + i).addClass('hidden');
    }
});
$('#no_of_xray').change(function () {
    var num = parseInt($(this).val());
    for (i = 1; i <= 10; i++) {
        if (i <= num) $('.xray' + i).removeClass('hidden');
        else $('.xray' + i).addClass('hidden');
    }
});
$('.nextother').change(function () {
    if ($(this).val() == 'O') {
        $(this).parent().parent().parent().parent().next().removeClass('hidden');
    }
    else {
        $(this).parent().parent().parent().parent().next().addClass('hidden');
    }
});
var ts, te;
var days, diff;
$('.ts').on('dp.change', function (e) {
    ts = new Date(e.date);
    if (te != null) {
        diff = te - ts;
        days = diff / 1000 / 60 / 60 / 24;
        $(this).parent().parent().next().next().find('input').val(Math.round(days)+1);
    }
});
$('.te').on('dp.change', function (e) {
    te = new Date(e.date);
    if (ts != null) {
        diff = te - ts;
        days = diff / 1000 / 60 / 60 / 24;
        $(this).parent().parent().next().find('input').val(Math.round(days)+1);
    }
});
$('.subpoenaedques').change(function () {
    if ($(this).val() == 'Y') {
        $(this).parent().parent().parent().parent().next().removeClass('hidden');
    }
    else {
        $(this).parent().parent().parent().parent().next().addClass('hidden');
    }
});
$('#no_of_defendants').change(function () {
    var num = parseInt($(this).val());
    for (i = 1; i <= 10; i++) {
        if (i <= num) $('.def' + i).removeClass('hidden');
        else $('.def' + i).addClass('hidden');
    }
});
$('.driverinfoques').change(function () {
    if ($(this).val() == 'N') {
        $(this).parent().parent().parent().parent().next().removeClass('hidden');
    }
    else {
        $(this).parent().parent().parent().parent().next().addClass('hidden');
    }
});
$('.noofattorney').change(function () {
    $(this).parent().parent().parent().parent().next().addClass('hidden');
    $(this).parent().parent().parent().parent().next().next().addClass('hidden');
    $(this).parent().parent().parent().parent().next().next().next().addClass('hidden');
    var noofattorney = parseInt($(this).val());
    if (noofattorney >= 1) $(this).parent().parent().parent().parent().next().removeClass('hidden');
    if (noofattorney >= 2) $(this).parent().parent().parent().parent().next().next().removeClass('hidden');
    if (noofattorney == 3) $(this).parent().parent().parent().parent().next().next().next().removeClass('hidden');
});
$("#court").change(function () {
    if ($(this).val() == 'S' || $(this).val() == 'C') {
        $("#court_county option[value='9']").hide();
        $("#court_county option[value='10']").hide();
        for (i = 1; i <= 8; i++) {
            $("#court_county option[value='" + i + "']").show();
        }
    }
    else if ($(this).val() == 'F') {
        for (i = 1; i <= 8; i++) {
            $("#court_county option[value='"+i+"']").hide();
        }
        $("#court_county option[value='9']").show();
        $("#court_county option[value='10']").show();
    }
    else {
        for (i = 1; i <= 8; i++) {
            $("#court_county option[value='" + i + "']").show();
        }
        $("#court_county option[value='9']").show();
        $("#court_county option[value='10']").show();
    }
});