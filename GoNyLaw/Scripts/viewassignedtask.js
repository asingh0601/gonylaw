﻿$('#typeofcase').change(function () {
    if ($(this).val() == null || $(this).val() == "")
        return;
    $('#__AntiForgeryForm').submit();
});
$('.setcomplete').on("click", function (e) {
    var origin = $(this);
    e.preventDefault();
    bootbox.dialog({
        message: "<i style='font-weight:600'>Press CONFIRM to proceed. Once confirmed, you CAN NOT UNDO this action !!!<input type='text' id='bb1' placeholder='Notes' style='width:5in;'/></i>",
        title: "<h4 style='color:red'>Mark this task as COMPLETE?</h4>",
        buttons: {
            success: {
                label: "Cancel",
                className: "btn-success",
                callback: function () {
                    
                }
            },
            danger: {
                label: "Confirm",
                className: "btn-danger",
                callback: function () {
                    $('.setcomplete').prop('href', $('.setcomplete').prop('href') + '&text=' + $('#bb1').val());
                    origin.off('click')[0].click();
                }
            }
        }
    });
});