﻿$(document).ready(function () {
    $('form').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
    //$(".fancybox").click();
    $('.form-control').tooltip();
    $("#mainnav-menu>li:nth-child(2)").next().addClass("active-link");
    //$("input,select").prop('required', false);
    if ($('#pssn').val() != '' && $('#pssn').val() != null)
        $('#ps').show();
    else
        $('#ps').hide();
    if ($('#dssn').val() != '' && $('#dssn').val() != null)
        $('#ds').show();
    else
        $('#ds').hide();
    if ($('#mcountry').val() == 'US')
        $('.mc1').show();
    else
        $('.mc1').hide();
    if (($('#pmaiden').val() != '' && $('#pmaiden').val() != null) || $('#psex').val() == 'F')
        $('#pm').show();
    else
        $('#pm').hide();
    if (($('#dmaiden').val() != '' && $('#dmaiden').val() != null) || $('#dsex').val() == 'F')
        $('#dm').show();
    else
        $('#dm').hide();
    $('#c5').parent().hide();
    $('#c6').parent().hide();
    $('.dates').datetimepicker({ format: 'MM/DD/YYYY' });
    if ($('#pcountry').val() == 'US') {
        $('#pc1').show();
        $('#pc2').show();
        $('#pc3').show();
    }
    else {
        $('#pc1').hide();
        $('#pc2').hide();
        $('#pc3').hide();
    }
    if ($('#dcountry').val() == 'US') {
        $('#dc1').show();
        $('#dc2').show();
        $('#dc3').show();
    }
    else {
        $('#dc1').hide();
        $('#dc2').hide();
        $('#dc3').hide();
    }
    var s = 1;
    $('#nxtBtn').click(function (e) {
        e.preventDefault();
        var curStep = $("#step-" + s),
                    curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select").not(":hidden"),
                    isValid = true;
        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid) {
            $("#step-" + s).hide();
            if (s < 6)
                s++;
            if (s == 2) {
                $('.panel-title').html('Defendant Information');
                $("#c1").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c2").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                $('#bkBtn').show();
            }
            else if (s == 3) {
                $('.panel-title').html('Marriage Information');
                $("#c2").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                $("#c3").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            }
            else if (s == 4 || s == 5 || s == 6) {
                if (s == 4 && $("#digrounds").val() == "1") {
                    $('#nxtBtn').hide();
                    $('#sbmtBtn').show();
                    $('.panel-title').html('Residency Information');
                    $("#c3").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                    $("#c4").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                }
                else if (s == 4) {
                    $('.panel-title').html('Residency Information');
                    $("#c3").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                    $("#c4").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                }
                else if (s == 5) {
                    if ($('#digrounds').val() == "4") {
                        s++;
                        $('.panel-title').html('Grounds Information');
                        $("#c4").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                        $("#c6").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                        $('#nxtBtn').hide();
                        $('#sbmtBtn').show();
                    }
                    else {
                        $('.panel-title').html('Grounds Information');
                        $("#c4").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                        $("#c5").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                        $('#nxtBtn').hide();
                        $('#sbmtBtn').show();
                    }
                }
                else if (s == 6) {
                    $('.panel-title').html('Grounds Information');
                    $("#c4").removeClass('btn-primary').addClass('btn-default').attr('disabled');
                    $("#c6").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
                    $('#nxtBtn').hide();
                    $('#sbmtBtn').show();
                }
            }
            $("#step-" + s).show();
        }
        e.stopPropagation();
    });
    $('#sbmtBtn').click(function (e) {
        e.preventDefault();
        var curStep = $("#step-" + s),
                    curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select").not(":hidden"),
                    isValid = true;
        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid) {
            $("input:hidden,select:hidden").prop('required', false);
            $("#__AjaxAntiForgeryForm").submit();
        }
        e.stopPropagation();
    });
    $('#bkBtn').click(function (e) {
        e.preventDefault();
        $('#sbmtBtn').hide();
        $('#nxtBtn').show();
        $("#step-" + s).hide();
        if (s == 2) {
            $('.panel-title').html('Plaintiff Information');
            $("#c2").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c1").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            $('#bkBtn').hide();
        }
        else if (s == 3) {
            $('.panel-title').html('Defendant Information');
            $("#c3").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c2").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
        }
        else if (s == 4) {
            $('.panel-title').html('Marriage Information');
            $("#c4").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c3").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
        }
        else if (s == 5) {
            $('.panel-title').html('Residency Information');
            $("#c5").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c4").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
        }
        else if (s == 6) {
            $('.panel-title').html('Residency Information');
            $("#c6").removeClass('btn-primary').addClass('btn-default').attr('disabled');
            $("#c4").removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            s--;
        }
        if (s > 1)
            s--;
        $("#step-" + s).show();
        e.stopPropagation();
    });
    $("#c1,#c2,#c3,#c4,#c5,#c6").click(function (e) {
        e.preventDefault();
        var x = parseInt($(this).attr('id').substring(1, 2));
        var j, ctr = 0, z;
        for (j = 1; j < x; j++) {
            var curStep = $("#step-" + j),
                       curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select").not(":hidden"),
                       isValid = true;
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
            if (isValid) {
                ctr++;
            }
        }
        if (ctr == x - 1) {
            if (x == 5) {
                if ($('#digrounds').val() != "2" && $('#digrounds').val() != "3") {
                    return;
                }
            }
            else if (x == 6) {
                if ($('#digrounds').val() != "4") {
                    return;
                }
            }
            for (z = 1; z < 7; z++) {
                $("#step-" + z).hide();
                $("#c" + z).removeClass('btn-primary').addClass('btn-default').attr('disabled');
            }
            if (x == 1) {
                $('.panel-title').html('Plaintiff Information');
            }
            if (x == 2) {
                $('.panel-title').html('Defendant Information');
                $('#bkBtn').show();
            }
            else if (x == 3) {
                $('.panel-title').html('Marriage Information');
            }
            else if (x == 4) {
                $('.panel-title').html('Residency Information');
                if ($('#digrounds').val() == "1") {
                    $('#sbmtBtn').show();
                    $('#nxtBtn').hide();
                }
            }
            else if (x == 5) {
                if ($('#digrounds').val() == "2" || $('#digrounds').val() == "3") {
                    $('.panel-title').html('Grounds Information');
                    $('#sbmtBtn').show();
                    $('#nxtBtn').hide();
                }
            }
            else if (x == 6) {
                if ($('#digrounds').val() == "4") {
                    $('.panel-title').html('Grounds Information');
                    $('#sbmtBtn').show();
                    $('#nxtBtn').hide();
                }
            }
            s = x;
            $("#c" + x).removeClass('btn-default').addClass('btn-primary').removeAttr('disabled');
            if (x != 1)
                $('#bkBtn').show();
            $("#step-" + x).show();
        }
        else {
            var m = parseInt($('.setup-panel').find('.btn-primary').attr('id').substring(1, 2));
            var curStep = $("#step-" + m),
                       curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select").not(":hidden"),
                       isValid = true;
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
        }
        e.stopPropagation();
    });
});
$('#digrounds').change(function () {
    if ($(this).val() == "1") {
        $('#c6').parent().hide();
        $('#c5').parent().hide();
        $('#nxtBtn').hide();
        $('#sbmtBtn').show();
    }
    else {
        $('#sbmtBtn').hide();
        $('#nxtBtn').show();
    }
    if ($(this).val() == "2" || $(this).val() == "3") {
        $('#c6').parent().hide();
        $('#c5').parent().show();
    }
    if ($(this).val() == "4") {
        $('#c5').parent().hide();
        $('#c6').parent().show();
    }
});
$('#psex').change(function () {
    if ($('#psex').val() == "M") {
        $('#dsex').val('F');
        $('#pm').hide();
        $('#dm').show();
    }
    if ($('#psex').val() == "F") {
        $('#dsex').val('M');
        $('#pm').show();
        $('#dm').hide();
        ;
    }
});
$('#dsex').change(function () {
    if ($('#dsex').val() == "F") {
        $('#dm').show();
    }
    if ($('#psex').val() == "M") {
        $('#dm').hide();
        ;
    }
});
$('#pcheckssn').change(function () {
    if ($('#pcheckssn').val() == "Y") {
        $('#ps').show();
    }
    if ($('#pcheckssn').val() == "N") {
        $('#ps').hide();
    }
});
$('#dcheckssn').change(function () {
    if ($('#dcheckssn').val() == "Y") {
        $('#ds').show();
    }
    if ($('#dcheckssn').val() == "N" || $('#dcheckssn').val() == "R") {
        $('#ds').hide();
    }
});
$('#pcountry').change(function () {
    if ($('#pcountry').val() == "US") {
        $('#pc1').show();
        $('#pc2').show();
        $('#pc3').show();
    }
    else {
        $('#pc1').hide();
        $('#pc2').hide();
        $('#pc3').hide();
    }
});
$('#dcountry').change(function () {
    if ($('#dcountry').val() == "US") {
        $('#dc1').show();
        $('#dc2').show();
        $('#dc3').show();
    }
    else {
        $('#dc1').hide();
        $('#dc2').hide();
        $('#dc3').hide();
    }
});
$('#mcountry').change(function () {
    if ($('#mcountry').val() == "US") {
        $('.mc1').show();
    }
    else {
        $('.mc1').hide();
    }
});
$('#acountry').change(function () {
    if ($('#acountry').val() == "US") {
        $('#a1').show();
        $('#a2').show();
    }
    else {
        $('#a1').hide();
        $('#a2').hide();
    }
});
$('input[type=tel]').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3');
        return true;
    }
    this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('input[type=tel]').bind('input', function (e) {
    if ($.isNumeric($(this).val())) {
        $(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3'));
        return true;
    }
});
$('.ssn').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        this.value = this.value.replace(/(\d{3})\-?(\d{2})\-?(\d{4})/, '$1-$2-$3');
        return true;
    }
    this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('.zip').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        if ($(this).val().length >= 5 && typeof google != 'undefined') {
            var zip = $(this).val();
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            var pref = 0;
            geocoder.geocode({ 'address': zip }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++) {
                            var types = results[0].address_components[ii].types.join(",");
                            if ((types == "locality,political") && (pref > 1 || pref == 0)) {
                                $('.city').val(results[0].address_components[ii].long_name);
                                pref = 1;
                            }
                            else if ((types == "neighborhood,political") && (pref > 2 || pref == 0)) {
                                if (results[0].address_components[ii].long_name.toLowerCase().indexOf("bronx") >= 0) {
                                    $('.city').val('Bronx');
                                    pref = 1;
                                }
                                else {
                                    $('.city').val(results[0].address_components[ii].long_name);
                                    pref = 2;
                                }
                            }
                            else if (types == "political,sublocality,sublocality_level_1" && pref == 0) {
                                $('.city').val(results[0].address_components[ii].long_name);
                                pref = 3;
                            }
                            if (types == "administrative_area_level_1,political") {
                                $('.state').val(results[0].address_components[ii].short_name);
                            }
                            if (types == "administrative_area_level_2,political") {
                                $('#pcounty').val(results[0].address_components[ii].short_name.replace(" County", ""));
                            }
                        }
                    }
                }
            });
        }
    }
    else
        this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('.zip1').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        if ($(this).val().length >= 5 && typeof google != 'undefined') {
            var zip = $(this).val();
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            var pref = 0;
            geocoder.geocode({ 'address': zip }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++) {
                            var types = results[0].address_components[ii].types.join(",");
                            if ((types == "locality,political") && (pref > 1 || pref == 0)) {
                                $('.city1').val(results[0].address_components[ii].long_name);
                                pref = 1;
                            }
                            else if ((types == "neighborhood,political") && (pref > 2 || pref == 0)) {
                                if (results[0].address_components[ii].long_name.toLowerCase().indexOf("bronx") >= 0) {
                                    $('.city1').val('Bronx');
                                    pref = 1;
                                }
                                else {
                                    $('.city1').val(results[0].address_components[ii].long_name);
                                    pref = 2;
                                }
                            }
                            else if (types == "political,sublocality,sublocality_level_1" && pref == 0) {
                                $('.city1').val(results[0].address_components[ii].long_name);
                                pref = 3;
                            }
                            if (types == "administrative_area_level_1,political") {
                                $('.state1').val(results[0].address_components[ii].short_name);
                            }
                            if (types == "administrative_area_level_2,political") {
                                $('#dcounty').val(results[0].address_components[ii].short_name.replace(" County", ""));
                            }
                        }
                    }
                }
            });
        }
    }
    else
        this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('.zip2').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        if ($(this).val().length >= 5 && typeof google != 'undefined') {
            var zip = $(this).val();
            var addr = {};
            var geocoder = new google.maps.Geocoder();
            var pref = 0;
            geocoder.geocode({ 'address': zip }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results.length >= 1) {
                        for (var ii = 0; ii < results[0].address_components.length; ii++) {
                            var types = results[0].address_components[ii].types.join(",");
                            if ((types == "locality,political") && (pref > 1 || pref == 0)) {
                                $('.city2').val(results[0].address_components[ii].long_name);
                                pref = 1;
                            }
                            else if ((types == "neighborhood,political") && (pref > 2 || pref == 0)) {
                                if (results[0].address_components[ii].long_name.toLowerCase().indexOf("bronx") >= 0) {
                                    $('.city2').val('Bronx');
                                    pref = 1;
                                }
                                else {
                                    $('.city2').val(results[0].address_components[ii].long_name);
                                    pref = 2;
                                }
                            }
                            else if (types == "political,sublocality,sublocality_level_1" && pref == 0) {
                                $('.city2').val(results[0].address_components[ii].long_name);
                                pref = 3;
                            }
                            if (types == "administrative_area_level_1,political") {
                                $('.state2').val(results[0].address_components[ii].short_name);
                            }
                        }
                    }
                }
            });
        }
    }
    else
        this.value = this.value.replace(/[^\-0-9]/g, '');
});