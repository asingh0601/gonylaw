﻿$(document).ready(function () {
    $('#othercourtaddress').hide();
    $('.form-control').tooltip();
    $('.dates').datetimepicker({ format: 'MM/DD/YYYY' });
    $('.datetime').datetimepicker({
        format: 'MM/DD/YYYY HH:mm A',
    });
});
$('.datetime').on('dp.change', function (e) {
    if (e.oldDate === null) {
        $(this).data('DateTimePicker').date(new Date(e.date._d.setHours(09, 00, 00)));
    }
});
$('.datetime').on('focusout', function (e) {
    var d = $(this).val();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = d.getFullYear() + '/' +
    (month < 10 ? '0' : '') + month + '/' +
    (day < 10 ? '0' : '') + day;
    $(".datetime").val(output + " 09:00");
});
$('input[type=tel]').keyup(function (e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3');
        return true;
    }
    this.value = this.value.replace(/[^\-0-9]/g, '');
});
$('input[type=tel]').bind('input', function (e) {
    if ($.isNumeric($(this).val())) {
        $(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3'));
        return true;
    }
});
$('.selectpicker').change(function () {
    $('#adminsassigned').val($(this).val());
});
$('#courtaddress').change(function () {
    if ($(this).val() == '9')
        $('#othercourtaddress').show();
    else
        $('#othercourtaddress').hide();
});