﻿$('.ui-tooltip').tooltip();
$('.details>img.delete').on("click", function (e) {
    var origin = $(this);
    bootbox.dialog({
        message: "<i style='font-weight:600'>Press CONFIRM to proceed. Once confirmed, you CAN NOT UNDO this action !!!</i>",
        title: "<h4 style='color:red'>DELETE this Client?</h4>",
        buttons: {
            success: {
                label: "Cancel",
                className: "btn-success",
                callback: function () {

                }
            },
            danger: {
                label: "Confirm",
                className: "btn-danger",
                callback: function () {
                    origin.parent().parent().parent().parent().children('.formdel').submit();
                }
            }
        }
    });
});
$('.details>img.reset').on("click", function (e) {
    var origin = $(this);
    bootbox.dialog({
        message: "<i style='font-weight:600'>Press CONFIRM to proceed. The NEW PASSWORD is the 10 DIGIT MOBILE NUMBER of Client without any special character (Example: 7184393600).</i>",
        title: "<h4 style='color:purple'>RESET the password?</h4>",
        buttons: {
            success: {
                label: "Cancel",
                className: "btn-success",
                callback: function () {

                }
            },
            danger: {
                label: "Reset",
                className: "btn-danger",
                callback: function () {
                    origin.parent().parent().parent().parent().children('.formreset').submit();
                }
            }
        }
    });
});
$('.details>img.edit').on("click", function (e) {
    $(this).parent().parent().parent().parent().children('.formedit').submit();
});